function extRunnSendEncashment(data, isScheduled) {
    return new Promise(function (resolve) {
        let obj;
        let url;
        try {
            if (isScheduled) obj = data;
            else {
                let tobj = xml2json.parser(data).dqc_collection;
                tobj.denominationstructure = xml2json.parser(tobj.denominationstructure.replace(/&lt;/g, '<').replace(/&gt;/g, '>'));
                let denom = tobj.denominationstructure.arrayofdenominationstructure.denominationstructure;
                denom = JSON.parse(JSON.stringify(denom).replace(/nominal/g, 'Nominal_ID').replace(/count/g, 'Quantity'));

                obj = {};
                obj.addEncashment = 1;
                obj.totalsum = tobj.totalsum;
                obj.banknotes = denom;
                obj.startDate = tobj.datestart;
                obj.clientInfo = 'test1';
                obj.endDate = tobj.dateend.replace(/\..+/, '');
                obj.encashmentNumber = tobj.checknumber;
                obj.terminalNumber = extGetTerminalNumber();
                obj.pointId = 27;
                obj.sessionId = tobj.collectionid;
                obj.clearCache = (new Date()).getTime();
            }
            url = 'http://localhost/sandbox/runners/request.php' + '?' + jQuery.param(obj);
            // alert('obj:\n'+obj+'\ntypeof obj: '+typeof obj+'\njson obj\n' + JSON.stringify(obj));
            // alert(url);
            logStep('[Курьеры] Отправляем данные инкассации #' + obj.encashmentNumber + ', sessionId: ' + obj.sessionId + '. Адрес: ' + url);
        } catch (e) {
            exception('try-catch: ' + e);
        }
        send();

        // sendToSchedule(obj);

        function send() {
            fetchTimeout(url, {
                method: 'get'
            }).then(onFullfilled, onRejected).catch(exception);
        }

        function exception(e) {
            logException(e);
        }

        function onFullfilled(result) {
            // alert('onfull');
            result.json().then(function (out) {
                // alert(JSON.stringify(out));
                if (out.status === undefined) throw new Error('out.status не определён');
                else if (out.status === 0) onRejected(out.body);
                else {
                    let body = out.body.AddEncashmentResult;
                    if (body.ErrorCode !== 'Apius(ok)') {
                        onRejected('ErrorCode: ' + body.ErrorCode + ' ErrorDescription: ' + body.ErrorDescription)
                    } else {
                        logStep('[Курьеры] Данные инкассации успешно отправлены на сервер');
                        // alert('success');
                        if (isScheduled) scheduleEncashDelete(obj.sessionId);
                        resolve('done');
                    }
                }
            });
        }

        function onRejected(error) {
            logStep('[Курьеры] Данные инкассации не были отправлены на сервер.' + (isScheduled ? '' : 'Перенаправление в очередь.') + 'Ошибка: ' + error);
            if (!isScheduled) {
                sendToSchedule(obj);
                resolve('to schedule');
            } else resolve(obj);
        }

        function sendToSchedule(obj) {
            let itemsJSON = JSON.stringify(obj);
            let encodedItems = encodeURIComponent(JSON.stringify(obj));
            try {
                fetchTimeout('http://localhost/sandbox/runners/scheduleencash.php' + '?addScheduleItem&items=' + encodedItems + '&clearCache=' + (new Date()).getTime(), {
                    method: 'get'
                }).then(onFullfilled, onRejected);

                function onFullfilled(addResponse) {
                    addResponse.text().then(function (addOut) {
                        // alert('extRunnSendEncashment -> addOut\n' + addOut);
                        if (addOut && addOut.match('2002'))
                            onRejected();
                        else {
                            // alert(addOut);
                            addOut = JSON.parse(addOut);
                            if (addOut.status === 0) {
                                if (addOut.log) {
                                    if (addOut.log.match('Duplicate entry')) {
                                        //в базе уже присутствует элемент для очереди
                                    } else onRejected();
                                } else {
                                    logException('runnExtSendPaymentToSchedule > fetchTimeout > onFullfilled: Неожиданный результат. Статус билета 0, но лог отсутсвует. Билеты: ' + itemsJSON);
                                    resolve(obj);
                                }
                            } else {
                                logStep('Билеты успешно отправлены в БД. Ответ сервера: ' + addOut.log);
                                resolve('done');
                            }
                        }
                    }).catch(function (e) {
                        logException('[aquaparkSendItems -> onFullfilled#1]: ' + e);
                    });
                }

                function onRejected() {
                    logStep('Локальная БД недоступна. Билеты будут сохранены в файл.');
                    fetchTimeout(url + '?addScheduleItemToFile&items=' + encodedItems + '&clearCache=' + (new Date()).getTime(), {
                        method: 'get'
                    }).then(onFullfilled).catch(function (e) {
                        logException('[aquaparkSendItems -> onFullfilled#2]: ' + e);
                    }).finally(function () {
                        resolve(obj);
                    });

                    function onFullfilled(toFileResponse) {
                        toFileResponse.json().then(function (toFileOut) {
                            if (!toFileOut.status) onRejected();
                            else SaveLog('Билеты успешно добавлены в файл. Ответ сервера: ' + toFileOut.log);
                        });
                    }
                }
            } catch (e) {
                logException('Ошибка на отправке билетов (\'' + itemsJSON + '\') в БД: ' + e);
            }
        }
    });
}

function extZooSendReportZ(url, login, password) {
    let timeout = 60; //orig 60
    let maxTries = 2;
    let tries = 0;
    url = encodeURIComponent(url + 'CloseKassaKKM');
    login = encodeURIComponent(login);
    password = encodeURIComponent(password);
    send();

    function send() {
        tries++;
        logStep('[ЗооОптТорг] Отправляем запрос по Z отчёту. #' + tries);
        fetchTimeout('http://localhost/sandbox/zooopttorg/request.php?ReportZ&url=' + url + '&login=' + login + '&password=' + password + '&clearCache=' + (new Date()).getTime(), {method: 'get'})
            .then(function (result) {
                result.json().then(function (out) {
                    if (out.status === 1) logStep('[ЗооОптТорг] Запрос по Z отчёту успешно отправлен');
                    else onRejected(JSON.stringify(out));
                })
            }, onRejected);
    }

    function onRejected(reason) {
        logWarning('[ЗооОптТорг] Запрос по Z отчёту выполнить не удалось. Причина: ' + reason);
        if (tries < maxTries) setTimeout(send, timeout * 1000); //
        else logWarning('[ЗооОптТорг] Запрос по Z отчёту не удалось отправить после ' + tries + ' попыток');
    }
}