/* Функции вызываемые приложением Terminal */

function extHandleBarCode(barcode) {
    logStep('Приложение вызвало обработчик баркодов с параметром: ' + barcode);
    try {
        switch (barcodeOperation) {
            case('staff'): {
                logStep('Операция баркодов: ' + barcodeOperation);
                if (extGetTopLevelGroupID() === 262) { // Юнион Моторс
                    if (barcode.length && barcode[0] === '0') barcode = barcode.slice(1);
                }
                staffExecutor(barcode);
                break;
            }
            default: {
                console.log('Не установлена операция для обработки баркода');
            }
        }
    } catch (e) {
        logException('Ошибка при выполнении extHandleBarCode. Ошибка: ' + e);
    }
}

/**
 * После отправки экстернала Menu_Incassation, в эту функцию приходят данные по проведённой инкассации
 * @param data XML-структура, в которой есть вложенный XML с вложенным закодированным XML (нужно соответственно преобразовывать)
 * @param debug отладочный параметр (не приходит от приложения)
 */
function DoInKass(data, debug) {
    logStep('От приложения пришли данные по инкассации: ' + data);
    if (debug) data = '<?xml version="1.0" encoding="utf-16"?> <DQC_Collection> <CollectionID>ada30191-6f7a-4d89-803d-2dc9e417f278</CollectionID> <UserID>00000000-0000-0000-0000-000000000000</UserID> <DateStart>2018-09-27T12:02:00</DateStart> <DateEnd>2019-02-11T12:26:17.2803987+03:00</DateEnd> <CheckNumber>2</CheckNumber> <TotalSum>104110</TotalSum> <TotalSumCommission>0.0000</TotalSumCommission> <DenominationStructure>&lt;?xml version="1.0" encoding="utf-16"?&gt; &lt;ArrayOfDenominationStructure&gt; &lt;DenominationStructure&gt; &lt;Nominal&gt;10&lt;/Nominal&gt; &lt;Count&gt;21&lt;/Count&gt; &lt;/DenominationStructure&gt; &lt;DenominationStructure&gt; &lt;Nominal&gt;50&lt;/Nominal&gt; &lt;Count&gt;78&lt;/Count&gt; &lt;/DenominationStructure&gt; &lt;DenominationStructure&gt; &lt;Nominal&gt;100000&lt;/Nominal&gt; &lt;Count&gt;1&lt;/Count&gt; &lt;/DenominationStructure&gt; &lt;/ArrayOfDenominationStructure&gt;</DenominationStructure> <DenominationCashOutStructure /> </DQC_Collection>';
    if (extGetTopLevelGroupID() === 368) extRunnSendEncashment(data); // Сколково Курьеры (Runners)
}