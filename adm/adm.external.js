function extSaveLog(text) {
    if ('SaveErrorInLog' in window.external) window.external.SaveErrorInLog(text);
}

function extPingInterface() {
    if ('PingInterface' in window.external) window.external.PingInterface();
}

function extLink(url) {
    logStep('Переход на ' + url);
    if ('LINK' in window.external) window.external.LINK(url);
    else window.location.href = url;
}

let extOperationStatus;

function extGetOperationStatus(testValue) {
    let val;
    if ('GetResponseOnlinePayment' in window.external) val = window.external.GetResponseOnlinePayment();
    else val = testValue;
    if (val !== extOperationStatus) {
        extOperationStatus = val;
        logStep('От приложения получен ответ по онлайн-платежу: ' + val);
    }
    return val;
}

function extPrintCustomCheck(str) {
    if (!str.length) str = 'Строка пустая!';
    logStep('Отправлена команда на печать чека со следующим содержимым: ' + str);
    if ('PrintFromInterface' in window.external) window.external.PrintFromInterface(str);
}

function extGetTabs() {
    let tabs;
    if ('GetTabForSysMenu' in window.external) {
        tabs = window.external.GetTabForSysMenu();
        logStep('От приложения получены следующие вкладки: ' + tabs);
    } else {
        tabs = '[]';
        logStep('extGetTabs вернула тестовые вкладки: ' + tabs.replace(/},/g, '}\n').replace('[', '[\n'));
    }
    if (debug) tabs = '[' +
        '{"id":"staff","level":0,"name":"Сотрудники"},' +
        '{"id":"kkm","level":0,"name":"Фискальный регистратор"},' +
        '{"id":"change","level":1,"name":"Устройства сдачи"},' +
        '{"id":"incass","level":0,"name":"Инкассация/Отчеты"},' +
        '{"id":"goods","level":0,"name":"Учёт товара"}]';
    return JSON.parse(tabs);
}

function extGetTabContent(tab) {
    let val;
    if ('GetTabContentForSysMenu' in window.external) val = window.external.GetTabContentForSysMenu(tab);
    else {
        if (debug)
            switch (tab) {
                case('incass'): {
                    val = '[1, 2, 4]';
                    break;
                }
                case('change'): {
                    val = '[' +
                        '{ "device": "nv200", "info": ' +
                        '{ "10": 12, "50": 23, "100": 34, "200": 45, "500": 56, "1000": 67, "2000": 78, "5000": 89 } }, ' +
                        '{ "device": "hopper", "info": ' +
                        '{ "10": 145, "50": 245, "100": 389, "200": 445, "500": 536, "1000": 456, "2000": 425, "5000": 45} }, ' +
                        '{ "device": "puloon", "info": ' +
                        '{ "cashboxes": [' +
                        '{ "number": 1, "cash": { "10": 121 } }, ' +
                        '{ "number": 2, "cash": { "5000": 89 } }, ' +
                        '{ "number": 4, "cash": { "200": 45 } } ]' +
                        ', "box": [' +
                        '{ "number": 1, "cash": ' +
                        '{ "10": 125, "50": 23, "100": 34, "200": 45, "500": 56, "1000": 67, "2000": 78, "5000": 89 } }' +
                        '] } }]';
                    break;
                }
                case('kkm'): {
                    val = '{' +
                        '"cashboxes":[1,2,4],' +
                        '"taxgroups":[' +
                        '{"group":1,"value":"НДС 18%"},' +
                        '{"group":2,"value":"НДС 0%"},' +
                        '{"group":5,"value":"БЕЗ НДС"},' +
                        '{"group":6,"value":"НДС 18/110"}]}';
                    break;
                }
                default: {
                    logException('extGetTabContent получила неожиданный параметр: ' + tab);
                }
            }
        else val = '{}';
    }
    logStep('От приложения получено следующее содержимое вкладки ' + tab + ': ' + val);
    return JSON.parse(val);
}

function extGetKeyForInterface(key) {
    try {
        let val;
        if ('GetKeyForInterface' in window.external) val = window.external.GetKeyForInterface(key);
        else val = 'отладка';
        logStep('От приложения получено содержимое по ключу ' + key + ': ' + val);
        return val;
    }
    catch (e) {
        logException('Экстернал GetKeyForInterface сломался.\nGetKeyForInterface должен был вернуть ключ: ' + key + '\nОшибка:' + e);
    }
}

function extEnableBarcode() {
    if ('EnableBarCode' in window.external) {
        logStep('Отправлена команда на включение чтения баркода');
        window.external.EnableBarCode();
    } else {
        logStep('Вызвана команда на включение баркода (устройства нет)')
    }
}

function extDisableBarcode() {
    if ('DisableBarCode' in window.external) {
        logStep('Отправлена команда на отключение чтения баркода');
        window.external.DisableBarCode();
    } else {
        logStep('Вызвана команда на отключение чтения баркода (устройства нет)')
    }
}

function extCheckAdminBarcode(barcode) { //не используется
    let val;
    if ('InterCodeEmployeeID' in window.external) {
        val = window.external.InterCodeEmployeeID(barcode);
        logStep('Получен следующий ответ на запрос на проверку админ-баркода ' + barcode + ': ' + val);
    }
    else {
        val = 'Фамилия Имя Отчество';
        logStep('Получен тестовый ответ на запрос на проверку админ-баркода ' + barcode + ': ' + val);
    }
    return val;
}

/**
 * Отправляет в личный кабинет для статистики информацию во время считывания баркодов
 * @param action
 * @param description
 */
function extStaffUserAction(action, description) {
    logStep('Отправлена команда UserActionAdd с параметрами action \'' + action + '\', description: \'' + description + '\'');
    if ('UserActionAdd' in window.external) window.external.UserActionAdd(action, description);
}

function extSetUsersBasket(key) {
    logStep('В экстернал UsersBasketSet отправлена ключ: ' + key);
    if ('UsersBasketSet' in window.external) window.external.UsersBasketSet(key);
}

/**
 * 1 наличка 2 мир 3 виза 4 мастер
 * @param sum
 * @param amount
 * @param operation Наименование товара
 * @param department
 * @param taxgroup
 * @param paymentoption
 */
function extKKMSell(sum, amount, operation, department, taxgroup, paymentoption) {
    try {
        let cashbox = selectedCashBox - 1;
        logStep('Отправлен запрос в ККМ на продажу со следующими параметрами: ' +
            'cashbox \'' + cashbox + '\', sum \'' + sum + '\', taxgroup \'' + taxgroup + '\', 0, 0, 0, ' +
            'amount \'' + amount + '\', department \'' + department + '\', operation \'' + operation + '\', paymentoption \'' + paymentoption + '\', 0');
        if ('admmenukkmsale' in window.external) {
            window.external.admmenukkmsale(cashbox, sum, taxgroup, 0, 0, 0, amount, department, operation, paymentoption, 0);
            // 0 после paymentoption - это discount
        }
    } catch (e) {
        logException('Ошибка в extKKMSell: ' + e);
    }
}

/**
 * 1 наличка 2 мир 3 виза 4 мастер
 * @param sum
 * @param amount
 * @param operation
 * @param department
 * @param taxgroup
 * @param paymentoption
 * @param accountingType Признак способа расчета
 * @param accountingItem Признак предемета расчета
 */
function extKKMRefund(sum, amount, operation, department, taxgroup, paymentoption, accountingType, accountingItem) {
    // alert('extKKMRefund\ntype: '+typeof accountingType + '\n' + 'item: '+typeof accountingItem);
    try {
        let cashbox;
        let arr;
        let obj;
        let json;
        cashbox = selectedCashBox - 1;
        arr = [];
        obj = {};
        obj.Department = department;
        obj.Name = operation;
        obj.Price = sum;
        obj.Quantity = amount;
        obj.Tax1 = taxgroup;
        obj.Tax2 = 0;
        obj.Tax3 = 0;
        obj.Tax4 = 0;
        obj.PaymentTypeSign = accountingType;
        obj.PaymentItemSign = accountingItem;
        arr.push(obj);
        json = JSON.stringify(arr);
        logStep('Отправлен запрос в ККМ на возврат со следующими параметрами: ' +
            'cashbox \'' + cashbox + '\', sum \'' + sum.replace('.', ',') + '\', taxgroup \'' + taxgroup + '\', 0, 0, 0, ' +
            'amount \'' + amount.replace('.', ',') + '\', department \'' + department + '\', operation \'' + operation + '\', paymentoption \'' + paymentoption + '\', JSON(obj) ' + JSON.stringify(obj) + ', 1' + '\', accountingItem \'' + accountingItem + '\', accountingType \'' + accountingType);
        if ('admmenukkmreturn' in window.external) window.external.admmenukkmreturn(cashbox, sum.replace('.', ','), taxgroup, 0, 0, 0, amount.replace('.', ','), department, operation, paymentoption, json, 1, accountingItem, accountingType);
        // 1 после obj - это printresult
    } catch (e) {
        logException('Ошибка в extKKMRefund: ' + e);
    }
}

/**
 * после успеха или не успеха операции возвращает текст для отображения пользователю
 * пример: Чек успешно зарегистрирован!
 * Успех/не успех определяется переменной OnlineOk
 * -1 еще не осуществлен процесс 0 не успех 1 успех
 * @returns string
 */
function extKKMOperationReport() {
    let val;
    if ('GetOnlineModeText2Screen' in window.external) val = window.external.GetOnlineModeText2Screen();
    logStep('Получен ответ по статусу операции: ' + val);
    return val;
}

function extKKMGetCurrentCash(cashbox) {
    let val;
    if ('GetKKMCashRegister' in window.external) {
        val = window.external.GetKKMCashRegister(cashbox).replace(/,/g, '.');
        logStep('Полученая текущая сумма в ККМ: ' + val + ' (после получения произведена замена запятых на точки)');
    }
    else {
        if (cashbox === 1) val = 100;
        else if (cashbox === 2) val = 200;
        else if (cashbox === 3) val = 300;
        else if (cashbox === 4) val = 400;
        else logException('В экстернал extKKMGetCurrentCash передан неверный параметр номера кассы');
        if (val) logStep('Полученая отладочная сумма в ККМ: ' + val);
    }
    return val;
}

/**
 * выплата из кассы, если передать в качестве аргумента 0, то выплатится все
 * @param cashbox
 * @param amount
 */
function extKKMTake(cashbox, amount) {
    logStep('Отправлен запрос на выдачу денег из кассы №' + cashbox + ' в размере: ' + amount);
    if ('KKMCashOutCome' in window.external) window.external.KKMCashOutCome(cashbox, amount);
    else logStep('Запрос на выдачу денег недоступен без устройства');
}

function extKKMFill(cashbox, amount) {
    logStep('Отправлен запрос на пополнение кассы №' + cashbox + ' в размере: ' + amount);
    if ('KKMCashIncome' in window.external) window.external.KKMCashIncome(cashbox, amount);
    else logStep('Запрос на выдачу денег недоступен без устройства');
}

function extIncassPrintReportZ(id) {
    logStep('Отправлен запрос на печать Z-отчёта');
    if ('Menu_PrintZReport' in window.external) window.external.Menu_PrintZReport(id);
    if (extGetTopLevelGroupID() === 331) { // ЗооОптТорг

    }
}

function extIncassPrintReportX(id) {
    logStep('Отправлен запрос на печать X-отчёта');
    if ('Menu_PrintXReport' in window.external) window.external.Menu_PrintXReport(id);
}

function extIncassEncashment() {
    logStep('Отправлен запрос на выполнение инкассации');
    if ('Menu_Incassation' in window.external) window.external.Menu_Incassation();
}

function extIncassPrintPreviousEncashment() {
    logStep('Отправлен запрос на повторную печать инкассационного чека');
    if ('Menu_CloneIncass' in window.external) window.external.Menu_CloneIncass();
    else logStep('Запрос на повторную печать инкассационного чека недоступен без устройства');
}

function extChangeCashreceiverManageSettingsGetDenominations() {
    let val;
    if ('ext' in window.external) {
        val = window.external.ext();
        logStep('Получены следующие значения по деноминациям в купюроприёмнике: ' + val);
    } else {
        if (debug) val = '{"10": 8, "50": 7, "100": 6, "200": 5, "500": 4, "1000": 3, "2000": 2, "5000": 1}';
        else val = '{}';
        logStep('Получены следующие отладочные значения по деноминациям в купюроприёмнике: ' + val);
    }
    return JSON.parse(val);
}

function extChangeHopperManageFillGetDenominations() {
    let val;
    if ('ext' in window.external) {
        val = window.external.ext();
        logStep('Получены следующие значения по деноминациям в хоппере: ' + val);
    } else {
        if (debug) val = '{"1": 8, "2": 7, "5": 6, "10": 5}';
        else val = '{}';
        logStep('Получены следующие отладочные значения по деноминациям в хоппере: ' + val);
    }
    return JSON.parse(val);
}

function extGetTopLevelGroupID() {
    if ('RetTopLevelGroupID' in window.external) return window.external.RetTopLevelGroupID();
}

function extGetTerminalNumber() {
    if ('RetTerminalNumber' in window.external) return window.external.RetTerminalNumber();
}