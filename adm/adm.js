let debug = 1;
let debugcss = 0;
let debugLog = 1;
let debugLogSteps = 1;
let debugLogWarnings = 1;
let debugLogExceptions = 1;
let setSteps = 1 && debug;

let fixes = {
    Union: 0,
    Airports: 0
};

//todo 1. неотловленный баг: (возможно на более старой версии) в IE при открытой всплывающей клавиатуре numpad при переключении
//todo на другую главную вкладку вылетает ошибка jquery
//todo 2. перевести все экстерналы на Promise, добавить соответствующие интерфейсные решения
//todo 3. для сдачников получить и прикрутить все экстерналы
//todo 4. получить экстернал по режиму работы с кассами (их должно быть два: работа с одной кассой, когда всегда параметром номера кассы
//todo передаётся 0, и работа с несколькими кассами, когда параметром номера кассы передаётся 1-5) и переделать отображение касс следующим образом:
//todo если включён первый режим (одна касса) панель переключения между кассами не отображается;
//todo если включён второй режим: кассы нет - она не отображается
//todo 5. сделать модалку "я ещё здесь" более отчётливо видимой

let tabsUI = null;
let contentUI = null;

let pingInterfaceInterval = null;
let timeoutTimer = null;
let timeoutTime = null;
let timeout = 60;
if (debug) timeout = 24 * 60 * 60 * 1000;
let operationActive = 0;

let processingTimer = null;

let operationWrapper;

let selectedCashBox = 0;
let cashboxCash;
let barcodeAdminName;

let barcodeOperation;

const monthNames = ["января", "февраля", "марта", "апреля", "мая", "июня",
    "июля", "августа", "сентября", "октября", "ноября", "декабря"];

let logInitStr = '[Инициализация интерфейса ADM] ';
if (debug) logInitStr += '\nВключён режим отладки';
if (debugLog) logInitStr += '\nВключено логирование:';
if (debugLog && debugLogSteps) logInitStr += '\n -Включено логирование шагов';
if (debugLog && debugLogWarnings) logInitStr += '\n -Включено логирование предупреждений и неопасных ошибок';
if (debugLog && debugLogExceptions) logInitStr += '\n -Включено логирование ошибок и исключений';
if (debugcss) logInitStr += '\nВключён режим отладки css. Кликни где-нибудь на странице, чтобы обновить границы элементов';
logInitStr += '\nТаймер таймаута установлен на ' + timeout + ' секунд';
extSaveLog(logInitStr);
console.log(logInitStr);

// добавляем шаги для отладки
if (debug && setSteps) {
    setTimeout(function () {
        changeTab('incass', 0);
        // drawKKMSellRefund('refund');
        // changePuloonManage();
        // showOperationPopup('keyboardInput', 'Кассета 1', '#cashbox1');
        // changeHopperManage(1);
        // drawKKMoperateCashbox('fill');
        // initKeyboard();
    }, 20);
    // console.log('Мы находимся здесь: '+document.currentScript.src);
}

function init() {
    barcodeAdminName = extGetKeyForInterface('managerfio');
    $('.exitButton').attr('onclick', 'extLink(\'main.html\')');

    $('.timeoutTime').html(timeout);
    tabsUI = $('.tabsWrapper');
    contentUI = $('.content');
    tabsUI.html('');
    contentUI.html('');
    setTabs();
    setTimeoutTimer(timeout);
    startPingInterface();
    logStep('Стартовая инициализация завершена');
}

function setTabs() {
    let response = extGetTabs();
    let tab;
    if (response.length) {
        for (let i = 0; i < response.length; i++) {
            tab = $('<div></div>');
            tab.attr('id', response[i].id);
            tab.addClass('tab');
            tab.attr('onclick', 'changeTab(\'' + response[i].id + '\', \'' + response[i].level + '\')');
            tab.html(response[i].name);
            tabsUI.append(tab);
        }
        tabsUI.children().first().addClass('selected');
        setContent(response[0].id, response[0].level);
    } else {
        setContent('empty');
    }
    logStep('Табы установлены');
}

function changeTab(id, level) {
    hideBackButton();
    tabsUI.find('.selected').toggleClass('selected');
    tabsUI.find('#' + id).toggleClass('selected');
    logStep('Выбрана ' + id + ' с уровнем доступа ' + level);
    setContent(id, level);
}

function setContent(id, level) {
    contentUI.html('');
    let content;
    switch (id) {
        case('incass'): {
            content = createIncass(level);
            break;
        }
        case('change'): {
            content = createChange(level);
            break;
        }
        case('staff'): {
            content = createStaff(level);
            break;
        }
        case('empty'): {
            content = createEmpty(level);
            break;
        }
        case('kkm'): {
            content = createKKM(level);
            break;
        }
        default: {
            logException('Не указан тип таба: \'' + id + '\'');
            return;
        }
    }
    contentUI.append(content);

    logStep('Вкладка переключена на ' + id + ' с ' + (typeof level === 'undefined' ? 'неизвестным ' : '') + 'уровнем доступа ' + (typeof level !== 'undefined' ? level : ''));
}

function createEmpty(accessLevel) {
    let content = $('.content');
    let obj = $('<div></div>');
    obj.addClass('emptyServerResponse');
    obj.html('Нет ответа от сервера');
    content.append(obj);
}

function createIncass(accessLevel) {
    let obj;
    let kassAmount;
    let cashboxWrapper;
    let operationsWrapper;
    obj = $("<div></div>");
    obj.addClass('incassWrapper');
    let incassContent = extGetTabContent('incass');
    cashboxWrapper = $("<div></div>");
    cashboxWrapper.addClass('incassCashboxWrapper');
    //добавляем кассы
    let found;
    let firstFound = 0;
    for (let i = 1; i <= 5; i++) {
        found = 0;
        let cashbox = $("<div></div>");
        cashbox.addClass('incassCashbox');
        cashbox.attr('id', 'cashbox' + i);
        incassContent.forEach(function (item, j, arr) {
            if (item == i) found = 1;
        });
        if (!found) cashbox.toggleClass('inactive');
        else {
            cashbox.attr('onclick', 'cashboxOperate(' + i + ')');
            if (!firstFound) {
                cashbox.toggleClass('selected');
                selectedCashBox = i;
                firstFound = 1;
            }
        }
        cashbox.html('Касса ' + i);
        cashboxWrapper.append(cashbox);
    }
    obj.append(cashboxWrapper);

    operationsWrapper = $("<div></div>");
    operationsWrapper.addClass('incassOperationsWrapper');

    let operationTimeout = 5;
    let ZReportUI = $("<div class='incassOperation'>Распечатать Z-отчёт</div>");
    let XReportUI = $("<div class='incassOperation'>Распечатать X-отчёт</div>");
    let executeIncassUI = $("<div class='incassOperation'>Выполнить инкассацию</div>");
    let printPreviousIncassUI = $("<div class='incassOperation'>Печать предыдущего инкассационного чека</div>");
    let openShiftUI = $("<div class='incassOperation'>Открыть смену</div>");

    if (!firstFound) {
        ZReportUI.toggleClass('inactive');
        XReportUI.toggleClass('inactive');
        printPreviousIncassUI.toggleClass('inactive');
    } else {
        executeIncassUI.attr('onclick', 'incassEncashment(' + operationTimeout + ')');

        if (fixes.Airports) {
            ZReportUI.on('click', function () {
                requestCashRegister('GetReportZ').catch(onError).finally(onFinally)
            });
            XReportUI.on('click', function () {
                requestCashRegister('GetReportX').catch(onError).finally(onFinally)
            });
            openShiftUI.on('click', function () {
                requestCashRegister('OpenNewShift').catch(onError).finally(onFinally)
            });
        } else {
            ZReportUI.attr('onclick', 'incassPrintReportZ(' + selectedCashBox + ', ' + operationTimeout + ')');
            XReportUI.attr('onclick', 'incassPrintReportX(' + selectedCashBox + ', ' + operationTimeout + ')');
            printPreviousIncassUI.attr('onclick', 'incassPrintPreviousEncashment(' + operationTimeout + ')');
        }
    }

    operationsWrapper.append(ZReportUI);
    operationsWrapper.append(XReportUI);
    operationsWrapper.append(executeIncassUI);
    if (fixes.Airports) operationsWrapper.append(openShiftUI);
    else operationsWrapper.append(printPreviousIncassUI);
    obj.append(operationsWrapper);

    return obj;

    function onError(error) {
        logException(error);
    }

    function onFinally(error) {
        unblockScreenProcessing();
    }
}

function incassPrintReportZ(id, time) {
    setProcessingTimer(time);
    extIncassPrintReportZ(id);
    if (extGetTopLevelGroupID() === 331) {
        let url = extGetKeyForInterface('IntegrationValA');
        let login = extGetKeyForInterface('IntegrationValB');
        let password = extGetKeyForInterface('IntegrationValC');
        if (url !== undefined && login !== undefined && password !== undefined) {
            extZooSendReportZ(url, login, password);
        } else logException('[ЗооОптТорг] Не задан один из параметров. url: ' + url + ' login: ' + login + ' password: ' + password);
    } // Сколково Курьеры (Runners)
}

function incassPrintReportX(id, time) {
    setProcessingTimer(time);
    extIncassPrintReportX(id);
}

function incassEncashment(time) {
    setProcessingTimer(time);
    extIncassEncashment();
}

function incassPrintPreviousEncashment(time) {
    setProcessingTimer(time);
    extIncassPrintPreviousEncashment();
}

function cashboxOperate(id) {
    $('.selected#cashbox1').toggleClass('selected');
    $('.selected#cashbox2').toggleClass('selected');
    $('.selected#cashbox3').toggleClass('selected');
    $('.selected#cashbox4').toggleClass('selected');
    $('#cashbox' + id).toggleClass('selected');
    selectedCashBox = id;
    refreshCashboxCash();
    $('#cashboxCash').html(cashboxCash === -1 ? "нет данных" : cashboxCash + ' руб.');
    logStep('Выбрана касса №' + id);
}

function refreshCashboxCash() {
    /*alert('returned value: '+extKKMGetCurrentCash(selectedCashBox)+'\n'+(typeof extKKMGetCurrentCash(selectedCashBox))+
        '\n'+Number(extKKMGetCurrentCash(selectedCashBox)));*/
    return cashboxCash = parseFloat(String(extKKMGetCurrentCash(selectedCashBox)));
}

let changeWrapper;
let changeInfo;
let changeDevicesWrapper;
let changeDevice;
let changeDeviceImageSrc;
let changeDeviceImage;
let changeDeviceInfoWrapper;
let changeDeviceInfo;
let changeDeviceCashbox;
let changeDeviceInfoDenominationsWrapper;
let changeDeviceInfoDenominationWrapper;
let changeDeviceInfoAmountWrapper;
let changeDeviceInfoSumWrapper;
let changeDeviceInfoDenomination;
let changeDeviceInfoAmount;
let changeDeviceInfoSum;
let changeDeviceButton;
let changeInfoKeys;
let changeSum;
let changeDeviceInfoTotalWrapper;
let changeDeviceInfoTotal;

function createChange(accessLevel) {
    changeWrapper = $("<div></div>");
    changeWrapper.toggleClass('changeWrapper');
    changeWrapper.html('');
    changeInfo = extGetTabContent('change');

    if (changeInfo) {
        changeDevicesWrapper = $("<div></div>");
        changeDevicesWrapper.toggleClass('changeDevicesWrapper');

        changeSum = 0;
        for (let i = 0; i < changeInfo.length; i++) {
            switch (changeInfo[i].device) {
                case('nv200'): {
                    changeDevice = $("<div></div>");
                    changeDevice.addClass('changeDevice');
                    changeDevice.attr('id', 'nv200');
                    changeDeviceImageSrc = "img/useradmin/devices/nv200_t.png";
                    changeDeviceImage = $('<img src=' + changeDeviceImageSrc + ' />');
                    changeDeviceImage.addClass('changeDeviceImage');
                    changeDevice.append(changeDeviceImage);
                    changeDeviceInfoWrapper = $("<div></div>");
                    changeDeviceInfoWrapper.addClass('changeDeviceInfoWrapper');

                    for (let sum in changeInfo[i].info) {
                        // console.log('sum: ' + sum + ' info[sum]: ' + changeInfo[i].info[sum]);
                        changeSum += sum * changeInfo[i].info[sum];
                    }

                    changeInfoKeys = changeInfo[i].info;
                    let tempKey;
                    let lowestKey;
                    for (let tk = 0; tk < changeInfoKeys.length; tk++) {
                        lowestKey = tk;
                        for (let tl = tk; tl < changeInfoKeys.length - 1; tl++) {
                            if (changeInfoKeys[tl + 1] < changeInfoKeys[tl]) lowestKey = tl;
                        }
                        if (lowestKey != tk) {
                            tempKey = changeInfoKeys[tk];
                            changeInfoKeys[tk] = changeInfoKeys[lowestKey];
                            changeInfoKeys[lowestKey] = tempKey;
                        }
                    }
                    changeDeviceInfoDenominationsWrapper = $("<div></div>");
                    changeDeviceInfoDenominationsWrapper.addClass('changeDeviceInfoDenominationsWrapper');
                    changeDeviceInfoDenominationWrapper = $("<div></div>");
                    changeDeviceInfoDenominationWrapper.addClass('changeDeviceInfoDenominationWrapper');
                    changeDeviceInfoAmountWrapper = $("<div></div>");
                    changeDeviceInfoAmountWrapper.addClass('changeDeviceInfoAmountWrapper');
                    changeDeviceInfoSumWrapper = $("<div></div>");
                    changeDeviceInfoSumWrapper.addClass('changeDeviceInfoSumWrapper');
                    for (let key in changeInfoKeys) {
                        changeDeviceInfo = $("<div></div>");
                        changeDeviceInfo.toggleClass('changeDeviceInfo');

                        changeDeviceInfoDenomination = $("<div></div>");
                        changeDeviceInfoDenomination.addClass('changeDeviceInfoDenomination');
                        changeDeviceInfoDenomination.html(key + ' x ');
                        changeDeviceInfoDenominationWrapper.append(changeDeviceInfoDenomination);

                        changeDeviceInfoAmount = $("<div></div>");
                        changeDeviceInfoAmount.addClass('changeDeviceInfoAmount');
                        changeDeviceInfoAmount.html(changeInfo[i].info[key] + ' =');
                        changeDeviceInfoAmountWrapper.append(changeDeviceInfoAmount);

                        changeDeviceInfoSum = $("<div></div>");
                        changeDeviceInfoSum.addClass('changeDeviceInfoSum');
                        changeDeviceInfoSum.html((Number(key) * Number(changeInfo[i].info[key])).toLocaleString('ru-RU'));
                        changeDeviceInfoSumWrapper.append(changeDeviceInfoSum);
                    }
                    changeDeviceInfoDenominationsWrapper.append(changeDeviceInfoDenominationWrapper);
                    changeDeviceInfoDenominationsWrapper.append(changeDeviceInfoAmountWrapper);
                    changeDeviceInfoDenominationsWrapper.append(changeDeviceInfoSumWrapper);
                    changeDeviceInfoWrapper.append(changeDeviceInfoDenominationsWrapper);

                    changeDeviceButton = $("<div></div>");
                    changeDeviceButton.addClass('changeDeviceButton');
                    changeDeviceButton.html('Управление');
                    changeDeviceButton.attr('onclick', 'changeCashreceiverManage(\'' + accessLevel + '\')');
                    changeDeviceInfoWrapper.append(changeDeviceButton);
                    changeDevice.append(changeDeviceInfoWrapper);
                    break;
                }
                case('hopper'): {
                    changeDevice = $("<div></div>");
                    changeDevice.addClass('changeDevice');
                    changeDevice.attr('id', 'hopper');
                    changeDeviceImageSrc = "img/useradmin/devices/hopper_t.png";
                    changeDeviceImage = $('<img src=' + changeDeviceImageSrc + ' />');
                    changeDeviceImage.addClass('changeDeviceImage');
                    changeDevice.append(changeDeviceImage);
                    changeDeviceInfoWrapper = $("<div></div>");
                    changeDeviceInfoWrapper.addClass('changeDeviceInfoWrapper');

                    for (let sum in changeInfo[i].info) {
                        changeSum += sum * changeInfo[i].info[sum];
                    }

                    changeInfoKeys = changeInfo[i].info;
                    let tempKey;
                    let lowestKey;
                    for (let tk = 0; tk < changeInfoKeys.length; tk++) {
                        lowestKey = tk;
                        for (let tl = tk; tl < changeInfoKeys.length - 1; tl++) {
                            if (changeInfoKeys[tl + 1] < changeInfoKeys[tl]) lowestKey = tl;
                        }
                        if (lowestKey != tk) {
                            tempKey = changeInfoKeys[tk];
                            changeInfoKeys[tk] = changeInfoKeys[lowestKey];
                            changeInfoKeys[lowestKey] = tempKey;
                        }
                    }
                    changeDeviceInfoDenominationsWrapper = $("<div></div>");
                    changeDeviceInfoDenominationsWrapper.addClass('changeDeviceInfoDenominationsWrapper');
                    changeDeviceInfoDenominationWrapper = $("<div></div>");
                    changeDeviceInfoDenominationWrapper.addClass('changeDeviceInfoDenominationWrapper');
                    changeDeviceInfoAmountWrapper = $("<div></div>");
                    changeDeviceInfoAmountWrapper.addClass('changeDeviceInfoAmountWrapper');
                    changeDeviceInfoSumWrapper = $("<div></div>");
                    changeDeviceInfoSumWrapper.addClass('changeDeviceInfoSumWrapper');
                    for (let key in changeInfoKeys) {
                        changeDeviceInfo = $("<div></div>");
                        changeDeviceInfo.addClass('changeDeviceInfo');

                        changeDeviceInfoDenomination = $("<div></div>");
                        changeDeviceInfoDenomination.addClass('changeDeviceInfoDenomination');
                        changeDeviceInfoDenomination.html(key + ' x ');
                        changeDeviceInfoDenominationWrapper.append(changeDeviceInfoDenomination);

                        changeDeviceInfoAmount = $("<div></div>");
                        changeDeviceInfoAmount.addClass('changeDeviceInfoAmount');
                        changeDeviceInfoAmount.html(changeInfo[i].info[key] + ' =');
                        changeDeviceInfoAmountWrapper.append(changeDeviceInfoAmount);

                        changeDeviceInfoSum = $("<div></div>");
                        changeDeviceInfoSum.addClass('changeDeviceInfoSum');
                        changeDeviceInfoSum.html((Number(key) * Number(changeInfo[i].info[key])).toLocaleString('ru-RU'));
                        changeDeviceInfoSumWrapper.append(changeDeviceInfoSum);
                    }
                    changeDeviceInfoDenominationsWrapper.append(changeDeviceInfoDenominationWrapper);
                    changeDeviceInfoDenominationsWrapper.append(changeDeviceInfoAmountWrapper);
                    changeDeviceInfoDenominationsWrapper.append(changeDeviceInfoSumWrapper);
                    changeDeviceInfoWrapper.append(changeDeviceInfoDenominationsWrapper);

                    changeDeviceButton = $("<div></div>");
                    changeDeviceButton.addClass('changeDeviceButton');
                    changeDeviceButton.html('Управление');
                    changeDeviceButton.attr('onclick', 'changeHopperManage(\'' + accessLevel + '\')');
                    changeDeviceInfoWrapper.append(changeDeviceButton);
                    changeDevice.append(changeDeviceInfoWrapper);
                    break;
                }
                case('puloon'): {
                    changeDevice = $("<div></div>");
                    changeDevice.addClass('changeDeviceBox');
                    changeDevice.attr('id', 'puloon');
                    changeDeviceImageSrc = "img/useradmin/devices/puloon_t.png";
                    changeDeviceImage = $('<img src=' + changeDeviceImageSrc + ' />');
                    changeDeviceImage.addClass('changeDeviceImage');
                    changeDevice.append(changeDeviceImage);
                    changeDeviceInfoWrapper = $("<div></div>");
                    changeDeviceInfoWrapper.addClass('changeDeviceInfoWrapper');

                    for (let cashbox in changeInfo[i].info.cashboxes) {
                        for (let sum in changeInfo[i].info.cashboxes[cashbox].cash) {
                            changeSum += sum * changeInfo[i].info.cashboxes[cashbox].cash[sum];
                        }
                    }

                    for (let j = 1; j <= 4; j++) {
                        let found = -1;
                        changeDeviceCashbox = $("<div></div>");
                        changeDeviceCashbox.addClass('changeDeviceCashbox');
                        for (let k = 0; k < changeInfo[i].info.cashboxes.length; k++) {
                            if (changeInfo[i].info.cashboxes[k].number == j) {
                                found = k;
                                break;
                            } else found = -1;
                        }
                        if (found >= 0) {
                            changeDeviceCashbox.html('Кассета №' + j);
                            changeDeviceInfoWrapper.append(changeDeviceCashbox);

                            changeInfoKeys = changeInfo[i].info.cashboxes[found].cash;
                            let tempKey;
                            let lowestKey;
                            for (let tk = 0; tk < changeInfoKeys.length; tk++) {
                                lowestKey = tk;
                                for (let tl = tk; tl < changeInfoKeys.length - 1; tl++) {
                                    if (changeInfoKeys[tl + 1] < changeInfoKeys[tl]) lowestKey = tl;
                                }
                                if (lowestKey != tk) {
                                    tempKey = changeInfoKeys[tk];
                                    changeInfoKeys[tk] = changeInfoKeys[lowestKey];
                                    changeInfoKeys[lowestKey] = tempKey;
                                }
                            }
                            changeDeviceInfoDenominationsWrapper = $("<div></div>");
                            changeDeviceInfoDenominationsWrapper.addClass('changeDeviceInfoDenominationsWrapper');
                            changeDeviceInfoDenominationWrapper = $("<div></div>");
                            changeDeviceInfoDenominationWrapper.addClass('changeDeviceInfoDenominationWrapper');
                            changeDeviceInfoAmountWrapper = $("<div></div>");
                            changeDeviceInfoAmountWrapper.addClass('changeDeviceInfoAmountWrapper');
                            changeDeviceInfoSumWrapper = $("<div></div>");
                            changeDeviceInfoSumWrapper.addClass('changeDeviceInfoSumWrapper');
                            for (let key in changeInfoKeys) {
                                changeDeviceInfo = $("<div></div>");
                                changeDeviceInfo.addClass('changeDeviceInfo');

                                changeDeviceInfoDenomination = $("<div></div>");
                                changeDeviceInfoDenomination.addClass('changeDeviceInfoDenomination');
                                changeDeviceInfoDenomination.html(key + ' x ');
                                changeDeviceInfoDenominationWrapper.append(changeDeviceInfoDenomination);

                                changeDeviceInfoAmount = $("<div></div>");
                                changeDeviceInfoAmount.addClass('changeDeviceInfoAmount');
                                changeDeviceInfoAmount.html(changeInfo[i].info.cashboxes[found].cash[key] + ' =');
                                changeDeviceInfoAmountWrapper.append(changeDeviceInfoAmount);

                                changeDeviceInfoSum = $("<div></div>");
                                changeDeviceInfoSum.addClass('changeDeviceInfoSum');
                                changeDeviceInfoSum.html((Number(key) * Number(changeInfo[i].info.cashboxes[found].cash[key])).toLocaleString('ru-RU'));
                                changeDeviceInfoSumWrapper.append(changeDeviceInfoSum);
                            }
                            changeDeviceInfoDenominationsWrapper.append(changeDeviceInfoDenominationWrapper);
                            changeDeviceInfoDenominationsWrapper.append(changeDeviceInfoAmountWrapper);
                            changeDeviceInfoDenominationsWrapper.append(changeDeviceInfoSumWrapper);
                            changeDeviceInfoWrapper.append(changeDeviceInfoDenominationsWrapper);
                        } else {
                            changeDeviceCashbox.html('Кассета №' + j);
                            changeDeviceInfoDenominationsWrapper = $("<div></div>");
                            changeDeviceInfoDenominationsWrapper.addClass('changeDeviceInfoDenominationsWrapper');
                            changeDeviceInfoDenominationsWrapper.html('нет информации');
                            changeDeviceInfoWrapper.append(changeDeviceCashbox);
                            changeDeviceInfoWrapper.append(changeDeviceInfoDenominationsWrapper);
                            continue;
                        }
                    }
                    changeDeviceButton = $("<div></div>");
                    changeDeviceButton.addClass('changeDeviceButton');
                    changeDeviceButton.html('Управление');
                    changeDeviceButton.attr('onclick', 'changePuloonManage(\'' + accessLevel + '\')');
                    changeDeviceInfoWrapper.append(changeDeviceButton);
                    changeDevice.append(changeDeviceInfoWrapper);

                    changeDeviceInfoWrapper = $("<div></div>");
                    changeDeviceInfoWrapper.addClass('changeDeviceInfoWrapper');

                    changeDeviceCashbox = $("<div></div>");
                    changeDeviceCashbox.addClass('changeDeviceCashbox');
                    changeDeviceCashbox.html('Бокс');
                    changeDeviceInfoWrapper.append(changeDeviceCashbox);

                    if (changeInfo[i].info.box[0] != undefined) {
                        changeInfoKeys = changeInfo[i].info.box[0].cash;
                        let tempKey;
                        let lowestKey;
                        for (let tk = 0; tk < changeInfoKeys.length; tk++) {
                            lowestKey = tk;
                            for (let tl = tk; tl < changeInfoKeys.length - 1; tl++) {
                                if (changeInfoKeys[tl + 1] < changeInfoKeys[tl]) lowestKey = tl;
                            }
                            if (lowestKey != tk) {
                                tempKey = changeInfoKeys[tk];
                                changeInfoKeys[tk] = changeInfoKeys[lowestKey];
                                changeInfoKeys[lowestKey] = tempKey;
                            }
                        }
                        changeDeviceInfoDenominationsWrapper = $("<div></div>");
                        changeDeviceInfoDenominationsWrapper.addClass('changeDeviceInfoDenominationsWrapper');
                        changeDeviceInfoDenominationWrapper = $("<div></div>");
                        changeDeviceInfoDenominationWrapper.addClass('changeDeviceInfoDenominationWrapper');
                        changeDeviceInfoAmountWrapper = $("<div></div>");
                        changeDeviceInfoAmountWrapper.addClass('changeDeviceInfoAmountWrapper');
                        changeDeviceInfoSumWrapper = $("<div></div>");
                        changeDeviceInfoSumWrapper.addClass('changeDeviceInfoSumWrapper');
                        for (let key in changeInfoKeys) {
                            changeDeviceInfo = $("<div></div>");
                            changeDeviceInfo.addClass('changeDeviceInfo');

                            changeDeviceInfoDenomination = $("<div></div>");
                            changeDeviceInfoDenomination.addClass('changeDeviceInfoDenomination');
                            changeDeviceInfoDenomination.html(key + ' x ');
                            changeDeviceInfoDenominationWrapper.append(changeDeviceInfoDenomination);

                            changeDeviceInfoAmount = $("<div></div>");
                            changeDeviceInfoAmount.addClass('changeDeviceInfoAmount');
                            changeDeviceInfoAmount.html(changeInfo[i].info.box[0].cash[key] + ' =');
                            changeDeviceInfoAmountWrapper.append(changeDeviceInfoAmount);

                            changeDeviceInfoSum = $("<div></div>");
                            changeDeviceInfoSum.addClass('changeDeviceInfoSum');
                            changeDeviceInfoSum.html((Number(key) * Number(changeInfo[i].info.box[0].cash[key])).toLocaleString('ru-RU'));
                            changeDeviceInfoSumWrapper.append(changeDeviceInfoSum);
                        }
                        changeDeviceInfoDenominationsWrapper.append(changeDeviceInfoDenominationWrapper);
                        changeDeviceInfoDenominationsWrapper.append(changeDeviceInfoAmountWrapper);
                        changeDeviceInfoDenominationsWrapper.append(changeDeviceInfoSumWrapper);
                        changeDeviceInfoWrapper.append(changeDeviceInfoDenominationsWrapper);
                    } else {
                        changeDeviceInfoWrapper.append('Нет данных');
                    }
                    changeDevice.append(changeDeviceInfoWrapper);
                    break;
                }
                default: {
                    logException('От сервера получено неизвестное устройство сдачи: \'' + changeInfo[i].device + '\'');
                }
            }
            changeDevicesWrapper.append(changeDevice);
        }
        changeWrapper.append(changeDevicesWrapper);
        changeDeviceInfoTotalWrapper = $("<div></div>");
        changeDeviceInfoTotalWrapper.toggleClass('changeDeviceInfoTotalWrapper');
        changeDeviceInfoTotal = $("<div></div>");
        changeDeviceInfoTotal.toggleClass('changeDeviceInfoTotal');
        changeDeviceInfoTotal.html('Всего сдачи: ' + changeSum + ' рублей');
        changeDeviceInfoTotalWrapper.append(changeDeviceInfoTotal);
        changeWrapper.append(changeDeviceInfoTotalWrapper);

        return changeWrapper;
    } else {
        return changeWrapper.html('<div>Ни одно устройство не подключено</div>');
    }
}

function changeCashreceiverManage(accessLevel) {
    setBackButton('changeTab(\'change\', \'' + accessLevel + '\')');
    let denominations = extChangeCashreceiverManageSettingsGetDenominations();
    let denominationsJSON = JSON.stringify(denominations);
    changeWrapper = $('.changeWrapper');
    changeWrapper.html('');

    let cashbox;

    let infoWrapper;
    infoWrapper = $('<div></div>');
    infoWrapper.toggleClass('changeCashreceiverManageInfoWrapper');
    changeWrapper.append(infoWrapper);
    let statusWrapper;
    statusWrapper = $('<div></div>');
    statusWrapper.toggleClass('changeCashreceiverManageStatusWrapper');
    infoWrapper.append(statusWrapper);
    let settingsWrapper;
    settingsWrapper = $('<div></div>');
    settingsWrapper.toggleClass('changeCashreceiverManageTestchangeWrapper');
    infoWrapper.append(settingsWrapper);
    let testchangeWrapper;
    testchangeWrapper = $('<div></div>');
    testchangeWrapper.toggleClass('changeCashreceiverManageTestchangeWrapper');
    infoWrapper.append(testchangeWrapper);


    let statusTitle = $('<div></div>');
    statusTitle.toggleClass('changeCashreceiverManageTitle');
    statusTitle.html('Текущая сдача');
    statusWrapper.append(statusTitle);

    let statusContent = $('<div></div>');
    statusContent.toggleClass('changeCashreceiverManageStatusContent');
    statusWrapper.append(statusContent);
    let contentTable;
    contentTable = $("<table cellspacing=\"0\" cellpadding=\"0\" align='center'></table>");
    contentTable.toggleClass('changeCashreceiverManageStatusContentTable');
    statusContent.append(contentTable);

    let mId;
    for (let i = 0; i < changeInfo.length; i++) {
        if (changeInfo[i].device == 'puloon') {
            {
                mId = i;
                break;
            }
        }
    }

    let td;
    let tr;

    tr = '<tr><th>№</th><th>Номинал</th><th>Количество</th><th>Сумма</th></tr>';
    contentTable.html(contentTable.html() + tr);
    let tempsum = 0;
    for (let j = 1; j <= 4; j++) {
        let found = -1;


        for (let k = 0; k < changeInfo[mId].info.cashboxes.length; k++) {
            if (changeInfo[mId].info.cashboxes[k].number == j) {
                found = k;
                break;
            } else found = -1;
        }
        if (found >= 0) {

            changeInfoKeys = changeInfo[mId].info.cashboxes[found].cash;
            let tempKey;
            let lowestKey;
            for (let tk = 0; tk < changeInfoKeys.length; tk++) {
                lowestKey = tk;
                for (let tl = tk; tl < changeInfoKeys.length - 1; tl++) {
                    if (changeInfoKeys[tl + 1] < changeInfoKeys[tl]) lowestKey = tl;
                }
                if (lowestKey != tk) {
                    tempKey = changeInfoKeys[tk];
                    changeInfoKeys[tk] = changeInfoKeys[lowestKey];
                    changeInfoKeys[lowestKey] = tempKey;
                }
            }

            tr = '<tr>';

            td = '<td>' + j + '</td>';
            tr += td;

            for (let key in changeInfoKeys) {
                td = '<td>' + key + '</td>';
                tr += td;
                td = '<td>' + changeInfo[mId].info.cashboxes[found].cash[key] + '</td>';
                tr += td;
                td = '<td style="text-align: right">' + (Number(key) * Number(changeInfo[mId].info.cashboxes[found].cash[key])).toLocaleString('ru-RU') + '</td>';
                tr += td;
                tempsum += Number(key) * Number(changeInfo[mId].info.cashboxes[found].cash[key]);
            }
            tr += '</tr>';
            contentTable.html(contentTable.html() + tr);
        } else {
            tr = '<tr>';
            td = '<td>' + j + '</td>';
            tr += td;
            td = '<td colspan=\"3\">нет информации</td>';
            tr += td;
            tr += '</tr>';
            contentTable.html(contentTable.html() + tr);
        }
    }

    tr = '<tr><td align="right" colspan="4">Итого: ' + tempsum.toLocaleString('ru-RU') + '</td></tr>';
    contentTable.html(contentTable.html() + tr);

    let button = $('<div></div>');
    button.toggleClass('changeCashreceiverManageStatusButton');
    button.html('Начать приём');
    button.attr('onclick', 'alert(\'Приём начат!\')'); //todo add ext
    statusWrapper.append(button);
    button = $('<div></div>');
    button.toggleClass('changeCashreceiverManageStatusButton');
    button.html('Остановить приём');
    button.attr('onclick', 'alert(\'Приём остановлен!\')'); //todo add ext
    statusWrapper.append(button);

    let settingsTitle = $('<div></div>');
    settingsTitle.toggleClass('changeCashreceiverManageTitle');
    settingsTitle.html('Настройки сдачи');
    settingsWrapper.append(settingsTitle);
    let settingsText = $('<div></div>');
    settingsText.toggleClass('changeCashreceiverManageSettingsTopText');
    settingsText.html('Установите количество\nкупюр для сдачи');
    settingsWrapper.append(settingsText);
    let settingsContentWrapper;
    settingsContentWrapper = $('<div></div>');
    settingsContentWrapper.toggleClass('changeCashreceiverManageSettingsContentWrapper');
    settingsWrapper.append(settingsContentWrapper);

    let columnElements = 0;
    let column;
    cashbox = 0;
    for (let denomination in denominations) {
        if (!columnElements) {
            columnElements = 4;
            column = $('<div></div>');
            column.toggleClass('changeCashreceiverManageSettingsContentColumn');
            settingsContentWrapper.append(column);
        }
        let obj = $('<div></div>');
        obj.toggleClass('changeCashreceiverManageSettingsContentDenomination');
        obj.html(denomination + '&nbsp;&nbsp;<input id="CScashbox' + denomination + '" class="numpadNoDec" placeholder=' + denominations[denomination] + '>');
        column.append(obj);
        columnElements--;
        cashbox++;
    }
    settingsText = $('<div></div>');
    settingsText.toggleClass('changeCashreceiverManageSettingsBottomText');
    settingsText.html('Указанное количество купюр для каждого номинала будет сохранено для сдачи.' +
        ' Внимание: общее количество купюр для всех номиналов не должно превышать 70 штук.');
    settingsWrapper.append(settingsText);
    let saveButton = $('<div></div>');
    saveButton.toggleClass('changeCashreceiverManageSaveButton');
    saveButton.html('Сохранить');
    saveButton.attr('onclick', 'changeCashreceiverManageSettingsSave(\'' + denominationsJSON + '\')');
    settingsWrapper.append(saveButton);

    let testchangeTitle = $('<div></div>');
    testchangeTitle.toggleClass('changeCashreceiverManageTitle');
    testchangeTitle.html('Тестовая выдача');
    testchangeWrapper.append(testchangeTitle);
    let testchangeContent;
    testchangeContent = $('<div></div>');
    testchangeContent.toggleClass('changeCashreceiverManageTestchangeContentWrapper');
    testchangeWrapper.append(testchangeContent);

    columnElements = 0;
    cashbox = 0;
    for (let denomination in denominations) {
        if (!columnElements) {
            columnElements = 4;
            column = $('<div></div>');
            column.toggleClass('changeCashreceiverManageTestchangeContentColumn');
            testchangeContent.append(column);
        }
        let obj = $('<div></div>');
        obj.toggleClass('changeCashreceiverManageTestchangeContentDenomination');
        obj.html(denomination + '&nbsp;&nbsp;<input id="TCcashbox' + denomination + '" class="numpadNoDec" placeholder="Нажмите">');
        column.append(obj);
        columnElements--;
        cashbox++;
    }
    let giveButton = $('<div></div>');
    giveButton.toggleClass('changeCashreceiverManageGiveButton');
    giveButton.html('Выдать');
    giveButton.attr('onclick', 'changeCashreceiverManageTestchangeGive(\'' + denominationsJSON + '\')');
    testchangeWrapper.append(giveButton);

    initKeyboard();

    logStep('Открыто управление купюроприёмником во вкладке устройств сдачи');
}

function changeCashreceiverManageSettingsSave(denominationsJSON) {
    let denominations = JSON.parse(denominationsJSON);
    let str = 'Сохранены следующие значения:\n';
    let val;
    let vals = 0;
    let uiInput;
    for (let denomination in denominations) {
        uiInput = $("#CScashbox" + denomination);
        val = uiInput.val();
        if (val) {
            str += 'Купюра: ' + denomination + ' Количество: ' + val + '\n';
            uiInput.attr('placeholder', val);
            uiInput.val('');
            vals++;
        }
    }
    if (vals) {
        //todo add ext
    } else str = "Никакие данные не были обновлены";
    alert(str);
}

function changeCashreceiverManageTestchangeGive(denominationsJSON) {
    let denominations = JSON.parse(denominationsJSON);
    let str = 'Деньги! Везде деньги! МНОГО ДЕНЕЕЕГ ! ! !\n\nПолучены следующие значения:\n';
    let vals = 0;
    let val;
    for (let denomination in denominations) {
        val = $("#TCcashbox" + denomination).val();
        if (val) {
            str += 'Купюра: ' + denomination + ' Количество: ' + val + '\n';
            vals++;
        }
    }
    if (vals) {
        //todo добавить обращение к серверу
    } else str = 'Вам не нужно золото, милорд?';
    alert(str);
    for (let denomination in denominations) {
        $('#TCcashbox' + denomination).val('');
    }
}

function changeHopperManage(accessLevel) {
    setBackButton('changeTab(\'change\', \'' + accessLevel + '\')');
    let denominations = extChangeHopperManageFillGetDenominations();
    let denominationsJSON = JSON.stringify(denominations);
    changeWrapper = $('.changeWrapper');
    changeWrapper.html('');

    let infoWrapper;
    infoWrapper = $('<div></div>');
    infoWrapper.toggleClass('changeHopperManageInfoWrapper');
    changeWrapper.append(infoWrapper);
    let statusWrapper;
    statusWrapper = $('<div></div>');
    statusWrapper.toggleClass('changeHopperManageStatusWrapper');
    infoWrapper.append(statusWrapper);

    let statusTitle = $('<div></div>');
    statusTitle.toggleClass('changeHopperManageTitle');
    statusTitle.html('Текущая сдача');
    statusWrapper.append(statusTitle);

    let statusContent = $('<div></div>');
    statusContent.toggleClass('changeHopperManageStatusContent');
    statusWrapper.append(statusContent);
    let contentTable;
    contentTable = $("<table cellspacing=\"0\" cellpadding=\"0\" align='center'></table>");
    contentTable.toggleClass('changeHopperManageStatusContentTable');
    statusContent.append(contentTable);

    let mId;
    for (let i = 0; i < changeInfo.length; i++) {
        if (changeInfo[i].device == 'puloon') {
            {
                mId = i;
                break;
            }
        }
    }

    let td;
    let tr;

    tr = '<tr><th>№</th><th>Номинал</th><th>Количество</th><th>Сумма</th></tr>';
    contentTable.html(contentTable.html() + tr);
    let tempsum = 0;
    for (let j = 1; j <= 4; j++) {
        let found = -1;


        for (let k = 0; k < changeInfo[mId].info.cashboxes.length; k++) {
            if (changeInfo[mId].info.cashboxes[k].number == j) {
                found = k;
                break;
            } else found = -1;
        }
        if (found >= 0) {

            changeInfoKeys = changeInfo[mId].info.cashboxes[found].cash;
            let tempKey;
            let lowestKey;
            for (let tk = 0; tk < changeInfoKeys.length; tk++) {
                lowestKey = tk;
                for (let tl = tk; tl < changeInfoKeys.length - 1; tl++) {
                    if (changeInfoKeys[tl + 1] < changeInfoKeys[tl]) lowestKey = tl;
                }
                if (lowestKey != tk) {
                    tempKey = changeInfoKeys[tk];
                    changeInfoKeys[tk] = changeInfoKeys[lowestKey];
                    changeInfoKeys[lowestKey] = tempKey;
                }
            }

            tr = '<tr>';

            td = '<td>' + j + '</td>';
            tr += td;

            for (let key in changeInfoKeys) {
                td = '<td>' + key + '</td>';
                tr += td;
                td = '<td>' + changeInfo[mId].info.cashboxes[found].cash[key] + '</td>';
                tr += td;
                td = '<td style="text-align: right">' + (Number(key) * Number(changeInfo[mId].info.cashboxes[found].cash[key])).toLocaleString('ru-RU') + '</td>';
                tr += td;
                tempsum += Number(key) * Number(changeInfo[mId].info.cashboxes[found].cash[key]);
            }
            tr += '</tr>';
            contentTable.html(contentTable.html() + tr);
        } else {
            tr = '<tr>';
            td = '<td>' + j + '</td>';
            tr += td;
            td = '<td colspan=\"3\">нет информации</td>';
            tr += td;
            tr += '</tr>';
            contentTable.html(contentTable.html() + tr);
        }
    }

    tr = '<tr><td align="right" colspan="4">Итого: ' + tempsum.toLocaleString('ru-RU') + '</td></tr>';
    contentTable.html(contentTable.html() + tr);

    //REWORK BELOW
    let fillWrapper;
    fillWrapper = $('<div></div>');
    fillWrapper.toggleClass('changeHopperManageFillWrapper');
    infoWrapper.append(fillWrapper);
    let fillTitle = $('<div></div>');
    fillTitle.toggleClass('changeHopperManageTitle');
    fillTitle.html('Пополнить');
    fillWrapper.append(fillTitle);
    let fillContent;
    fillContent = $('<div></div>');
    fillContent.toggleClass('changeHopperManageFillContentWrapper');
    fillWrapper.append(fillContent);

    let columnElements = 0;
    let cashbox = 0;
    let column;
    for (let denomination in denominations) {
        if (!columnElements) {
            columnElements = 4;
            column = $('<div></div>');
            column.toggleClass('changeHopperManageFillContentColumn');
            fillContent.append(column);
        }
        let obj = $('<div></div>');
        obj.toggleClass('changeHopperManageFillContentDenomination');
        obj.html(denomination + '&nbsp;&nbsp;<input id="fillCashbox' + denomination + '" class="numpadNoDec" placeholder=\'' + denominations[denomination] + '\'>');
        column.append(obj);
        columnElements--;
        cashbox++;
    }
    let saveButton = $('<div></div>');
    saveButton.toggleClass('changeHopperManageSaveButton');
    saveButton.html('Сохранить');
    saveButton.attr('onclick', 'changeHopperManageFillSave(\'' + denominationsJSON + '\')');
    fillWrapper.append(saveButton);

    let testchangeWrapper;
    testchangeWrapper = $('<div></div>');
    testchangeWrapper.toggleClass('changeHopperManageTestchangeWrapper');
    if (accessLevel == 1) infoWrapper.append(testchangeWrapper);
    let testchangeTitle = $('<div></div>');
    testchangeTitle.toggleClass('changeHopperManageTitle');
    testchangeTitle.html('Тестовая выдача');
    testchangeWrapper.append(testchangeTitle);
    let testchangeContentWrapper;
    testchangeContentWrapper = $('<div></div>');
    testchangeContentWrapper.toggleClass('changeHopperManageTestchangeContentWrapper');
    testchangeWrapper.append(testchangeContentWrapper);

    testchangeContentWrapper.html('Сумма<br /><input id="hopperTCSum" class="numpadNoDec" placeholder="Нажмите">');

    let giveButton = $('<div></div>');
    giveButton.toggleClass('changeHopperManageGiveButton');
    giveButton.html('Выдать');
    giveButton.attr('onclick', 'changeHopperManageTestchangeGive(\'' + denominationsJSON + '\')');
    testchangeWrapper.append(giveButton);

    initKeyboard();

    logStep('Открыто управление хоппером во вкладке устройств сдачи');
}

function changeHopperManageFillSave(denominationsJSON) {
    let denominations = JSON.parse(denominationsJSON);
    let str = 'Сохранены следующие значения:\n';
    let val;
    let vals = 0;
    let uiInput;
    for (let denomination in denominations) {
        uiInput = $("#fillCashbox" + denomination);
        val = uiInput.val();
        if (val) {
            str += 'Купюра: ' + denomination + ' Количество: ' + val + '\n';
            uiInput.attr('placeholder', val);
            uiInput.val('');
            vals++;
        }
    }
    if (vals) {
        //todo add ext
    } else str = "Никакие данные не были обновлены";
    alert(str);
}

function changeHopperManageTestchangeGive(denominationsJSON) {
    let denominations = JSON.parse(denominationsJSON);
    let sumUI = $("#hopperTCSum");
    let val = sumUI.val();
    let str = 'Монеты! Везде монеты! МНОГО МОНЕЕЕТ ! ! !\n\nВас засыпало монетами на сумму: ' + val + ' руб.';
    if (val) {
        //todo добавить обращение к серверу
    } else str = 'Вам не нужно золото, милорд?';
    alert(str);
    sumUI.val('');
}

function changePuloonManage(accessLevel) {
    setBackButton('changeTab(\'change\', \'' + accessLevel + '\')');

    changeWrapper = $('.changeWrapper');
    changeWrapper.html('');

    let changePuloonManageWrapper;
    changePuloonManageWrapper = $('<div></div>');
    changePuloonManageWrapper.toggleClass('changePuloonManageWrapper');
    let changePuloonManageInfoWrapper;
    changePuloonManageInfoWrapper = $('<div></div>');
    changePuloonManageInfoWrapper.toggleClass('changePuloonManageInfoWrapper');
    let changePuloonManageStatusWrapper;
    changePuloonManageStatusWrapper = $('<div></div>');
    changePuloonManageStatusWrapper.toggleClass('changePuloonManageStatusWrapper');
    let changePuloonManageTestchangeWrapper;
    changePuloonManageTestchangeWrapper = $('<div></div>');
    changePuloonManageTestchangeWrapper.toggleClass('changePuloonManageTestchangeWrapper');
    let changePuloonManageChangeCassetteWrapper;
    changePuloonManageChangeCassetteWrapper = $('<div></div>');
    changePuloonManageChangeCassetteWrapper.toggleClass('changePuloonManageChangeCassetteWrapper');

    changeWrapper.append(changePuloonManageWrapper);
    changePuloonManageWrapper.append(changePuloonManageInfoWrapper);
    changePuloonManageWrapper.append(changePuloonManageChangeCassetteWrapper);
    changePuloonManageInfoWrapper.append(changePuloonManageStatusWrapper);
    if (accessLevel == 1) changePuloonManageInfoWrapper.append(changePuloonManageTestchangeWrapper);

    let changePuloonManageStatusTitle = $('<div></div>');
    changePuloonManageStatusTitle.toggleClass('changePuloonManageTitle');
    changePuloonManageStatusTitle.html('Текущая сдача');
    changePuloonManageStatusWrapper.append(changePuloonManageStatusTitle);

    let changePuloonManageStatusContent = $('<div></div>');
    changePuloonManageStatusContent.toggleClass('changePuloonManageStatusContent');
    changePuloonManageStatusWrapper.append(changePuloonManageStatusContent);

    let mId;
    for (let i = 0; i < changeInfo.length; i++) {
        if (changeInfo[i].device == 'puloon') {
            {
                mId = i;
                break;
            }
        }
    }

    let td;
    let tr;
    let changePuloonManageStatusContentTable;
    changePuloonManageStatusContentTable = $("<table cellspacing=\"0\" cellpadding=\"0\" align='center'></table>");
    changePuloonManageStatusContentTable.toggleClass('changePuloonManageStatusContentTable');
    changePuloonManageStatusContent.append(changePuloonManageStatusContentTable);

    tr = '<tr><th>№</th><th>Номинал</th><th>Количество</th><th>Сумма</th></tr>';
    changePuloonManageStatusContentTable.html(changePuloonManageStatusContentTable.html() + tr);
    let tempsum = 0;
    for (let j = 1; j <= 4; j++) {
        let found = -1;


        for (let k = 0; k < changeInfo[mId].info.cashboxes.length; k++) {
            if (changeInfo[mId].info.cashboxes[k].number == j) {
                found = k;
                break;
            } else found = -1;
        }
        if (found >= 0) {

            changeInfoKeys = changeInfo[mId].info.cashboxes[found].cash;
            let tempKey;
            let lowestKey;
            for (let tk = 0; tk < changeInfoKeys.length; tk++) {
                lowestKey = tk;
                for (let tl = tk; tl < changeInfoKeys.length - 1; tl++) {
                    if (changeInfoKeys[tl + 1] < changeInfoKeys[tl]) lowestKey = tl;
                }
                if (lowestKey != tk) {
                    tempKey = changeInfoKeys[tk];
                    changeInfoKeys[tk] = changeInfoKeys[lowestKey];
                    changeInfoKeys[lowestKey] = tempKey;
                }
            }

            tr = '<tr>';

            td = '<td>' + j + '</td>';
            tr += td;

            for (let key in changeInfoKeys) {
                td = '<td>' + key + '</td>';
                tr += td;
                td = '<td>' + changeInfo[mId].info.cashboxes[found].cash[key] + '</td>';
                tr += td;
                td = '<td style="text-align: right">' + (Number(key) * Number(changeInfo[mId].info.cashboxes[found].cash[key])).toLocaleString('ru-RU') + '</td>';
                tr += td;
                tempsum += Number(key) * Number(changeInfo[mId].info.cashboxes[found].cash[key]);
            }
            tr += '</tr>';
            changePuloonManageStatusContentTable.html(changePuloonManageStatusContentTable.html() + tr);
        } else {
            tr = '<tr>';
            td = '<td>' + j + '</td>';
            tr += td;
            td = '<td colspan=\"3\">нет информации</td>';
            tr += td;
            tr += '</tr>';
            changePuloonManageStatusContentTable.html(changePuloonManageStatusContentTable.html() + tr);
        }
    }

    tr = '<tr><td align="right" colspan="4">Итого: ' + tempsum.toLocaleString('ru-RU') + '</td></tr>';
    changePuloonManageStatusContentTable.html(changePuloonManageStatusContentTable.html() + tr);

    let changePuloonManagePurge = $('<div></div>');
    changePuloonManagePurge.toggleClass('changePuloonManagePurgeButton');
    changePuloonManagePurge.html('Очистка');
    changePuloonManagePurge.attr('onclick', 'alert(\'С Мистер Пропер веселей, в доме чисто в два раза быстрей! Мистер Пропер!\')');
    changePuloonManageStatusWrapper.append(changePuloonManagePurge);

    let changePuloonManageTestchangeTitle = $('<div></div>');
    changePuloonManageTestchangeTitle.toggleClass('changePuloonManageTitle');
    changePuloonManageTestchangeTitle.html('Тестовая выдача');
    changePuloonManageTestchangeWrapper.append(changePuloonManageTestchangeTitle);
    let changePuloonManageTestchangeContent;
    changePuloonManageTestchangeContent = $('<div></div>');
    changePuloonManageTestchangeContent.toggleClass('changePuloonManageTestchangeContent');
    changePuloonManageTestchangeWrapper.append(changePuloonManageTestchangeContent);

    for (let i = 1; i <= 4; i++) {
        let obj = $('<div></div>');
        obj.toggleClass('changePuloonManageTestchangeContentCashbox');
        obj.html('Кассета ' + i + '&nbsp;&nbsp;<input id="cashbox' + i + '" class="numpadNoDec" placeholder="Нажмите">');
        changePuloonManageTestchangeContent.append(obj);
    }

    let changePuloonManageTestchangeGiveButton = $('<div></div>');
    changePuloonManageTestchangeGiveButton.toggleClass('changePuloonManageGiveButton');
    changePuloonManageTestchangeGiveButton.html('Выдать');
    changePuloonManageTestchangeGiveButton.attr('onclick', 'changePuloonManageTestchangeGive()');
    changePuloonManageTestchangeWrapper.append(changePuloonManageTestchangeGiveButton);

    let changePuloonManageChangeCassetteButton = $('<div></div>');
    changePuloonManageChangeCassetteButton.toggleClass('changePuloonManageChangeCassetteButton');
    changePuloonManageChangeCassetteButton.html('Сменить кассету');
    changePuloonManageChangeCassetteButton.attr('onclick', 'showOperationPopup(\'changePuloonCassette\')');
    changePuloonManageChangeCassetteWrapper.append(changePuloonManageChangeCassetteButton);

    initKeyboard();

    logStep('Открыто управление пулоном во вкладке устройств сдачи');
}

function changePuloonManageTestchangeGive() {
    let str = 'Деньги! Везде деньги! МНОГО ДЕНЕЕЕГ ! ! !\n\nПолучены следующие значения:\n';
    let vals = 0;
    for (let i = 1; i <= 4; i++) {
        let val = $('#cashbox' + i).val();
        if (val) {
            str += 'Кассета ' + i + ': ' + val + '\n';
            vals++;
        }
    }
    if (vals) {
        //todo добавить обращение к серверу
    } else str = 'Вам не нужно золото, милорд?';
    alert(str);
    for (let i = 1; i <= 4; i++) {
        $('#cashbox' + i).val('');
    }
}

let cpcPWrap;
let cpcHint;
let cpcLegend;
let cpcLegendCassette;
let cpcLegendDenomination;
let cpcLegendAmount;
let cpcContent;
let cpcControl;
let cpcPrevious;
let cpcNext;
let cpcCassette = 0;
let cpcDenomination = 0;
let cpcAmount = -1;

let cpcCassettes = [1, 2, 3, 4];
let cpcDenominations = [10, 50, 100, 200, 500, 1000, 2000, 5000];

function changePuloonCassetteSteps(step) {
    switch (step) {
        case(0): {
            cpcCassette = 0;
            cpcDenomination = 0;
            cpcAmount = -1;
            shutdownOperation('cpc');
            unblockOperationScreen();
            logStep('Открыт первый шаг операции смены кассет в пулоне');
            break;
        }
        case(1): {
            changeCreateCassette();
            changeUpdateLegend();
            cpcPrevious.attr('onclick', 'changePuloonCassetteSteps(0)');
            cpcNext.attr('onclick', 'changePuloonCassetteSteps(2)');
            cpcHint.html('Выберите номер кассеты');
            logStep('Открыт шаг выбора номера кассеты операции смены касет в пулоне');
            break;
        }
        case(2): {
            changeCreateDenomination();
            changeUpdateLegend();
            cpcPrevious.attr('onclick', 'changePuloonCassetteSteps(1)');
            cpcNext.attr('onclick', 'changePuloonCassetteSteps(3)');
            cpcHint.html('Выберите номинал');
            logStep('Открыт шаг выбора номинала кассеты операции смены касет в пулоне');
            break;
        }
        case(3): {
            changeCreateAmount();
            changeUpdateLegend();
            $('.ui-keyboard-button button').css('font-size', '2em');
            cpcPrevious.attr('onclick', 'if(Number($(\'.cpcAmount\').val()))cpcAmount=Number($(\'.cpcAmount\').val());else cpcAmount=0;changePuloonCassetteSteps(2)');
            cpcNext.attr('onclick', 'if(Number($(\'.cpcAmount\').val()))cpcAmount=Number($(\'.cpcAmount\').val());else cpcAmount=0;changePuloonCassetteSteps(4)');
            cpcNext.html('Далее');
            $('.operationButton.inactive').toggleClass('inactive');
            cpcHint.html('Укажите количество купюр в кассете\nили Далее для ввода нулевого значения');
            initKeyboard();
            logStep('Открыт шаг выбора количества купюр в кассете операции смены касет в пулоне');
            break;
        }
        case(4): {
            logStep('Открыт шаг проверки данных операции смены касет в пулоне');
            cpcContent.html('');
            if (!cpcAmount) cpcAmount = 0;
            changeUpdateLegend();
            let obj = $('<div></div>');
            obj.css("font-size", "30px");
            if (cpcCassette && cpcDenomination) {
                logStep('Данные введены верно');
                obj.html("Проверьте правильность введённых данных. Продолжить?");
                cpcNext.attr('onclick', 'cpcComplete()');
                $('.operationButton.inactive').toggleClass('inactive');
            } else {
                logStep('Данные введены неверно');
                obj.html("Неправильно введены данные!");
                cpcNext.attr('onclick', '');
                cpcNext.toggleClass('inactive');
            }
            cpcContent.append(obj);
            cpcPrevious.attr('onclick', 'changePuloonCassetteSteps(3)');
            cpcNext.html('Готово');
            break;
        }
        default: {
            logException('Выбран неверный шаг changePuloonCassetteSteps');
            consoole.log();
            break;
        }
    }
}

function cpcComplete() {
    cpcCassette = 0;
    cpcDenomination = 0;
    cpcAmount = -1;

    //todo Получить функции от Андрея
    shutdownOperation('cpc');
    alert('По легенде, данные отправились на сервер и далее следует возврат на экран "Устройства сдачи"');
    unblockOperationScreen();
    logStep('Операция смены кассет в пулоне завершена');
}

function changeUpdateLegend() {
    let cassette = cpcCassette ? cpcCassette : 'не выбрана';
    let denomination = cpcDenomination ? cpcDenomination : 'не выбран';
    let amount = cpcAmount >= 0 ? cpcAmount : 'не выбрано';
    $('#legendCassette').html('Кассета: <p style=\'display: inline; color: red\'>' + cassette + '</p>');
    $('#legendDenomination').html('Номинал: <p style=\'display: inline; color: red\'>' + denomination + '</p>');
    $('#legendAmount').html('Количество купюр: <p style=\'display: inline; color: red\'>' + amount + '</p>');
    logStep('Текущие значения - Кассета: ' + cassette + ' Деноминация: ' + denomination + ' Сумма: ' + amount);
}

function changeCreateCassette() {
    cpcContent.html('');
    let template = $('<div></div>');
    template.toggleClass('operationSelectOption');
    cpcCassettes.forEach(function (item) {
        let button = template.clone();
        button.html('Кассета №' + item);
        button.attr('onclick', 'changeChooseCassette(' + item + ')');
        button.attr('id', 'cpcCassette' + item);
        cpcContent.append(button);
    });
    if (cpcCassette) $('#cpcCassette' + cpcCassette).toggleClass('selected');
}

function changeChooseCassette(id) {
    $('.operationSelectOption.selected').toggleClass('selected');
    $('#cpcCassette' + id).toggleClass('selected');
    cpcCassette = id;
    changeUpdateLegend();
    logStep('Выбрана кассета №' + id);
}

function changeCreateDenomination() {
    cpcContent.html('');
    let template = $('<div></div>');
    template.toggleClass('operationSelectOption');
    cpcDenominations.forEach(function (item) {
        let button = template.clone();
        button.html(item + 'р');
        button.attr('onclick', 'changeChooseDenomination(' + item + ')');
        button.attr('id', 'cpcDenomination' + item);
        cpcContent.append(button);
    });
    if (cpcDenomination) $('#cpcDenomination' + cpcDenomination).toggleClass('selected');
}

function changeChooseDenomination(id) {
    $('.operationSelectOption.selected').toggleClass('selected');
    $('#cpcDenomination' + id).toggleClass('selected');
    cpcDenomination = id;
    changeUpdateLegend();
    logStep('Выбрана деноминация' + id);
}

function changeCreateAmount(id) {
    cpcContent.html('');
    let obj = $('<input>');
    obj.toggleClass('cpcAmount');
    obj.toggleClass('numpad');
    obj.css("font-size", "30px");
    obj.attr('placeholder', 'Нажмите для ввода');
    cpcContent.append(obj);
}


function createStaff(accessLevel) {
    let content = $('.content');
    let staffWrapper = $('<div></div>');
    staffWrapper.toggleClass('staffWrapper');

    let master = $('<div></div>');
    master.toggleClass('staffMaster');
    master.html('Мастер: ' + barcodeAdminName);
    staffWrapper.append(master);
    let buttons = $('<div></div>');
    buttons.toggleClass('staffButtonsWrapper');
    staffWrapper.append(buttons);
    let lastbutton = $('<div></div>');
    lastbutton.toggleClass('staffLastbutton');
    staffWrapper.append(lastbutton);

    let element;
    element = $('<div></div>');
    element.toggleClass('staffMenuButton');
    element.attr('id', 'startWork');
    element.attr('onclick', 'staffStartWork()');
    element.html('Начало смены');
    buttons.append(element);

    element = $('<div></div>');
    element.toggleClass('staffMenuButton');
    element.attr('id', 'stopWork');
    element.attr('onclick', 'staffStopWork()');
    element.html('Конец смены');
    buttons.append(element);

    element = $('<div></div>');
    element.toggleClass('staffMenuButton');
    element.attr('id', 'endSession');
    element.attr('onclick', 'staffRegSession()');
    element.html('Регистрация продаж');
    lastbutton.append(element);

    content.append(staffWrapper);
}

function staffStartWork() {
    let startWork = $('#startWork');
    startWork.html('Смена началась<br>Хорошего дня! :-)');
    startWork.toggleClass('selected');
    extStaffUserAction('9', 'Начало смены');
    startWork.attr('onclick', '');
    logStep('Смена началась');
}

function staffStopWork() {
    let stopWork = $('#stopWork');
    stopWork.html('Смена закончилась<br>Улыбнитесь! :-)');
    stopWork.toggleClass('selected');
    extStaffUserAction('10', 'Конец смены');
    stopWork.attr('onclick', '');
    logStep('Смена закончилась');
}

let staffCount = 0;
let staffBarcode;
let staffBarcodes = [];
let staffPrintBarcodes = [];
let staffPolling;
let staffDatestr;
let staffUIText;
let staffUIRegStatus;
let staffUITimer;
let staffUITimeout = 3;

function staffRegSession() {
    barcodeOperation = 'staff';
    showOperationPopup('scissorsRegisterSales');
    if (!debug) extEnableBarcode();
    staffCount = 0;
    staffUIText = $('#registeredBarcodes');
    staffUIText.html('Зарегистрировано чеков: <span id=\'staffBarcodesAmount\'>' + staffCount + '</span>');
    staffUIRegStatus = $('<div></div>');
    staffUIRegStatus.toggleClass('operationText');
    staffUIRegStatus.html('Продажа отправлена на регистрацию...');
    staffUIRegStatus.css('visibility', 'hidden');
    staffUIText.after(staffUIRegStatus);
    logStep('Продажа отправлена на регистрацию');
}

function staffExecutor(barcode) {
    if (barcode.length > 1) setTimeoutTimer(timeout);

    if (barcode.length >= 21 && !isRegistered(barcode)) {
        clearTimeout(staffUITimer);
        staffBarcodes.push(barcode);
        staffPrintBarcodes.push(barcode);
        logStep('Поднесен штрих код регистрации: ' + barcode);
        extSetUsersBasket(barcode);
        staffCount++;

        staffUIRegStatus.html('Продажа отправлена на регистрацию...');
        staffUIText.html('Зарегистрировано чеков: <span id=\'staffBarcodesAmount\'>' + staffCount + '</span>');
        staffUIRegStatus.css('visibility', 'visible');
        staffUITimer = setTimeout(function () {
            staffUIRegStatus.css('visibility', 'hidden');
        }, staffUITimeout * 1000);
    } else if (barcode.length > 1 && barcode.length < 21) {
        clearTimeout(staffUITimer);
        logStep('Поднесен НЕВЕРНЫЙ штрих код для регистрации: ' + barcode);
        staffUIRegStatus.html('Некорректный штрих код, попробуйте еще');
        staffUIRegStatus.css('visibility', 'visible');
        staffUITimer = setTimeout(function () {
            staffUIRegStatus.css('visibility', 'hidden');
        }, staffUITimeout * 1000);
    } else if (isRegistered(barcode)) {
        clearTimeout(staffUITimer);
        logStep('Штрих код ' + barcode + ' поднесён повторно');
        staffUIRegStatus.html('Штрих код уже зарегистрирован, поднесите другой');
        staffUIRegStatus.css('visibility', 'visible');
        staffUITimer = setTimeout(function () {
            staffUIRegStatus.css('visibility', 'hidden');
        }, staffUITimeout * 1000);
    }

    function isRegistered(code) {
        let found = 0;
        for (let i in staffBarcodes) {
            if (staffBarcodes[i] === code) {
                found = 1;
                break;
            }
        }
        return found;
    }
}

function staffFinishReg(datestr) {
    let str;
    let divider = '--------------------------------\r\n';
    let fio;
    if (barcodeAdminName) fio = barcodeAdminName; else fio = 'Фамилия Имя Отчество';
    str = 'ОТЧЁТ ПО ОПЕРАЦИЯМ\r\n';
    str += divider;
    fio = 'ФИО: ' + fio;
    if (fio.length > 32) {
        fio = fio.replace(/\s/g, '\r\n');
    }
    str += fio + '\r\n';
    str += 'Дата: ' + datestr + '\r\n';
    str += 'Количество чеков: ' + $('#staffBarcodesAmount').html() + '\r\n';
    str += divider;
    if (staffPrintBarcodes.length) str += 'Отсканированные чеки:\r\n\r\n';
    staffPrintBarcodes.forEach(function (item, i, arr) {
        let ii = ((i + 1) < 10) ? '0' + (i + 1) : (i + 1);
        str += ii + '. ' + item + '\r\n';
    });
    logStep('Операция регистрации чеков завершена');
    if (staffPrintBarcodes.length) {
        extPrintCustomCheck(str);
        shutdownOperation('scissorsRegisterSales', 1);
    } else shutdownOperation('scissorsRegisterSales', 0);
    staffPrintBarcodes = [];
}

let kkmContent;
let kkmFirstFound;
let kkmSelectedTaxGroup;
let kkmSelectedTaxGroupValue;
let kkmSelectedPaymentOption;

function createKKM(accessLevel) {
    let temp;
    let lowest;
    let found;

    kkmContent = extGetTabContent('kkm');
    kkmContent.taxgroups.forEach(function (item, i, arr) {
        if (i === 0) found = item;
        if (item.value === "БЕЗ НДС") {
            found = arr[i];
            arr[i] = arr[0];
            arr[0] = found;
        }
    });

    let obj;
    let tab;
    obj = $("<div></div>");
    obj.toggleClass('kkmWrapper');
    let cashboxesWrapper = $("<div></div>");
    cashboxesWrapper.toggleClass('kkmCashboxesWrapper');
    obj.append(cashboxesWrapper);
    let contentWrapper = $("<div></div>");
    contentWrapper.toggleClass('kkmContentWrapper');
    obj.append(contentWrapper);
    let operationTabsWrapper = $("<div></div>");
    operationTabsWrapper.toggleClass('kkmOperationTabsWrapper');
    contentWrapper.append(operationTabsWrapper);
    let operationContentWrapper = $("<div></div>");
    operationContentWrapper.toggleClass('kkmOperationContentWrapper');
    contentWrapper.append(operationContentWrapper);

    //добавляем кассы
    kkmFirstFound = 0;
    for (let i = 1; i <= 5; i++) {
        found = 0;
        let cashbox = $("<div></div>");
        cashbox.toggleClass('kkmCashbox');
        cashbox.attr('id', 'cashbox' + i);
        kkmContent.cashboxes.forEach(function (item, j, arr) {
            if (item == i) found = 1;
        });
        if (!found) cashbox.toggleClass('inactive');
        else {
            cashbox.attr('onclick', 'cashboxOperate(' + i + ')');
            if (!kkmFirstFound) {
                cashbox.toggleClass('selected');
                cashboxOperate(i);
                kkmFirstFound = 1;
            }
        }
        cashbox.html('Касса ' + i);
        if (fixes.Union) {
            cashbox.css('font-size', '28px');
            switch (i) {
                case 1:
                    break;
                case 2: {
                    cashbox.html('Юнион Моторс');
                    break;
                }
                case 3: {
                    cashbox.html('ИП Егоров');
                    break;
                }
                case 4: {
                    cashbox.html('ИП Сильченков');
                    break;
                }
                case 5: {
                    cashbox.html('ИП Селезнев');
                    break;
                }
                default: {
                    logException('На фиксе Юниона i приняло неожиданное значение')
                }
            }
        }
        cashboxesWrapper.append(cashbox);
    }

    tab = $("<div></div>");
    tab.toggleClass('kkmOperationTab');
    tab.toggleClass('selected');
    tab.html('Отчёты');
    tab.attr('onclick', 'drawKKMReports()');
    tab.attr('id', 'kkmReports');
    operationTabsWrapper.append(tab);
    tab = $("<div></div>");
    tab.toggleClass('kkmOperationTab');
    tab.html('Продажа');
    tab.attr('onclick', 'drawKKMSellRefund(\'sell\')');
    tab.attr('id', 'kkmSell');
    operationTabsWrapper.append(tab);
    tab = $("<div></div>");
    tab.toggleClass('kkmOperationTab');
    tab.html('Возврат');
    tab.attr('onclick', 'drawKKMSellRefund(\'refund\')');
    tab.attr('id', 'kkmRefund');
    operationTabsWrapper.append(tab);
    tab = $("<div></div>");
    tab.toggleClass('kkmOperationTab');
    tab.html('Внесение<br>в кассу');
    tab.attr('onclick', 'drawKKMoperateCashbox(\'fill\')');
    tab.attr('id', 'kkmFill');
    operationTabsWrapper.append(tab);
    tab = $("<div></div>");
    tab.toggleClass('kkmOperationTab');
    tab.html('Выплата<br>из кассы');
    tab.attr('onclick', 'drawKKMoperateCashbox(\'take\')');
    tab.attr('id', 'kkmTake');
    operationTabsWrapper.append(tab);

    operationContentWrapper.append(createKKMReports());
    return obj;
}

function drawKKMReports() {
    let operationContentWrapper = $('.kkmOperationContentWrapper');
    operationContentWrapper.html('');
    $('.kkmOperationTab.selected').toggleClass('selected');
    $('#kkmReports').toggleClass('selected');
    operationContentWrapper.append(createKKMReports());
    logStep('Вкладка Отчёты вкладки ККМ готова');
}

function createKKMReports() {
    let obj = $("<div></div>");
    obj.toggleClass('kkmOperationReportsWrapper');
    let operation;

    let operationTimeout = 5;
    operation = $("<div></div>");
    operation.toggleClass('kkmReport');
    operation.html('Распечатать Z-отчёт');
    if (!kkmFirstFound) operation.toggleClass('inactive');
    else operation.attr('onclick', 'incassPrintReportZ(' + selectedCashBox + ', ' + operationTimeout + ')');
    obj.append(operation);

    operation = $("<div></div>");
    operation.toggleClass('kkmReport');
    operation.html('Распечатать X-отчёт');
    if (!kkmFirstFound) operation.toggleClass('inactive');
    else operation.attr('onclick', 'incassPrintReportX(' + selectedCashBox + ', ' + operationTimeout + ')');
    obj.append(operation);

    return obj;
}

function drawKKMSellRefund(param) {
    let operationContentWrapper = $('.kkmOperationContentWrapper');
    operationContentWrapper.html('');
    $('.kkmOperationTab.selected').toggleClass('selected');
    if (param === 'sell') $('#kkmSell').toggleClass('selected');
    else $('#kkmRefund').toggleClass('selected');
    operationContentWrapper.append(createKKMSellRefund(param));
    initKeyboard();
    logStep('Вкладка ' + param + ' вкладки ККМ готова');
}

function createKKMSellRefund(param) {
    let obj = $("<div class='kkmOperationSellRefundWrapper'></div>");
    let table = $("<table class='kkmOperationSellRefundInputsTable'></table>");
    obj.append(table);
    let taxgroups = $("<div class='kkmOperationSellRefundTaxgroupsWrapper'></div>");
    obj.append(taxgroups);
    let paymentOptions = $("<div class='kkmOperationSellRefundPaymentOptionsWrapper'></div>");
    obj.append(paymentOptions);
    let button = $("<div class='kkmOperationSellRefundButton'></div>");
    obj.append(button);
    let accountingTypes = ['Предоплата 100%', 'Частичная предоплата', 'Аванс', 'Полный расчёт', 'Частичый расчёт и кредит', 'Передача в кредит', 'Оплата кредита'];
    let accountingItems = ['Товар', 'Подакцизный товар', 'Работа', 'Услуга', 'Ставка азартной игры', 'Выигрыш азартной игры', 'Лотерейный билет', 'Выигрыш лотереи', 'Предоставление РИД', 'Платёж', 'Агентское вознаграждение', 'Составной предмет расчёта', 'Иной предмет расчёта', 'Имущественное право', 'Внереализационный доход', 'Страховые взносы', 'Торговый сбор', 'Курортный сбор'];

    let tableContent = '';
    let cash = refreshCashboxCash();
    cash = (cash === -1) ? "нет данных" : cash + ' руб.';
    tableContent += '<tr>';
    tableContent += '<td class="kkmOperationSellRefundInputText">Сейчас в кассе:</td>';
    tableContent += '<td><span id="cashboxCash" style="margin-left: 25px">' + cash + '</span></td>';
    if (param === 'refund') tableContent += '<td style="text-align: center">Опции</td>';
    tableContent += '</tr>';
    tableContent += '<tr><td>&nbsp;</td><td>&nbsp;</td></tr>';
    tableContent += '<tr>';
    tableContent += '<td class="kkmOperationSellRefundInputText">Сумма:</td>';
    tableContent += '<td><input id="kkmSum" class="numpad kkmOperationSellRefundInput" placeholder="Нажмите"></td>';
    if (param === 'refund') {
        tableContent += '<td><select id="accountingTypes" style="height: 1.8rem;">';
        accountingTypes.forEach(function (item, i) {
            tableContent += '<option id="' + (i + 1) + '">' + item + '</option>';
        });
        tableContent += '</select></td>';
    }
    tableContent += '</tr>';
    tableContent += '<tr>';
    tableContent += '<td class="kkmOperationSellRefundInputText">Количество:</td>';
    tableContent += '<td><input id="kkmAmount" class="numpad kkmOperationSellRefundInput" placeholder="Нажмите"></td>';
    if (param === 'refund') {
        tableContent += '<td><select id="accountingItems" style="height: 1.8rem;">';
        accountingItems.forEach(function (item, i) {
            tableContent += '<option id="' + (i + 1) + '">' + item + '</option>';
        });
        tableContent += '</select></td>';
    }
    tableContent += '</tr>';
    tableContent += '<tr>';
    tableContent += '<td class="kkmOperationSellRefundInputText" valign="top">Номенклатура:</td>';
    tableContent += '<td><textarea id="kkmOperation" class="keyboardStub kkmOperationSellRefundTextArea"' +
        ' onclick="showOperationPopup(\'keyboardInput\', \'Операция\', \'#kkmOperation\')" placeholder="Нажмите"></textarea></td>';
    if (param === 'refund') tableContent += '<td></td>';
    tableContent += '</tr>';
    tableContent += '<tr>';
    tableContent += '<td class="kkmOperationSellRefundInputText">Отдел:</td>';
    tableContent += '<td><input id="kkmDepartment" class="kkmNumpad16noDec kkmOperationSellRefundInput" placeholder="Нажмите"></td>';
    if (param === 'refund') tableContent += '<td></td>';
    tableContent += '</tr>';
    table.html(tableContent);

    let taxgroup;
    kkmContent.taxgroups.forEach(function (item, i, arr) {
        taxgroup = $('<div></div>');
        taxgroup.toggleClass('kkmOperationSellRefundTaxgroup');
        taxgroup.attr('id', 'taxgroup' + item.group);
        taxgroup.attr('onclick', 'kkmSelectTaxGroup(\'' + item.group + '\')');
        taxgroup.html(item.value);
        if (i === 0) {
            kkmSelectedTaxGroup = item.group;
            kkmSelectedTaxGroupValue = item.value;
            taxgroup.toggleClass('selected');
        }
        taxgroups.append(taxgroup);
    });

    // 1 - наличные
    // 2 - МИР
    // 3 - VISA
    // 4 - MasterCard
    kkmSelectedPaymentOption = 1; // по-умолчанию наличные

    let paymentOption;
    paymentOption = $('<div></div>');
    paymentOption.toggleClass('kkmOperationSellRefundPaymentOption');
    paymentOption.toggleClass('selected');
    paymentOption.attr('id', 'paymentOption1');
    paymentOption.attr('onclick', 'kkmSelectPaymentOption(\'1\')');
    paymentOption.html('Наличные');
    paymentOptions.append(paymentOption);
    paymentOption = $('<div></div>');
    paymentOption.toggleClass('kkmOperationSellRefundPaymentOption');
    paymentOption.attr('id', 'paymentOption3');
    paymentOption.attr('onclick', 'kkmSelectPaymentOption(\'3\')');
    paymentOption.html('Visa');
    paymentOptions.append(paymentOption);
    paymentOption = $('<div></div>');
    paymentOption.toggleClass('kkmOperationSellRefundPaymentOption');
    paymentOption.attr('id', 'paymentOption4');
    paymentOption.attr('onclick', 'kkmSelectPaymentOption(\'4\')');
    paymentOption.html('MasterCard');
    paymentOptions.append(paymentOption);
    paymentOption = $('<div></div>');
    paymentOption.toggleClass('kkmOperationSellRefundPaymentOption');
    paymentOption.attr('id', 'paymentOption2');
    paymentOption.attr('onclick', 'kkmSelectPaymentOption(\'2\')');
    paymentOption.html('МИР');
    paymentOptions.append(paymentOption);

    if (param === 'sell') button.html('Осуществить <span style="font-size: 26px; color: #faff28; font-weight: bolder;padding-left: 5px">продажу</span>');
    else if (param === 'refund') button.html('Осуществить <span style="font-size: 26px; color: #faff28; font-weight: bolder;padding-left: 5px">возврат</span>');
    button.attr('onclick', 'checkKKMFill(\'' + param + '\')');

    return obj;
}

function drawKKMoperateCashbox(param) {
    let operationContentWrapper = $('.kkmOperationContentWrapper');
    operationContentWrapper.html('');
    $('.kkmOperationTab.selected').toggleClass('selected');
    if (param === 'fill') $('#kkmFill').toggleClass('selected');
    else if (param === 'take') $('#kkmTake').toggleClass('selected');
    operationContentWrapper.append(createKKMoperateCashbox(param));
    initKeyboard();
    logStep('Вкладка ' + param + ' вкладки ККМ готова');
}

function createKKMoperateCashbox(param) {
    let obj = $("<div></div>");
    obj.toggleClass('kkmOperationOperateCashboxWrapper');

    let amount = $('<div></div>');
    amount.toggleClass('');
    let str = '<table>';
    let cash = refreshCashboxCash();
    cash = cash === -1 ? "нет данных" : cash + ' руб.';
    str += '<tr>';
    str += '<td class="kkmOperationSellRefundInputText">Сейчас в кассе:</td>';
    str += '<td><span id="cashboxCash" style="margin-left: 25px">' + cash + '</span></td>';
    str += '</tr>';
    str += '<tr><td>&nbsp;</td><td>&nbsp;</td></tr>';
    str += '<tr><td>Сумма</td><td><input id="kkmAmount" class="numpad kkmOperationSellRefundInput" placeholder="Нажмите"></td></tr>';
    amount.html(str);
    obj.append(amount);
    let button = $("<div></div>");
    button.toggleClass('kkmOperationOperateCashboxButton');
    if (param === 'fill') button.html('Внести деньги');
    else if (param === 'take') button.html('Осуществить выплату');
    button.attr('onclick', 'confirmKKMoperateCashbox(\'' + param + '\')');
    obj.append(button);

    return obj;
}

function confirmKKMoperateCashbox(param) {
    let strParam;
    let amountInput = $('#kkmAmount');
    let amount = amountInput.val();
    if (amount === '') {
        amountInput.addClass('red-shadow');
        setTimeout(function () {
            amountInput.removeClass('red-shadow');
        }, 1500);
    } else {
        if (param === 'fill') strParam = 'Внесение в кассу';
        else if (param === 'take') {
            strParam = 'Выплата из кассы';
            console.log('amount: ' + amount + ' parseFloat-amount: ' + parseFloat(amount) + ' cashboxCash: ' + parseFloat(cashboxCash));
            if (parseFloat(amount) > parseFloat(cashboxCash)) {
                let button = $('.kkmOperationOperateCashboxButton');
                let originalMessage = button.html();
                let originalOnclick = button.attr('onclick');
                button.html('В кассе недостаточно денег');
                button.attr('onclick', '');
                button.addClass('inactive');
                setTimeout(function () {
                    if (button.html() === 'В кассе недостаточно денег') {
                        button.html(originalMessage);
                        button.attr('onclick', originalOnclick);
                        button.removeClass('inactive');
                    }
                }, 4 * 1000);
                return;
            }
        }
        let str = 'Тип операции: ' + strParam + ' Сумма: ' + amount + ' руб.';
        showOperationPopup('simpleConfirm', 'Проверьте данные операции', str, 'Провести операцию', 'KKMoperateCashbox(\'' + param + '\', ' + amount + '), \'unblockOperationScreen()\'');
    }
    logStep('Открыто окно подтверждения операции ККМ: ' + param);
}

function KKMoperateCashbox(param, amount) {
    if (param === 'fill') extKKMFill(selectedCashBox, amount);
    if (param === 'take') extKKMTake(selectedCashBox, amount);
    $('#kkmAmount').val('');
    setTimeout(function () {
        let cash = refreshCashboxCash();
        cash = (Number(cash) === -1) ? "нет данных" : cash + ' руб.';
        $('#cashboxCash').html(cash);
    }, 600);
    unblockOperationScreen();
}

function checkKKMFill(param) {
    let sumInput = $('#kkmSum');
    let amountInput = $('#kkmAmount');
    let operationInput = $('#kkmOperation');
    let departmentInput = $('#kkmDepartment');
    let sum = sumInput.val().replace(/,/g, '.');
    let amount = amountInput.val();
    let operation = operationInput.val();
    let department = departmentInput.val();
    let accountingType;
    let accountingItem;
    if (param === 'refund') {
        let typeEl = $('select#accountingTypes option:selected');
        accountingType = {
            id: Number(typeEl.attr('id')),
            name: typeEl.text()
        };
        let accountEl = $('select#accountingItems option:selected');
        accountingItem = {
            id: Number(accountEl.attr('id')),
            name: accountEl.text()
        };
    }
    if (sum === '' || amount === '' || operation === '' || department === '') {
        if (sum === '') {
            sumInput.addClass('red-shadow');
            setTimeout(function () {
                sumInput.removeClass('red-shadow');
            }, 1500);
        }
        if (amount === '') {
            amountInput.addClass('red-shadow');
            setTimeout(function () {
                amountInput.removeClass('red-shadow');
            }, 1500);
        }
        if (operation === '') {
            operationInput.addClass('red-shadow');
            setTimeout(function () {
                operationInput.removeClass('red-shadow');
            }, 1500);
        }
        if (department === '') {
            departmentInput.addClass('red-shadow');
            setTimeout(function () {
                departmentInput.removeClass('red-shadow');
            }, 1500);
        }
    } else if (parseFloat(sum) > parseFloat(cashboxCash) && param == 'refund') {
        logStep('В кассе недостаточно денег. Запрошенная сумма: ' + parseFloat(sum) + ' Сумма в кассе: ' + parseFloat(cashboxCash));
        let button = $('.kkmOperationSellRefundButton');
        let originalMessage = button.html();
        let originalOnclick = button.attr('onclick');
        button.html('В кассе недостаточно денег');
        button.attr('onclick', '');
        button.addClass('inactive');
        setTimeout(function () {
            if (button.html() === 'В кассе недостаточно денег') {
                button.html(originalMessage);
                button.attr('onclick', originalOnclick);
                button.removeClass('inactive');
            }
        }, 4 * 1000);
        return;
    } else {
        if (param === 'sell') showOperationPopup('confirmKKMSellRefund', param, sum, amount, operation, department);
        if (param === 'refund') showOperationPopup('confirmKKMSellRefund', param, sum, amount, operation, department, accountingType, accountingItem);
    }
}

function kkmSelectTaxGroup(id) {
    $('.kkmOperationSellRefundTaxgroup.selected').toggleClass('selected');
    let taxgroupUI = $('#taxgroup' + id);
    taxgroupUI.toggleClass('selected');
    kkmSelectedTaxGroup = id;
    kkmSelectedTaxGroupValue = taxgroupUI.html();
}

function kkmSelectPaymentOption(id) {
    $('.kkmOperationSellRefundPaymentOption.selected').toggleClass('selected');
    $('#paymentOption' + id).toggleClass('selected');
    kkmSelectedPaymentOption = id;
}

function kkmExecuteOperation(type, sum, amount, operation, department, taxgroup, paymentoption, accountingType, accountingItem) {
    let status = -1;
    let operationTime = 0;
    let processingTimeout = 30 * 1000;
    blockScreenProcessing();
    if (type == 'sell') extKKMSell(sum, amount, operation, department, taxgroup, paymentoption);
    if (type == 'refund') extKKMRefund(sum, amount, operation, department, taxgroup, paymentoption, accountingType, accountingItem);
    let statusMessageUI = $('.operationKKMstatusMessage');
    //setTimeout(function(){status=0;},1000);
    // setTimeout(function () {status = 1;}, 1700);
    let executor = setInterval(function () {
        if (operationTime < processingTimeout) {
            status = extGetOperationStatus();
            if (status > -1) {
                if (status == 0) {
                    clearInterval(executor);
                    unblockScreenProcessing();
                    statusMessageUI.show();
                    statusMessageUI.html(extKKMOperationReport());
                }
                if (status == 1) {
                    clearInterval(executor);
                    unblockScreenProcessing();
                    extLink('adm.html'); //todo разработать возврат в ту же вкладку
                }
            }
            operationTime += 300;
        } else {
            clearInterval(executor);
            unblockScreenProcessing();
            statusMessageUI.show();
            statusMessageUI.html('Не удалось провести операцию');
        }
    }, 300);
}

function startPingInterface() {
    if (pingInterfaceInterval === null) {
        extPingInterface();
        pingInterfaceInterval = setInterval(function () {
            extPingInterface();
        }, 60 * 1000);
    }
}

function setTimeoutTimer(x) {
    clearTimeout(timeoutTimer);
    clearInterval(timeoutTime);

    timeoutTimer = setTimeout(function () {
        blockScreenTimeout();
        handleOperation(operationActive, 'pause');
        let uiTimeoutTime = $('.timeoutTime');
        $('.stillhereButton').attr('onclick', 'removeTimeoutMessage(' + x + ')');
        timeoutTime = setInterval(function () {
            if (x > 0) {
                x--;
                uiTimeoutTime.html(x);
            } else {
                //345
                handleOperation(operationActive, 'finish');
                shutdownOperation(operationActive);
                //setTimeout(function(){}, 300);
                extLink('main.html');
            }
        }, 1000)
    }, x * 1000);
}

function stopTimeoutTimer() {
    setTimeout(function () {
        clearTimeout(timeoutTimer);
        clearInterval(timeoutTime);
    }, 500);
}

function removeTimeoutMessage(x) {
    handleOperation(operationActive, 'continue');
    clearInterval(timeoutTime);
    $('.timeoutTime').html(x);
    setTimeoutTimer(x);
    unblockScreenTimeout();
}

function setTimer(x) {
    /*
    clearTimeout(timeoutTimer);
    timeoutTimer = setTimeout(function () {
        extLink('main.html')
    }, x * 1000);
    */
}

function showOperationPopup(operation, param, param2, param3, param4, param5, param6, param7) {
    blockOperationScreen('white');
    operationWrapper = $('.operationWrapper');
    operationWrapper.css('display', 'flex');
    operationWrapper.html('');
    operationActive = operation;

    switch (operation) {
        case ('simpleConfirm'): {
            // param - заголовок
            // param2 - сообщение пользователю
            // param3 - текст кнопки
            // param4 - функция на кнопку
            let wrapper = $('<div></div>');
            wrapper.toggleClass('operationScissorsRegisterSalesWrapper');
            operationWrapper.append(wrapper);
            let header = $('<div></div>');
            header.toggleClass('operationHeader');
            header.html(param);
            wrapper.append(header);
            let text = $('<div></div>');
            text.toggleClass('operationText');
            text.html(param2);
            wrapper.append(text);

            let button = $('<div class="operationButton"></div>');
            button.html(param3);
            button.attr('onclick', param4);
            wrapper.append(button);
            button = $('<div class="operationButton"></div>');
            button.html('Назад');
            if (param5) button.attr('onclick', param5);
            else button.attr('onclick', 'unblockOperationScreen()');
            wrapper.append(button);
            logStep('Открыто окно подтверждения операции ' + operation + ' с параметрами - заголовок: ' + param + ', сообщение пользователю: ' + param2 + ', текст кнопки: ' + param3 + ', функция на кнопку: ' + param4 + '');
            break;
        }
        case ('scissorsRegisterSales'): {
            operationActive = 'scissorsRegisterSales';
            if (debug) {
                barcodeAdminName = 'Фамилия Имя Отчество';
            }
            let wrapper = $('<div></div>');
            wrapper.toggleClass('operationScissorsRegisterSalesWrapper');
            operationWrapper.append(wrapper);
            let header = $('<div></div>');
            header.toggleClass('operationHeader');
            header.html('Поднесите штрих-код регистрации');
            wrapper.append(header);

            let master = $('<div class="operationText"></div>');
            let d = new Date;
            let dHours = d.getHours() < 10 ? '0' + d.getHours() : d.getHours();
            let dMinutes = d.getMinutes() < 10 ? '0' + d.getMinutes() : d.getMinutes();
            let dSeconds = d.getSeconds() < 10 ? '0' + d.getSeconds() : d.getSeconds();
            staffDatestr = d.getDate() + ' ' + monthNames[d.getMonth()] + ' ' + d.getFullYear() + ' ' + dHours + ':' + dMinutes + ':' + dSeconds;
            let adminName;
            if (barcodeAdminName.length > 32) adminName = barcodeAdminName.replace(/\s/g, '\n'); else adminName = barcodeAdminName;
            master.html('Мастер: ' + adminName + '\nДата: ' + d.getDate() + ' ' + monthNames[d.getMonth()] + ' ' + d.getFullYear());
            wrapper.append(master);

            wrapper.append('<img src="img/loading.gif" style="padding: 10px;" />');

            let text = $('<div id="registeredBarcodes" class="operationText"></div>');
            wrapper.append(text);

            let button = $('<div></div>');
            button.toggleClass('operationButton');
            button.html('Завершить сканирование');
            button.attr('onclick', 'staffFinishReg(\'' + staffDatestr + '\')');
            wrapper.append(button);

            if (debug) {
                button = $('<div></div>');
                button.toggleClass('operationButton');
                button.html('Register');
                button.attr('onclick', 'setBarcode(\'123456789012345678901\')');
                wrapper.append(button);
                button = $('<div></div>');
                button.toggleClass('operationButton');
                button.html('Not exactly Wrong');
                button.attr('onclick', 'setBarcode(\'111\')');
                wrapper.append(button);
            }
            logStep('Открыто окно сканирования чеков продаж "Ножниц"');
            break;
        }
        case ('changePuloonCassette'): {
            let wrapper = $('<div></div>');
            wrapper.toggleClass('operationScissorsRegisterSalesWrapper');
            operationWrapper.append(wrapper);

            cpcPWrap = $('<div></div>');
            cpcPWrap.toggleClass('pWrap');
            wrapper.append(cpcPWrap);

            cpcHint = $('<div></div>');
            cpcHint.toggleClass('operationHint');
            cpcLegend = $('<div></div>');
            cpcLegend.toggleClass('operationPopupLegend');
            cpcLegendCassette = $('<div></div>');
            cpcLegendCassette.attr('id', 'legendCassette');
            cpcLegendCassette.toggleClass('operationPopupLegendElement');
            cpcLegend.append(cpcLegendCassette);
            cpcLegendDenomination = $('<div></div>');
            cpcLegendDenomination.attr('id', 'legendDenomination');
            cpcLegendDenomination.toggleClass('operationPopupLegendElement');
            cpcLegend.append(cpcLegendDenomination);
            cpcLegendAmount = $('<div></div>');
            cpcLegendAmount.attr('id', 'legendAmount');
            cpcLegendAmount.toggleClass('operationPopupLegendElement');
            cpcLegend.append(cpcLegendAmount);

            cpcContent = $('<div></div>');
            cpcContent.toggleClass('operationPopupContent');

            cpcControl = $('<div></div>');
            cpcControl.toggleClass('operationPopupControl');
            cpcNext = $('<div>Далее</div>');
            cpcNext.toggleClass('operationButton');
            cpcNext.attr('onclick', 'changePuloonCassette.chooseCassette()');
            cpcPrevious = $('<div>Назад</div>');
            cpcPrevious.toggleClass('operationButton');
            cpcPrevious.attr('onclick', 'unblockOperationScreen()');
            cpcControl.append(cpcPrevious);
            cpcControl.append(cpcNext);

            cpcPWrap.append(cpcLegend);
            cpcPWrap.append(cpcContent);
            cpcPWrap.append(cpcControl);
            wrapper.append(cpcHint);
            wrapper.append(cpcPWrap);
            changePuloonCassetteSteps(1);
            break;
        }
        case ('keyboardInput'): {
            //param - Заголовок
            //param2 - css-селектор, откуда брать предыдущее значение

            let wrapper = $('<div></div>');
            wrapper.toggleClass('operationKeyboardWrapper');
            operationWrapper.append(wrapper);

            let header = $('<div></div>');
            header.toggleClass('operationKeyboardHeader');
            header.html(param);
            wrapper.append(header);

            let kbInput = $('<input>');
            kbInput.toggleClass('operationKeyboardInput');
            kbInput.attr('onclick', '');
            kbInput.attr('placeholder', 'Название операции');
            wrapper.append(kbInput);

            initOperationKeyboard(param2);
            logStep('Открыто окно ввода с клавиатуры с заголовком: ' + param + ' и селектором источника предыдущего значения: ' + param2);
            break;
        }
        case ('confirmKKMSellRefund'): {
            //param - продажа/возврат
            //param2 - сумма
            //param3 - количество
            //param4 - операция
            //param5 - отдел

            //(refund)
            //param6 - accountingType
            //param7 - accountingItem

            let paymentOption;
            switch (kkmSelectedPaymentOption) {
                case(0): {
                    paymentOption = '';
                    break;
                }
                case(1): {
                    paymentOption = '';
                    break;
                }
                case(2): {
                    paymentOption = '';
                    break;
                }
                case(3): {
                    paymentOption = '';
                    break;
                }
                default: {
                    paymentOption = '';
                    break;
                }
            }

            let textType;
            if (param === 'sell') textType = 'продажа';
            else if (param === 'refund') textType = 'возврат';

            let wrapper = $('<div></div>');
            wrapper.toggleClass('operationKKMConfirmWrapper');
            operationWrapper.append(wrapper);

            let header = $('<div></div>');
            header.toggleClass('operationKKMConfirmHeader');
            header.html('Подтвердите действие');
            wrapper.append(header);

            let info = $('<table></table>');
            info.toggleClass('operationKKMConfirmInfo');
            let str = '';
            str += '<tr><td align="right">Тип:</td><td style="color: red;text-align: left">' + textType + '</td></tr>';
            str += '<tr><td align="right">Касса:</td><td style="color: red;text-align: left">' + selectedCashBox + '</td></tr>';
            str += '<tr><td align="right">Сумма:</td><td style="color: red;text-align: left">' + param2 + '</td></tr>';
            str += '<tr><td align="right">Количество:</td><td style="color: red;text-align: left">' + param3 + '</td></tr>';
            str += '<tr><td align="right">Номенклатура:</td><td style="color: red;text-align: left">' + param4 + '</td></tr>';
            str += '<tr><td align="right">Отдел:</td><td style="color: red;text-align: left">' + param5 + '</td></tr>';
            str += '<tr><td align="right">Налоговая группа:</td><td style="color: red;text-align: left">' + kkmSelectedTaxGroupValue + '</td></tr>';
            str += '<tr><td align="right">Тип оплаты:</td><td style="color: red;text-align: left">' + $('.kkmOperationSellRefundPaymentOption.selected').html() + '</td></tr>';
            if (param === 'refund') {
                str += '<tr><td align="right">Признак способа расчета:</td><td style="color: red;text-align: left">' + param6.name + '</td></tr>';
                str += '<tr><td align="right">Признак предемета расчета:</td><td style="color: red;text-align: left">' + param7.name + '</td></tr>';
            }
            info.html(str);
            wrapper.append(info);

            let failMessage = $('<div></div>');
            failMessage.toggleClass('operationKKMstatusMessage');
            failMessage.html('Не удалось провести операцию');
            wrapper.append(failMessage);

            let buttonsWrapper = $('<div></div>');
            buttonsWrapper.toggleClass('operationKKMConfirmButtonsWrapper');
            wrapper.append(buttonsWrapper);

            let button;
            button = $('<div></div>');
            button.toggleClass('operationKKMConfirmButton');
            button.html('Назад');
            button.attr('onclick', '$(\'.operationKKMstatusMessage\').hide();unblockOperationScreen()');
            buttonsWrapper.append(button);
            button = $('<div></div>');
            button.toggleClass('operationKKMConfirmButton');
            if (param === 'sell') {
                button.attr('onclick', '' +
                    '$(\'.operationKKMstatusMessage\').hide();' +
                    'kkmExecuteOperation(\'' + param + '\', \'' + param2 + '\', \'' + param3 + '\', \'' + param4 + '\', \'' + param5 + '\', \'' + kkmSelectedTaxGroup + '\', \'' + kkmSelectedPaymentOption + '\')');
                button.html('Выполнить продажу');
            } else if (param === 'refund') {
                button.attr('onclick', '' +
                    '$(\'.operationKKMstatusMessage\').hide();' +
                    'kkmExecuteOperation(\'' + param + '\', \'' + param2 + '\', \'' + param3 + '\', \'' + param4 + '\', \'' + param5 + '\', \'' + kkmSelectedTaxGroup + '\', \'' + kkmSelectedPaymentOption + '\', ' + param6.id + ', ' + param7.id + ')');
                button.html('Выполнить возврат');
            }
            buttonsWrapper.append(button);
            logStep('Открыто окно подтверждения операции "' + param4 + '"(тип операции ' + param + ') в количестве ' + param3 + ' на сумму ' + param2 + ' в отдел №' + param5);
            break;
            //param - продажа/возврат
            //param2 - сумма
            //param3 - количество
            //param4 - операция
            //param5 - отдел
        }
        default: {
            break;
        }
    }
}

function setBackButton(execute) {
    let button = $('.backButton');
    button.css('visibility', 'visible');
    button.attr('onclick', execute);
}

function hideBackButton() {
    let button = $('.backButton');
    button.attr('onclick', 'logException(\'Непредвиденное нажатие кнопки "Назад".\')');
    button.css('visibility', 'hidden');
}

function handleOperation(operation, order) {
    switch (operation) {
        case('scissorsRegisterSales'): {
            if (order === 'pause') {
                logStep('Вызван обработчик паузы операции регистрации чеков');
                extDisableBarcode();
            }
            if (order === 'continue') {
                logStep('Вызван обработчик продолжения операции регистрации чеков');
                extEnableBarcode();
            }
            if (order === 'finish') {
                logStep('Вызван обработчик завершения операции регистрации чеков');
                staffFinishReg(staffDatestr);
            }
            break;
        }
        default: {
            logException('Функция handleOperation вызвана с неверным параметром или без него: \'' + operation + '\'');
            break;
        }
    }
}

function shutdownOperation(operation, param) {
    operationWrapper = $('.operationWrapper');
    switch (operation) {
        case 'scissorsRegisterSales': {
            logStep('Завершение операции ' + operation);
            // param - флаг наличия зарегистрированных чеков
            extDisableBarcode();
            let printTime = 2;
            if (param) {
                blockScreenProcessing('Печатается чек');
                setTimeout(function () {
                    unblockScreenProcessing();
                }, printTime * 1000);
            } else unblockScreenProcessing();
            break;
        }
        case 'cpc': {
            break;
        }
        default: {
            logException('Функция завершения текущей операции по таймауту вызвана с неизвестным параметром: \'' + operation + '\' Возможно какие-то устройства не были отключены.');
            break;
        }
    }
    operationWrapper.html('');
    unblockOperationScreen();
}

/* ОТЛАДКА для регистрации чеков персонала*/
let qqq = 678901;
let tqqq = '123456789012345';
let aqqq = [];
for (let sg = 0; sg < 100; sg++) {
    aqqq.push(tqqq + qqq);
    qqq++;
}
let iqqq = 0;

function setBarcode(num) {
    staffBarcode = aqqq[iqqq];
    iqqq++;
    //можно переделать с использованием экстернала window.external.SetKeyFromInterface("barcode", '0');
}

/* ОТЛАДКА для регистрации чеков персонала*/

function hideOperationPopup(operation, poll) {
    // no need right now
}

function setProcessingTimer(time) {
    blockScreenProcessing();
    clearTimeout(processingTimer);
    processingTimer = setTimeout(function () {
        unblockScreenProcessing();
    }, time * 1000)
}

function blockScreenProcessing(text) {
    stopTimeoutTimer();
    $('.processingTimerText').html(text || 'Операция выполняется');
    $('.processingScreenBlocker').show();
}

function unblockScreenProcessing() {
    setTimeoutTimer(timeout);
    $('.processingScreenBlocker').hide();
}

function blockOperationScreen(color) {
    let screenBlock = $('.operationScreenBlocker');
    screenBlock.css('display', 'flex');
    switch (color) {
        case('white'): {
            screenBlock.css('background-color', 'rgba(255, 255, 255,1)');
            break;
        }
        case('black'): {
            screenBlock.css('background-color', 'rgba(0, 0, 0, 0.6)');
            break;
        }
        default: {
            logException('Не выбран ни один ожидаемый фоновый цвет блокировки экрана: \'' + color + '\'');
            screenBlock.css('background-color', 'rgba(255, 255, 255,0.7)');
            break;
        }
    }
    clearInterval(timeoutTimer);
}

function unblockOperationScreen() {
    $('.operationScreenBlocker').hide();
    setTimeoutTimer(timeout);
}

function blockScreenTimeout() {
    $('.screenBlockedTimeout').css('display', 'flex');
}

function unblockScreenTimeout() {
    $('.screenBlockedTimeout').hide();
}

function logStep(str) {
    if (str === undefined) {
        logException('На logStep передан пустой параметр');
    } else {
        str = '[Шаги] ' + str;
        if (debugLog && debugLogSteps) {
            extSaveLog(str);
            console.log(str);
        }
    }
}

function logWarning(str) {
    if (str === undefined) {
        logException('На logWarning передан пустой параметр');
    } else {
        str = '[Предупреждение] ' + str;
        if (debugLog && debugLogWarnings) {
            extSaveLog(str);
            console.log(str);
        }
    }
}

function logException(str) {
    if (!str.length) str = 'logException вызван с пустым параметром';
    str = '[Исключение] ' + str;
    if (debugLog && debugLogExceptions) {
        extSaveLog(str);
        console.log(str);
    }
}

function test() {
    //whatever
}

function initOperationKeyboard(selector) {
    $('.operationKeyboardInput').keyboard({
        language: "ru",  // string or array
        rtl: false, // language direction right-to-left
        layout: 'ru-qwerty', //customnum qwerty ru-qwerty
        customLayout: {'normal': ['{cancel}']},
        position: {
            of: null,
            my: 'center top',
            at: 'center top',
            at2: 'center bottom'
        },
        reposition: true,
        usePreview: true,
        alwaysOpen: true,
        initialFocus: true,
        noFocus: false,
        stayOpen: false,
        userClosed: false,
        ignoreEsc: false,
        closeByClickEvent: false,
        wheelMessage: 'Use mousewheel to see other keys',
        autoAccept: true,
        autoAcceptOnEsc: false,
        lockInput: false,
        restrictInput: false,
        restrictInclude: '', // e.g. 'a b foo \ud83d\ude38'
        acceptValid: true,
        autoAcceptOnValid: false,
        cancelClose: true,
        tabNavigation: false,
        enterNavigation: true,
        enterMod: 'altKey',
        stopAtEnd: true,
        appendLocally: false,
        appendTo: 'body',
        stickyShift: true,
        caretToEnd: false,
        preventPaste: false,
        scrollAdjustment: 10,
        maxLength: false,
        maxInsert: true,
        repeatDelay: 500,
        repeatRate: 20,
        resetDefault: false,
        openOn: 'focus',
        keyBinding: 'mousedown touchstart',
        useWheel: true,
        useCombos: true,
        initialized: function (e, keyboard, el) {
            el.value = $(selector).val();
        },
        beforeVisible: function (e, keyboard, el) {
        },
        visible: function (e, keyboard, el) {
            $.keyboard.caret(keyboard.$preview, el.value.length);
        },
        beforeInsert: function (e, keyboard, el, textToAdd) {
            return textToAdd;
        },
        change: function (e, keyboard, el) {
        },
        beforeClose: function (e, keyboard, el, accepted) {
        },
        accepted: function (e, keyboard, el) {
            unblockOperationScreen();
            $(selector).val(el.value);
            setTimeout(function () {
                keyboard.destroy();
            }, 15);
        },
        canceled: function (e, keyboard, el) {
            unblockOperationScreen();
            setTimeout(function () {
                keyboard.destroy();
            }, 15);
        },
        restricted: function (e, keyboard, el) {
        },
        hidden: function (e, keyboard, el) {
        },
        switchInput: function (keyboard, goToNext, isAccepted) {
        },
        create: function (keyboard) {
            return keyboard.buildKeyboard();
        },
        buildKey: function (keyboard, data) {
            return data;
        },
        validate: function (keyboard, value, isClosing) {
            return true;
        }
    });
}

function initKeyboard() {

    IKQwe('.qwerty');
    IKNum('.numpad');

    function IKQwe(selector) {
        //stub
    }

    function IKNum(selector) {
        $(selector).keyboard({
            language: "en",  // string or array
            rtl: false, // language direction right-to-left
            layout: 'customnum', //customnum qwerty
            customLayout: {'normal': ['{cancel}']},
            position: {
                of: null,
                my: 'center top',
                at: 'center top',
                at2: 'center bottom'
            },
            reposition: true,
            usePreview: true,
            alwaysOpen: false,
            initialFocus: true,
            noFocus: false,
            stayOpen: false,
            userClosed: false,
            ignoreEsc: false,
            closeByClickEvent: false,
            display: {
                a: "\u2705:Сохранить (Shift+Enter)",
                accept: "Сохранить:Сохранить (Shift+Enter)",
                alt: "РУС:Русская клавиатура",
                b: "←:Удалить символ слева",
                bksp: "⇦:Удалить символ слева",
                c: "✖:Отменить (Esc)",
                cancel: "Отменить:Отменить (Esc)",
                clear: "C:Очистить",
                combo: "ö:Toggle Combo Keys",
                dec: ".:Decimal",
                e: "↵:Ввод",
                enter: "Ввод:Перевод строки",
                lock: "⇪ Lock:Caps Lock",
                s: "⇧:Верхний регистр",
                shift: "⇧:Верхний регистр",
                sign: "±:Сменить знак",
                space: "Пробел:",
                t: "⇥:Tab",
                tab: "⇥ Tab:Tab"
            },
            wheelMessage: 'Use mousewheel to see other keys',
            autoAccept: true,
            autoAcceptOnEsc: false,
            lockInput: false,
            restrictInput: false,
            restrictInclude: '', // e.g. 'a b foo \ud83d\ude38'
            acceptValid: true,
            autoAcceptOnValid: false,
            cancelClose: true,
            tabNavigation: false,
            enterNavigation: true,
            enterMod: 'altKey',
            stopAtEnd: true,
            appendLocally: false,
            appendTo: 'body',
            stickyShift: true,
            caretToEnd: false,
            preventPaste: false,
            scrollAdjustment: 10,
            maxLength: false,
            maxInsert: true,
            repeatDelay: 500,
            repeatRate: 20,
            resetDefault: false,
            openOn: 'focus',
            keyBinding: 'mousedown touchstart',
            useWheel: true,
            useCombos: true,
            initialized: function (e, keyboard, el) {
            },
            beforeVisible: function (e, keyboard, el) {
            },
            visible: function (e, keyboard, el) {
            },
            beforeInsert: function (e, keyboard, el, textToAdd) {
                return textToAdd;
            },
            change: function (e, keyboard, el) {
            },
            beforeClose: function (e, keyboard, el, accepted) {
            },
            accepted: function (e, keyboard, el) {
            },
            canceled: function (e, keyboard, el) {
            },
            restricted: function (e, keyboard, el) {
            },
            hidden: function (e, keyboard, el) {
            },
            switchInput: function (keyboard, goToNext, isAccepted) {
            },
            create: function (keyboard) {
                return keyboard.buildKeyboard();
            },
            buildKey: function (keyboard, data) {
                return data;
            },
            validate: function (keyboard, value, isClosing) {
                return true;
            }
        });
    }

    $('.kkmNumpad16noDec').keyboard({
        language: "en",  // string or array
        rtl: false, // language direction right-to-left
        layout: 'numNoDec', //customnum qwerty
        customLayout: {'normal': ['{cancel}']},
        position: {
            of: null,
            my: 'center top',
            at: 'center top',
            at2: 'center bottom'
        },
        reposition: true,
        usePreview: true,
        alwaysOpen: false,
        initialFocus: true,
        noFocus: false,
        stayOpen: false,
        userClosed: false,
        ignoreEsc: false,
        closeByClickEvent: false,
        display: {
            'a': '\u2705:Accept (Shift-Enter)',
            'accept': 'Accept:Accept (Shift-Enter)',
            'alt': 'AltGr:Alternate Graphemes',
            'b': '\u232b:Backspace',
            'bksp': 'Bksp:Backspace',
            'c': '\u2716:Cancel (Esc)',
            'cancel': 'Cancel:Cancel (Esc)',
            'clear': 'C:Clear',
            'combo': '\u00f6:Toggle Combo Keys',
            'dec': '.:Decimal',
            'e': '\u21b5:Enter',
            'empty': '\u00a0', // &nbsp;
            'enter': 'Enter:Enter',
            'left': '\u2190',
            'lock': '\u21ea Lock:Caps Lock',
            'next': 'Next',
            'prev': 'Prev',
            'right': '\u2192',
            's': '\u21e7:Shift',
            'shift': 'Shift:Shift',
            'sign': '\u00b1:Change Sign',
            'space': '&nbsp;:Space',
            't': '\u21e5:Tab',
            'tab': '\u21e5 Tab:Tab',
            'toggle': ' ',
            'valid': 'valid',
            'invalid': 'invalid',
            'active': 'active',
            'disabled': 'disabled'
        },
        wheelMessage: 'Use mousewheel to see other keys',
        autoAccept: true,
        autoAcceptOnEsc: false,
        lockInput: false,
        restrictInput: false,
        restrictInclude: '', // e.g. 'a b foo \ud83d\ude38'
        acceptValid: true,
        autoAcceptOnValid: false,
        cancelClose: true,
        tabNavigation: false,
        enterNavigation: true,
        enterMod: 'altKey',
        stopAtEnd: true,
        appendLocally: false,
        appendTo: 'body',
        stickyShift: true,
        caretToEnd: false,
        preventPaste: false,
        scrollAdjustment: 10,
        maxLength: false,
        maxInsert: true,
        repeatDelay: 500,
        repeatRate: 20,
        resetDefault: false,
        openOn: 'focus',
        keyBinding: 'mousedown touchstart',
        useWheel: true,
        useCombos: true,
        initialized: function (e, keyboard, el) {
        },
        beforeVisible: function (e, keyboard, el) {
        },
        visible: function (e, keyboard, el) {
        },
        beforeInsert: function (e, keyboard, el, textToAdd) {
            return textToAdd;
        },
        change: function (e, keyboard, el) {
        },
        beforeClose: function (e, keyboard, el, accepted) {
        },
        accepted: function (e, keyboard, el) {

        },
        canceled: function (e, keyboard, el) {
        },
        restricted: function (e, keyboard, el) {
        },
        hidden: function (e, keyboard, el) {
        },
        switchInput: function (keyboard, goToNext, isAccepted) {
        },
        create: function (keyboard) {
            return keyboard.buildKeyboard();
        },
        buildKey: function (keyboard, data) {
            return data;
        },
        validate: function (keyboard, value, isClosing) {
            let valid;
            if (value === '') return true;
            else valid = /\b([0-9]|1[0-6])\b$/g.test(value);

            if (isClosing && valid) {
                return true;
            } else if (isClosing && !valid) {
                /*
                keyboard.$preview.addClass('red-border');
                setTimeout(function () {
                    keyboard.$preview.removeClass('red-border');
                }, 5000);
                */
                keyboard.$el.trigger('canceled', [keyboard, keyboard.el]);
                return false;
            }
            return valid;
        }
    });

    $('.numpadNoDec').keyboard({
        language: "en",  // string or array
        rtl: false, // language direction right-to-left
        layout: 'numNoDec', //customnum qwerty
        customLayout: {'normal': ['{cancel}']},
        position: {
            of: null,
            my: 'center top',
            at: 'center top',
            at2: 'center bottom'
        },
        reposition: true,
        usePreview: true,
        alwaysOpen: false,
        initialFocus: true,
        noFocus: false,
        stayOpen: false,
        userClosed: false,
        ignoreEsc: false,
        closeByClickEvent: false,
        display: {
            'a': '\u2705:Accept (Shift-Enter)',
            'accept': 'Accept:Accept (Shift-Enter)',
            'alt': 'AltGr:Alternate Graphemes',
            'b': '\u232b:Backspace',
            'bksp': 'Bksp:Backspace',
            'c': '\u2716:Cancel (Esc)',
            'cancel': 'Cancel:Cancel (Esc)',
            'clear': 'C:Clear',
            'combo': '\u00f6:Toggle Combo Keys',
            'dec': '.:Decimal',
            'e': '\u21b5:Enter',
            'empty': '\u00a0', // &nbsp;
            'enter': 'Enter:Enter',
            'left': '\u2190',
            'lock': '\u21ea Lock:Caps Lock',
            'next': 'Next',
            'prev': 'Prev',
            'right': '\u2192',
            's': '\u21e7:Shift',
            'shift': 'Shift:Shift',
            'sign': '\u00b1:Change Sign',
            'space': '&nbsp;:Space',
            't': '\u21e5:Tab',
            'tab': '\u21e5 Tab:Tab',
            'toggle': ' ',
            'valid': 'valid',
            'invalid': 'invalid',
            'active': 'active',
            'disabled': 'disabled'
        },
        wheelMessage: 'Use mousewheel to see other keys',
        autoAccept: true,
        autoAcceptOnEsc: false,
        lockInput: false,
        restrictInput: false,
        restrictInclude: '', // e.g. 'a b foo \ud83d\ude38'
        acceptValid: true,
        autoAcceptOnValid: false,
        cancelClose: true,
        tabNavigation: false,
        enterNavigation: true,
        enterMod: 'altKey',
        stopAtEnd: true,
        appendLocally: false,
        appendTo: 'body',
        stickyShift: true,
        caretToEnd: false,
        preventPaste: false,
        scrollAdjustment: 10,
        maxLength: false,
        maxInsert: true,
        repeatDelay: 500,
        repeatRate: 20,
        resetDefault: false,
        openOn: 'focus',
        keyBinding: 'mousedown touchstart',
        useWheel: true,
        useCombos: true,
        initialized: function (e, keyboard, el) {
        },
        beforeVisible: function (e, keyboard, el) {
        },
        visible: function (e, keyboard, el) {
        },
        beforeInsert: function (e, keyboard, el, textToAdd) {
            return textToAdd;
        },
        change: function (e, keyboard, el) {
        },
        beforeClose: function (e, keyboard, el, accepted) {
        },
        accepted: function (e, keyboard, el) {

        },
        canceled: function (e, keyboard, el) {
        },
        restricted: function (e, keyboard, el) {
        },
        hidden: function (e, keyboard, el) {
        },
        switchInput: function (keyboard, goToNext, isAccepted) {
        },
        create: function (keyboard) {
            return keyboard.buildKeyboard();
        },
        buildKey: function (keyboard, data) {
            return data;
        },
        validate: function (keyboard, value, isClosing) {
            return true;
        }
    });

    $('.keyboardStub').keyboard({
        language: "en",  // string or array
        rtl: false, // language direction right-to-left
        layout: 'customnum', //customnum qwerty
        customLayout: {'normal': ['{cancel}']},
        position: {
            of: null,
            my: 'center top',
            at: 'center top',
            at2: 'center bottom'
        },
        reposition: true,
        usePreview: true,
        alwaysOpen: false,
        initialFocus: true,
        noFocus: false,
        stayOpen: false,
        userClosed: false,
        ignoreEsc: false,
        closeByClickEvent: false,
        display: {
            'a': '\u2705:Accept (Shift-Enter)',
            'accept': 'Accept:Accept (Shift-Enter)',
            'alt': 'AltGr:Alternate Graphemes',
            'b': '\u232b:Backspace',
            'bksp': 'Bksp:Backspace',
            'c': '\u2716:Cancel (Esc)',
            'cancel': 'Cancel:Cancel (Esc)',
            'clear': 'C:Clear',
            'combo': '\u00f6:Toggle Combo Keys',
            'dec': '.:Decimal',
            'e': '\u21b5:Enter',
            'empty': '\u00a0', // &nbsp;
            'enter': 'Enter:Enter',
            'left': '\u2190',
            'lock': '\u21ea Lock:Caps Lock',
            'next': 'Next',
            'prev': 'Prev',
            'right': '\u2192',
            's': '\u21e7:Shift',
            'shift': 'Shift:Shift',
            'sign': '\u00b1:Change Sign',
            'space': '&nbsp;:Space',
            't': '\u21e5:Tab',
            'tab': '\u21e5 Tab:Tab',
            'toggle': ' ',
            'valid': 'valid',
            'invalid': 'invalid',
            'active': 'active',
            'disabled': 'disabled'
        },
        wheelMessage: 'Use mousewheel to see other keys',
        css: {
            input: 'ui-widget-content ui-corner-all',
            container: 'ui-widget-content ui-widget ui-corner-all ui-helper-clearfix',
            popup: '',
            buttonDefault: 'ui-state-default ui-corner-all',
            buttonHover: 'ui-state-hover',
            buttonAction: 'ui-state-active',
            buttonDisabled: 'ui-state-disabled',
            buttonEmpty: 'ui-keyboard-empty'
        },
        autoAccept: true,
        autoAcceptOnEsc: false,
        lockInput: false,
        restrictInput: false,
        restrictInclude: '', // e.g. 'a b foo \ud83d\ude38'
        acceptValid: true,
        autoAcceptOnValid: false,
        cancelClose: true,
        tabNavigation: false,
        enterNavigation: true,
        enterMod: 'altKey',
        stopAtEnd: true,
        appendLocally: false,
        appendTo: 'body',
        stickyShift: true,
        caretToEnd: false,
        preventPaste: false,
        scrollAdjustment: 10,
        maxLength: false,
        maxInsert: true,
        repeatDelay: 500,
        repeatRate: 20,
        resetDefault: false,
        openOn: '',
        keyBinding: 'mousedown touchstart',
        useWheel: true,
        useCombos: true,
        initialized: function (e, keyboard, el) {
        },
        beforeVisible: function (e, keyboard, el) {
        },
        visible: function (e, keyboard, el) {
        },
        beforeInsert: function (e, keyboard, el, textToAdd) {
            return textToAdd;
        },
        change: function (e, keyboard, el) {
        },
        beforeClose: function (e, keyboard, el, accepted) {
        },
        accepted: function (e, keyboard, el) {
        },
        canceled: function (e, keyboard, el) {
        },
        restricted: function (e, keyboard, el) {
        },
        hidden: function (e, keyboard, el) {
        },
        switchInput: function (keyboard, goToNext, isAccepted) {
        },
        create: function (keyboard) {
            return keyboard.buildKeyboard();
        },
        buildKey: function (keyboard, data) {
            return data;
        },
        validate: function (keyboard, value, isClosing) {
            let valid;
            if (value === '') return true;
            else valid = /\b([0-9]|1[0-6])\b$/g.test(value);

            if (isClosing && valid) {
                return true;
            } else if (isClosing && !valid) {
                keyboard.$preview.addClass('red-border');
                setTimeout(function () {
                    keyboard.$preview.removeClass('red-border');
                }, 5000);
                keyboard.$el.trigger('canceled', [keyboard, keyboard.el]);
                return false;
            }
            return valid;
        }
    });
}

/*
//тест таймеров таймаута
let Ptimer = timeoutTimer;
let Ptime = timeoutTime;
setInterval(function () {
    let time = new Date;
    if (timeoutTime != Ptime) {
        console.log('timeoutTime ID: ' + timeoutTime + '. ' + time.getMinutes() + ':' + time.getSeconds() + ':' + time.getMilliseconds());
        console.log('Ptime ' + Ptime);
        Ptime = timeoutTime;
    }
    if (timeoutTimer != Ptimer) {
        console.log('timeoutTimer ID: ' + timeoutTimer + '. ' + time.getMinutes() + ':' + time.getSeconds() + ':' + time.getMilliseconds());
        Ptimer = timeoutTimer;
    }
}, 30);
*/
