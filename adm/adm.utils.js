function requestCashRegister(action) {
    return new Promise(function (resolve, reject){
        blockScreenProcessing();
        let baseURL = 'http://192.168.1.48:64436/api/CashRegister/';
        logStep('Отправляем запрос на открытие кассы');
        fetch(baseURL + 'OpenCashRegister').then(function (response) {
            response.json().then(function (result) {
                logWarning('Ответ: ' + JSON.stringify(result));
                if (result.resultState === 0) {
                    logStep('Касса открыта. Отправляем запрос ' + action);
                    fetch(baseURL + action).then(function (response) {
                        response.json().then(function (result) {
                            logWarning('Ответ: ' + JSON.stringify(result));
                            if (result.resultState === 0) {
                                logStep('Успех. Отправляем запрос на закрытие кассы');
                                fetch(baseURL + 'CloseCashRegister').then(function (response) {
                                    response.json().then(function (result) {
                                        logWarning('Ответ: ' + JSON.stringify(result));
                                        if (result.resultState === 0) {
                                            logStep('Касса закрыта');
                                            resolve('Касса закрыта');
                                        } else onError('Касса не была закрыта')
                                    }).catch(onError);
                                })
                            } else onError('Ошибка при запросе ' + action + '#' + result.resultState + ': ' + result.errorMessage);
                        }).catch(onError);
                    })
                } else onError('Ошибка при открытии кассы #' + result.resultState + ': ' + result.errorMessage)
            }).catch(onError);
        });

        function onError(error) {
            logException(error);
            reject(error);
        }
    });
}