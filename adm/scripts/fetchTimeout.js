function fetchTimeout(url, options, timeout) {
    if (!timeout) timeout = 30 * 1000;
    return Promise.race([
        fetch(url, options),
        new Promise(function (_, reject) {
            setTimeout(function () {
                reject('timeout')
            }, timeout)
        })
    ])
}