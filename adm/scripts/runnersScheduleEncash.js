let scheduleEncashDebug = 0;
let scheduleEncashTimer;
let scheduleEncashItems = [];
let scheduleEncashInterval = 10 * 60 * 1000;
if (scheduleEncashDebug) scheduleEncashInterval = 10 * 1000;
let scheduleEncashURL = 'http://localhost/sandbox/runners/scheduleencash.php';
let scheduleEncashDebugCheck = 0;

setTimeout(function () {
    // scheduleEncashCheck();
    // scheduleEncashStart();
}, 3000);

function scheduleEncashStart() {
    clearInterval(scheduleEncashTimer);
    scheduleEncashTimer = setInterval(function () {
        scheduleEncashCheck();
    }, scheduleEncashInterval);
}

function scheduleEncashStop() {
    clearInterval(scheduleEncashTimer);
}

function scheduleEncashCheck() {
    if (scheduleEncashDebug) {
        scheduleEncashDebugCheck++;
        $('#debugmessage').show().html('Проверка очереди #' + scheduleEncashDebugCheck);
    }

    try {
        scheduleEncashStop();
        fetchTimeout(scheduleEncashURL + '?getScheduledItems&clearCache=' + (new Date()).getTime(), {
            method: 'get'
        }).then(onFullfilled, onRejected)
            .catch(function (e) {
                scheduleEncashSaveLog('Ошибка fetch:' + e);
            });
    } catch (e) {
        scheduleEncashSaveLog('Ошибка в scheduleEncashCheck: ' + e);
    }

    function onFullfilled(r) {
        r.text().then(function (out) {
            if (out && out.match('2002'))
                onRejected();
            else {
                // alert(JSON.stringify(out));
                // alert(out);
                out = JSON.parse(out);
                // out.docsNums = JSON.parse(out.docsNums);
                if (out.status === 0) {
                    onRejected();
                } else {
                    // alert('out.result:\n' + JSON.stringify(out.result) + '\nscheduleEncashItems:\n' + scheduleEncashItems);
                    out.result.forEach(function (item) {
                        if (scheduleEncashItems.findIndex(function (val) {
                            if (val.sessionId === item.sessionId) return 1;
                        }) === -1) scheduleEncashItems.push(item);
                    });
                    if (scheduleEncashItems.length) {
                        // alert('typeof scheduleEncashitems: ' + typeof scheduleEncashItems);
                        // alert(JSON.stringify(scheduleEncashItems));
                        // alert(scheduleEncashItems);
                        scheduleEncashSend(scheduleEncashItems)
                    } else {
                        scheduleEncashStart();
                    }
                }
            }
        }).catch(function (e) {
            scheduleEncashSaveLog('scheduleEncashCheck > fetch > onFullfilled#3: ' + e);
            scheduleEncashStart();
        });
    }

    function onRejected() {
        scheduleEncashSaveLog('Локальный веб-сервер недоступен. Очередь работать не может');
        scheduleEncashStart();
    }
}

function scheduleEncashSend(Items) {
    let promises = [];
    let session;
    let obj;
    try {
        Items.forEach(function (item) {
            obj = item;
            session = item.sessionId;
            // item = encodeURI(JSON.stringify(item));
            promises.push(extRunnSendEncashment(item, 1));
        });
    } catch (e) {
        scheduleEncashSaveLog('Ошибка в scheduleEncashSend: ' + e);
    }
    Promise.all(promises).then(function (results) {
        // alert('Promise.all results:\n' + results);
        while (results.findIndex(isDone) !== -1) results.splice(results.findIndex(isDone), 1);
        scheduleEncashItems = results;
        // alert(JSON.stringify(results));

        function isDone(el) {
            if (el === 'done') return 1;
        }
    }, function (e) {
        scheduleEncashSaveLog('Promise.All получил rejected. e: ' + e); // это в принципе не должно произойти
    }).catch(function (e) {
        scheduleEncashSaveLog('Ошибка на выполнении Promise.All в очереди: ' + e);
    }).finally(function () {
        scheduleEncashStart();
    });
}

function scheduleEncashDelete(sessionId) {
    // alert('scheduleEncashDelete #' + sessionId);
    fetchTimeout(scheduleEncashURL + '?removeScheduleItem&sessionId=' + sessionId + '&clearCache=' + (new Date()).getTime(), {
        method: 'get'
    }).then(onFullfilled, onRejected);

    function onFullfilled(response) {
        try {
            response.text().then(function (out) {
                if (out && out.match('2002'))
                    onRejected();
                else {
                    // alert('delete out:\n' + out);
                    out = JSON.parse(out);
                    if (out.status === 0) {
                        if (out.log) success(out);
                        else scheduleEncashSaveLog('scheduleEncashDelete > fetchTimeout > onFullfilled: Неожиданный результат. Статус 0, но лог отсутсвует. Билеты #' + session);
                    } else success(out);
                }
            });
        } catch (e) {
            scheduleEncashSaveLog('Ошибка [scheduleEncashDelete -> onFullfilled#1]: ' + e);
        }
    }

    function success(out) {
        // alert('scheduleEncashDelete #' + sessionId + ' succeeded');
        try {
            scheduleEncashItems.splice(scheduleEncashItems.findIndex(function (item) {
                // alert('itemses ' + typeof item.sessionId + ' sess ' + typeof sessionId);
                if (item.sessionId === sessionId) return 1;
            }), 1);
        } catch (e) {
            scheduleEncashSaveLog('Ошибка [scheduleEncashDelete -> success]: ' + e);
        }
        scheduleEncashSaveLog('Элемент #' + sessionId + ' успешно удалён из очереди в БД. Ответ сервера: ' + out.log);
    }

    function onRejected(e) {
        scheduleEncashSaveLog('Не удалось удалить элемент #' + sessionId + ' из очереди БД: ' + e);
    }
}

function scheduleEncashSaveLog(string) {
    SaveLog('[Очередь инкассации]' + string);
}