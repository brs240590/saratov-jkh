const errorMessages = Object.freeze({
  '': '',
});

const constants = Object.freeze({
  apiCashPayment: 1,
  apiCardPayment: 2,
});
