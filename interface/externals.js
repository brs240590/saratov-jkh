function getAdminToken() {
  return request(server.address + '/auth', {
    method: 'post',
    headers: server.bearerHeaders,
    data: JSON.stringify({
      login: server.admLogin,
      password: server.admPass
    })
  }).then(function (r) {
    if ('user_token' in r.data) return r.data.user_token;
    else return null;
  }, function (e) {
    logException(formatErrorResponse(e.response.data));
  }).catch(function (e) {
    logException(e);
    return null;
  });
}

function extSendSMSCode() {
  request(server.address + '/book_code/' + server.companyID, {
    method: 'post',
    headers: server.bearerHeaders,
    data: JSON.stringify({
      phone: stepsData.userInfo.phone,
      fullname: stepsData.userInfo.fio,
    })
  }).then(function (r) {
    logDebug('Ответ сервера по отправке смс: ' + JSON.stringify(r.data));
  }, function (e) {
    logException(formatErrorResponse(e.response.data));
  }).catch(function (e) {
    logException(e);
  });
}

function extGetUserToken() {
  return request(server.address + '/user/auth', {
    method: 'post',
    headers: server.bearerHeaders,

  }).then(function (r) {
    logDebug(JSON.stringify(r.data));
    if ('user_token' in r.data) return r.data.user_token;
    else return null;
  }, function (e) {
    logException(formatErrorResponse(e.response.data));
  }).catch(function (e) {
    logException(e);
    return null;
  });
}

function extGetReservationServices() {
  let params = {
    staff_id: stepsData.selectedMaster ? stepsData.selectedMaster.id : undefined,
    datetime: stepsData.selectedDate ? stepsData.selectedDate.iso : undefined,
    service_ids: stepsData.servicesId,
  };
  return request(server.address + '/book_services/' + server.companyID + '?' + $.param(params), {
    method: 'get',
    headers: server.adminHeaders
  }).then(function (r) {
    return r.data;
  }, function (e) {
    logException(formatErrorResponse(e.response.data));
  }).catch(function (e) {
    logException(e);
    return null;
  });
}

function extGetServices(categoryID) {
  let params = {
    staff_id: stepsData.selectedMaster ? stepsData.selectedMaster.id : undefined,
    category_id: categoryID,
    service_id: stepsData.servicesId,
  };
  return request(server.address + '/services/' + server.companyID + '?' + $.param(params), {
    method: 'get',
    headers: server.adminHeaders
  }).then(function (r) {
    return r.data;
  }, function (e) {
    logException(formatErrorResponse(e.response.data));
  }).catch(function (e) {
    logException(e);
    return null;
  });
}

/**
 * Получить список сотрудников
 * @param [getSessions=true] {Boolean|undefined}
 * @return {Promise<T>}
 */
function extGetReservationEmployees(getSessions) {
  getSessions = getSessions || true;
  let params = {
    staff_id: stepsData.selectedMaster ? stepsData.selectedMaster.id : undefined,
    datetime: stepsData.selectedDate ? stepsData.selectedDate.iso : undefined,
    service_ids: stepsData.servicesId,
    without_seances: !getSessions
  };
  // alert(JSON.stringify(params)+'\n' + server.address + '/book_staff/' + server.companyID + '?' + $.param(params))
  return request(server.address + '/book_staff/' + server.companyID + '?' + $.param(params), {
    method: 'get',
    headers: server.adminHeaders
  }).then(function (r) {
    return r.data;
  }, function (e) {
    logException(formatErrorResponse(e.response.data));
  }).catch(function (e) {
    logException(e);
    return null;
  });
}

function extGetReservationSessions() {
  let masterID = stepsData.selectedMaster ? stepsData.selectedMaster.id : 0;
  let params = {
    service_ids: stepsData.servicesId
  };
  return request(server.address + '/book_times/' + server.companyID + '/' + masterID + '/' + stepsData.selectedDate.iso + '?' + $.param(params), {
    method: 'get',
    headers: server.adminHeaders
  }).then(function (r) {
    // alert(JSON.stringify(r.data));
    return r.data;
  }, function (e) {
    logException(formatErrorResponse(e.response.data));
  }).catch(function (e) {
    logException(e);
    return null;
  });
}

function extCheckRecord() {
  let data = {
    appointments: [{
      id: 0,
      services: stepsData.servicesId,
      staff_id: stepsData.selectedMaster.id,
      datetime: stepsData.selectedSession.datetime
    }]
  };
  logDebug(JSON.stringify(data));
  return request(server.address + '/book_check/' + server.companyID, {
    method: 'post',
    headers: server.bearerHeaders,
    data: JSON.stringify(data)
  }).then(function () {
    return true; // сервер возвращает пустой массив, поэтому игнорируем его
  }, function (e) {
    logException(formatErrorResponse(e.response.data));
    //todo сделать детализацию ошибок сервера (согласно документации)
    return false;
  }).catch(function (e) {
    logException(e);
    return false;
  });
}

function extCreateRecord() {
  for (let key in stepsData.userInfo) {
    if (stepsData.userInfo[key] === undefined) {
      logException('В собранных данных отсуствует ключ ' + key);
      return Promise.reject('Ошибка создания записи');
    }
  }
  let params = {
    phone: stepsData.userInfo.phone,
    fullname: stepsData.userInfo.fio,
    email: 'noreply@example.com',
    code: stepsData.userInfo.code,
    comment: stepsData.userInfo.comment || '',
    type: 'terminal',
    notify_by_sms: 1,
    notify_by_email: 0,
    // api_id: '777',
    appointments: [{
      id: 1,
      services: stepsData.servicesId,
      staff_id: stepsData.selectedMaster.id,
      datetime: stepsData.selectedSession.datetime
    }]
  };
  logDebug('Параметры записи перед созданием записи: ' + JSON.stringify(params));
  logDebug('ПОЛНЫЕ параметры записи перед созданием записи: ' + JSON.stringify({
    method: 'post',
    headers: server.adminHeaders,
    data: JSON.stringify(params)
  }));
  return request(server.address + '/book_record/' + server.companyID, {
    method: 'post',
    headers: server.adminHeaders,
    data: JSON.stringify(params)
  }).then(function (r) {
    logDebug('Ответ сервера: ' + JSON.stringify(r.data));
    return r.data;
  }, function (e) {
    logException(formatErrorResponse(e.response.data));
    return Promise.reject('Ошибка создания записи');
  }).catch(function (e) {
    logException(e);
    return Promise.reject('Ошибка создания записи');
  });
}

function extGetRecord(debugRecordId) {
  let recordId = debugRecordId || stepsData.recordId;
  logDebug('extGetRecord-recordId: ' + recordId);
  return request(server.address + '/record/' + server.companyID + '/' + recordId, {
    method: 'get',
    headers: server.adminHeaders
  }).then(function (r) {
    logDebug(JSON.stringify(r.data));
    return r.data;
  }, function (e) {
    logException(formatErrorResponse(e.response.data));
  }).catch(function (e) {
    logException(e);
    return null;
  });
}

function extSendPayment(debugRecordId) {
  let record = stepsData.record;
  let client = record.client;
  client.phone = stepsData.userInfo.phone;
  client.name = stepsData.userInfo.fio;
  client.email = 'noreply@example.com';

  let params = {
    staff_id: record.staff_id,
    services: record.services,
    client: client,
    datetime: record.datetime,
    seance_length: record.seance_length,
    save_if_busy: true,
    send_sms: record.sms_now,
    fast_payment: constants.apiCardPayment,
    paid_full: 1,
    attendance: record.attendance === 0 ? 1 : record.attendance
  };
  logDebug('Параметры записи перед отправкой оплаты: ' + JSON.stringify(params));
  logDebug('ПОЛНЫЕ параметры записи перед отправкой оплаты: ' + JSON.stringify({
    method: 'put',
    headers: server.adminHeaders,
    data: JSON.stringify(params)
  }));
  return request(server.address + '/record/' + server.companyID + '/' + (debugRecordId || stepsData.recordId), {
    method: 'put',
    headers: server.adminHeaders,
    data: JSON.stringify(params)
  }).then(function (r) {
    logDebug(JSON.stringify(r.data));
    return r.data;
  }, function (e) {
    logException(formatErrorResponse(e.response.data));
  }).catch(function (e) {
    logException(e);
    return null;
  });
}


function extSaveLog(text) {
  let maxLength = 2000;
  let cut = 0;
  if (cut) cutAndSend(text);
  else if ('SaveErrorInLog' in window.external) window.external.SaveErrorInLog(text);
  else console.log(text);

  function cutAndSend(str) {
    // alert('str.length: ' + str.length + ' 500');
    if (str.length > maxLength) {
      if ('SaveErrorInLog' in window.external) {
        window.external.SaveErrorInLog(str.slice(0, maxLength - 1));
        cutAndSend(str.slice(maxLength, str.length - 1));
      }
    } else if ('SaveErrorInLog' in window.external) window.external.SaveErrorInLog(str);
  }
}

/**
 * Включение чтения баркодов
 */
function extEnableBarcode() {
  if ('EnableBarCode' in window.external) {
    logStep('Отправлена команда на включение чтения баркода');
    window.external.EnableBarCode();
  } else {
    logException('Вызвана команда на включение баркода (устройства нет)')
  }
}

/**
 * Отключение чтения баркодов
 */
function extDisableBarcode() {
  if ('DisableBarCode' in window.external) {
    logStep('Отправлена команда на отключение чтения баркода');
    window.external.DisableBarCode();
  } else {
    logException('Вызвана команда на отключение чтения баркода (устройства нет)')
  }
}

/**
 * Перенаправление на страницу
 * @param url
 */
function extLink(url) {
  logStep('Переход на: ' + url);
  if ('LINK' in window.external) window.external.LINK(url);
  else window.location.href = url;
}

/**
 * Отправка экстернала на сервер, чтобы приложение не перезагрузило "не мёртвую" страницу.
 */
function extPingInterface() {
  if ('PingInterface' in window.external) window.external.PingInterface();
}

/**
 * Запуск купюроприёмника
 */
function extStartBill() {
  if ('START_BILL' in window.external) window.external.START_BILL();
  logStep('Отправлен запрос на включение купюроприёмника');
}

/**
 * Остановка купюроприёмника. После первой команды нужно повторить её через 5 секунд
 */
function extStopBill() {
  logStep('Купюроприёмник остановлен');
  if ('STOP_BILL' in window.external) window.external.STOP_BILL();
}

/**
 * Получить информацию из локальной БД (sqlite)
 * @param key
 * @returns {String}
 */
function extGetKeyForInterface(key) {
  if ('GetKeyForInterface' in window.external) {
    key = String(key);
    let data = window.external.GetKeyForInterface(key);
    logStep('Получено значение по ключу ' + key + ': ' + data);
    return data;
  } else logException('window.external.GetKeyForInterface не существует (ключ ' + key + ')');
}

/**
 * Записать информацию в локальную БД (sqlite)
 * @param key
 * @param value
 */
function extSetKeyFromInterface(key, value) {
  value = String(value);
  logStep('Записываем в базу по ключу ' + key + ' значение ' + value);
  if ('SetKeyFromInterface' in window.external) window.external.SetKeyFromInterface(key, value);
  else logException('window.external.SetKeyFromInterface не существует');
}

let extGetPaymentInputDebugValue = null;

let extDebugPaymentInput = 0;

//рекомендованная частота опроса - 250мс
function extGetPaymentInput() {
  // if (debug) {
  //     return extDebugPaymentInput;
  // } else {
  let val;
  if ('RetACCT' in window.external) val = window.external.RetACCT();
  else logException('window.external.RetACCT не существует');
  if (extGetPaymentInputDebugValue !== val) {
    extGetPaymentInputDebugValue = val;
    logStep('Текущая введённая сумма: ' + val + ' руб.');
  }
  return val;
  // }
}

/**
 * -1 = no devices
 * 1 = cup
 * 2 = card only
 * 3 = coins
 * 4 = all
 * @returns {Promise<Number>}
 */
function extGetPaymentMethod() {
  return new Promise(function (resolve) {
    if (debug) resolve(4);
    else if ('GetTypeOfMoneyIn' in window.external) resolve(window.external.GetTypeOfMoneyIn());
    else logException('window.external.GetTypeOfMoneyIn не существует')
  });
}

function extStartPinpad() {
  if ('StartPinPad' in window.external) window.external.StartPinPad();
  logStep('Пинпад включён');
}

function extGetPinpadTimeout() {
  try {
    let val;
    if (debug) {
      val = 90 * 1000;
      logStep('Получено отладочное значение таймаута пинпада: ' + val + ' мс');
    } else {
      if ('GetPinPadTimeOut' in window.external) {
        val = window.external.GetPinPadTimeOut();
        logStep('Получено значение таймаута пинпада: ' + val + ' мс');
      } else {
        val = -1;
        logException('window.external.GetPinPadTimeOut не существует');
      }
    }
    return val;
  } catch (e) {
    logException('Ошибка в extGetPinpadTimeout: ' + e);
  }
}

function extSetPaymentSum(sum) {
  let val = Number(sum);
  logStep('setPaymentSum установила необходимую сумму для оплаты: ' + val);
  if ('SetFixedSumTxt' in window.external) window.external.SetFixedSumTxt(val);
  extAddPaymentParameters("Acct", sum);
  extAddPaymentParameters("FixedSum", sum);
}

function extGetPaymentSum() {
  if ('RetFixedSum' in window.external) return Number(window.external.RetFixedSum().replace(',', '.'));
  else logException('getPaymentSum: RetFixedSum отсутствует в window.external');
}

/**
 * Установка введённой суммы на этапе оплаты наличными
 * @param sum - целочисленное
 */
function extDebugSetInputSum(sum) {
  let val;
  val = Number(sum);
  // if (debug) {
  //     logStep('extDebugSetInputSum в режиме отладки добавила к текущей введённой сумме на этапе оплаты: ' + val + ' руб.');
  //     extDebugPaymentInput += val;
  // } else {
  if (debug) {
    logStep('extDebugSetInputSum добавила к текущей введённой сумме на этапе оплаты: ' + val + ' руб.');
    if ('SetXrub' in window.external) window.external.SetXrub(val);
  } else alert('Недостаточно прав для тестового введения купюр');
  // }
}

/**
 * Начинает новую транзакцию (существующий объект транзакции в приложении перезаписывается заново)
 * 0 - ?
 * @param gateID
 * @returns Возвращает 0 - если печать доступна и -1 если печать чека в обратном случае
 */
function extStartPayCycle(gateID) {
  let val;
  logStep('Самир, спешу сообщить, что мы дошли до StartPayCycle по гейту ' + gateID);
  if ('StartPayCycleJavaInt' in window.external) val = window.external.StartPayCycleJavaInt(0, gateID);
  logStep('Отправлен запрос на старт платёжного цикла с параметрами 0 и gateID \'' + gateID + '\'. Ответ: ' + val);
  return val;
}

function extSetStepValue(stepnum, value) {
  if ('SetStepVal' in window.external) window.external.SetStepVal(stepnum, value);
  logStep('Установлено значение шага №' + stepnum + ': ' + value);
}

function extRetStepValue(stepnum, value) {
  if ('RetStepVal' in window.external) return window.external.RetStepVal(stepnum, value);
}

function extGetPaymentReady() {
  if (debugConfig.useMockData) {
    logDebug('Имитируем готовность кассы');
    return Promise.resolve('i can start new payment');
  } else return new Promise(function (resolve) {
    let check = 0;
    let maxChecks = 60;
    logStep('Начинаем проверять готовность ККМ к проведению следующего платежа и печати');
    let polling = setInterval(function () {
      if (check++ >= maxChecks) {
        logWarning('Таймаут ожидания готовности ККМ');
        clearInterval(polling);
        resolve('timeout');
      }
      if (extGetKKMReady()) {
        logStep('ККМ готова');
        clearInterval(polling);
        resolve('i can start new payment');
      }
    }, 1000);
  });
}

function extGetPaymentParameters(key) {
  let value;
  if ('RetAdditionalPaymentParameters' in window.external) value = window.external.RetAdditionalPaymentParameters(key);
  logStep('Получен дополнительный параметр шага №' + key + ': ' + value);
  return value;
}

function extAddPaymentParameters(key, value) {
  if ('AddAdditionalPaymentParameters' in window.external) window.external.AddAdditionalPaymentParameters(key, value);
  logStep('Установлен дополнительный параметр шага №' + key + ': ' + value);
}

function extAddCheckParameter(key, value) {
  if ('AddCheckParameter' in window.external) window.external.AddCheckParameter(key, value);
  logStep('Установлена дополнительная строчка чека - ' + key + ': ' + value);
}

function extSendPaymentConfirm() {
  // alert('Вызван PaymentConfirm');
  logDebug('PaymentConfirm called');

  clearInterval(timers.paymentCash);
  serviceData.paymentSuccess = true;
  let requestExistsAndSent = extSendPayConfirmNew();
  if (!requestExistsAndSent) ShowPrintCheckPage();
}

function extSendPaymentCancel() {
  // alert('Вызван PaymentCancel');
  logDebug('PaymentCancel called');
  serviceData.paymentSuccess = false;
  clearInterval(timers.paymentCash);
  if ('PaymentCancel' in window.external) window.external.PaymentCancel();
  else {
    logWarning('window.external.PaymentCancel не существует');
    ShowPrintCheckPage();
  }
}

function extSendPayConfirmNew() {
  setGoodsList();
  drawLoading('Идёт проведение платежа\nНе забудьте взять чек', '', 'rgba(255,255,255,0.8)');
  stopPinpadTimer();
  if ('PayConfirmNew' in window.external) {
    window.external.PayConfirmNew();
    return true;
  } else {
    logWarning('window.external.PayConfirmNew не существует');
    return false;
  }
}

/**
 * @param cashbox Номер кассы (отсчёт с нуля)
 * @param sum Сумма
 * @param taxgroup Налоговая группа
 * @param amount Количество
 * @param department Отдел
 * @param operation Название позиции
 * @param paymentoption 1 - наличные, 2 - МИР, 3 - VISA, 4 - MasterCard
 */
function extExecuteDirectPayment(cashbox, sum, taxgroup, amount, department, operation, paymentoption) {
  logWarning(cashbox + ', ' + sum + ', ' + taxgroup + ', ' + 0 + ', ' + 0 + ', ' + 0 + ', ' + amount + ', ' + department + ', ' + operation + ', ' + paymentoption + ', ' + 0);
  if ('admmenukkmsale' in window.external) window.external.admmenukkmsale(cashbox, sum, taxgroup, 0, 0, 0, amount, department, operation, paymentoption, 0);
  else logWarning('admmenukkmsale отсуствует в window.external');
}

function extGenerateGUID() {
  if ('RetNewGuid' in window.external) return window.external.RetNewGuid();
}

function extGetCurrentSession() {
  if ('RetCurrentSession' in window.external) return window.external.RetCurrentSession();
}

function extGetDealerID() {
  if ('RetGetDealerID' in window.external) return window.external.RetGetDealerID();
}

function extGetCurrentEncashment() {
  if ('RetCollectionCheckNumber' in window.external) return window.external.RetCollectionCheckNumber();
}

function extGetTerminalNumber(type) {
  if ('RetTerminalNumberSystemMini' in window.external) {
    if (type === 'globalShort') return window.external.RetTerminalNumberSystemMini()
  }
}

function extGetKKMReady() {
  if ('CanIStartNewPayment' in window.external) return window.external.CanIStartNewPayment();
}

function extClearCache() {
  if ('ClearInetCash' in window.external) window.external.ClearInetCash();
}

function extPrintPDF() {
  if ('KDL_PrintPDF' in window.external) window.external.KDL_PrintPDF();
  else logException('window.external.KDL_PrintPDF не существует');
}

function extChangeInterface(skinName) {
  if ('ChangeInterface' in window.external) window.external.ChangeInterface(skinName);
  else logException('window.external.ChangeInterface не существует');
}

/**
 * Произвести возврат денег
 * @param sum сумма
 * @param nomenclature номенклатура
 * @param refNumber номер ссылки (0, если нал)
 * @param authCode код авторизации (0, если нал)
 * @param paymentType тип оплаты (1 - безнал, 0 - нал)
 * @param goodsList
 * @param isReturn нужно передавать 1
 */
function extReturnPayment(sum, nomenclature, refNumber, authCode, paymentType, goodsList, isReturn) {
  logDebug('extReturnPayment');
  paymentType = Boolean(paymentType);
  // alert(JSON.stringify(arguments).replace(/,/g, '\n'));

  if ('IsReturn' in window.external) window.external.IsReturn(); // выполняется в любом случае перед ReturnPaymentDo
  else logException('window.external.IsReturn не существует');
  logStep('window.external.ReturnPaymentDo(' + sum + ', ' + '\'' + nomenclature + '\'' + ', ' + refNumber + ', ' + authCode + ', ' + paymentType + ', \'' + goodsList + '\')', isReturn);
  if ('ReturnPaymentDo' in window.external) window.external.ReturnPaymentDo(sum, '\'' + nomenclature + '\'', refNumber, authCode, paymentType, '\'' + goodsList + '\'', isReturn);
  else logException('window.external.ReturnPaymentDo не существует');
}

function extADMRefund(cashbox, sum, taxgroup, amount, department, operation, paymentoption, json, accountingItem, accountingType) {
  // alert(Array.from(arguments).join('\n'));
  try {
    if (debugConfig.useMockData) logDebug('Имитируем возврат на стороне бекэнда');
    else if ('admmenukkmreturn' in window.external)
      window.external.admmenukkmreturn(cashbox, String(sum).replace('.', ','), taxgroup, 0, 0, 0, String(amount).replace('.', ','),
        department, operation, paymentoption, json, 1, accountingItem, accountingType);
    else logException('window.external.admmenukkmreturn не существует');
  } catch (e) {
    logException('Ошибка выполнения extADMRefund: ' + e);
  }
}

/**
 * Печать произвольного текста
 * @param str <String>
 */
function extPrintCustomCheck(str) {
  if (typeof str === 'string' && str.length) {
    logStep('Отправлена команда на печать чека со следующим содержимым:\n' + str);
    if ('PrintFromInterface' in window.external) window.external.PrintFromInterface(str);
    else logException('window.external.PrintFromInterface не существует');
  } else logWarning('На печать передана не строка или пустая строка');
}

/**
 * Получить самый главный GroupID, в котором находится интерфейс
 */
function extGetTopLevelGroupID() {
  if (topLevelGroupID) return topLevelGroupID;
  else if ('RetTopLevelGroupID' in window.external) return window.external.RetTopLevelGroupID();
  else logException('window.external.RetTopLevelGroupID не существует');
}

/**
 * Получить гейты группы
 */
function extGetGroupGates(GroupID) {
  if ('RetGatesForGroup' in window.external) return window.external.RetGatesForGroup(GroupID);
  else logException('window.external.RetGatesForGroup не существует');
}

/**
 * Получить подгруппы группы
 */
function extGetGroupSubgroups(GroupID) {
  if ('RetSubGroups' in window.external) return window.external.RetSubGroups(GroupID);
  else logException('window.external.RetSubGroups не существует');
}

/**
 * Закрыть приложение терминала
 */
function extCloseTerminal() {
  if ('Exit' in window.external) window.external.Exit();
  else logException('window.external.Exit не существует');
}

/**
 * Узнать сколько денег в кассе
 * @param cashbox - номер кассы
 * @return {Number}
 */
function extGetKKMCash(cashbox) {
  if ('GetKKMCashRegister' in window.external) return window.external.GetKKMCashRegister(cashbox);
  else logException('window.external.GetKKMCashRegister не существует');
}

/**
 * Внести деньги в кассу
 * @param cashbox - номер кассы
 * @param amount - сумма для внесения
 */
function extAddKKMCash(cashbox, amount) {
  if ('KKMCashIncome' in window.external) window.external.KKMCashIncome(cashbox, amount);
  else logException('window.external.GetKKMCashRegister не существует');
}

/**
 * Внесение недостающей суммы в кассу
 * @param sumToAdd сумма для внесения
 * @param cashbox касса, по умолчанию 0 (первая)
 * @return {Promise<>}
 */
function extAddMissingMoneyToKKM(sumToAdd, cashbox) {
  logDebug('extAddMissingMoneyToKKM');

  cashbox = cashbox || 0;
  sumToAdd = Number(sumToAdd);

  logStep('Проверяем наличие ' + sumToAdd + ' руб. в кассе #' + cashbox);

  if (debugConfig.useMockData) {
    logDebug('Имитируем внесение в кассу');
    return Promise.resolve();
  }

  let get, put;
  if (!'GetKKMCashRegister' in window.external) {
    logException('window.external.GetKKMCashRegister не существует');
    return Promise.reject();
  }
  if (!'KKMCashIncome' in window.external) {
    logException('window.external.KKMCashIncome не существует');
    return Promise.reject();
  }

  return new Promise(function (resolve) {
    let sum = extGetKKMCash(cashbox);
    logStep('В кассе сейчас: ' + sum);
    setTimeout(function () {
      extGetPaymentReady().then(function () {
        resolve(sum);
      });
    }, 200);
  }).then(function (cashboxSum) {
    if (Number(cashboxSum) < sumToAdd) {
      logStep('В кассе нехватает денег');
      return new Promise(function (resolve) {
        logStep('Вносим указанную сумму в кассу');
        extAddKKMCash(cashbox, sumToAdd);
        setTimeout(function () {
          extGetPaymentReady().then(function () {
            resolve();
          });
        }, 200);
      });
    } else {
      logStep('В кассе достаточно денег');
      return Promise.resolve();
    }
  });
}

/**
 * Получить json по выбранному гейту
 * @param id ID гейта
 * @returns {string}
 */
function extGetGateJSON(id) {
  if ('RetGateJSON' in window.external) return window.external.RetGateJSON(id);
  else logException('window.external.RetGateJSON не существует');
}

/**
 * Получить номер терминала в платёжной системе
 */
function extGetPSTerminalNumber() {
  logStep('Получаем номер терминала в платёжной системе');
  if ('RetTerminalNumberSystemMini' in window.external) return window.external.RetTerminalNumberSystemMini();
  else logException('window.external.RetTerminalNumberSystemMini не существует');
}

/**
 * Получить номер терминала у дилера
 */
function extGetDealerTerminalNumber() {
  logStep('Получаем номер терминала у дилера');
  if ('RetTerminalNumber' in window.external) return window.external.RetTerminalNumber();
  else logException('window.external.RetTerminalNumber не существует');
}

/**
 * Возвращает пользователей текущего дилера
 * @return {String|undefined}
 */
function extGetUsers() {
  if ('GetUsersJson' in window.external) return window.external.GetUsersJson();
  else logException('window.external.GetUsersJson не существует')
}


/**
 * Отправляет в личный кабинет для статистики информацию во время считывания баркодов
 * @param action
 * @param description
 */
function extSendUserEvent(action, description) {
  if ('UserActionAdd' in window.external) {
    window.external.UserActionAdd(action, description);
    logStep('Отправлено событие \'' + action + '\' с описанием: \'' + description + '\'');
  } else logException('window.external.GetUsersJson не существует')
}