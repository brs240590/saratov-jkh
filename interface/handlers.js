/* Функции вызываемые приложением Terminal */

/**
 * Переход на главную страницу
 */
function Main() {
  SetCurrentPage(Pages.Link.MAIN);
  StopTimeOutMain();
  clearTimeOutReturn();
}

/**
 * Переопределяем функцию
 * Определяет условия по которым терминал считается свободен и может выполнять удаленные команды
 * @return {boolean}
 */

function IsMain() {
  if (serviceData)
    if ('mainPage' in serviceData) return serviceData.mainPage;
    else logWarning('Отсутствуют переменные для определения на главной ли интерфейс');
  else return true;
}

/**
 * Переопределяем функцию
 * Отправка на бэк статуса на главной ли интерфейс
 * @param {Boolean} status true - на главной, false - на другой странице
 */
function CurrentPageMain(status) {
  if ('CurrentPageMain' in window.external) window.external.CurrentPageMain(status || IsMain());
  else logException('window.external.CurrentPageMain не существует');
}

/**
 * Переопределяем функцию для отключения срабатывания стандартного таймаута
 */
function StartTimeOut() {
  //
}

/**
 * Переопределяем функцию обновления количества введённых денег
 * @param string
 */
function moneyinchange(string) {
  // alert('moneyinchange '+string);
}

/**
 * Переопределяем функцию обновления количества денег для зачисления на счёт
 * @param string
 */
function moneyacctchange(string) {
  // alert('moneyacctchange '+string);
}

/**
 * Переопределяем функцию обновления процентов комиссии
 * @param string
 */
function moneypercentchange(string) {
  // alert('moneypercentchange '+string);
}

/**
 * Переопределяем функцию обновления количества денег для сдачи
 * @param string
 */
function moneychange(string) {
  // alert('moneychange '+string);
}

/**
 * Переопределяем функцию обновления оставшегося количества денег для ввода
 * @param string
 */
function moneyneeded(string) {
  // alert('moneyneeded '+string);
}

/**
 * Переопределяем функцию отрисовки модального окна
 * @param id ... элемента, который отвечает за модалку(НЕ НУЖНО)
 * @param action 1 - показать, 0 - скрыть
 * @param msgtext текст
 * @param timer *не тестировалось*
 */
function OnlineDiv(id, action, msgtext, timer) {
  if (msgtext) msgtext = msgtext.replace(/<br>/g, '').replace(/<BR>/g, '').replace(/<.+>/g, '');
  if (msgtext && msgtext.search('Please, wait') !== -1) msgtext = undefined;
  // if (msgtext && msgtext.search('получится больше') !== -1) {
  //     logStep('Скорее всего установлен неверный гейт!');
  //     return;
  // }
  logStep('Вызван OnlineDiv с параметрами: id ' + id + ', action ' + action + ', msgtext ' + msgtext + ', timer ' + timer);
  if (action) {
    stopTimeoutTimer();
    if (msgtext) drawLoading(msgtext, '', 'rgba(255,255,255,0.8)');
  } else {
    setTimeoutTimer(timeout);
    hideLoading();
  }
}

/**
 * Переопределяем функцию для вызова страницы ошибки проведения операции
 */
function CardFail() {
  extCardFail();
}

/**
 * Переопределяем функцию для вызова страницы печати чека по наличной оплате
 */
function ShowPrintCheckPage() {
  if (!ShowPrintCheckPage.disabled) { // защита от двойного вызова
    ShowPrintCheckPage.disabled = 1;
    setTimeout(function () {
      ShowPrintCheckPage.disabled = 0;
    }, 2000);

    extGetPaymentReady().catch(function (e) {
      logException('Ошибка в ShowPrintCheckPage > extGetPaymentReady: ' + e);
    }).finally(function () {
      drawFinish('check');
    })
  }
}

/**
 * Переопределяем функцию для вызова страницы печати чека после успешной операции по безналичной оплате
 */
function PayConfirmNew() {
  if (!PayConfirmNew.disabled) { // защита от двойного вызова
    PayConfirmNew.disabled = 1;
    setTimeout(function () {
      PayConfirmNew.disabled = 0;
    }, 2000);
    serviceData.paymentSuccess = true;
    extSendPayConfirmNew();
  }
}

//вызова страницы ошибки проведения операции
function extCardFail() {
  logStep('Приложение вызвало страницу ошибки оплаты картой');

  drawQuestion('pinpadRepeat');
  serviceData.paymentSuccess = false;
  clearInterval(timers.paymentCard);
}

function extDrawCheck() {
  logStep('Приложение вызвало обработчик перехода на страницу печати чека');
  drawFinish('check');
}

function extReceivePayConfirm() {
  logStep('Приложение вызвало обработчик успешной оплаты');
  drawFinish('check');
}

function extHandleBarCode(barcode) {
  logStep('Приложение вызвало обработчик баркодов с параметром: ' + barcode);
  try {
    extDisableBarcode();
    handleCodeInput(barcode);
  } catch (e) {
    logException('Ошибка при выполнении extHandleBarCode. Ошибка: ' + e);
  }
}

/**
 * Функция, вызываемая после успешного возврата денег
 * @param actualSum сумма сколько вышло по факту
 * @param requiredSum сумма сколько надо было выдать
 * @param method True - безнал, False - нал
 * @constructor
 */
function ReturnSuccess(actualSum, requiredSum, method) {
  logStep('Возврат денег прошёл успешно. Требуемая сумма: ' + requiredSum + ' Фактическая сумма: ' + actualSum + ' Тип: ' + (method ? 'безнал' : 'нал'));
  stepsData.refundSum = Number(actualSum);
  logDebug('stepsData.refundSum = ' + stepsData.refundSum);
  serviceData.refundDone = 1;
}

/**
 * Функция, вызываемая после неуспешного возврата денег
 * @param actualSum сумма сколько вышло по факту
 * @param requiredSum сумма сколько надо было выдать
 * @param method True - безнал, False - нал
 * @constructor
 */
function ReturnFail(actualSum, requiredSum, method) {
  logStep('Неуспех при возврате денег. Требуемая сумма: ' + requiredSum + ' Фактическая сумма: ' + actualSum + ' Тип: ' + (method ? 'безнал' : 'нал'));
  stepsData.refundSum = Number(actualSum);
  logDebug('stepsData.refundSum = ' + stepsData.refundSum);
  serviceData.refundDone = 2;
}

function handleSendPayment(array) {
  let schedule = schedules.sendPayment;
  schedule.log('Начинаем формировать запросы из очереди (в кол-ве ' + array.length + ') на отправку платежа');

  return new Promise(function (resolve, reject) {
    let requests = [];
    array.forEach(function (scheduleObj) {
      let obj = JSON.parse(scheduleObj.json);

      requests.push(new Promise(function (resolve) {
        let url = obj.url;
        let options = obj.options;
        schedule.log('Отправляем запрос на отправку платежа (ID: ' + scheduleObj.id + ') c параметрами (' + (options.body.split('&').join(', ')) + ') по адресу ' + url);
        fetchTimeout(url, options).then(function (response) {
          if (response.ok) {
            response.json().then(function (responseObj) {
              if (responseObj.status) resolve(scheduleObj.id);
              else onRejected('ошибка из PHP/API: ' + JSON.stringify(responseObj.error));
            }).catch(onRejected);
          } else onRejected('код ответа ' + response.status);
        }).catch(onRejected);

        function onRejected(msg) {
          schedule.log('Не удалось отправить запрос на отправку платежа (ID: ' + scheduleObj.id + '): ' + JSON.stringify(msg));
          resolve('fail');
        }
      }));
    });

    Promise.all(requests).then(function (resultsArray) {
      schedule.logDebug('результаты запросов: ' + JSON.stringify(resultsArray));
      resolve(resultsArray.filter(function (item) {
        return item !== 'fail';
      }));
    });
  });
}
