function main_keypress(e) {
  let exceptionCode = window.event.keyCode;
  let keyCode;
  if (debugConfig.hotkeys) keyCode = window.event.keyCode;

  if (lockInput) {
    e.preventDefault();
    return;
  }

  if (keyCode === 48) {
  } //0
  if (keyCode === 49) {
    drawPersonalPage("180039017", "drawQRCodePage();");
  } //1
  if (keyCode === 50) {
  } //2
  if (keyCode === 51) {
    init('wtf');
  } //3
  if (keyCode === 52) {
    drawQuestion('pinpadRepeat');
  } //4
  if (keyCode === 53) {
    drawFinish();
  } //5
  if (keyCode === 54) {
    alert($('.content').html().replace(/>/g,'>\n').replace(/<\/div>\n/g, ''));
  } //6
  if (keyCode === 55) {
    alert(JSON.stringify(stepsData.cart.list))
  } //7
  if (keyCode === 56) {
    logDebug($('.datepicker').html());
  } //8
  if (keyCode === 57) {
    alert(JSON.stringify())
  } //9
  if (keyCode === 66) {
  } //B
  if (keyCode === 68) {
  } //D
  if (keyCode === 69) {
    extGetRecord(152330364)
  } //E
  if (keyCode === 70) {
    serviceData.paymentSuccess = true;
    drawFinish();
  } //F
  if (keyCode === 71) {
  } //G
  if (keyCode === 72) {
    extDebugSetInputSum(250);
  } //H
  if (keyCode === 73) {
  } //I
  if (keyCode === 74) {
    extDebugSetInputSum(500);
  } //J
  if (keyCode === 75) {
    extDebugSetInputSum(5000);
  } //K
  if (keyCode === 76) {
  } //L
  if (keyCode === 77) {
  } //M
  if (keyCode === 78) {
  } //N
  if (keyCode === 79) {
    handleCodeInput(prompt('',''));
  } //O
  if (keyCode === 80) {
    logDebug(extGetUsers());
  } //P
  if (keyCode === 81) {
    prompt('1111');
  } //Q
  if (keyCode === 82) {
  } //R
  if (keyCode === 83) {
    let url = 'http://192.168.1.4/sandbox/rzd/luggage/employee/openShift.php';
    let body = {
      host: 'localhost'
    };
    let options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      body: $.param(body)
    };
    fetchTimeout(url, options).then(function(response) {
      response.text().then(function(r){
        alert(r);
      })
    })
  } //S
  if (keyCode === 84) {
  } //T
  if (keyCode === 85) {
  } //U
  if (keyCode === 89) {
  } //Y
  if (keyCode === 97) {
    extChangeInterface('airport');
    extLink('main.html');
  } //Num-1
  if (keyCode === 98) {
    extChangeInterface('izumi');
    extLink('main.html');
  } //Num-2
  if (keyCode === 99) {
    extChangeInterface('Aquapark');
    extLink('main.html');
  } //Num-3
  if (keyCode === 100) {
    extChangeInterface('moyka');
    extLink('main.html');
  } //Num-4
  if (keyCode === 101) {
    extChangeInterface('');
    extLink('main.html');
  } //Num-5
  if (keyCode === 102) {
    extChangeInterface('');
    extLink('main.html');
  } //Num-6
  if (keyCode === 103) {
    extChangeInterface('');
    extLink('main.html');
  } //Num-7
  if (keyCode === 104) {
    extChangeInterface('');
    extLink('main.html');
  } //Num-8
  if (keyCode === 105) {
    extChangeInterface('');
    extLink('main.html');
  } //Num-9
  if (keyCode === 111) {
    extChangeInterface('Ugor');
    extLink('main.html');
  } //Num-/
  if (keyCode === 110) {
    extLink('adm.html');
  } //Num-.
  if (exceptionCode === 109) {
    // debugConfig.hotkeys = false;
  } //Num- -
  if (exceptionCode === 107) {
    // debugConfig.hotkeys = true;
  } //Num- +
  if (keyCode === 90 || keyCode === 96) {
    extLink('Main.html');
  } //Z || Num-0
  if (keyCode === 17) {
    // extCloseTerminal();
  } //Ctrl

  function logResponse(response) {
    response.text().then(logDebug)
  }
}

document.onkeydown = main_keypress;
