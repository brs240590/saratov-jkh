let debug = 0;
let debugConfig = {
  data: 0,
  hotkeys: 0,
  showBorders: 0,
  layout: 0,
  testTerminal: debug,
  setSteps: 0 && debug,
  alert: 1 && debug,
  useMockData: 0 && debug,
  devTerminal: 1 && debug,
  setIntegration: 1 && debug,
};

let scheduleEnabled = 0;
let paymentAutofinish = 0;
let paymentPartial = 0;
let lockInput = 0;

let log = {
  on: 1,
  steps: 1,
  warnings: 1,
  exceptions: 1,
  debug: 1,
};

let timeout = debug ? 86400 : 59;
let gateID = debug ? 1718 : 1924;
let topLevelGroupID = debug && 69;
let URL = 'http://127.0.0.1:32500/';
let WS = 'ws://127.0.0.1:65000/';
let socket = new WebSocket(WS);

let UI;
let timers;
let schedules = {
  on: Boolean(scheduleEnabled)
};

let stepsData;
let serviceData;

let months = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'];

function stepsExecutor() {
  let delay = 10;

  drawPayment('card');
  UI.content.attr('onclick', '');
  setTimeout(function () {
    setTimeout(function () {
      setTimeout(function () {
        setTimeout(function () {
          setTimeout(function () {
            setTimeout(function () {
              setTimeout(function () {}, delay);
            }, delay);
          }, delay);
        }, delay);
      }, delay);
    }, delay);
  }, delay);
}

setInterval(function () {
  extPingInterface();
  CurrentPageMain();
}, 30 * 1000);

$('*').on('click', function (e) {
  // ---debug layout---
  let el = e.target;
  let jel = $(el);
  if (debugConfig.layout) {
    $('#debugmessage').show().html(
      'tag: ' + el.tagName + '\n' +
      'class: ' + el.className + '\n' +
      'font-size: ' + jel.css('font-size') + '\n' +
      'font-family: ' + jel.css('font-family') + '\n' +
      'width: ' + jel.css('width') + '\n' +
      (el.id ? 'id: ' + el.id + '\n' : '') +
      'onclick: ' + el.onclick + '\n' +
      // 'z-index: ' + jel.css('z-index') + '\n' +
      // 'border: ' + jel.css('border') + '\n' +
      // (serviceData.dCurrentGroups ? 'currentGroups: ' + beautifyArray(JSON.stringify(serviceData.dCurrentGroups)) + '\n' : '') +
      // (serviceData.dCurrentServices ? 'currentServices: ' + beautifyArray(JSON.stringify(serviceData.dCurrentServices)) + '\n' : '') +
      // 'previousGroupPageArray: ' + beautifyArray(JSON.stringify(serviceData.previousGroupPageArray) +
      ''
    ).draggable();
  }

  if (debugConfig.showBorders) {
    $('*').css('border', '1px dotted red');
  }

  function beautifyArray(string) {
    return string.replace(/},/g, '}\n').replace('[{', '[\n{').replace(/[\[\]{}]/g, '');
  }
});

function getFunctionName(args) {
  let fn = args.callee.toString();
  fn = fn.substr('function '.length);
  let fnName = fn.substr(0, fn.indexOf('('));

  return fnName + "();";
}

function init(errorText) {
  logDebug('------------------------------------------------------------------------------------------------');
  logDebug('----------------------------------------------INIT----------------------------------------------');
  logDebug('------------------------------------------------------------------------------------------------');

  extStopBill();
  removeKeyboard();
  socket.close(1000, "Закрытие соединение на init()");

  if (timers && 'clockUpdater' in timers) {
    clearInterval(timers.clockUpdater);
  }

  let wrapper = $('.wrapper');
  let body = $('body');

  if (!wrapper.length) {
    let debugUI = $('<div id="debugmessage" class="debugMessage">debug</div>');
    wrapper = $('<div class="wrapper"></div>');
    body.append(wrapper, debugUI);
    body.append($('<div id="keyboardAnchor">'));

    // debugUI.show().css('color', 'red').html('НЕ финальная версия\nПредставлен только функционал');

    // поддержка старого интерфейса, не убирать
    $('#group5').hide();
    $('#wowtext').hide();
    if (typeof window.SetCurrentPage === 'function') SetCurrentPage(Pages.Link.MAIN);
    // поддержка старого интерфейса, не убирать

    $(document).bind('click', function () {
      if (!debug && serviceData && !serviceData.mainPage) {
        setTimeoutTimer(timeout);
      }
      // if (timers && 'finish' in timers) clearTimeout(timers.finish); // не должно быть включено, когда на странице чека нет кнопки выхода
    });

    // if (debugConfig.testTerminal) debugUI.show().css('color', 'red').html('Отладочный режим');

    if (schedules.on && !debugConfig.testTerminal) {
      /*schedules.sendPayment = new Schedule({
        urlDB: 'localhost',
        username: 'hilovoterminal',
        password: 'tmg4StqnqFFilGNt',
        database: 'hilovo',
        table: 'send_payment',
        urlBackend: 'http://localhost/sandbox/hilovo/schedule.php',
        label: 'Оплата',
        delay: 2 * 60 * 1000,
        log: extSaveLog,
        autoStart: true,
      }, handleSendPayment);*/
    }

    let startClock = false;
    if (startClock) setInterval(function () {
      let date = getDateObj();
      $('.clock').html(date.day + '.' + date.month + '.' + date.year + ' ' + date.hours + ':' + date.minutes + ':' + date.seconds);
    }, 1000);

    /*console.error = logException;
    console.log = logStep;
    console.info = logDebug;*/

    logStep('Первичная подготовка к установке интерфейса завершена');
  }

  if (timers && 'paymentCard' in timers) clearInterval(timers.paymentCard);
  if (timers && 'finish' in timers) clearTimeout(timers.finish);
  if (timers && 'sendsms' in timers) clearTimeout(timers.sendsms);

  stopTimeoutTimer();

  stepsData = {
    price: undefined,
    totalSum: 0,
    inputSum: 0,
    selectedPayment: undefined,
    TaxGroup: 1,
    PaymentTypeSign: 4,
    PaymentItemSign: 1,
    userToken: undefined,
    categories: undefined,
    services: undefined,
    employees: undefined,
    sessions: {},
    cart: new Cart(),
    selectedDate: undefined,
    selectedSession: undefined,
    selectedMaster: undefined,
    userInfo: {
      fio: undefined,
      phone: undefined,
      comment: '',
      code: undefined,
      remind: undefined,
    },
    get servicesId() {
      return stepsData.cart.keys()
    },
    recordId: undefined,
    record: undefined,
  };
  serviceData = {
    pinpad: undefined,
    mainPage: true,
    isPayment: false,
    cashTimeout: undefined,
    currentPageType: 'idle',
    paymentSuccess: false,
    timeoutTriggered: false,
    allowSendSms: true,
    custom: {
      paymentType: null,
      navCurrFn: null,
      keyboardInput: null,
      currentContractor: null,
      authCode: null,
      cardNumber: null,
      cardTransactionNumber: null,
      KTT: extGetKeyForInterface('IntegrationValF'),
      fileObj: null,
      waterMeterData: {},
      from: null,
      user: {
        accountId: null,
        personalAcc: null,
        clientSumma: 0,
        terror: 0
      }
    }
  };
  timers = {
    pinpad: undefined,
    away: undefined,
    awayTime: undefined,
    paymentCash: undefined,
    paymentCard: undefined,
    finish: undefined,
    checkPaymentStatus: undefined,
    keyboard: undefined,
    sendsms: undefined,
    barcode: undefined
  };

  /*
  getAdminToken().then(function (token) {
    if (token) server.admToken = token;
    else logException('Ошибка авторизации администратора');
  });
  */

  let loadingWrapper = $('<div class="loadingWrapper"></div>');
  let loadingUI = $('<div class="loading"></div><div id="loading" class="button">Отмена</div>');
  let loadingBarUI = $('<div class="loadingBar">' +
    '<img src="interface/img/loading-spinner.gif" alt="Loading">' +
    '</div>');
  let timeoutWrapper = $('<div class="timeoutWrapper"></div>');
  let timeoutUI = $('<div class="timeout"><span class="stillHereQuestion">Вы ещё здесь?</span><span class="regular">До перехода на главную страницу <span id="timeoutTime"></span></span>' +
    '<br><div class="timeoutButton">Я здесь</div></div>');
  let clockUI = $('<div class="clock">');
  let headerUI = $('<div class="header">');
  let contentUI = $('<div class="contentWrapper"></div>');
  let footerUI = $('<div class="footer">');
  let buttonBack = $('<div id="back" class="button">Вернуться назад</div>');
  let buttonMainMenu = $('<div id="mainMenu" class="button">Меню</div>');
  let buttonNext = $('<div id="next" class="button">Далеe</div>');

  footerUI.append(clockUI, buttonBack, buttonMainMenu, buttonNext);
  loadingWrapper.append(loadingBarUI, loadingUI);
  timeoutWrapper.append(timeoutUI);
  wrapper.html('').append(headerUI, contentUI, footerUI, loadingWrapper, timeoutWrapper);

  UI = {
    body: body,
    header: headerUI,
    content: contentUI,
    back: buttonBack,
    mainMenu: buttonMainMenu,
    next: buttonNext,
    skud: $('#listenskud'),
    stubs: {
      group: $('<div class="group">')
    },
  };

  UI.content.append(
    $("<h1/>", {
      class: "contentWrapper__title",
      text: "Выберите способ оплаты"
    }),
    $("<div/>", {
      class: "contentWrapper__mainBtns",
    }).append(
      $("<div/>", {
        class: "contentWrapper__mainBtn",
        text: "С помощью QR-кода",
        click: drawQRCodePage
        // click: drawPaymentPick
      }),
      $("<div/>", {
        class: "contentWrapper__mainBtn",
        text: "Выбор контрагента",
        click: drawPickContractorPage
      })
    )
  );

  const stopPropagation = function (e) {
    e.stopPropagation()
  };

  loadingWrapper.on('click', stopPropagation);
  timeoutWrapper.on('click', stopPropagation);

  UI.next.hide();
  UI.back.hide();
  UI.mainMenu.hide();
  UI.skud.show();

  UI.content.attr('onclick', '');
  loadingWrapper.find('.button').attr('onclick', 'extDisableBarcode();hideLoading();');

  if (errorText) {
    let errorUI = $('.error');
    errorUI.html(errorText).css('visibility', 'visible');

    setTimeout(function () {
      if (errorUI.html() === errorText) errorUI.css('visibility', 'hidden');
    }, 8 * 1000);
  }

  if (debugConfig.data) {
    let debuginfo = $('<div class="debugInfo"></div>');
    body.append(debuginfo);
  }

  hideLoading();
  extEnableBarcode();

  UI.next.on("click", function () {
    removeKeyboard();
  });

  UI.back.on("click", function () {
    removeKeyboard();
  });

  logStep('Инициализация главной страницы завершена');
  if (debugConfig.setSteps) {
    logStep('Выполняем тестовые шаги');
    stepsExecutor();
  }
}

function drawQRCodePage() {
  serviceData.mainPage = false;
  setTimeoutTimer(timeout);
  logStep('Переход на страницу сканирования QR-кода');

  UI.content.html('').attr('onclick', '');
  UI.skud.hide();
  UI.back.show().attr('onclick', 'init()');

  try {
    drawLoading('Идёт загрузка...', '', 'rgba(255, 255, 255, 0.8)');
    fetch(URL + "drawQRCodePage", {
      'Content-Type': 'text/html',
      'Pragma': 'no-cache',
      'Cache-Control': 'no-cache, no-store, must-revalidate'
    }).then(function (res) {
      return res.text();
    }).then(function (html) {
      UI.content.append(html);
    }).catch(function (err) {
      drawError(err, "drawQRCodePage", "Ошибка запроса");
    }).finally(function () {
      hideLoading();
    });
  } catch (err) {
    drawError(err, "drawQRCodePage", "Ошибка интерфейса");
  }

  socket = new WebSocket(WS);

  socket.onmessage = function (event) {
    socket.close(1000, "Баркод получен");
    logStep("Получен баркод от [drawQRCodePage]: " + event.data);

    try {
      rePersonalAcc = /PersonalAcc=([0-9]*)/gmi;
      serviceData.custom.user.personalAcc = rePersonalAcc.exec(event.data)[1];

      reSumma = /Sum=([0-9]+\.?[0-9]*)/gmi;
      serviceData.custom.user.clientSumma = parseFloat(reSumma.exec(event.data)[1]);

      fetch(URL + "getPersonalAccount?personalAcc=" + serviceData.custom.user.personalAcc).then(function (res) {
        return res.json();
      }).then(function (json) {
        if (!json.length) {
          drawError(json, "drawQRCodePage", "Контрагент не обнаружен");
        } else {
          serviceData.custom.currentContractor = Array.isArray(json) ? json[0] : json;
          drawInputPersonalData();
        }
      }).catch(function (err) {
        drawError(err, "drawQRCodePage", "Ошибка запроса");
      });
    } catch (err) {
      drawError(err, "drawQRCodePage", "Неверный QR-код");
    }
  }

  serviceData.custom.navCurrFn = getFunctionName(arguments);
}

function drawPickContractorPage() {
  logStep('Переход на страницу выбора контрагента');
  socket.close(1000, "Закрытие соединение на drawPickContractorPage()");
  let args = arguments;

  serviceData.mainPage = false;
  setTimeoutTimer(timeout);

  UI.skud.hide();
  UI.next.hide();

  UI.content.html('').attr('onclick', '');
  UI.back.show().attr('onclick', "init()");
  UI.next.attr('onclick', "drawPickAuthBtn()");

  try {
    let title, table, tbody, row, checkbox, name, nav, btnPrev, btnNext, currPage, currPageVal, pages, page, pagesInRow, dataLens;

    title = $("<h1>", {
      class: "contentWrapper__title",
      text: "Выберите вашего контрагента"
    });

    table = $("<form class='contentWrapper__table'></div>");
    tbody = $("<div class='contentWrapper__table-tbody'></div>");

    drawLoading('Идёт загрузка...', '', 'rgba(255, 255, 255, 0.8)');
    fetch(URL + "getContractorList", {
      headers: {
        'Content-Type': 'application/json',
        'Pragma': 'no-cache',
        'Cache-Control': 'no-cache, no-store, must-revalidate'
      }
    }).then(function (res) {
      return res.json();
    }).then(function (data) {
      if (data.error) {
        drawError(JSON.stringify(data.error), "drawPickContractorPage", "Ошибка запроса");

        return false;
      }

      pages = [];
      page = [];
      pagesInRow = 5;
      dataLens = data.length - 1;
      currPageVal = typeof args[0] === 'number' ? args[0] : 0;

      if (!dataLens) {
        return false;
      }

      for (let i = 1, k = 0; k <= dataLens; i++, k++) {
        if (i % pagesInRow === 0) {
          page.push(data[k]);
          pages.push(page);
          page = [];
        } else {
          page.push(data[k]);
          if (k === dataLens) {
            pages.push(page);
            page = [];
          }
        }
      }

      UI.mainMenu.show().text("Поиск").on('click', function () {
        return drawSearchPopup(data, pages);
      });

      nav = $("<div class='contentWrapper__nav'></div>");
      btnPrev = $("<div/>", {
        class: 'contentWrapper__nav-btn-prev',
        text: '',
        click: function () {
          if (currPageVal !== 0) {
            currPageVal--;
            currPage.text(currPageVal + 1);
            paginator(currPageVal, pages);
          }

          if (currPageVal <= 0) {
            $(this).css("visibility", "hidden");
          }

          if (btnNext.css("visibility") === "hidden") {
            btnNext.css("visibility", "visible");
          }
        }
      });
      btnNext = $("<div/>", {
        class: 'contentWrapper__nav-btn-next',
        text: '',
        click: function () {
          if (btnPrev.css("visibility") === "hidden") {
            btnPrev.css("visibility", "visible");
          }

          currPageVal++;
          currPage.text(currPageVal + 1);
          paginator(currPageVal, pages);

          if ((pages.length - 1) === currPageVal) {
            $(this).css("visibility", "hidden");
          }
        }
      });
      currPage = $("<div class='contentWrapper__nav-curr-page'>" + (currPageVal + 1) + "</div>");

      function paginator(page, arrNavPages) {
        $(".contentWrapper__table-tbody-row").hide();

        if (!arrNavPages.length) {
          return false;
        }

        arrNavPages[page].forEach(function (el, id) {
          let item = $(".contentWrapper__table-tbody-row[data-page='elem_" + page + "_" + id + "']");

          if (item.is(":hidden")) {
            item.show();
          } else {
            row = $("<div/>", {
              class: 'contentWrapper__table-tbody-row',
              "data-page": "elem_" + page + "_" + id
            });

            if (id !== 0 && id % pagesInRow === 0) {
              $(row).addClass("hide-border");
            }

            checkbox = $("<label/>", {
              class: "contentWrapper__table-tbody-row-label",
            }).append(
              $("<input/>", {
                class: "contentWrapper__table-tbody-row-checkbox",
                type: "radio",
                name: "contractor",
                value: JSON.stringify(el),
                change: function () {
                  if ($(this).is(":checked")) {
                    UI.next.show();
                    serviceData.custom.currentContractor = el;
                  }
                }
              }),
              $("<span/>", {
                class: "contentWrapper__table-tbody-row-checkmark"
              })
            );

            name = $("<div/>", {
              class: "contentWrapper__table-tbody-row-name",
              text: el.DESCR
            });

            row.append(checkbox, name).appendTo(tbody);
          }
        });
      }

      if (typeof args[0] === 'number') {
        paginator(args[0], pages);
        currPage.text(args[0] + 1);
        btnPrev.css('visibility', 'visible');
      } else {
        paginator(currPageVal, pages);
      }

      table.append(tbody);

      if (pages.length > 1) {
        nav.append(btnPrev, currPage, btnNext);
      }

      UI.content.append(title, table, nav);

      if (typeof args[0] === 'number') {
        let radioBtnFromSearch = $(".contentWrapper__table-tbody-row[data-page='elem_" + args[0] + "_" + args[1] + "'] input");
        radioBtnFromSearch.prop("checked", true);
        serviceData.custom.currentContractor = JSON.parse(radioBtnFromSearch.val());
        // UI.next.show().attr('onclick', 'drawAuthPage()');
        UI.next.show().attr('onclick', 'drawPickAuthBtn()');
      }
    }).catch(function (err) {
      drawError(err, "drawPickContractorPage", "Ошибка запроса");
    }).finally(function () {
      hideLoading();
    });

    UI.content.append(title);
  } catch (err) {
    drawError(err, "drawPickContractorPage", "Ошибка интерфейса");
  }

  serviceData.custom.navCurrFn = getFunctionName(arguments);
}

function drawSearchPopup(data, rawPages) {
  logStep('Всплывающее окно поиска');
  let popup, search, close, title, table;

  $(".contentWrapper__search-popup").remove();

  popup = $("<div class='contentWrapper__search-popup'></div>");
  search = $("<input class='contentWrapper__search-popup-input'></div>");

  close = $("<div/>", {
    class: "contentWrapper__search-popup-close",
    text: "×",
    click: function () {
      popup.remove();
      removeKeyboard();
    }
  });

  title = $("<div class='contentWrapper__search-popup-title'>Введите наименование контрагента</div>");
  table = $("<div class='contentWrapper__search-popup-table'></div>");

  popup.append(title, table, search, close).appendTo($('.wrapper'));

  search.initKeyboard({
    layout: "custom",
    alwaysOpen: true,
    maxLength: 50,
    caretToEnd: true,
    position: {
      of: null,
      my: 'bottom',
      at: 'bottom',
      // at2: 'center bottom'
    },
    customLayout: {
      'normal': [
        "ё 1 2 3 4 5 6 7 8 9 0 - = {bksp} {clear}",
        "й ц у к е н г ш щ з х ъ ",
        "ф ы в а п р о л д ж э ;",
        "{shift} я ч с м и т ь б ю , . {accept}",
        "{space}"
      ],
      'shift': [
        'Ё ! @ # $ % ^ & * ( ) - = {bksp} {clear}',
        "Й Ц У К Е Н Г Ш Щ З Х Ъ",
        "Ф Ы В А П Р О Л Д Ж Э ;",
        "{shift} Я Ч С М И Т Ь Б Ю , . {accept}",
        "{space}"
      ]
    },
    display: {
      'bksp': '⌫',
      'shift': '⇑',
      'accept': 'Искать'
    },
    css: {
      container: 'contentWrapper__search-popup-keyboard'
    },
    initialized: function (e, keyboard, el) {
      setTimeout(function () {
        keyboard.$preview.blur();
      }, 550);
    },
    change: function (e, kbd, el) {
      setTimeoutTimer(timeout);
    },
    accepted: function (e, kbd, el) {
      let val, formatted, regexp, arrItems;

      table.html('');
      arrItems = [];
      val = kbd.$preview.val().trim();

      if (val.length) {
        val = escapeRegExp(val);
        // formatted = ".*" + val.replace(/\s{2,}/g, ' ').split(" ").join(".*") + ".*";
        formatted = "(" + val.replace(/\s{2,}/g, ' ').split(" ").join("|") + ")";
        regexp = new RegExp(formatted, "gi");

        data.forEach(function (el) {
          // let matched = regexp.test(el.DESCR);
          let matched = regexp.exec(el.DESCR);
          if (matched) {
            arrItems.push(el);
          }
        });

        table.append(drawSearchPaginator(arrItems, rawPages));
      }

      if (!arrItems.length) {
        table.append("<div class='contentWrapper__search-popup-noresult'>Нет результата поиска</div>");
        $(".contentWrapper__search-popup-noresult").fadeIn().css('display', 'flex');
      }
    }
  });

  function escapeRegExp(string) {
    return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
  }

  serviceData.custom.navCurrFn = getFunctionName(arguments);
}

function drawSearchPaginator(data, rawPages) {
  let pages, page, pagesInRow, dataLens, currPageVal, currPage, tbody, nav, item, name, btnPrev, btnNext;

  pages = [];
  page = [];
  pagesInRow = 8;
  dataLens = data.length - 1;
  currPageVal = 0;

  if (!data.length) {
    return false;
  }

  for (let i = 1, k = 0; k <= dataLens; i++, k++) {
    if (i % pagesInRow === 0) {
      page.push(data[k]);
      pages.push(page);
      page = [];
    } else {
      page.push(data[k]);
      if (k === dataLens) {
        pages.push(page);
        page = [];
      }
    }
  }

  tbody = $("<div>", {
    class: "contentWrapper__search-popup-tbody"
  });
  nav = $("<div>", {
    class: "contentWrapper__search-popup-nav"
  });
  btnPrev = $("<div/>", {
    class: 'contentWrapper__search-popup-nav-btn-prev',
    click: function () {
      if (currPageVal !== 0) {
        currPageVal--;
        currPage.text(currPageVal + 1);
        paginator(currPageVal, pages);
      }

      if (currPageVal <= 0) {
        $(this).css("visibility", "hidden");
      }

      if (btnNext.css("visibility") === "hidden") {
        btnNext.css("visibility", "visible");
      }
    }
  });
  btnNext = $("<div/>", {
    class: 'contentWrapper__search-popup-nav-btn-next',
    click: function () {
      if (btnPrev.css("visibility") === "hidden") {
        btnPrev.css("visibility", "visible");
      }

      currPageVal++;
      currPage.text(currPageVal + 1);
      paginator(currPageVal, pages);

      if ((pages.length - 1) === currPageVal) {
        $(this).css("visibility", "hidden");
      }
    }
  });

  currPage = $("<div class='contentWrapper__search-popup-nav-curr-page'>1</div>");

  function paginator(page, arrNavPages) {
    $(".contentWrapper__search-popup-table-tbody-item").hide();

    if (!arrNavPages.length) {
      return false;
    }

    arrNavPages[page].forEach(function (el, id) {
      item = $(".contentWrapper__search-popup-table-tbody-item[data-page='elem_" + page + "_" + id + "']");

      if (item.is(":hidden")) {
        item.show();
      } else {
        item = $("<div/>", {
          class: 'contentWrapper__search-popup-table-tbody-item',
          "data-page": "elem_" + page + "_" + id
        });

        if (id !== 0 && id % pagesInRow === 0) {
          $(row).addClass("hide-border");
        }

        pick = $("<div>", {
          class: 'contentWrapper__search-popup-pick',
          text: 'Выбрать',
          click: function () {
            rawPages.forEach(function (arr, indx) {
              arr.forEach(function (elem, id) {
                if (el.CODE === elem.CODE) {
                  $(".contentWrapper__search-popup").remove();
                  // drawPickContractorPage(parseInt(indx), parseInt(id));
                  drawPickContractorPage(~~indx, ~~id);
                  removeKeyboard();
                }
              });
            });
          }
        });

        name = $("<div/>", {
          class: "contentWrapper__search-popup-table-tbody-row-name",
          text: el.DESCR
        });

        item.append(name, pick).appendTo(tbody);
      }
    });
  }

  paginator(currPageVal, pages);

  if (pages.length > 1) {
    nav.append(btnPrev, currPage, btnNext);
  }

  tbody.append(nav);

  return tbody;
}

function drawPickAuthBtn() {
  logStep('Страница выбора ввода лицевого счёта');

  try {
    UI.content.html('').attr('onclick', '');
    UI.mainMenu.hide().attr('onclick', '').text('');
    UI.next.hide().attr('onclick', '');
    UI.back.attr('onclick', 'drawPickContractorPage()');

    drawLoading('Идёт загрузка...', '', 'rgba(255, 255, 255, 0.8)');
    fetch(URL + "drawPickAuthBtn").then(function (res) {
      return res.text();
    }).then(function (html) {
      UI.content.append(html);
    }).catch(function (err) {
      drawError(err, "drawPickAuthBtn", "Ошибка запроса");
    }).finally(function () {
      hideLoading();
    });
  } catch (err) {
    drawError(err, "drawPickAuthBtn", "Ошибка интерфейса");
  }

  serviceData.custom.navCurrFn = getFunctionName(arguments);
}

function drawBarcodeAuthPage() {
  logStep('Страница штрих-кода');

  try {
    UI.content.html('').attr('onclick', '');
    UI.mainMenu.hide().attr('onclick', '').text('');
    UI.back.attr('onclick', 'drawPickAuthBtn()');
    UI.next.hide().attr('onclick', '');

    drawLoading('Идёт загрузка...', '', 'rgba(255, 255, 255, 0.8)');
    fetch(URL + "drawBarcodeAuthPage").then(function (res) {
      return res.text();
    }).then(function (html) {
      UI.content.append(html);

      socket = new WebSocket(WS);
      socket.onmessage = function (event) {
        let personalAcc, reSumma, rePesonalAcc;
        socket.close(1000, "Баркод получен");
        logStep("Получен баркод от [drawBarcodeAuthPage]: " + event.data);

        try {
          rePesonalAcc = /лс-([0-9]*)/gmi;
          reSumma = /сумма\s?([0-9]+\.?[0-9]*)/gmi;

          summa = parseFloat(reSumma.exec(event.data)[1]);
          personalAcc = rePesonalAcc.exec(event.data)[1];

          serviceData.custom.from = "drawBarcodeAuthPage();";

          serviceData.custom.user.accountId = personalAcc;
          serviceData.custom.user.clientSumma = summa;
          serviceData.custom.keyboardInput = null;

          if (serviceData.custom.currentContractor.SP10030) {
            drawInputPersonalData();
          } else {
            drawPersonalPage();
          }
        } catch (err) {
          drawError(err, "drawBarcodeAuthPage", "Неверный QR-код");
        }
      }
    }).catch(function (err) {
      drawError(err, "drawBarcodeAuthPage", "Ошибка запроса");
    }).finally(function () {
      hideLoading();
    });
  } catch (err) {
    drawError(err, "drawBarcodeAuthPage", "Ошибка интерфейса");
  }

  serviceData.custom.navCurrFn = getFunctionName(arguments);
}

function drawAuthPage() {
  logStep('Переход на страницу авторизации');

  UI.content.html('').attr('onclick', '');
  UI.next.hide();
  UI.back.show().attr('onclick', 'drawPickAuthBtn()');

  serviceData.custom.from = "drawAuthPage();";
  serviceData.custom.user.accountId = null;
  serviceData.custom.keyboardInput = null;

  let backFn = serviceData.custom.currentContractor.SP10030 ? "drawInputPersonalData()" : "drawPersonalPage()";

  UI.next.attr('onclick', backFn);
  UI.mainMenu.hide().attr('onclick', '').text('');

  try {
    let title, input;

    title = $("<h1>", {
      class: "contentWrapper__title",
      text: "Введите номер лицевого счёта"
    });

    input = $("<input>", {
      type: "text",
      class: "contentWrapper__kbd-anchor"
    });

    UI.content.append(title, input);

    input.initKeyboard({
      layout: "custom",
      alwaysOpen: true,
      maxLength: 20,
      caretToEnd: true,
      position: {
        of: null,
        my: 'center center',
        at: 'center center',
        // at2: 'center bottom'
      },
      customLayout: {
        'normal': [
          "ё 1 2 3 4 5 6 7 8 9 0 {bksp} {clear}",
          "й ц у к е н г ш щ з х ъ ",
          "ф ы в а п р о л д ж э",
          "{shift} я ч с м и т ь б ю {shift}",
          "{space}"
        ],
        'shift': [
          'Ё 1 2 3 4 5 6 7 8 9 0 {bksp} {clear}',
          "Й Ц У К Е Н Г Ш Щ З Х Ъ",
          "Ф Ы В А П Р О Л Д Ж Э",
          "{shift} Я Ч С М И Т Ь Б Ю {shift}",
          "{space}"
        ]
      },
      display: {
        'bksp': '⌫',
        'shift': '⇑',
        'accept': 'OK'
      },
      initialized: function (e, keyboard, el) {
        setTimeout(function () {
          if (keyboard.$preview.blur) {
            keyboard.$preview.blur();
          }
        }, 550); // use 550 because internally it uses 500 (for IE)
      },
      change: function (e, kbd, el) {
        setTimeoutTimer(timeout);
        let val = kbd.$preview.val();

        if (val.length === 4) {
          UI.next.show();
        }
        if (val.length < 4) {
          UI.next.hide();
        }

        serviceData.custom.keyboardInput = val;
      }
    });
  } catch (err) {
    drawError(err, "drawAuthPage", "Ошибка интерфейса");
  }

  serviceData.custom.navCurrFn = getFunctionName(arguments);
}

function drawPersonalPage() {
  logStep('Переход на страницу персональных данных');

  UI.content.html('').attr('onclick', '');
  UI.next.hide();

  UI.back.show().attr('onclick', serviceData.custom.from);
  UI.next.attr('onclick', 'drawPaymentPick()');

  try {
    drawLoading('Идёт загрузка...', '', 'rgba(255, 255, 255, 0.8)');
    let query = $.param({
      accNum: serviceData.custom.user.accountId || serviceData.custom.keyboardInput,
      dbfFileName: serviceData.custom.currentContractor.SP10021,
      // dbfFileName: "C:\\interfaceData\\saratov_jkh\\os.DBF",
      from: serviceData.custom.from
    });

    fetch(URL + "checkPersonalAcc?" + query, {
      headers: {
        'Pragma': 'no-cache',
        'Cache-Control': 'no-cache, no-store, must-revalidate'
      }
    }).then(function (res) {
      return res.text();
    }).then(function (html) {
      UI.content.append(html);
    }).catch(function (err) {
      drawError(err, "drawPersonalPage", "Ошибка запроса");
    }).finally(function () {
      hideLoading();
    });
  } catch (err) {
    drawError(err, "drawPersonalPage", "Ошибка интерфейса");
  }

  serviceData.custom.navCurrFn = getFunctionName(arguments);
}

function drawInputPersonalData() {
  logStep('Переход на страницу ввода признаков');

  UI.content.html('').attr('onclick', '');
  UI.next.hide();

  UI.back.show().attr('onclick', serviceData.custom.from);

  try {
    let featuresList;
    drawLoading('Идёт загрузка...', '', 'rgba(255, 255, 255, 0.8)');

    fetch(URL + "drawInputPersonalData", {
      headers: {
        'Content-Type': 'text/html',
        'Pragma': 'no-cache',
        'Cache-Control': 'no-cache, no-store, must-revalidate'
      }
    }).then(function (res) {
      // Из кастомного заголовка достаём данные в base64 и преобразуем их в объект, с целью получения списка признаков...
      featuresList = JSON.parse(decodeURIComponent(escape(atob(res.headers.map["x-json-base64"]))));
      return res.text();
    }).then(function (html) {
      let data, currPage, pagesOnPage, pages, btnNext, btnPrev, close, popup, input, featuresContractorKeys, contractorElem, contractorObj, reSGUA;

      featuresContractorKeys = [];
      contractorElem = serviceData.custom.currentContractor;
      contractorObj = {};

      for (let prop in featuresList) {
        featuresContractorKeys.push(prop);
      }

      featuresContractorKeys.forEach(function (el) {
        contractorObj[el] = contractorElem[el];
      });

      html = $(html);
      reSGUA = /^SGUA[0-9]*/gi;

      [].forEach.call(html.find(".contentWrapper__features-item"), function (el, id) {
        if (!contractorElem.SP10107) { // SP10107 это СГЮА
          if (!contractorObj[el.dataset.id]) {
            $(el).remove();
          }
        } else {
          if (!contractorObj[el.dataset.id] && el.dataset.id.match(reSGUA) === null) {
            $(el).remove();
          }
        }
      });

      UI.content.append(html);

      currPageVal = 0; // Текущая страница
      pagesOnPage = 8; // Элементов на странице

      currPage = $(".contentWrapper__features-nav-page");
      pages = $(".contentWrapper__features-item");

      btnPrev = $(".contentWrapper__features-nav-prev");
      btnNext = $(".contentWrapper__features-nav-next");

      popup = $(".contentWrapper__features-popup");
      close = $(".contentWrapper__features-popup-close");
      input = $(".contentWrapper__features-popup-keyboard");

      $(".contentWrapper__features-item-btn").on("click", function (e) {
        let elem = e.target;
        input.val("");
        $(".contentWrapper__title[data-role='features-popup']").text(elem.dataset.name);
        popup.css("display", "flex");
        input.initKeyboard({
          layout: "custom",
          alwaysOpen: true,
          maxLength: 100,
          caretToEnd: true,
          position: {
            of: null,
            my: 'center center',
            at: 'center center',
            // at2: 'center bottom'
          },
          customLayout: {
            'normal': [
              "ё 1 2 3 4 5 6 7 8 9 0 - = {bksp} {clear}",
              "й ц у к е н г ш щ з х ъ { }",
              "ф ы в а п р о л д ж э \\ \' ;",
              "{shift} я ч с м и т ь б ю , . / {accept}",
              "{space}"
            ],
            'shift': [
              'Ё ! @ # $ % ^ & * ( ) _ + {bksp} {clear}',
              "Й Ц У К Е Н Г Ш Щ З Х Ъ [ ]",
              "Ф Ы В А П Р О Л Д Ж Э | \" :",
              "{shift} Я Ч С М И Т Ь Б Ю < > ? {accept}",
              "{space}"
            ]
          },
          display: {
            'bksp': '⌫',
            'shift': '⇑',
            'clear': 'CE',
            'accept': 'Подтвердить'
          },
          initialized: function (e, keyboard, el) {
            setTimeout(function () {
              keyboard.$preview.blur();
            }, 550);
          },
          change: function (e, kbd, el) {
            setTimeoutTimer(timeout);
          },
          accepted: function (e, kbd, el) {
            let val, id, states, isReady;

            val = kbd.$preview.val().trim();
            id = elem.dataset.id;
            states = [];

            serviceData.custom.currentContractor[id] = val;

            $(".contentWrapper__features-item[data-id='" + id + "']").find(".contentWrapper__features-item-output").text(val);
            [].forEach.call($(".contentWrapper__features-item-output"), function (el, id) {
              states.push(($(el).text() === "" ? 0 : 1));
            });

            isReady = states.every(function (el) {
              return el === 1;
            });

            if (isReady) {
              if (serviceData.custom.currentContractor.SP10107) {
                UI.next.show().text("Перейти к вводу суммы").attr('onclick', 'drawSetPayment()');
              } else {
                UI.next.show().text("Перейти к показаниям счётчиков").attr('onclick', 'drawWaterMeter();');
              }
            } else {
              UI.next.hide();
            }

            setTimeout(function () {
              popup.hide();
              removeKeyboard();
            }, 550);
          },
          validate: function (keyboard, value, isClosing) {
            return value.length > 0;
          }
        });
      });

      close.on("click", function () {
        popup.hide();
        removeKeyboard();
      });

      btnNext.on("click", function () {
        if (btnPrev.css("visibility") === "hidden") {
          btnPrev.css("visibility", "visible");
        }

        currPageVal++;
        currPage.text(currPageVal + 1);
        paginator(".contentWrapper__features-item", pagesOnPage, currPageVal);

        if (parseInt((pages.length - 1) / pagesOnPage) === currPageVal) {
          $(this).css("visibility", "hidden");
        }
      });

      btnPrev.on("click", function () {
        if (currPageVal !== 0) {
          currPageVal--;
          currPage.text(currPageVal + 1);
          paginator(".contentWrapper__features-item", pagesOnPage, currPageVal);
        }

        if (currPageVal <= 0) {
          $(this).css("visibility", "hidden");
        }

        if (btnNext.css("visibility") === "hidden") {
          btnNext.css("visibility", "visible");
        }
      });

      paginator(".contentWrapper__features-item", pagesOnPage, currPageVal);

      function paginator(items, pagesOnPage, page) {
        let pagesOnPageVal, pages;

        pagesOnPageVal = pagesOnPage;
        pages = 0;

        $(items).css("display", "none");

        [].forEach.call($(items), function (el, id) {
          el.dataset.page = pages;

          if (!(pagesOnPageVal-- > 1)) {
            pages++;
            pagesOnPageVal = pagesOnPage;
          }
        });

        $(items + "[data-page=" + page + "]").show();

        if (!pages) {
          $(".contentWrapper__features-nav").css('visibility', 'hidden');
        }
      }
    }).catch(function (err) {
      drawError(err, "drawInputPersonalData", "Ошибка запроса");
    }).finally(function () {
      hideLoading();
    });
  } catch (err) {
    drawError(err, "drawInputPersonalData", "Ошибка интерфейса");
  }

  serviceData.custom.navCurrFn = getFunctionName(arguments);
}

function drawWaterMeter() {
  logStep('Переход на страницу вывода счётчиков');

  UI.content.html('').attr('onclick', '');
  UI.next.show().text("Далее").attr('onclick', 'drawSetPayment()');
  UI.back.show().attr('onclick', 'drawInputPersonalData()');

  try {
    drawLoading('Идёт загрузка...', '', 'rgba(255, 255, 255, 0.8)');

    let query = $.param({
      accNum: serviceData.custom.user.accountId || serviceData.custom.keyboardInput,
      dbfFileName: serviceData.custom.currentContractor.SP10022,
      // dbfFileName: "C:\\interfaceData\\saratov_jkh\\ss.DBF",
      from: serviceData.custom.from
    });

    fetch(URL + "drawWaterMeter?" + query).then(function (res) {
      return res.text();
    }).then(function (html) {
      UI.content.append(html);

      $(".contentWrapper__sidebar-item-input").initKeyboard({
        layout: "custom",
        maxLength: 10,
        position: {
          of: $(".contentWrapper__sidebar-anchor"),
          my: "left top",
          at: "left top"
          // at: 'center bottom',
          // at2: 'center bottom'
        },
        customLayout: {
          'normal': [
            '{clear} {empty} {b}',
            '1 2 3',
            '4 5 6',
            '7 8 9',
            '{dec} 0 {a}'
          ]
        },
        display: {
          'bksp': '⌫',
          'shift': '⇑',
          'clear': "CE",
          'a': "OK"
        },
        change: function (e, kbd, elem) {},
        accepted: function (e, kbd, elem) {
          let val, rng, obj;

          val = parseFloat(kbd.$preview.val()).toFixed(2);
          rng = Math.floor(Math.random() * Math.pow(10, 5));
          obj = serviceData.custom.waterMeterData["WM" + rng] = {};

          elem.value = val;

          obj["waterMeterName" + rng] = {
            text: "Имя счетчик",
            code: null,
            value: elem.dataset.id
          };
          obj["waterMeterValue" + rng] = {
            text: "Показатель счетчика",
            code: null,
            value: val
          };
          obj["waterMeterDate" + rng] = {
            text: "Дата показаний счётчика",
            code: null,
            value: $.format.date(new Date(), "dd/MM/yyyy")
          };
        },
        validate: function(keyboard, value, isClosing) {
          return value.length > 0;
        }
      });
    }).catch(function (err) {
      drawError(err, "drawWaterMeter", "Ошибка запроса");
    }).finally(function () {
      hideLoading();
    });
  } catch (err) {
    drawError(err, "drawWaterMeter", "Ошибка интерфейса");
  }

  serviceData.custom.navCurrFn = getFunctionName(arguments);
}

function drawSetPayment() {
  logStep("Страница ввода сумму к оплате");

  UI.content.html('').attr('onclick', '');
  UI.next.hide();

  UI.back.show().attr('onclick', serviceData.custom.navCurrFn);
  UI.next.show().attr('onclick', "drawPaymentPick();").text("Перейти к выбору оплаты");

  try {
    drawLoading('Идёт загрузка...', '', 'rgba(255, 255, 255, 0.8)');
    fetch(URL + "drawSetPayment?clientSumma=" + serviceData.custom.user.clientSumma).then(function (res) {
      return res.text();
    }).then(function (html) {
      let elemWrap, elemInput, elemCurrency, value, currency;
      UI.content.append(html);

      elemWrap = document.querySelector(".contentWrapper__set-payment-text");
      elemInput = elemWrap.querySelector(".contentWrapper__set-payment-input");
      elemCurrency = elemWrap.querySelector(".contentWrapper__set-payment-currency");

      $(".contentWrapper__set-payment-btn").on("click", function (e) {
        UI.next.prop("disabled", true).addClass("disabled");
        var kb = $(".contentWrapper__set-payment-input").getkeyboard();
        kb.reveal();
      });

      $(".contentWrapper__set-payment-input").initKeyboard({
        layout: "custom",
        maxLength: 10,
        usePreview: false,
        alwaysOpen: false,
        position: {
          of: $(".contentWrapper__set-payment-anchor"),
          my: "center top",
          at: "center top"
          // at: 'center bottom',
          // at2: 'center bottom'
        },
        customLayout: {
          'normal': [
            '{clear} {empty} {b}',
            '1 2 3',
            '4 5 6',
            '7 8 9',
            '{dec} 0 {a}'
          ]
        },
        display: {
          'bksp': '⌫',
          'shift': '⇑',
          'clear': "CE",
          'a': "OK"
        },
        initialized: function (e, kbd, elem) {
          // value = elemWrap.dataset.value;
          // elemInput.value = parseFloat(value).toFixed(2);
          // kbd.caret($(".contentWrapper__set-payment-input"), 'end');
          // currency = elemWrap.dataset.currency;
          // elemCurrency.innerHTML = " " + currency;
        },
        change: function (e, kbd, elem) {},
        accepted: function (e, kbd, elem) {
          let val = parseFloat(kbd.$preview.val()).toFixed(2);
          elem.value = val;
          stepsData.totalSum = isNaN(val) ? 0 : val;

          UI.next.prop("disabled", false).removeClass("disabled");
        },
        validate: function(keyboard, value, isClosing) {
          return value.length > 0;
        }
        // validate: function (keyboard, value, isClosing) {
        // $.keyboard.caret(keyboard.$preview, 'end');
        // return true;
        // }
      });

      // сумма, которую должен контрагент
      stepsData.totalSum = serviceData.custom.user.clientSumma;
    }).catch(function (err) {
      drawError(err, "drawSetPayment", "Ошибка запроса");
    }).finally(function () {
      hideLoading();
    });
  } catch (err) {
    drawError(err, "drawSetPayment", "Ошибка интерфейса");
  }

  serviceData.custom.navCurrFn = getFunctionName(arguments);
}

function getTerroristFromTable(fullName) {
  fullName = fullName.replace(/\s*/gi, "").toUpperCase();

  return new Promise(function (resolve, reject) {
    fetch(URL + "getTerrorTable").then(function (res) {
      return res.json();
    }).then(function (json) {
      json.forEach(function (el) {
        if (el.FIO.match(RegExp(fullName, 'gi'))) {
          serviceData.custom.user.terror = 1;
        }
      });

      resolve(serviceData.custom.user.terror);
    }).catch(function (err) {
      reject(err);
      drawError(err, "getTerroristFromTable", "Ошибка запроса");
    });
  });
}

function getSGUAStudentFromTable() {
  return new Promise(function (resolve, reject) {
    fetch().then().then().catch();
  });
}

function serializeSaratovData() {
  serviceData.custom.fileObj = {
    sessionNumber: { // номер; (id операции,10),если возможно должно начинаться на "r19_"
      text: "Номер операции",
      code: null,
      value: "r19_" + Math.floor(Math.random() * Math.pow(10, 10)) // генерация рандомного числа состоящего из 10 цифр
    },
    sessionDate: { // дата; (дата операции,"дд.мм.гггг")
      text: "Дата операции",
      code: null,
      value: $.format.date(new Date(), "dd/MM/yyyy")
    },
    paymentType: { // тип платежа(5);
      text: "Тип платежа",
      code: "SP9257",
      value: 5
    },
    contractorId: { // контрагент(id контрагента); 
      text: "Идентификатор контрагент",
      code: "CODE",
      value: ""
    },
    clientSumma: { // сумма(Сумма,полученная с клиента); 
      text: "Сумма клиента",
      code: null,
      value: stepsData.totalSum
    },
    remunerationSumma: { // сумма вознаграждения;
      text: "Сумма вознаграждения",
      code: null,
      value: ""
    },
    discountSumma: { // сумма скидки;
      text: "Сумма скидки",
      code: "SP9668",
      value: ""
    },
    penalties: { // пени (сумма пени не входит в сумма );
      text: "Пени",
      code: "SP8862",
      value: ""
    },
    accountNumber: { // счет/номер телефона;
      text: "Счёт контрагента или номер телефона",
      code: "SP8860",
      value: ""
    },
    clientAddress: { // адрес клиента
      text: "Адрес клиента",
      code: "SP8870",
      value: ""
    },
    periodMonth: { // за_период_мес;
      text: "За период месяц",
      code: "SP8871",
      value: ""
    },
    periodYear: { // за_период_год; ???
      text: "За период год",
      code: null,
      value: ""
    },
    clientName: { // фамилия имя отчество;
      text: "Ф.И.О",
      code: "SP8869",
      value: ""
    },
    bankName: { // банк наименование;
      text: "Наименование банка",
      code: "SP8874",
      value: ""
    },
    bik: { // бик;
      text: "БИК",
      code: "SP8875",
      value: ""
    },
    settlementAccount: { // р_сч (расчётный счёт);
      text: "Расчётный счёт",
      code: "SP8872",
      value: ""
    },
    comment: { // комментарий;
      text: "Комментарий",
      code: null,
      value: ""
    },
    paymentOption: { // безналичная/наличная оплата;(0-наличная,1-безнал)
      text: "Безналичная/наличная оплата",
      code: null,
      value: serviceData.custom.paymentType
    },
    cardTransactionNumber: { // номер транзакции безнал;
      text: "Номер транзакции",
      code: null,
      value: serviceData.custom.cardTransactionNumber
    },
    cardNumber: { // номер карты;
      text: "Номер карты",
      code: null,
      value: serviceData.custom.cardNumber
    },
    ktt: { // КТТ;
      text: "КТТ",
      code: null,
      value: serviceData.custom.KTT
    },
    authCode: { // authcode;
      text: "Authcode",
      code: null,
      value: serviceData.custom.authCode
    },
    uin: { // ИНН;
      text: "ИНН",
      code: "SP8873",
      value: ""
    },
    recipient: { // Получатель;
      text: "Получатель",
      code: "SP8858",
      value: ""
    },
    cyberSession: { // Сессия(для кибера);
      text: "Сессия(для кибера)",
      code: null,
      value: ""
    },
    sguaContract: {
      text: "Договор",
      code: "SP9631",
      value: ""
    },
    sguaDate: {
      text: "Дата СГЮА",
      code: "SP9632",
      value: ""
    },
    educationFaculty: { // Факультет;
      text: "Факультет",
      code: "SGUA1001",
      value: ""
    },
    educationSpecialization: { // Специальность;
      text: "Специальность",
      code: "SGUA1003",
      value: ""
    },
    educationDepartment: { // Отделение;
      text: "Отделение",
      code: "SGUA1002",
      value: ""
    },
    educationYear: { // Курс;
      text: "Курс",
      code: "SGUA1004",
      value: ""
    },
    educationGroup: { // Группа;
      text: "Группа",
      code: "SGUA1005",
      value: ""
    },
    educationClient: { // Вноситель;
      text: "Вноситель",
      code: "SGUA1000",
      value: ""
    },
    phrase: { // Фраза;
      text: "Фраза",
      code: "SP10135",
      value: ""
    },
    terrorism: { // Проверка на терроризм
      text: "Террорист",
      code: null,
      value: ""
    }
  };

  let waterMeterData = serviceData.custom.waterMeterData;
  let currentContractor = serviceData.custom.currentContractor;
  let fileObj = serviceData.custom.fileObj;

  for (let prop in waterMeterData) {
    let WMData = waterMeterData[prop];
    for (let p in WMData) {
      serviceData.custom.fileObj[p] = WMData[p];
    }
  }

  for (let prop in currentContractor) {
    for (let p in fileObj) {
      if (fileObj[p].code === prop) {
        serviceData.custom.fileObj[p].value = currentContractor[prop];
      }
    }
  }

  return serviceData.custom.fileObj;
}

function createFileAfterPayment() {
  getTerroristFromTable(serviceData.custom.fileObj.clientName.value).then(function (res) {
    serviceData.custom.fileObj.terrorism.value = res;

    logStep("[createFileAfterPayment] начало создания файла");
    fetch(URL + "createFile", {
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(serviceData.custom.fileObj)
    }).then(function (res) {
      logStep("[createFileAfterPayment] отправка данных: " + JSON.stringify(serviceData.custom.fileObj));
      return res.json();
    }).then(function (data) {
      logStep("[createFileAfterPayment] получение данных: " + JSON.stringify(data));
    }).catch(function (err) {
      drawError(err, "createFileAfterPayment", "Ошибка запроса");
      // logStep("[createFileAfterPayment] операция завершена с ошибкой: " + JSON.stringify(err));
    }).finally(function () {
      logStep("[createFileAfterPayment] операция завершена");
    });
  });
}
//------------------//

function drawError(err, fnname, text, backFn) {
  let errContent;
  logStep("Страница ошибки");
  logException("Ошибка при вызове " + fnname + " | " + err);
  hideLoading();
  text = text ? text : "Ой, что-то пошло не так!";
  backFn = backFn ? backFn : "init();";

  UI.content.html('').removeAttr('onclick');
  UI.skud.hide();
  UI.next.hide();
  UI.back.show().attr("onclick", backFn);

  errContent = $("<div />", {
    class: "errContent",
    text: text
  });

  UI.content.append(errContent);
  //setTimeout(init, 5000);
}

function drawPaymentPick() {
  logStep('Переход на страницу выбора типа оплаты');
  extDisableBarcode();
  UI.content.html('');
  // UI.back.show();
  UI.back.show().attr('onclick', serviceData.custom.navCurrFn);
  UI.next.hide().removeClass('pay');

  extGetPaymentMethod().then(function (type) {
    let wrapper = $('<div class="paymentPick">');
    let info = $('<div class="info">');
    let directive = $('<h1 class="contentWrapper__title">Выберите способ оплаты</div>');
    let imgWrapper = $('<div></div>');
    if (type === 1 || type === 4) imgWrapper.append($('<img id="cashButton" src="img/select_cash.png" onclick="drawPayment(\'cash\')" alt="Наличные">'));
    if (type === 2 || type === 4) imgWrapper.append($('<img id="cardButton" src="img/select_card.png" onclick="drawPayment(\'card\')" alt="Карта">'));
    wrapper.append(directive, info, imgWrapper);
    UI.content.append(wrapper);
  });
}

function drawPayment(type) {
  // if (debug) type = 'cash';
  // if (debug) type = 'card';
  extStartPayCycle(gateID);

  logStep('Переход на страницу платежа. Тип: ' + type);
  serviceData.custom.paymentType = type === "card" ? 1 : 0;
  serviceData.isPayment = 1;
  stepsData.selectedPayment = type + 'Payment';

  let toDo;
  let info = '';
  let imgSrc;
  let rowspan;
  let timeRemain = '';

  if (type === 'cash') {
    toDo = 'Внесите деньги';
    info += '' +
      '<tr class="blue"><td>Осталось внести</td> <td class="paymentTable__sum"><span id="remainSum">*</span> руб.</td></tr>' +
      '<tr><td>Внесено</td> <td class="paymentTable__sum"><span id="inputSum">0</span> руб.</td></tr>' +
      '<tr class="red change"><td>Сдача</td> <td class="paymentTable__sum"><span id="changeSum">*</span> руб.</tdc></tr>' +
      '';
    imgSrc = 'img/insertmoney.png';
    rowspan = 4;
  }

  if (type === 'card') {
    toDo = 'Вставьте карту';
    info += '<tr class="blue"><td>Сумма к оплате</td> <td><span id="totalSum">' + stepsData.totalSum + '</span> руб.</td></tr>';
    imgSrc = 'img/card_process.png';
    rowspan = 2;
    timeRemain = '<div class="remainText">На совершение операции осталось:</div><span class="timeRemain blue"></span>';
  }

  UI.content.html('');
  UI.back.show().attr('onclick', 'backFromPayment()');
  UI.next.hide().addClass('finishPayButton').attr('onclick', 'handleOperation(stepsData.selectedPayment, \'confirm\');').html('Завершить оплату');

  UI.content.html('<div class="paymentWrapper"><table class="paymentTable">' +
    '<tr><th class="toDo" colspan="2">' + toDo + '</th><th rowspan="' + rowspan + '"><img src="' + imgSrc + '" alt="' + type + '"></th></tr>' +
    info + '</table>' + timeRemain +
    '</div>');

  if (type === 'cash') {
    startPaymentCash();
  }

  if (type === 'card') {
    UI.back.hide();
    startPaymentCard();
  }

  function startPaymentCash() {
    extStartBill();
    extSetPaymentSum(stepsData.totalSum);
    paymentPolling();
    timers.paymentCash = setInterval(paymentPolling, 500);
  }

  function startPaymentCard() {
    stopTimeoutTimer();
    setTimeout(stopTimeoutTimer, 500);
    extSetPaymentSum(stepsData.totalSum);
    startPinpadTimer();
    extStartPinpad();
  }
}

function drawFinish(reason) {
  hideLoading();

  let logStr = 'Переход на страницу чека. Причина';
  switch (reason) {
    case 'check':
      logStep(logStr + ': чек');
      break;
    case 'pinpadTimeout':
      logStep(logStr + ': таймаут пинпада');
      break;
    case undefined:
      logWarning(logStr + ' отсутствует');
      break;
    default:
      logWarning(logStr + ' неожиданная: ' + reason);
  }

  let currentTransaction = extGetCurrentSession();

  UI.content.html('<div class="finishWrapper">' +
    (stepsData.failReason ? '<span class="message">' + stepsData.failReason + '</span>' : '') +
    '<span class="toDo finalText finishWrapper__result">Возьмите, пожалуйста, вашу квитанцию!</span>' +
    '<img class="finishWrapper__result-img" src="img/printcheck.png" alt="чек">' +
    '</div>');

  if (serviceData.paymentSuccess) {
    waitBasket().then(function (basket) {
      let check = basket.data.cardterminalcheck;
      if (stepsData.selectedPayment === 'cardPayment') {
        let cardNumberResults = check.match(/\*\*\*\*\*\d+/);
        stepsData.cardInfo = {
          authCode: basket.data.authorizationcode,
          referenceNumber: basket.data.referencenumber,
          cardNumber: cardNumberResults !== null ? cardNumberResults[0].substring(5) : 1234,
          basketSession: basket.data.basketsession
        };

        serviceData.custom.cardNumber = stepsData.cardInfo.cardNumber;
        serviceData.custom.authCode = stepsData.cardInfo.authCode;
        serviceData.custom.cardTransactionNumber = stepsData.cardInfo.basketSession;

        logStep('Собраны данные по проведённому безналичному платежу: ' + objToLog(stepsData.cardInfo));
      }

      serializeSaratovData()
      createFileAfterPayment();
    });

    if (!debugConfig.testTerminal) {
      extExecutePayment(0).then(function (body) {
        logStep('Платёж проведён успешно (№ транзакции ' + currentTransaction + '). Ответ: ' + JSON.stringify(body));
      }, function (reason) {
        logWarning('Платёж не проведён' + (reason ? ' по причине: ' + reason : ''));
      }).catch(function (e) {
        logException('Ошибка проведения платежа: ' + e);
      });
    }

    serviceData.paymentSuccess = false;
  }

  if ('finish' in timers) {
    clearTimeout(timers.finish);
  }

  timers.finish = setTimeout(init, 5 * 1000);

  UI.back.hide();
  UI.next.hide();
  UI.mainMenu.hide();
  // UI.mainMenu.show().attr('onclick', 'clearTimeout(timers.finish);init()');
}

function drawQuestion(question) {
  drawLoading('', '', 'rgba(255,255,255,0.4)', false);
  let container = $('.loading');
  container.css('visibility', 'visible');
  container.html('');
  let table = $('<table class="questionTable"></table>');
  let str = '';
  switch (question) {
    case 'interruptPayment': {
      str += '<tr>';
      str += '<td colspan="20">Внесено недостаточно средств.<br>Желаете прервать операцию?</td>';
      str += '</tr>';
      str += '<tr>';
      str += '<td onclick="init();"><div class="betaButton" style="padding: 7px 0">Прервать</div></td><td class="alphaButton" style="text-align: right" onclick="handleOperation(\'cashPayment\', \'continue\')"><div class="alphaButton" style="padding: 7px 0">Продолжить</div></td>';
      str += '</tr>';
      break;
    }
    case 'pinpadRepeat': {
      clearInterval(timers.paymentCard);
      setTimeoutTimer(timeout);
      str += '<tr>';
      str += '<th colspan="20">Желаете повторить операцию?</th>';
      str += '</tr>';
      str += '<tr>';
      str += '<td onclick="handleOperation(stepsData.selectedPayment, \'cancel\');"><div class="betaButton" style="padding: 7px 0">Отменить</div></td><td onclick="handleOperation(\'cardPayment\', \'repeat\')"><div class="alphaButton" style="padding: 7px 0">Повторить</div></td>';
      str += '</tr>';
      break;
    }
    default: {
      logException('Функция отриосовки оверлея с вопросом вызвана с неизвестным параметром' + (question ? ': ' + question : ''));
    }
  }
  table.html(str);
  container.html('');
  container.append(table);
}

function drawLoading(str, seconds, color, image, button) {
  let wrapper = $('.loadingWrapper');
  let textUI = $('.loading');
  let buttonUI = $('.button#loading');
  let loadingBarUI = $('.loadingBar');

  wrapper.css('display', 'flex');
  wrapper.find('img').show();

  if (image === false) {
    wrapper.find('img').hide();
    loadingBarUI.hide();
  } else {
    loadingBarUI.show();
  }

  if (str === '' || str === undefined) {
    textUI.css('visibility', 'hidden');
  } else {
    textUI.css('visibility', 'visible');
  }

  if (str === 'loading') {
    textUI.html('<span ></span>');
  }

  if (color) {
    wrapper.css('background-color', color);
  } else {
    wrapper.css('background-color', 'black');
  }

  wrapper.css('display', 'flex');
  textUI.html(str);

  if (seconds) {
    setTimeout(function () {
      hideLoading();
    }, seconds * 1000);
  }

  button ? buttonUI.show() : buttonUI.hide();
}

function hideLoading() {
  logStep('Скрываем модальное окно (hideLoading)');
  $('.loadingWrapper').hide();
}

function paymentPolling() {
  if (debugConfig.data) {
    if (!'polltest' in paymentPolling) paymentPolling.polltest = 0;
    let debugInfo = $('.debugInfo');
    debugInfo.show();
    debugInfo.html(paymentPolling.polltest++);
  }
  let inputSum = Number(extGetPaymentInput());
  if (stepsData.inputSum !== inputSum) setTimeoutTimer(timeout);
  stepsData.inputSum = Number(inputSum);
  $('#inputSum').html(inputSum);
  let remain = (stepsData.totalSum * 100 - inputSum * 100) / 100;
  $('#remainSum').html(remain < 0 ? 0 : remain);
  let change = (inputSum * 100 - stepsData.totalSum * 100) / 100;
  if (change > 0) {
    $('.change').css('visibility', 'visible');
    $('#changeSum').html(change);
  }
  if (inputSum > 0) {
    UI.back.hide();
    if (!paymentAutofinish) UI.next.show();
  }

  if (paymentAutofinish && inputSum >= extGetPaymentSum()) {
    handleOperation(stepsData.selectedPayment, 'confirm');
  }
}

function handleCodeInput(barcode) {}

function backFromPayment() {
  if (stepsData.inputSum === 0) {
    handleOperation(stepsData.selectedPayment, 'cancel');
  } else {
    handleOperation(stepsData.selectedPayment, 'pause');
  }
}

function askBarcode() {
  logStep('Включено считывание баркодов из БД');
  timers.askBarcode = setInterval(function () {
    let barcode = extGetKeyForInterface('barcode');
    if (barcode.length > 1) {
      clearInterval(timers.askBarcode);
      extDisableBarcode();
      extSetKeyFromInterface('barcode', '0');
      handleCodeInput(barcode);
    }
  }, 350);
}

function handleOperation(operation, order) {
  switch (operation) {
    case ('cashPayment'): {
      switch (order) {
        case 'pause': {
          logStep('Вызван обработчик паузы наличной оплаты');
          extStopBill();
          clearInterval(timers.paymentCash);
          drawQuestion('interruptPayment');
          break;
        }
        case 'continue': {
          logStep('Вызван обработчик продолжения наличной оплаты');
          extStartBill();
          timers.paymentCash = setInterval(paymentPolling, 100);
          hideLoading();
          break;
        }
        case 'cancel': {
          serviceData.isPayment = 0;
          stepsData.selectedPayment = undefined;
          logStep('Вызван обработчик отмены наличной оплаты');
          removeTimeoutMessage(timeout);
          drawLoading('Возврат к платежу', '', 'rgba(255,255,255,0.8)');
          extStopBill();
          setTimeout(function () {
            extStopBill();
            clearInterval(timers.paymentCash);
            hideLoading();
            if (stepsData.inputSum === 0) {
              $('.modal').remove();
              if (serviceData.timeoutTriggered) init();
              else drawPaymentPick();
            } else if (stepsData.inputSum >= stepsData.totalSum) {
              extSendPaymentConfirm();
            } else extSendPaymentCancel();
          }, 8 * 1000);

          break;
        }
        case 'confirm': {
          serviceData.isPayment = 0;
          logStep('Вызван обработчик успешного выполнения наличной оплаты');
          drawLoading('Идёт проведение платежа\nНе забудьте взять чек', '', 'rgba(255,255,255,0.8)');
          extStopBill();
          setTimeout(function () {
            extStopBill();
            clearInterval(timers.paymentCash);
            if (paymentPartial ? stepsData.inputSum > 0 : stepsData.inputSum >= extGetPaymentSum()) {
              extSendPaymentConfirm();
            } else extSendPaymentCancel();
          }, 8 * 1000);
          break;
        }
        default: {
          logException('Функция handleOperation вызвана для операции ' + operation + ' c неизвестным параметром: ' + order);
        }
      }
      break;
    }
    case ('cardPayment'): {
      switch (order) {
        case 'repeat': {
          logStep('Вызван обработчик повтора безналичной оплаты');
          hideLoading();
          serviceData.pinpadTimeout = extGetPinpadTimeout() / 1000;
          serviceData.isPayment = 1;
          startPinpadTimer();
          extStartPinpad();
          break;
        }
        case 'cancel': {
          serviceData.isPayment = 0;
          logStep('Вызван обработчик отмены безналичной оплаты');
          removeTimeoutMessage(timeout);
          clearInterval(timers.paymentCash);
          init();
          break;
        }
        default: {
          logException('Функция handleOperation вызвана для операции ' + operation + ' c неизвестным параметром: ' + order);
        }
      }
      break;
    }
    default: {
      logException('Функция handleOperation вызвана для неизвестной операции ' + operation + ' с параметром: ' + order);
    }
  }
}