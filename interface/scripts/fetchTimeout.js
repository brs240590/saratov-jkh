function fetchTimeout(url, options, timeout) {
    timeout = timeout || 30 * 1000;
    if (typeof options === 'object') {
        if (!('Pragma' in options)) options.Pragma = 'no-cache';
        if (!('Cache-Control' in options)) options['Cache-Control'] = 'no-store';
    } else options = {
        Pragma: 'no-cache',
        'Cache-Control': 'no-store'
    };
    return Promise.race([
        fetch(url, options),
        new Promise(function (_, reject) {
            setTimeout(function () {
                reject('timeout')
            }, timeout)
        })
    ])
}
