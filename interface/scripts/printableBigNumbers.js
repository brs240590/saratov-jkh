/**
 * Формирует строку для печати
 * @param number <Number|String>
 * @param center <Boolean> нужно ли центрировать последнюю строчку
 * @return {string}
 */
function getPrintableBigNumbers(number, center) {
  let formCharacter = '*'; // из чего будут рисоваться цифра. Длина - 1 символ / цифра
  let checkDigitWidth = 5; // ширина одной цифры на чеке, в символах
  let checkDigitHeight = 7; // высота одной цифры на чеке, в строчках
  let spacesBetweenDigits = 1;
  let maxSymbolsInRow = 41; // максимальное количество символов на одной строке
  let lineBreak = '\r\n';
  let maxDigitsOnCheck = Math.floor(maxSymbolsInRow / (checkDigitWidth + spacesBetweenDigits));

  let spacer = '';
  for (let i = 0; i < maxSymbolsInRow - 4; i++) spacer += ' ';
  spacer += lineBreak;

  if (!(typeof number === 'string' || typeof number === 'number')) throw new TypeError('Передаваемое число должно иметь тип Number или String');
  center = Boolean(center);
  number = String(number);
  if (number.match(/\D/)) throw new TypeError('Переданная строка не является целым числом');
  number = number.split('').map(function (digit) {
    return Number(digit);
  });
  let numbers = [];
  let currentRow;
  number.forEach(function (digit, i) {
    if (!(i % maxDigitsOnCheck)) {
      currentRow = [];
      numbers.push(currentRow);
    }
    currentRow.push(digit);
  });
  let totalStrings = numbers.length;
  let digitsOnLastString = numbers[numbers.length - 1].length;

  // при ручном изменении этого объекта нужно изменять checkDigitWidth и checkDigitHeight соответствующим образом
  const digits = {
    0: [
      ' ### ',
      '#   #',
      '#   #',
      '#   #',
      '#   #',
      '#   #',
      ' ### '
    ],
    1: [
      '  #  ',
      ' ##  ',
      '# #  ',
      '  #  ',
      '  #  ',
      '  #  ',
      '#####'
    ],
    2: [
      ' ### ',
      '#   #',
      '    #',
      ' ### ',
      '#    ',
      '#    ',
      '#####'
    ],
    3: [
      ' ### ',
      '#   #',
      '    #',
      ' ### ',
      '    #',
      '#   #',
      ' ### '
    ],
    4: [
      '#    ',
      '#  # ',
      '#  # ',
      '#  # ',
      '#####',
      '   # ',
      '   # '
    ],
    5: [
      '#####',
      '#    ',
      '#    ',
      '#### ',
      '    #',
      '#   #',
      ' ### '
    ],
    6: [
      ' ### ',
      '#   #',
      '#    ',
      '#### ',
      '#   #',
      '#   #',
      ' ### '
    ],
    7: [
      '#####',
      '#   #',
      '   # ',
      '  #  ',
      '  #  ',
      '  #  ',
      '  #  '
    ],
    8: [
      ' ### ',
      '#   #',
      '#   #',
      ' ### ',
      '#   #',
      '#   #',
      ' ### '
    ],
    9: [
      ' ### ',
      '#   #',
      '#   #',
      ' ####',
      '    #',
      '#   #',
      ' ### '
    ]
  };
  if (String(formCharacter).length === 1) {
    for (let digit in digits) {
      digits[digit] = digits[digit].map(function (row) {
        return row.replace(/#/g, formCharacter);
      })
    }
  }

  let check = '';
  let currentString = 0;
  let spacesBefore;
  let spacesAfter;
  let spacingBefore = '';
  let spacingBetweenDigits = '';
  let spacingAfter = '';
  let isLastString;
  for (let i = 0; i < spacesBetweenDigits; i++) spacingBetweenDigits += ' ';

  numbers.forEach(function (row, i, arr) {
    isLastString = center && i === arr.length - 1;
    if (isLastString) { // если это последняя строчка и нужно центрировать
      let spacingTotal = maxSymbolsInRow - digitsOnLastString * (checkDigitWidth + spacesBetweenDigits);
      if (spacingTotal % 2) {
        spacesBefore = Math.floor(spacingTotal / 2);
        spacesAfter = spacesBefore + 1;
      } else spacesBefore = spacesAfter = spacingTotal / 2;
      for (let i = 0; i < spacesBefore; i++) spacingBefore += ' ';
      for (let i = 0; i < spacesAfter; i++) spacingAfter += ' ';
    }

    let rowString = '';

    for (let i = 0; i < checkDigitHeight; i++) {
      if (isLastString) rowString += spacingBefore;
      row.forEach(function (currentCheckRowNumber) {
        let digit = digits[currentCheckRowNumber][i];
        rowString += digit + spacingBetweenDigits;
      });
      if (isLastString) rowString += spacingAfter;
      rowString += lineBreak;
    }
    check += rowString;
  });

  check = spacer + check;
  check += spacer;
  return check;
}
