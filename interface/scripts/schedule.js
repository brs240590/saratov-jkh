const Schedule = function (options, executor) {
  this.debug = options.debug || false;
  this.debugCheck = 1 && this.debug;
  this.debugCurrentCheck = 0;
  this.announcePause = undefined;
  this.working = false;

  if (window.scheduleManager === undefined) throw new Error('[Очередь] Отсутствует зависимость scheduleManager');
  // Исполняющая функция
  if (executor === undefined) throw new Error('[Очередь] Не передана функция для выполнения платежа');
  else if (typeof executor !== 'function') throw new TypeError('[Очередь] Переданный executor не является функцей');
  else this.executor = executor;
  // Адрес БД
  if ('urlDB' in options) {
    if (typeof options.urlDB !== 'string') throw new TypeError('[Очередь] URL-адрес БД ожидался в виде строки');
    else this.urlDB = options.urlDB;
  } else throw new Error('[Очередь] Не задан URL-адрес базы данных');
  // Имя БД
  if ('database' in options) {
    if (typeof options.database !== 'string') throw new TypeError('[Очередь] Имя БД ожидалось в виде строки');
    else this.database = options.database;
  } else throw new Error('[Очередь] Не задано имя БД');
  // Имя таблицы в БД
  if ('table' in options) {
    if (typeof options.table !== 'string') throw new TypeError('[Очередь] Имя таблицы БД ожидалось в виде строки');
    else this.table = options.table;
  } else throw new Error('[Очередь] Не задано имя таблицы БД');
  // Логин пользователя БД
  if ('username' in options) {
    if (typeof options.username === 'string') this.username = options.username;
    else if (typeof options.username === 'number') this.username = String(options.username);
    else throw new TypeError('[Очередь] Логин пользователя БД ожидался в виде строки или числа');
  } else throw new Error('[Очередь] Не задан логин пользователя БД');
  // Пароль пользователя БД
  if ('password' in options) {
    if (typeof options.password === 'string') this.password = options.password;
    else if (typeof options.password === 'number') this.password = String(options.password);
    else throw new TypeError('[Очередь] Пароль пользователя БД ожидался в виде строки или числа');
  } else throw new Error('[Очередь] Не задан пароль пользователя БД');
  // Адрес PHP-скрипта очереди
  if ('urlBackend' in options) {
    if (typeof options.urlBackend !== 'string') throw new TypeError('[Очередь] URL-адрес скрипта очереди ожидался в виде строки');
    else this.urlBackend = options.urlBackend;
  } else throw new Error('[Очередь] Не задан URL-адрес скрипта очереди');
  // Режим работы очереди
  // parallel - параллельный (по-умолчанию). Запросы с помощью исполняющей функции будут отправляться одновременно
  // sequential - последовательный. Запросы с помощью исполняющей функции будут отправляться последовательно
  this.mode = options.mode || 'parallel';
  if (!(this.mode === 'parallel' || this.mode === 'sequential')) throw new Error('[Очередь] Задан неизвестный режим очереди');
  // Интервал между тактами очереди. В миллисекундах
  if ('delay' in options) {
    if (typeof options.delay === 'number') this.delay = options.delay;
    else throw new TypeError('[Очередь] Задержка ожидалась в виде числа');
  } else this.delay = 2 * 60 * 1000; // 2 минуты по-умолчанию
  // Название очереди. Если работает одновременно несколько очередей, то название поможет быстро различить их
  if ('label' in options) {
    if (typeof options.label === 'string') this.label = options.label;
    else if (typeof options.label === 'number') this.label = String(options.label);
    else this.label = scheduleManager.size + 1;
  } else this.label = scheduleManager.size + 1;
  // Функция для выполнения логирования. По-умолчанию будет использована console.log
  if ('log' in options) {
    if (typeof options.log !== 'function') {
      console.log('[Очередь][Предупреждение] В поле log переданных опций ожидалась функция, получено: ' + typeof options.log +
        '. В качестве логирующей функции будет использоваться console.log');
      this.externalLog = console.log;
    } else this.externalLog = options.log;
  } else {
    console.log('[Очередь][Предупреждение] В опциях не найден параметр log. В качестве логирующей функции будет использоваться console.log');
    this.externalLog = console.log;
  }
  if ('autoStart' in options) {
    this.autoStart = Boolean(options.autoStart);
  } else this.autoStart = false;
  // Пул очереди
  this.items = [];
  // Флаг приостановки очереди
  this.stop = false;

  if (this.debug) {
    this.logWarning('Включён режим отладки очереди');
    if (!'delay' in options) this.delay = 10 * 1000;
  }

  // Добавляем очередь в хранилище очередей
  if (scheduleManager) scheduleManager.add(this.label, this);
  else throw new ReferenceError('Менеджер очередей не был инициализирован. Очередь не может быть создана');

  this.log('Очередь успешно создана');

  if (this.autoStart) this.start();
};

Schedule.prototype.start = function () {
  if (!this.working) {
    this.working = true;
    this.schedule();
    this.log('Очередь запущена' + (this.autoStart ? ' автоматически' : ''));
    return true;
  } else {
    this.log('Очередь уже запущена');
    return false;
  }
};

Schedule.prototype.schedule = function () {
  let that = this;
  if (this.stop) {
    setTimeout(function () {
      that.schedule();
    }, 1000);
  } else {
    this.check()
      .then(function () {
        if (that.items.length) {
          let minutes = 0;
          let minutesDelay = 1;
          let longTimer = setInterval(function () {
            minutes += minutesDelay;
            that.logWarning('Исполняющая функция выполняется долго. Прошло минут с момента вызова: ' + minutes);
          }, minutesDelay * 60 * 1000);
          that.send(that.items)
            .then(function (arrayOfSentID) {
              if (!Array.isArray(arrayOfSentID)) {
                throw new TypeError('Исполняющая функция при успехе вернула не массив');
              } else if (
                !arrayOfSentID.every(
                  function (item) {
                    let type = typeof item;
                    if (type === 'number' || type === 'string') return true;
                    else {
                      that.log('В массиве переданном исполняющей функцией встречен элемент с неожиданным типом: ' + type);
                      return false;
                    }
                  }
                )
              ) {
                throw new TypeError('В массиве переданном исполняющей функцией находятся элементы, не являющиеся строкой или числом');
              } else {
                that.log('Исполняющая функция вернула успех по следующим id: ' + arrayOfSentID.join(', '));
                that.remove(arrayOfSentID);
                scheduleNextCheck();
              }
            }, function (reason) {
              that.logWarning('Исполняющая функция вернула неуспех' + (reason !== undefined ? '. Причина: ' + reason : ''));
              scheduleNextCheck();
            })
            .catch(function (error) {
              throw error;
            })
            .finally(function () {
              clearInterval(longTimer);
            })
        } else {
          scheduleNextCheck();
        }
      }, function (reason) {
        that.pause(reason);
      })
      .catch(function (error) {
        that.pause(error);
      });
  }

  function scheduleNextCheck() {
    setTimeout(function () {
      that.schedule();
    }, that.delay);
  }
};

Schedule.prototype.check = function () {
  let that = this;
  this.debugCurrentCheck++;
  if (this.debugCheck) this.log('Проверка №' + this.debugCurrentCheck);
  return new Promise(function (resolve, reject) {
    let body = {
      action: 'check',
      url: that.urlDB,
      database: that.database,
      table: that.table,
      username: that.username,
      password: that.password,
    };
    let options = {
      method: 'post',
      headers: {
        'Pragma': 'no-cache',
        'Cache-Control': 'no-store',
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      body: that.param(body)
    };
    fetchTimeout(that.urlBackend, options)
      .then(function (httpResponse) {
        if (httpResponse.ok) {
          httpResponse.text()
            .then(function (response) {
              let text = response;
              try {
                response = JSON.parse(response);
                if (response.status) {
                  if (Array.isArray(response.content)) {
                    that.items = response.content;
                    that.log('Получено элементов из очереди:' + response.content.length);
                    resolve()
                  } else {
                    that.log('Очередь пуста');
                    resolve();
                  }
                } else reject('Ошибка на проверке: ' + response.error);
              } catch (e) {
                reject('Ошибка на проверке при чтении JSON: ' + text);
              }
            })
            .catch(function (error) {
              reject('Ошибка на проверке при чтении JSON: ' + error);
            });
        } else reject('HTTP-ответ от скрипта очереди: ' + httpResponse.status + ' ' + httpResponse.statusText);
      }, function (reason) {
        resolve('Запрос на проверку не выполнен по причине: ' + reason + '. Причиной ожидается таймаут, очередь не будет приостановлена');
      })
      .catch(function (error) {
        reject('Ошибка fetch на проверке:' + error);
      });
  });
};

Schedule.prototype.send = function () {
  let that = this;
  return new Promise(function (resolve, reject) {
    let promise = that.executor(that.items);
    if (typeof promise.then === 'function') {
      promise
        .then(function (arrayOfSentID) {
          resolve(arrayOfSentID);
        }, function (reason) {
          if (reason !== undefined) {
            reject(reason)
          } else reject();
        })
        .catch(function (error) {
          let str = 'Ошибка при проведении оплаты: ' + error;
          that.logException(str);
          that.stop = true;
          reject(str);
        });
    } else throw new Error('Исполняющая функция вернула не Promise: (тип: ' + typeof promise + ', значение: ' + promise);
  });
};

Schedule.prototype.remove = function (arrayOfSentID) {
  let that = this;
  let removeRequests = [];

  if (arrayOfSentID.length) this.log('Удаляем из очереди элемент' + (arrayOfSentID.length === 1 ? '' : 'ы') + ' с id: ' + arrayOfSentID.join(', '));
  else this.logWarning('Массив переданный на удаление пуст');

  arrayOfSentID.forEach(function (id) {
    removeRequests.push(new Promise(function (resolve) {
      let body = {
        action: 'remove',
        url: that.urlDB,
        database: that.database,
        table: that.table,
        username: that.username,
        password: that.password,
        id: id
      };
      let options = {
        method: 'POST',
        headers: {
          'Pragma': 'no-cache',
          'Cache-Control': 'no-store',
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        body: that.param(body)
      };
      fetchTimeout(that.urlBackend, options)
        .then(function (httpResponse) {
            httpResponse.text()
              .then(function (response) {
                let text = response;
                try {
                  response = JSON.parse(response);
                  if (response.status) {
                    that.log('Элемент с id ' + id + ' успешно удалён из очереди в БД');
                    let index = that.items.indexOf(function (item) {
                      return item === id;
                    });
                    if (index === undefined) that.logWarning('В локальной очереди уже отсутствует элемент с id ' + id + ', удалённый из БД. Пропускаем удаление');
                    else {
                      that.items.splice(index, 1);
                      that.log('Элемент с id ' + id + ' удалён из локальной очереди');
                    }
                    resolve({ id: id, status: 1 });
                  } else {
                    that.logException('Элемент с id ' + id + ' не был удалён из очереди БД по причине: ' + response.error);
                    resolve({ id: id, status: 0, reason: response.error });
                  }
                } catch (error) {
                  that.logException('Элемент с id ' + id + ' не был удалён из очереди БД из-за невалидного JSON: ' + text);
                  resolve({ id: id, status: 0, reason: error });
                }
              })
              .catch(function (error) {
                  that.logException('Элемент с id ' + id + ' не был удалён из очереди БД из-за невалидного JSON: ' + error);
                  resolve({ id: id, status: 0, reason: error });
                }
              );
          }, function (reason) {
            that.logWarning('Элемент с id ' + id + ' не был удалён из очереди БД: ' + reason + '. Причиной ожидается таймаут, очередь не будет приостановлена');
            resolve({ id: id, status: 0, reason: reason });
          }
        )
        .catch(function (e) {
          that.logException('Ошибка fetch при попытке удаления элементат с id ' + id + ': ' + e);
          resolve({ id: id, status: 0, reason: e });
        });
    }));
  });
  Promise.all(removeRequests)
    .then(function (results) {
      let finished = results.filter(function (item) {
        return item.status;
      });
      let finishedCount = finished.length;
      if (finishedCount) {
        finished = finished.map(function (item) {
          return item.id;
        }).join(', ');
        that.log('Успешно удалены ' + finishedCount + ' элементов: ' + finished);
      }
      let failed = results.filter(function (item) {
        return !item.status;
      });
      let failedCount = results.length - finished.length;
      if (failedCount > 0) {
        failed = failed.map(function (item) {
          return item.id + ' ' + item.reason;
        }).join('\n');
        that.logWarning('Не удалены ' + failedCount + ' элементов:\n' + failed);
      }
    })
    .catch(function (error) {
      that.logException('Ошибка при отправке запросов на удаление выполненных запросов: ' + error);
    })
};

Schedule.prototype.purge = function (reason) {
  let that = this;
  let body = {
    action: 'purge',
    url: that.urlDB,
    database: that.database,
    table: that.table,
    username: that.username,
    password: that.password,
  };
  let options = {
    method: 'post',
    headers: {
      'Pragma': 'no-cache',
      'Cache-Control': 'no-store',
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    body: that.param(body)
  };
  this.pause('Очистка очереди' + (reason ? '(' + reason + ')' : ''));

  return new Promise(function (resolve, reject) {
    fetchTimeout(that.urlBackend, options)
      .then(function (httpResponse) {
          httpResponse.json()
            .then(function (response) {
              let str;
              if (response.status) {
                str = 'Очередь очищена';
                that.log(str);
                resolve(str);
              } else {
                str = 'Ошибка очистки очереди на бэкенде: ' + response.error;
                that.logException(str);
                reject(str);
              }
            })
            .catch(function (error) {
                let str = 'Ошибка очистки очереди ' + error;
                that.logException(str);
                reject(str);
              }
            );
        }, function (reason) {
          let str = 'Очередь не очищена по причине: ' + reason + '. Причиной ожидается таймаут, очередь не будет приостановлена';
          that.logWarning(str);
          reject(str);
        }
      )
      .catch(function (e) {
        let str = 'Ошибка fetch на добавлении элемента:' + e;
        that.logException(str);
        reject(str);
      })
      .finally(function () {
        that.resume();
      });
  })
};

Schedule.prototype.add = function (object) {
  let that = this;
  let id;
  let json;
  if (typeof object !== 'object') {
    return responseError('На добавление в очередь ожидался объект');
  } else {
    if (!('id' in object)) {
      return responseError('В объекте на добавление в очередь отсутствует поле id');
    } else if (!(typeof object.id === 'number' || typeof object.id === 'string')) {
      return responseError('Переданное поле id ожидалось в виде числа или строки')
    } else id = object.id;
    if (!('json' in object)) {
      this.logWarning('В объекте на добавление в очередь отсутствует поле json. В поле JSON будет передана строка \'empty\'');
      json = object.json = 'empty';
    } else if (typeof object.json !== 'string') {
      return responseError('Переданное поле json ожидалось в виде строки');
    } else json = object.json;
    let body = {
      action: 'add',
      id: id,
      json: json,
      url: that.urlDB,
      database: that.database,
      table: that.table,
      username: that.username,
      password: that.password,
    };
    let options = {
      method: 'post',
      headers: {
        'Pragma': 'no-cache',
        'Cache-Control': 'no-store',
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      body: that.param(body)
    };
    return new Promise(function (resolve) {
      fetchTimeout(that.urlBackend, options)
        .then(function (httpResponse) {
            httpResponse.json()
              .then(function (response) {
                if (response.status) {
                  let str = 'Элемент с id ' + id + ' успешно добавлен в очередь БД';
                  that.log(str);
                  resolve({ id: id, status: 1, message: str });
                } else {
                  let str = 'Ошибка при попытке добавить элемент с id ' + id + ' в очередь: ' + response.error;
                  that.logException(str);
                  resolve({ id: id, status: 0, error: str });
                }
              })
              .catch(function (error) {
                  let str = 'Ошибка JSON при попытке добавить элемент с id ' + id + ' в очередь: ' + error;
                  that.logException(str);
                  resolve({ id: id, status: 0, error: str });
                }
              );
          }, function (reason) {
            let str = 'Запрос на добавление элемента с id ' + id + ' не выполнен по причине: ' + reason + '. Причиной ожидается таймаут, очередь не будет приостановлена';
            that.logWarning(str);
            resolve({ id: id, status: 0, error: str });
          }
        )
        .catch(function (e) {
          let str = 'Ошибка fetch на добавлении элемента:' + e;
          that.logException(str);
          resolve({ id: id, status: 0, error: str });
        });
    })
  }

  function responseError(str) {
    this.logException(str);
    return Promise.reject({ id: id || null, status: 0, error: str });
  }
};

Schedule.prototype.pause = function (reason) {
  let that = this;
  this.stop = true;
  let str = 'Очередь приостановлена' + (reason !== undefined ? '. Причина: ' + reason : '');
  this.logWarning(str);
  this.announcePause = setInterval(function () {
    that.log(str);
  }, 20 * 1000);

};

Schedule.prototype.resume = function () {
  this.stop = false;
  clearInterval(this.announcePause);
  this.log('Очередь восстановлена');
};

Schedule.prototype.log = function (message) {
  try {
    this.externalLog('[Очередь "' + this.label + '"] ' + message);
  } catch (e) {
    console.log('Ошибка при отправке сообщения \'' + message + '\' в переданную функцию логирования: ' + e)
  }
};

Schedule.prototype.logWarning = function (message) {
  try {
    this.externalLog('[Очередь "' + this.label + '"][Предупреждение] ' + message);
  } catch (e) {
    console.log('Ошибка при отправке сообщения \'' + message + '\' в переданную функцию логирования: ' + e)
  }
};

Schedule.prototype.logException = function (message) {
  try {
    this.externalLog('[Очередь "' + this.label + '"][Исключение] ' + message);
  } catch (e) {
    console.log('Ошибка при отправке сообщения \'' + message + '\' в переданную функцию логирования: ' + e)
  }
};

Schedule.prototype.param = function (obj) {
  if (typeof obj !== 'object') throw new TypeError('[Очередь ' + this.label + '] Метод param: Переданный параметр ожидался в виде объекта');
  else return Object.keys(obj).map(function (key) {
    return encodeURIComponent(key) + '=' + encodeURIComponent(obj[key])
  }).join('&');
};

if (typeof fetchTimeout === 'undefined') {
  window.fetchTimeout = function (url, options, timeout) {
    if (!timeout) timeout = 30 * 1000;
    return Promise.race([
      fetch(url, options),
      new Promise(function (_, reject) {
        setTimeout(function () {
          reject('timeout')
        }, timeout)
      })
    ])
  }
}

(function () {
    if ('scheduleManager' in window) console.log('Менеджер очереди не смог запуститься, т.к. переменная scheduleManager уже занята');
    else {
      window.scheduleManager = {
        list: new Map(),
        size: 0,
        externalLog: undefined,
        add: function (label, scheduleObject) {
          if (this.list.has(label)) this.list.set(this.list.size + 1, scheduleObject);
          else this.list.set(label, scheduleObject);
          this.size = this.list.size;
          scheduleObject.log('');
          if (this.externalLog === undefined) this.externalLog = scheduleObject.externalLog;
          this.log('Очередь "' + scheduleObject.label + '" помещена в список очередей. Всего очередей: ' + scheduleManager.size);
        },
        get: function (label) {
          if (this.list.has(label)) return this.list.get(label);
          else {
            this.log('Запрошена очередь с неизвестным названием: ' + label);
            return 'Нет очереди с таким названием';
          }
        },
        getAll: function () {
          return this.list;
        },
        getAllLabels: function () {
          if (Map.prototype.keys) return Array.from(this.list.keys());
          else {
            let array = [];
            this.list.forEach(function (_, key) {
              array.push(key);
            });
            return array;
          }
        },
        log: function (message) {
          this.externalLog('[Менеджер очередей] ' + message);
        }
      }
    }
  }
)();