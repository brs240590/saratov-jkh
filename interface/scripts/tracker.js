const Tracker = function () {
  this.data = undefined;
  this.url = 'http://localhost/sandbox/metalprofile/tracker/tracker.php';
  this.cashoutURL = 'http://localhost/sandbox/metalprofile/cashout.php';
};

Tracker.prototype.get = function () {
  this.logDebug('Получение данных');
  let that = this;
  return new Promise(function (resolve, reject) {
    let body = {
      action: 'get'
    };
    let options = {
      method: 'post',
      headers: {
        'Pragma': 'no-cache',
        'Cache-Control': 'no-store',
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: $.param(body)
    };

    fetchTimeout(that.url, options)
      .then(function (response) {
        if (response.ok) {
          return response.json().then(function (obj) {
            if (obj.status) resolve(obj.data);
            else reject(obj.error);
          })
        } else reject('Tracker.get получил ответ ' + response.status + ' ' + response.statusText);
      })
      .catch(function (e) {
        that.logException('Tracker.get: ' + e);
        reject(e);
      });
  })
};

Tracker.prototype.check = function (data) {
  this.logDebug('Проверка полученных данных: ' + JSON.stringify(data));
  let that = this;
  return new Promise(function (resolve, reject) {
    let systemString = 'systemMenuTriggered' in data ? data.systemMenuTriggered : '';
    let skudString = 'skudPassChecked' in data ? data.skudPassChecked : '';
    let systemTime = systemString ? Number(systemString.substring(0, systemString.length - 3)) : -100;
    let skudTime = skudString ? Number(skudString.substring(0, skudString.length - 3)) : -50;
    // logDebug('\nsystemTime(' + systemTime + ', ' + tracker.convertToHMS(systemTime) + ') - \n__skudTime(' + skudTime + ', ' + tracker.convertToHMS(skudTime) + ') = \n___________' + (systemTime - skudTime));
    if (Math.abs(systemTime - skudTime) < 10) { // разница в секундах между успешным вводом скуда и открытием системного меню
      that.log('Было открыто системное меню');
      resolve(data);
    } else reject('Системное меню НЕ было открыто после предыдущей загрузки интерфейса');
  })
};

Tracker.prototype.saveEvent = function (key, value) {
  this.logDebug('Сохраняем событие ' + key + ' со значением ' + value);
  let that = this;
  return new Promise(function (resolve, reject) {
    let body = {
      action: 'get'
    };
    let options = {
      method: 'post',
      headers: {
        'Pragma': 'no-cache',
        'Cache-Control': 'no-store',
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: $.param(body)
    };

    fetchTimeout(that.url, options)
      .then(function (response) {
        return response.text();
      })
      .then(function (json) {
        that.logDebug('Получено: ' + json);
        let obj = JSON.parse(json).data;
        obj[key] = value;

        body = {
          action: 'put',
          json: JSON.stringify(obj)
        };
        options.body = $.param(body);

        fetchTimeout(that.url, options).then(function (response) {
          response.json().then(function (response) {
            that.logDebug('Отправлено ' + JSON.stringify(response));
            if (response.status) resolve(obj);
            else reject('Не удалось сохранить событие ' + key + ' со значением ' + value);
          });
        });
      })
      .catch(function (e) {
        that.logException(e);
      });
  })
};

Tracker.prototype.send = function (obj) {
  this.logDebug('Отправляем данные');
  let that = this;
  let systemMenuTime = obj.systemMenuTriggered;
  let skudPassTime = obj.skudPassChecked;
  let date = new Date();
  let offset = date.getTimezoneOffset() * -1;
  let ms = date.getTime() + offset * 60 * 1000;
  date = new Date(ms);
  let nextDate = new Date(ms + 60 * 1000);
  let outTime = date.toISOString().replace(/\..+/, '');
  let inTime = nextDate.toISOString().replace(/\..+/, '');
  let promises = [];

  let data = {};
  // alert(xml2json.parser(extGetKeyForInterface('DispensorDenominationStructure')));
  let settings = {
    dispenser: xml2json.parser(extGetKeyForInterface('DispensorDenominationStructure')).arrayofdispensordenominationstructure,
    hopper: xml2json.parser(extGetKeyForInterface('HopperDenominationStructure')).arrayofdispensordenominationstructure,
    inkas: xml2json.parser(extGetKeyForInterface('Bills')).arrayofdenominationstructure,
  };
  let savedSettings = extGetKeyForInterface('savedSettings');
  let settingsJSON = JSON.stringify(settings);
  let savedSettingsJSON = savedSettings;

  if (savedSettings.match(/\/\/\/\//) !== null || savedSettings.match(/\\\\\\\\/) !== null) {
    savedSettings = '';
    extSetKeyFromInterface('savedSettings', '');
  }

  let DEBUG = 0;

  let debugSettings = '{"dispenser":{"dispensordenominationstructure":[{"nominal":100,"count":2,"rejectedcount":0,"box":2},{"nominal":50,"count":5,"rejectedcount":0,"box":1}]},"hopper":{"dispensordenominationstructure":[{"nominal":1,"count":5,"rejectedcount":0,"box":0},{"nominal":2,"count":3,"rejectedcount":0,"box":0}]},"inkas":{"denominationstructure":[{"nominal":20,"count":1},{"nominal":30000,"count":2},{"nominal":99000,"count":2},{"nominal":1,"count":8}]}}';
  let debugSavedSettings = '{"dispenser":{"dispensordenominationstructure":[{"nominal":100,"count":2,"rejectedcount":0,"box":2},{"nominal":50,"count":5,"rejectedcount":0,"box":1}]},"hopper":{"dispensordenominationstructure":[{"nominal":1,"count":5,"rejectedcount":0,"box":0},{"nominal":2,"count":3,"rejectedcount":0,"box":0}]}}';


  if (DEBUG) {
    this.logException('ВКЛЮЧЕНЫ ОТЛАДОЧНЫЕ ДАННЫЕ ПО СОДЕРЖИМОМУ ОБОРУДОВАНИЯ. ЭТОГО СООБЩЕНИЯ НЕ ДОЛЖНО БЫТЬ НА БОЕВОМ ТЕРМИНАЛЕ!');
    this.logException('ВКЛЮЧЕНЫ ОТЛАДОЧНЫЕ ДАННЫЕ ПО СОДЕРЖИМОМУ ОБОРУДОВАНИЯ. ЭТОГО СООБЩЕНИЯ НЕ ДОЛЖНО БЫТЬ НА БОЕВОМ ТЕРМИНАЛЕ!');
    this.logException('ВКЛЮЧЕНЫ ОТЛАДОЧНЫЕ ДАННЫЕ ПО СОДЕРЖИМОМУ ОБОРУДОВАНИЯ. ЭТОГО СООБЩЕНИЯ НЕ ДОЛЖНО БЫТЬ НА БОЕВОМ ТЕРМИНАЛЕ!');
    settings = JSON.parse(debugSettings);
    settingsJSON = debugSettings;
    savedSettings = JSON.parse(debugSavedSettings);
    savedSettingsJSON = debugSavedSettings;
  }

  logDebug('Текущее содержимое оборудования:\n' + settingsJSON + '\nСохранённое содержимое оборудования:\n' + savedSettingsJSON);
  if (savedSettings === '') {
    extSetKeyFromInterface('savedSettings', settingsJSON);
    this.log('Нет предыдущего сохранённого состояния оборудования. Предыдущее состояние сохранено. Запросы отправлены не будут');
  } else {
    if (!DEBUG) savedSettings = JSON.parse(savedSettings);

    if (settingsJSON !== savedSettingsJSON) {
      this.log('Содержимое оборудования отличается');
      data.dispenser = getDifferenceInDispenser(settings.dispenser, savedSettings.dispenser);
      data.hopper = getDifferenceInHopper(settings.hopper, savedSettings.hopper);
      data.inkas = getInkasDifference(settings.inkas, savedSettings.inkas);
      extSetKeyFromInterface('savedSettings', settingsJSON);
      logDebug('Рассчитана разница по содержимому: ' + JSON.stringify(data));

      Object.keys(data).forEach(function (deviceKey) {
        let device = data[deviceKey];
        if (Object.keys(device).length) {
          Object.keys(device).forEach(function (sumKey, i) {
            let sum = device[sumKey];
            let type = sumKey === 'in' ? 'in' : 'out';
            let dataInfo = {
              Amount: sum,
              Type: type,
              Date: type === 'out' ? outTime : inTime
            };

            let body = {
              url: serviceData.serverURL,
              login: serviceData.serverUsername,
              password: serviceData.serverPassword,
              transaction: extGenerateGUID(),
              data: JSON.stringify(dataInfo)
            };
            logDebug('body.data ' + body.data);
            let options = {
              method: 'post',
              headers: {
                'Pragma': 'no-cache',
                'Cache-Control': 'no-store',
                'Content-Type': 'application/x-www-form-urlencoded',
              },
              body: $.param(body)
            };
            logDebug('\nPromise #' + i + ' info:\n' + (options.body.replace(/&/g, '\n')));
            promises.push(new Promise(function (resolve) {
              fetchTimeout(that.cashoutURL, options).then(function (response) {
                if (response.ok) {
                  response.json().then(function (obj) {
                    // alert(JSON.stringify(options.body));
                    // alert(obj);
                    // alert(JSON.stringify(obj));
                    that.logDebug('Promise #' + i + ': ' + JSON.stringify(obj));
                    if (obj.status) {
                      // alert(JSON.stringify(obj));
                      let body = obj.body.return;
                      if (!body.ResultState) {
                        that.logDebug('Запрос #' + i + ' успешно завершён (№ транзакции ' + body.transaction + ')');
                        resolve('success');
                      } else onRejected(body.Message);
                    } else {
                      // alert(JSON.stringify(obj.body));
                      that.logException('Запрос #' + i + ', отрицательный результат из PHP: ' + JSON.stringify(obj));
                      onRejected('Запрос #' + i + ', отрицательный результат из PHP: ' + JSON.stringify(obj));
                    }
                  }).catch(onRejected);
                } else onRejected('Promise #' + i + ' код ответа: ' + response.status);
              }).catch(onRejected);

              function onRejected(msg) {
                // alert('onrejected');
                resolve('#' + i + '. Данные запроса: № транзакции ' + body.transaction +
                  ', информация по оборудованию: ' + body.data + '(' + JSON.stringify(msg) + ')');
              }
            }));
          });
        } else that.log('В ' + deviceKey + ' ничего не изменилось или там пусто');
      });

      if (promises.length) {
        Promise.all(promises).then(function (resultsArray) {
          let array = resultsArray.filter(function (item) {
            return item !== 'success'
          });
          if (array.length) {
            let reportStr;
            if (array.length > 1) {
              reportStr = '\nНеудавшиеся запросы:';
              array.forEach(function (item) {
                reportStr += '\n' + item;
              });
              that.logException(reportStr);
            } else reportStr = 'Неудавшийся запрос: ' + array[0];
          } else that.log('Все запросы в удалённое API успешно выполнены');
        });
      } else that.logWarning('Неожиданное поведение отслеживания. JSON состояния оборудования отличался, но запросы не были сформированы');
    } else that.log('Содержимое отслеживаемого оборудования не изменилось');
  }

  function getDifferenceInDispenser(current, saved) {
    let sums = {
      in: 0,
      out: 0,
      rejected: 0
    };
    if ('dispensordenominationstructure' in current) {
      // logDebug('--current--');
      let structure = current.dispensordenominationstructure;
      if (!Array.isArray(structure)) structure = [structure];
      // alert('getDifferenceInDispenser current lenght ' + structure.length);
      structure.forEach(function (item) {
        sums.in += item.nominal * item.count;
        // logDebug('nominal ' + item.nominal + ' count ' + item.count);
      });
      that.logDebug('Сейчас в диспенсоре: ' + sums.in);
    } else that.logDebug('Сейчас в диспенсоре ничего нет');
    if ('dispensordenominationstructure' in saved) {
      // logDebug('--saved--');
      let structure = saved.dispensordenominationstructure;
      if (!Array.isArray(structure)) structure = [structure];
      structure.forEach(function (item, i) {
        sums.out += item.nominal * item.count;
        sums.rejected += item.nominal * item.rejectedcount;
        // logDebug('nominal ' + item.nominal + ' count ' + item.count);
      });
      that.logDebug('В диспенсоре было: ' + sums.out);
    } else that.logDebug('В диспенсоре ничего не было');
    // logDebug('getDifferenceInDispenser: ' + JSON.stringify(sums));
    if (sums.in === sums.out) {
      sums.in = 0;
      sums.out = 0;
    }

    Object.keys(sums).forEach(function (key) {
      if (!sums[key]) delete sums[key];
    });

    return sums;
  }

  function getDifferenceInHopper(current, saved) {
    let disableOut = 1; // Отключаем отправку суммы, которая была в хоппере, так как эта сумма будет учтена на отправке инкассации

    let sums = {
      in: 0,
      out: 0
    };
    if ('dispensordenominationstructure' in current) {
      let structure = current.dispensordenominationstructure;
      if (!Array.isArray(structure)) structure = [structure];
      // alert('getDifferenceInHopper current lenght ' + structure.length);
      structure.forEach(function (item, i) {
        // if (i ===0) alert('sums.in ' + typeof sums.in + ' ' + sums.in + ' item.nominal ' + typeof item.nominal + ' ' + item.nominal + ' item.count ' + typeof item.count + ' ' + item.count);
        sums.in += item.nominal * item.count;
      });
      that.logDebug('Сейчас в хоппере: ' + sums.in);
    } else that.logDebug('Сейчас в хоппере ничего нет');
    if (disableOut) {
      if ('dispensordenominationstructure' in saved) {
        let structure = saved.dispensordenominationstructure;
        if (!Array.isArray(structure)) structure = [structure];
        structure.forEach(function (item) {
          sums.out += item.nominal * item.count;
        });
        that.logDebug('В хоппере было: ' + sums.out);
      } else that.logDebug('В хоппере ничего не было');
    }
    // logDebug('getDifferenceInHopper: ' + JSON.stringify(sums));
    if (sums.in === sums.out) {
      sums.in = 0;
      if (!disableOut) sums.out = 0;
    }

    Object.keys(sums).forEach(function (key) {
      if (sums[key] === 0) delete sums[key];
    });

    return sums;
  }

  function getInkasDifference(current, saved) {
    let sums = {
      out: 0
    };
    if (!saved || JSON.stringify(current) !== JSON.stringify(saved)) {
      if ('denominationstructure' in current) {
        let structure = current.denominationstructure;
        if (!Array.isArray(structure)) structure = [structure];
        structure.forEach(function (item, i) {
          // if (i ===0) alert(item + ' ' + JSON.stringify(item));
          sums.out += (item.nominal === 11 ? 10 : item.nominal) * item.count;
        });
        that.log('Сумма по инкассации: ' + sums.out);
      } else that.logWarning('Отсутствует структура по инкассации');
    } else that.log('Структура инкассации не изменилась');
    Object.keys(sums).forEach(function (key) {
      if (sums[key] === 0) delete sums[key];
    });

    return sums;
  }
};

Tracker.prototype.log = function (message) {
  extSaveLog('[Отслеживание] ' + message);
};

Tracker.prototype.logWarning = function (message) {
  extSaveLog('[Отслеживание][Предупреждение] ' + message);
};

Tracker.prototype.logException = function (message) {
  extSaveLog('[Отслеживание][Исключение] ' + message);
};

Tracker.prototype.logDebug = function (message) {
  extSaveLog('[Отслеживание][Отладка] ' + message);
};

Tracker.prototype.convertToHMS = function (unix) {
  if (String(unix).length === 10) unix += '000';
  unix = Number(unix);
  let date = new Date(unix);
  return date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
};
