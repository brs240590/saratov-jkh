function transliterate(from, to, string) {
  let literals = {
    'ru': ['Я','я','Ю','ю','Ч','ч','Ш','ш','Щ','щ','Ж','ж','А','а','Б','б','В','в','Г','г','Д','д','Е','е','Ё','ё','З','з','И','и','Й','й','К','к','Л','л','М','м','Н','н','О','о','П','п','Р','р','С','с','Т','т','У','у','Ф','ф','Х','х','Ц','ц','Ы','ы','Ь','ь','Ъ','ъ','Э','э'],
    'en': ['Ya','ya','Yu','yu','Ch','ch','Sh','sh','Sh','sh','Zh','zh','A','a','B','b','V','v','G','g','D','d','E','e','E','e','Z','z','I','i','J','j','K','k','L','l','M','m','N','n','O','o','P','p','R','r','S','s','T','t','U','u','F','f','H','h','C','c','Y','y','skip','skip','skip','skip','E', 'e']
  };
  if (typeof string !== "string" || !(from in literals) || !(to in literals)) return false;
  else {
    string = string.replace(/[^\w{а-яА-Я} ]+/g, '');
    let doubleSpaces = string.match(/ {2}/);
    while(doubleSpaces) {
      string = string.replace(/ {2}/g, ' ');
      doubleSpaces = string.match(/ {2}/);
    }
    string = string.replace(/ /g, '_');
    literals[from].forEach(function(letter,i) {
      if (letter !== 'skip') {
        let regex = new RegExp(letter, 'g');
        let replacement = literals[to][i] === 'skip' ? '' : literals[to][i];
        string = string.replace(regex, replacement);
      }
    });
    return string;
  }
}
