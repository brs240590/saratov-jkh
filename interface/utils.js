/**
 * Возвращает строковый формат ошибки
 * @param obj {Object} Axios reject response
 * @return {string}
 */
function formatErrorResponse(obj) {
  if ('errors' in obj) {
    let str = '[Ошибка API] ';
    if ('code' in obj.errors) str += obj.errors.code + ': ';
    if ('message' in obj.errors) str += obj.errors.message;
    return str;
  } else return JSON.stringify(obj);
}

function copyJsonObj(obj) {
  return JSON.parse(JSON.stringify(obj));
}

/**
 * конструктор корзины
 * @param obj <Object>
 * @constructor
 */
function Cart(obj) {
  if (obj) Object.assign(this, JSON.parse(JSON.stringify(obj)));
  else {
    this.size = 0;
    this.sum = 0;
    this.list = {};
  }
}

/**
 * добавить товара в корзину
 * @param id - ID товара
 * @param origin - источник запроса
 * @param amount - количество
 */
Cart.prototype.add = function (id, origin, amount) {
  // alert(Array.from(arguments).join(', '));
  id = Number(id);
  amount = amount || 1;

  let obj;
  if (id in this.list) {
    obj = this.list[id];
    obj.amount += amount;
    let sum = obj.price * amount;
    this.sum += sum;
  } else {
    this.list[id] = obj = {
      price: data.FixedPrice,
      id: id,
      description: data.Information,
      name: data.Name,
      amount: amount,
    };
    this.sum += obj.price * amount;
  }
  obj.sum = obj.price * obj.amount;

  if (this.size === 0) extStartPayCycle(obj.id);

  this.size += amount;
  this.update(id, origin);

  logStep('Добавление в корзину: ' + obj.name + ' (id: ' + obj.id + (amount && amount > 1 ? ', кол-во ' + amount + ' шт.' : '') + ')');
};

Cart.prototype.toggle = function (id) {
  if (id in this.list) {
    logStep('Элемент с id ' + id + ' удалён из корзины');
    delete this.list[id];
    this.size -= 1;
    this.update('toggle');
    return false;
  } else {
    let obj = stepsData.services.find(function (service) {
      return service.id === id;
    });
    if (obj) {
      this.list[id] = copyJsonObj(obj);
      this.size += 1;
      this.update('toggle');
      return true;
    } else {
      logException('Элемент с таким ID не найден в списке услуг');
      this.update('toggle');
      return false;
    }
  }
};

/**
 * удалить товар из корзины
 * @param id - ID товара
 * @param origin - источник запроса
 */
Cart.prototype.remove = function (id, origin) {
  id = Number(id);
  // alert(JSON.stringify(this.list));
  // alert((id in this.list) + '\n' + id+' '+typeof id);
  if (this.size) {
    if (id in this.list) {
      let obj = this.list[id];
      obj.sum -= obj.price;
      this.sum -= obj.price;
      if (this.list[id].amount > 1) {
        obj.amount--;
      } else delete this.list[id];
      // alert();
      this.size--;
      logStep('Из корзины удалён товар ' + obj.name + ' (id: ' + obj.id + ')')
    } else logException('Попытка удалить отсутствующий товар с ID ' + id);
  } else logException('Попытка удалить товар c ID ' + id + ' из пустой корзины');
  this.update(id, origin);
};

/**
 * удалить позицию товара из корзины
 * @param id - ID товара
 */
Cart.prototype.clear = function (id) {
  id = Number(id);
  let obj = this.list[id];
  if (id in this.list) {
    logStep('Удаление позиции ' + obj.name);
    this.sum -= obj.sum;
    this.size -= obj.amount;
    delete this.list[id];
    this.update(id, 'cart');
  } else logException('Попытка удалить отсутствующую позицию товара с ID ' + id);
};

/**
 * очистить корзину
 */
Cart.prototype.purge = function (obj) {
  this.size = 0;
  this.sum = 0;
  this.list = {};
  this.update();
  if (obj) logStep('Корзина очищена ' + (typeof obj === 'object' && 'reason' in obj ? 'по причине ' + obj.reason : 'без указания причины'));
  else logStep('Корзина очищена без указания причины');
};

/**
 * обновить значения по корзине на странице
 */
Cart.prototype.update = function (origin) {
  if (origin) logDebug('Корзина обновлена по причине: ' + origin);
  this.size ? UI.next.show() : UI.next.hide();
};

/**
 * получить количество товара в корзине
 * @param id - ID товара
 */
Cart.prototype.amount = function (id) {
  id = Number(id);
  if (id in this.list) return this.list[id];
  else return 0;
};

/**
 * получить массив с ID товаров в корзине
 */
Cart.prototype.keys = function () {
  return Object.keys(this.list);
};

function prepareSecondsTime(seconds) {
  seconds = Number(seconds);
  if (isNaN(seconds)) return false;
  else return {
    hours: Math.floor(seconds / 3600),
    minutes: Math.floor(seconds % 3600 / 60),
  };
}

/**
 * Получить сообщение об ошибке
 * @param key {string}
 * @param defaultMessage {string} - сообщение по-умолчанию
 * @return {string}
 */
function getErrorMessage(key, defaultMessage) {
  return key in errorMessages ? errorMessages[key] : defaultMessage || 'Ошибка';
}

function getDatetime() {
  let date = getDateObj();
  return date.year + '-' + date.month + '-' + date.day + ' ' + date.hours + ':' + date.minutes + ':' + date.seconds;
}

function getDateObj() {
  let date = new Date();
  let day = date.getDate();
  day = day < 10 ? '0' + day : day;
  let month = date.getMonth() + 1;
  month = month < 10 ? '0' + month : month;
  let year = date.getFullYear();
  let seconds = date.getSeconds();
  seconds = seconds < 10 ? '0' + seconds : seconds;
  let minutes = date.getMinutes();
  minutes = minutes < 10 ? '0' + minutes : minutes;
  let hours = date.getHours();
  hours = hours < 10 ? '0' + hours : hours;

  return {
    day: day,
    month: month,
    year: year,
    seconds: seconds,
    minutes: minutes,
    hours: hours,
  };
}

/**
 * Возвращает случайное целое число между min (включительно) и max (не включая max)
 * @param min минимум
 * @param max максимум
 * @return {Number}
 */
function getRandomIntInRange(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}

function setGoodsList(list) {
  logStep('Устанавливаем GoodsList' + (list ? ' через параметр' : ''));

  let name = serviceData.custom.currentContractor.SP10107 ? "Оплата за обучение" : serviceData.custom.currentContractor.SP9723;

  let goodsList = [{
    Department: 1,
    Name: name,
    Price: stepsData.totalSum,
    Quantity: 1,
    Tax1: 1,
    Tax2: 0,
    Tax3: 0,
    Tax4: 0,
    PaymentTypeSign: 4,
    PaymentItemSign: 1,
  }];

  let goodsListJSON = JSON.stringify(goodsList);
  extAddPaymentParameters("GoodsList", goodsListJSON);

  serializeSaratovData();

  if (serviceData.custom.currentContractor.SP10107) {
    extAddCheckParameter("Платёжный агент", "СарРЦ ООО");
    extAddCheckParameter("ИНН", "6450938172");
    extAddCheckParameter("Договор", "№ 169/2015-1 от 30.01.15");
    extAddCheckParameter("Адрес", ",410028,,,Саратов,,им. Чернышевского Н.Г.,124,литер Д,");
    extAddCheckParameter("Тел.", "8(452)39-00-14");
    extAddCheckParameter("Конечный получатель (поставщик):", "ФГБОН ВО 'СГЮА'");
    extAddCheckParameter("ИНН", "6454006276");
    extAddCheckParameter("КПП", "645401001");

    extAddCheckParameter("ФИО студента", serviceData.custom.fileObj.clientName.value);
    extAddCheckParameter("ФИО вносителя", serviceData.custom.fileObj.educationClient.value);
    extAddCheckParameter("Факультет", serviceData.custom.fileObj.educationFaculty.value);
    extAddCheckParameter("Отделение", serviceData.custom.fileObj.educationDepartment.value);
    extAddCheckParameter("Специальность", serviceData.custom.fileObj.educationSpecialization.value);
    extAddCheckParameter("Курс", serviceData.custom.fileObj.educationYear.value);
    extAddCheckParameter("Группа", serviceData.custom.fileObj.educationGroup.value);
  } else {
    extAddCheckParameter("Получатель средств", serviceData.custom.currentContractor.SP9866);
    extAddCheckParameter("ИНН", serviceData.custom.fileObj.uin.value);
    extAddCheckParameter("Договор", serviceData.custom.currentContractor.SP10108);
    extAddCheckParameter("Адрес", serviceData.custom.currentContractor.SP9863);
    extAddCheckParameter("Тел.", serviceData.custom.currentContractor.SP8879);
  }

  return goodsListJSON;
}

/**
 * Если включён параметр debugConfig.alert, то ошибки будут выводиться на экран алертами
 * @param inbox - строка или объект Error
 */
function devAlert(inbox) {
  // alert(JSON.stringify(inbox));return;
  if (debugConfig.alert) {
    let str = inbox;
    if (inbox.constructor.constructor === Function) // IE11
      str = str.stack;
    alert(str);
  }
  logDebug(inbox);
}

function setBasket(list) {
  logStep('Устанавливаем корзину' + (list ? ' через параметр' : ''));
  list = list || stepsData.cart.list;
  let listArray = Object.keys(list).map(function (id) {
    return list[id];
  });

  let basketList = listArray.map(function (item) {
    return {
      gate: item.id,
      name: item.name,
      price: item.price,
      amount: item.amount,
    }
  });

  let basketStr = '<?xml version="1.0" encoding="utf-16"?><ArrayOfInterfaceBasketStructure>';
  basketList.forEach(function (item) {
    for (let i = 0; i < item.amount; i++) {
      basketStr += '<InterfaceBasketStructure>' +
        '<GateID>' + item.gate + '</GateID>' +
        '<ExecutorID>621</ExecutorID>' +
        '<Summ>' + item.price + '</Summ>' +
        '<Steps>' + item.name + ';</Steps>' +
        '</InterfaceBasketStructure>';
    }
  });
  basketStr += '</ArrayOfInterfaceBasketStructure>';

  extAddPaymentParameters('InterfaceBasket', basketStr);
}

function logStep(str) {
  if (log && log.steps) extSaveLog('[Шаги] ' + str);
}

function logWarning(str) {
  if (log && log.warnings) extSaveLog('[Предупреждение] ' + str);
}

function logException(str) {
  if (log) {
    if (log.exceptions) extSaveLog('[Исключение] ' + str);
    if (log.alert) alert(str);
  }
}

function logDebug(str) {
  if (log && log.debug) extSaveLog('[Отладка] ' + str);
}

function setTimeoutTimer(x) {
  clearTimeout(timers.away);
  clearInterval(timers.awayTime);
  CurrentPageMain();

  let wrapper = $('.timeoutWrapper');
  let button = $('.timeoutButton');

  let minutesStrings = ['минута', 'минуты', 'минут'];
  let secondsStrings = ['секунда', 'секунды', 'секунд'];

  let debugTimer = 0;

  if (debugTimer) {
    clearInterval(window.debugtimer);
    let ddd = x;
    window.debugtimer = setInterval(function () {
      if (ddd) $('#debugmessage').show().html(ddd--);
    }, 1000)
  }

  timers.away = setTimeout(function () {
    hideKeyboards();
    wrapper.css('display', 'flex');
    button.attr('onclick', 'removeTimeoutMessage(' + x + ');');
    redrawTimer(x);

    if (serviceData.isPayment) {
      extStopBill();
    }

    timers.awayTime = setInterval(function () {
      if (x > 0) {
        x--;
        redrawTimer(x);
      } else {
        clearInterval(timers.awayTime);
        if (serviceData.isPayment) {
          serviceData.timeoutTriggered = true;
          logStep('Выход по таймауту с отменой оплаты');
          handleOperation(stepsData.selectedPayment, 'cancel');
        } else {
          logStep('Выход по таймауту без оплаты');
          init();
        }
      }
    }, 1000)
  }, x * 1000);

  function redrawTimer(x) {
    let minutesSeconds = secondsToMinutesSeconds(x);
    let minutes = minutesSeconds[0];
    let seconds = minutesSeconds[1];
    let visualMinutes = minutes ? '<span id="timeout">' + minutes + '</span>' + ' ' + timeToString(minutes, minutesStrings, 'ru') : '';
    $('span#timeoutTime').html('<br>' + visualMinutes + ' ' + '<span id="timeout">' + seconds + '</span>' + ' ' + timeToString(seconds, secondsStrings, 'ru'));
  }
}

/**
 * Включение блокировки ввода с клавиатуры
 * @param reason <string> причина блокировки
 */
function lockKeyboardInput(reason) {
  if (reason !== undefined) reason = 'по причине ' + reason;
  else reason = 'без указания причины';
  lockInput = 1;
  logStep('Заблокирован ввод с клавиатуры ' + reason);
}

/**
 * Отключение блокировки ввода с клавиатуры
 */
function unlockKeyboardInput() {
  lockInput = 0;
  logStep('Ввод с клавиатуры разблокирован');
}

function removeTimeoutMessage(x) {
  clearInterval(timers.awayTime);
  $('span#timeout').html(x);
  setTimeoutTimer(x);
  $('.timeoutWrapper').hide();
  showKeyboards();
}

function stopTimeoutTimer() {
  if (timers) clearTimeout(timers.away);
  if (serviceData) clearInterval(timers.awayTime);
}

function startPinpadTimer() {
  serviceData.pinpadTimeout = extGetPinpadTimeout() / 1000;
  clearInterval(timers.paymentCard);
  let minutesStrings = ['минута', 'минуты', 'минут'];
  let secondsStrings = ['секунда', 'секунды', 'секунд'];

  redrawTimer();
  timers.paymentCard = setInterval(function () {
    serviceData.pinpadTimeout--;
    redrawTimer();
  }, 1000);
  logStep('Таймер пинпада включён');

  function redrawTimer() {
    if (serviceData.pinpadTimeout > -1) {
      let minutesSeconds = secondsToMinutesSeconds(serviceData.pinpadTimeout);
      let minutes = minutesSeconds[0];
      let seconds = minutesSeconds[1];
      $('.timeRemain')
        .html((minutes ? minutes + ' ' + timeToString(minutes, minutesStrings, 'ru') : '') + ' ' + seconds + ' ' + timeToString(seconds, secondsStrings, 'ru'));
    } else {
      clearInterval(timers.paymentCard);
      drawFinish('pinpadTimeout');
    }
  }
}

function stopPinpadTimer() {
  clearInterval(timers.paymentCard);
  logStep('Таймер пинпада выключён');
}

function drawStepLoading() {
  drawLoading('', '', 'rgba(255,255,255,0.8)');
}

/**
 * Преобразует секунды в минуты и секунды
 * @param number секунды
 * @returns {number[]} массив вида [минуты, секунды]
 */
function secondsToMinutesSeconds(number) {
  if (typeof number === 'string') number = Number(number);
  return [Math.floor(number / 60), number % 60]
}

/**
 * Возвращает подходящее название для указанного времени
 * @param number количество секунд/минут/...
 * @param array массив строк, типа ['минута', 'минуты', 'минут'] или ['minute', 'minutes']
 * @param lang язык ru/en
 * @returns string
 */
function timeToString(number, array, lang) {
  if (typeof number === 'string') number = Number(number);
  if (lang === 'ru') {
    let tens = number % 100;
    let ones = number % 10;
    if (tens > 10 && tens < 20) return array[2];
    if (ones > 1 && ones < 5) return array[1];
    if (ones === 1) return array[0];
    return array[2];
  } else if (lang === 'en') {
    if (number === 1) return array[1];
    else return array[0];
  }
}

function docsNext() {
  let data = stepsData.productsList;
  let docsPerPage = serviceData.docsPerPage;

  let docsRight = $('#docsRight');
  let docsLeft = $('#docsLeft');
  let item;
  let str;

  str = '<tr>' +
    '<th>Номенклатура</th>' +
    '<th>Количество</th>' +
    '<th>Цена за шт.</th>' +
    '<th>Сумма скидки</th>' +
    '<th>Сумма всего</th>' +
    '</tr>';

  let start;
  let finish;
  let blankCount;
  if (++serviceData.docsList === serviceData.docsListCount) {
    docsRight.attr('onclick', '');
    docsRight.addClass('inactive');
    start = docsPerPage * (serviceData.docsList - 1);
    finish = data.length;
    blankCount = finish - start;
  } else {
    start = docsPerPage * (serviceData.docsList - 1);
    finish = docsPerPage * serviceData.docsList;
  }

  for (let i = start; i < finish; i++) {
    item = data[i];
    let name = item.PositionName;
    str += '<tr>' +
      '<td style="text-align: left">' + (name.length > serviceData.nameLimit ? name.substring(0, serviceData.nameLimit) + '...' : name) + '</td>' +
      '<td style="text-align: center">' + item.PositionQuantity + '</td>' +
      '<td style="text-align: center">' + item.PositionPrice.toLocaleString('ru-RU').replace(',', '.') + '</td>' +
      '<td style="text-align: center">' + item.PositionDiscountSum.toLocaleString('ru-RU').replace(',', '.') + '</td>' +
      '<td style="text-align: right">' + item.PositionSum.toLocaleString('ru-RU').replace(',', '.') + '</td>' +
      '</tr>';
  }

  if (blankCount !== undefined) {
    for (let i = 0; i < docsPerPage - blankCount; blankCount++) {
      str += '<tr><td colspan="20" style="color: rgba(0,0,0,0)">0</td></tr>';
    }
  }

  if (serviceData.docsList > 1) {
    docsLeft.attr('onclick', 'docsLeft()');
    docsLeft.removeClass('inactive');
  }
  $('#docsCounter').html(serviceData.docsList);
  $('.docsTable').html(str);
}

function docsLeft() {
  let data = stepsData.productsList;
  let docsPerPage = serviceData.docsPerPage;

  let docsRight = $('#docsRight');
  let docsLeft = $('#docsLeft');
  let item;
  let str;

  str = '<tr>' +
    '<th>Номенклатура</th>' +
    '<th>Количество</th>' +
    '<th>Цена за шт.</th>' +
    '<th>Сумма скидки</th>' +
    '<th>Сумма всего</th>' +
    '</tr>';

  let start;
  let finish;
  if (--serviceData.docsList === 1) {
    docsLeft.attr('onclick', '');
    docsLeft.addClass('inactive');
    start = 0;
    finish = docsPerPage;
  } else {
    start = docsPerPage * (serviceData.docsList - 1);
    finish = docsPerPage * (serviceData.docsList);
  }
  for (let i = start; i < finish; i++) {
    item = data[i];
    let name = item.PositionName;
    str += '<tr>' +
      '<td style="text-align: left">' + (name.length > serviceData.nameLimit ? name.substring(0, serviceData.nameLimit) + '...' : name) + '</td>' +
      '<td style="text-align: center">' + item.PositionQuantity + '</td>' +
      '<td style="text-align: center">' + item.PositionPrice.toLocaleString('ru-RU').replace(',', '.') + '</td>' +
      '<td style="text-align: center">' + item.PositionDiscountSum.toLocaleString('ru-RU').replace(',', '.') + '</td>' +
      '<td style="text-align: right">' + item.PositionSum.toLocaleString('ru-RU').replace(',', '.') + '</td>' +
      '</tr>';
  }

  if (serviceData.docsList < serviceData.docsListCount) {
    docsRight.attr('onclick', 'docsNext()');
    docsRight.removeClass('inactive');
  }
  $('#docsCounter').html(serviceData.docsList);
  $('.docsTable').html(str);
}

/**
 * Уничтожает объект клавиатуры
 */

function removeKeyboard() {
  $("*").map(function (i, e) {
    if ($(e).data().keyboard) {
      $(e).keyboard().getkeyboard().destroy();
    }
  });
}

/**
 * Показывает клавиатуры
 */

function showKeyboards() {
  $("*").map(function (i, e) {
    if ($(e).data().keyboard) {
      $(e).show();
    }
  });
}

/**
 * Скрывает клавиатуры
 */

function hideKeyboards() {
  $("*").map(function (i, e) {
    if ($(e).data().keyboard) {
      $(e).hide();
    }
  });
}

/**
 * Возвращает объект клавиатуры
 * @param settings <Object>
 * @return {object}
 */

(function () {
  let keyboardObj = {
    init: function (settings, el) {
      let self = this;
      self.elem = $(el);
      self.settings = $.extend({}, $.fn.initKeyboard.settings, settings)
      self.elem.keyboard(settings);
    }
  };

  $.fn.initKeyboard = function (settings) {
    return this.each(function () {
      let customKeyboardObj = Object.create(keyboardObj);
      customKeyboardObj.init(settings, this);
    });
  };

  $.fn.initKeyboard.settings = {
    language: "ru", // string or array
    rtl: false, // language direction right-to-left
    layout: "ru-qwerty",
    display: {
      'accept': 'Далее:Accept (Shift-Enter)',
    },
    css: {
      // input & preview
      input: 'ui-widget-content ui-corner-all',
      // keyboard container
      container: 'ui-widget-content ui-widget ui-corner-all ui-helper-clearfix',
      // keyboard container extra class (same as container, but separate)
      popup: '',
      // default state
      buttonDefault: 'ui-state-default ui-corner-all',
      // hovered button
      buttonHover: 'ui-state-hover',
      // Action keys (e.g. Accept, Cancel, Tab, etc);
      // this replaces "actionClass" option
      buttonAction: 'ui-state-active',
      // Active keys
      // (e.g. shift down, meta keyset active, combo keys active)
      buttonActive: 'ui-state-active',
      // used when disabling the decimal button {dec}
      // when a decimal exists in the input area
      buttonDisabled: 'ui-state-disabled',
      // {empty} button class name
      buttonEmpty: 'ui-keyboard-empty'
    },
    customLayout: {
      'normal': ['{cancel}']
    },
    position: {
      of: null,
      my: 'center top',
      at: 'center bottom',
      at2: 'center bottom'
    },
    reposition: true,
    usePreview: false,
    alwaysOpen: false,
    initialFocus: false,
    noFocus: false,
    stayOpen: false,
    userClosed: false,
    ignoreEsc: false,
    closeByClickEvent: false,
    wheelMessage: 'Use mousewheel to see other keys',
    autoAccept: false,
    autoAcceptOnEsc: false,
    lockInput: false,
    restrictInput: false,
    restrictInclude: '', // e.g. 'a b foo \ud83d\ude38'
    acceptValid: false,
    autoAcceptOnValid: false,
    cancelClose: true,
    tabNavigation: false,
    enterNavigation: true,
    enterMod: 'altKey',
    stopAtEnd: true,
    appendLocally: false,
    appendTo: 'body',
    stickyShift: false,
    caretToEnd: true,
    preventPaste: false,
    scrollAdjustment: 10,
    maxLength: 10,
    maxInsert: true,
    repeatDelay: 500,
    repeatRate: 20,
    resetDefault: false,
    openOn: '',
    keyBinding: 'mousedown touchstart',
    useWheel: true,
    useCombos: true,
    showTyping: true,
    lockTypeIn: false,
    delay: 250,
    initialized: function (e, keyboard, el) {},
    beforeVisible: function (e, keyboard, el) {},
    visible: function (e, keyboard, el) {},
    beforeInsert: function (e, keyboard, el, textToAdd) {},
    change: function (e, keyboard, el) {},
    beforeClose: function (e, keyboard, el, accepted) {},
    accepted: function (e, keyboard, el) {},
    canceled: function (e, keyboard, el) {},
    restricted: function (e, keyboard, el) {},
    hidden: function (e, keyboard, el) {},
    switchInput: function (keyboard, goToNext, isAccepted) {},
    create: function (keyboard) {},
    buildKey: function (keyboard, data) {},
    validate: function (keyboard, value, isClosing) {}
  }
}());

function getDataFromXML(paramsArray) {
  try {
    let debug = 1;
    let debugXML = '<?xml version="1.0" encoding="utf-16"?><ArrayOfDataPaymentQuery>  <DataPaymentQuery>    <Summ_Client>0</Summ_Client>    <Summ_Gate>0</Summ_Gate>    <Status>0</Status>    <RawQuery />    <Params4Search />    <Comment>Сумма комиссии для оператора сдачи больше размера сдачи</Comment>    <SysComment />    <TerminalID>f193ad67-dd23-4593-9def-b392402251f1</TerminalID>    <DT_Terminal>2019-06-14T17:40:52</DT_Terminal>    <DT_Gate>2019-06-14T17:40:52.2645416+03:00</DT_Gate>    <DT_Recieved>2019-06-14T17:40:52.2645416+03:00</DT_Recieved>    <stp>      <Steps>        <OrderID>0</OrderID>        <Value />      </Steps>    </stp>    <Signature />    <Command>PAY</Command>    <Session>201906141740522804484</Session>    <GateID>0</GateID>    <ServerID>0</ServerID>    <ExecutorID>0</ExecutorID>    <DeviceType>1</DeviceType>    <OnlineMode>0</OnlineMode>    <AdditionalParametersList />    <AdditionalParametersForCheck />    <UserComment />    <isBlocked>true</isBlocked>  </DataPaymentQuery></ArrayOfDataPaymentQuery>';
    debugXML = '{"arrayofdatapaymentquery":{"datapaymentquery":{"summ_client":30000,"summ_gate":30000,"status":0,"rawquery":{},"params4search":{},"comment":"Сумма платежа больше лимита для этой точки приема","syscomment":{},"terminalid":"60b63791-2486-426d-8f6f-3f4d6f84ce3c","dt_terminal":"2019-06-14T18:05:31","dt_gate":"2019-06-14T18:05:31.4593853+03:00","dt_recieved":"2019-06-14T18:05:31.4593853+03:00","stp":{},"signature":{},"command":"PAY","session":201906141805314730000,"gateid":2311,"serverid":0,"executorid":621,"devicetype":1,"onlinemode":0,"additionalparameterslist":{"additionalparameters":[{"key":"Acct","value":30000},{"key":"FixedSum","value":30000},{"key":"noFiscaltag","value":0},{"key":"BasketStr","value":"&lt;?xml version=\\"1.0\\" encoding=\\"utf-16\\"?&gt;&lt;BasketStructure&gt;  &lt;BasketSession&gt;201906141805314732888&lt;/BasketSession&gt;  &lt;isCard&gt;false&lt;/isCard&gt;  &lt;CardType&gt;0&lt;/CardType&gt;  &lt;CardTerminalCheck /&gt;  &lt;Summ_Client&gt;30000&lt;/Summ_Client&gt;  &lt;Summ_Change&gt;0&lt;/Summ_Change&gt;  &lt;Summ_Change_Out&gt;0&lt;/Summ_Change_Out&gt;  &lt;Summ_Change_Error&gt;0&lt;/Summ_Change_Error&gt;  &lt;Check /&gt;  &lt;Charge_info /&gt;  &lt;Description /&gt;  &lt;DT_Terminal&gt;2019-06-14T18:05:31&lt;/DT_Terminal&gt;  &lt;DT_Recieved&gt;2019-06-14T18:05:31.4583852+03:00&lt;/DT_Recieved&gt;  &lt;SessionStructure /&gt;  &lt;Summ_Basket&gt;30000&lt;/Summ_Basket&gt;  &lt;DealerID&gt;c0f90ec9-ebaf-4525-b66e-48b6e385560a&lt;/DealerID&gt;  &lt;TerminalID&gt;60b63791-2486-426d-8f6f-3f4d6f84ce3c&lt;/TerminalID&gt;  &lt;isReturn&gt;false&lt;/isReturn&gt;  &lt;AuthorizationCode /&gt;  &lt;ReferenceNumber /&gt;  &lt;Integration&gt;0&lt;/Integration&gt;  &lt;IntegrationData /&gt;  &lt;ClientID&gt;00000000-0000-0000-0000-000000000000&lt;/ClientID&gt;  &lt;UserID&gt;cf6063e5-6b14-4697-a69c-c73f1a4cfcc7&lt;/UserID&gt;  &lt;DiscountID&gt;0&lt;/DiscountID&gt;  &lt;Summ_Discount&gt;0&lt;/Summ_Discount&gt;  &lt;isAcct&gt;false&lt;/isAcct&gt;  &lt;InvoiceNumber /&gt;  &lt;InvoiceNumberKKM&gt;0&lt;/InvoiceNumberKKM&gt;&lt;/BasketStructure&gt;"}]},"additionalparametersforcheck":{},"usercomment":{},"isblocked":"true"}}}';
    let xml = !debug ? window.external.GetKeyForInterface('Payments') : debugXML;
    logDebug('XML ' + xml);
    let tobj = xml2json.parser(xml);
    let array = tobj.arrayofdatapaymentquery.datapaymentquery.additionalparameterslist.additionalparameters;
    let index = array.findIndex(function (item) {
      return item.key === 'BasketStr';
    });
    array[index].value = xml2json.parser(array[index].value);
    let json = JSON.stringify(tobj);
    logDebug('JSON ' + json);
    // alert(json);
  } catch (e) {
    logException(e)
  }
  // return window.external.GetKeyForInterface('Payments');
}

/**
 * Проеобразование объекта в удобный вид для чтения в логе
 * @param obj {object}
 * @return {string|null}
 */
function objToLog(obj) {
  if (typeof obj !== 'object') {
    logException('(objToLog) Полученное значение не является объектом: ' + typeof obj);
    return null;
  } else return $.param(obj).replace(/&/g, ', ').replace(/=/g, ': ');
}

function objToEncodedParams(obj) {
  if (typeof obj !== 'object') throw new TypeError('В качестве параметра ожидался объект');
  else {
    for (let key in obj) obj[key] = encodeURIComponent(obj[key]);
    return $.param(obj);
  }
}

/**
 * Формирует строку для печати
 * @param number <Number|String>
 * @param center <Boolean> нужно ли центрировать последнюю строчку
 * @return {string}
 */
function getPrintableBigNumbers(number, center) {
  let checkDigitWidth = 5; // ширина одной цифры на чеке, в символах
  let checkDigitHeight = 7; // высота одной цифры на чеке, в строчках
  let spacesBetweenDigits = 1;
  let maxSymbolsInRow = 41; // максимальное количество символов на одной строке
  let lineBreak = '\r\n';
  let maxDigitsOnCheck = Math.floor(maxSymbolsInRow / (checkDigitWidth + spacesBetweenDigits));

  let spacer = '';
  for (let i = 0; i < maxSymbolsInRow - 4; i++) spacer += ' ';
  spacer += lineBreak;

  if (!(typeof number === 'string' || typeof number === 'number')) throw new TypeError('Передаваемое число должно иметь тип Number или String');
  center = Boolean(center);
  number = String(number);
  if (number.match(/\D/)) throw new TypeError('Переданная строка не является целым числом');
  number = number.split('').map(function (digit) {
    return Number(digit);
  });
  let numbers = [];
  let currentRow;
  number.forEach(function (digit, i) {
    if (!(i % maxDigitsOnCheck)) {
      currentRow = [];
      numbers.push(currentRow);
    }
    currentRow.push(digit);
  });
  let totalStrings = numbers.length;
  let digitsOnLastString = numbers[numbers.length - 1].length;

  // при ручном изменении этого объекта нужно изменять checkDigitWidth и checkDigitHeight соответствующим образом
  const digits = Object.freeze({
    0: [
      ' ### ',
      '#   #',
      '#   #',
      '#   #',
      '#   #',
      '#   #',
      ' ### '
    ],
    1: [
      '  #  ',
      ' ##  ',
      '# #  ',
      '  #  ',
      '  #  ',
      '  #  ',
      '#####'
    ],
    2: [
      ' ### ',
      '#   #',
      '    #',
      ' ### ',
      '#    ',
      '#    ',
      '#####'
    ],
    3: [
      ' ### ',
      '#   #',
      '    #',
      ' ### ',
      '    #',
      '#   #',
      ' ### '
    ],
    4: [
      '#    ',
      '#  # ',
      '#  # ',
      '#  # ',
      '#####',
      '   # ',
      '   # '
    ],
    5: [
      '#####',
      '#    ',
      '#    ',
      '#### ',
      '    #',
      '#   #',
      ' ### '
    ],
    6: [
      ' ### ',
      '#   #',
      '#    ',
      '#### ',
      '#   #',
      '#   #',
      ' ### '
    ],
    7: [
      '#####',
      '#   #',
      '   # ',
      '  #  ',
      '  #  ',
      '  #  ',
      '  #  '
    ],
    8: [
      ' ### ',
      '#   #',
      '#   #',
      ' ### ',
      '#   #',
      '#   #',
      ' ### '
    ],
    9: [
      ' ### ',
      '#   #',
      '#   #',
      ' ####',
      '    #',
      '#   #',
      ' ### '
    ]
  });

  let check = '';
  let currentString = 0;
  let spacesBefore;
  let spacesAfter;
  let spacingBefore = '';
  let spacingBetweenDigits = '';
  let spacingAfter = '';
  let isLastString;
  for (let i = 0; i < spacesBetweenDigits; i++) spacingBetweenDigits += ' ';

  numbers.forEach(function (row, i, arr) {
    isLastString = center && i === arr.length - 1;
    if (isLastString) { // если это последняя строчка и нужно центрировать
      let spacingTotal = maxSymbolsInRow - digitsOnLastString * (checkDigitWidth + spacesBetweenDigits);
      if (spacingTotal % 2) {
        spacesBefore = Math.floor(spacingTotal / 2);
        spacesAfter = spacesBefore + 1;
      } else spacesBefore = spacesAfter = spacingTotal / 2;
      for (let i = 0; i < spacesBefore; i++) spacingBefore += ' ';
      for (let i = 0; i < spacesAfter; i++) spacingAfter += ' ';
    }

    let rowString = '';

    for (let i = 0; i < checkDigitHeight; i++) {
      if (isLastString) rowString += spacingBefore;
      row.forEach(function (currentCheckRowNumber) {
        let digit = digits[currentCheckRowNumber][i];
        rowString += digit + spacingBetweenDigits;
      });
      if (isLastString) rowString += spacingAfter;
      rowString += lineBreak;
    }
    check += rowString;
  });

  check = spacer + check;
  check += spacer;
  return check;
}

/**
 * Ожидает, когда бэкенд заполнит BasketStr и возвращает его в resolve в случае успеха и строку 'timeout' в reject в случае таймаута
 * @param {number} [timeout=30] таймаут, в секундах (по умолчанию 30)
 * @param {number} [delay=300] задержка проверки, в миллисекундах (по умолчанию 300)
 * @return {Promise<Object|string>}
 */
function waitBasket(timeout, delay) {
  timeout = timeout || 30;
  delay = delay || 300;
  logStep('BasketStr отсутствует, ждём бекэнд');
  return new Promise(function (resolve, reject) {
    let trynum = 0;
    let maxtries = Math.round(timeout * 1000 / delay);
    check();

    function check() {
      trynum++;
      if (trynum < maxtries) {
        let xml = extGetPaymentParameters('BasketStr');
        if (xml.length) {
          let XMLData = xml2json.parser(xml);
          let basket = XMLData.basketstructure;
          logStep('BasketStr получен');
          logDebug('basket: ' + JSON.stringify(basket));
          resolve({
            source: xml,
            data: basket,
          });
        } else setTimeout(check, delay);
      } else {
        logStep('Таймаут ожидания BasketStr');
        reject('timeout');
      }
    }
  });
}