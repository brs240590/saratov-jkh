﻿function LoadPage() {
    hidden();
    StartTimeOut();

    jsClock();

    var titl = window.document.title;


    if (titl == 'CHECK') {
        sounder.src = "../sound/take_cheque.wav";
    }
    if (titl == 'PAY') {
        sounder.src = "../sound/bills.mp3";
    }
    if (titl == 'CHECKDATA') {
        sounder.src = "../sound/confirm_number.wav";
    }
    if (titl == 'NUMBER') {
        sounder.src = "../sound/enter_number.wav";
        dFilter(999, numbox, GetMaskToInterface());
    }
    if (titl == 'NUMBER8') {
        sounder.src = "../sound/enter_number.wav";
        dFilter(999, numbox, GetMaskToInterface());
    }
    if (titl == 'KEYBOARD') {
        sounder.src = "../sound/enter_number.wav";
        dFilter(999, search, GetMaskToInterface());
    }
    if (titl == 'CONFIRM') {
        sounder.src = "../sound/confirm_number.wav";
    }

    if (titl == 'PAYMENT') {
        sounder.src = "../sound/insert_money.wav";
    }

}

//<!-- скрыть линейки -->
function hidden() {
    // document.body.style.overflow='hidden';
}
function unhidden() {
    document.body.style.overflow = '';
}
//<!--скрыть линейки -->

//таймауты
//document.onkeypress = StartTimeOut;
//document.onmousemove = StartTimeOut;
var oldx = 1;


document.onmousemove = Coords;

function Coords() {
    if (document.all) {
        x = event.x + document.body.scrollLeft;
        y = event.y + document.body.scrollTop;
    }
    if (oldx == x) {
    } else {
        oldx = x;
        StartTimeOut();
    }

}


var timeout = null;
function StartTimeOut() //val
{

    var val = window.external.RetTimeOut();
    if (timeout) {
        clearTimeout(timeout);
        timeout = null;
    }

    timeout = setTimeout('gt_screen()', val);
}

var timeout_screen = null;


function gt_screen() {
    var titl = window.document.title;
    if (titl == 'PAYMENT') {
        window.external.STOP_BILL();
        stoptimeouttimer = 1;
        document.getElementById("screentimeoutbg").style.visibility = "visible";
        document.getElementById("screentimeout").style.visibility = "visible";
        setTimeout(timeouttimer(), 1000);
    }
    else {
        gt();
    }
}


function gt() {
    var titl = window.document.title;
    if (titl == 'MAIN') {
        //ни чего не делать, уже на первой странице
    }
    else if (titl == 'PAYMENT') {

        window.external.STOP_BILL();
        if (window.external.RetACCT() > 0) {
            window.external.PayConfirm()
        }
        else {
            window.external.LINK('main.html');
        }
    }
    else if (titl == 'ERROR') {

        //document.getElementById("adminpass").style.visibility = "hidden";
        //мы на странице терминал заблокирован и ошибка
    }
    else if (titl == 'CODE') {
        window.external.LINK('main.html');
    }
    else if (titl == 'CHECK') {
        window.external.LINK('main.html');
    }

    else {
        window.external.LINK('main.html');
    }
}

//нажата кнопка отмены проверки номера в Online режиме
function CanselButtonClick() {
    //alert('dfhdfh');
    //document.getElementById('loadingdiv').style.visibility='hidden'; 
    //document.getElementById('loadingdivmsg').style.visibility='hidden'; 
    stoptimer = 0;
    window.external.CanselButtonClick();
}
//нажата кнопка меню проверки номера в Online режиме
function MenuButtonClick() {
    //document.getElementById('loadingdiv').style.visibility='hidden'; document.getElementById('loadingdivmsg').style.visibility='hidden'; document.getElementById('loadingdivmsgyesnomenu').style.visibility='hidden'; window.external.LINK('main.html');
    window.external.MenuButtonClick();
}
//нажата кнопка далее проверки номера в Online режиме
function NextButtonClick() {
    window.external.NextButtonClick();
}
//нажата кнопка назад проверки номера в Online режиме
function PrevButtonClick() {
    window.external.PrevButtonClick();
}

////поиск провайдера на странице поиска
function RetSearchPanel(txt) {
    var ret = "";
    txt = replace(txt, '*', '');
    var strForSplit = window.external.RetSearchPanel(txt);
    var strArray = [];
    try {
        strArray = strForSplit.split(";");
        var column = 0;//по сколько в ряд в таблице
        if (strArray.length > 1) {
            ret += "<table cellpadding=0 border=0>";
            for (var i = 0; i < strArray.length; i = i + 2) {
                if (column == 0)ret += '<TR>';
                ret += "<td CLASS=\"KB\" ID='operatorsearch" + strArray[i] + "' WIDTH=\"398\" HEIGHT=\"54\" background=\"img/fo_search_res_btn.png\" onclick=\"buttonclick('btn_r.png','btn_press.png','operatorsearch" + strArray[i] + "');\">" + strArray[i + 1] + "</td>"; //strArray[i]+"р. до "+strArray[i+1]+"";
                column = column + 1;
                if (column == 2) {
                    column = 0;
                    ret += '</TR>';
                }
            }
            if (column == 1)ret += '</TR>';//если в цикле не закрылась TR ка
            ret += "</table>";
        }//if
    }
    catch (e) {
//       Alert(e);
    }
//alert(ret);
    document.getElementById('searchresult').innerHTML = ret;

}

function RetFastPanel(PanelID) {
    var ret = "";
    var GateID = window.external.RetFastPanelGateID(PanelID);
    if (GateID) {//есть шлюз для этой быстрой панели
        ret = "<a onclick='ClickOperator(" + GateID + ");'><img src=\"../logo/" + window.external.RetFastPanelLogotipImg(PanelID) + "\" border=0></a>";
    }
    return ret;
}

function RetLogotipImg() {
    var ret = "";
    ret = window.external.RetLogotipImg();
    if (ret == 'nologo') {//нет логотипа
        ret = '<font class="grfontnologo">' + window.external.RetGateName() + '</font>';
    }
    else {//есть логотип
        ret = '<img src="../logo/' + ret + '" border=0>';
    }
    //можно задействовать и RetButtonImg()
    return ret;
}

function RetStepInfo() {
    var ret = "";
    ret = '<P CLASS="TOPTEXT">' + window.external.RetStepInfo() + '</p>';
    return ret;
}

function RetStepInfoFull() {
//alert(window.external.RetStepInfoFull());
    var ret = "";
    ret = window.external.RetStepInfoFull();
    return ret;
}

function RetComission() {
    var ret = "";
    var strForSplit = window.external.RetComission();
    strArray = new Array();
//alert(strForSplit);
    ret = "Комиссия<br><br>";
    try {
        strArray = strForSplit.split(";");
        if (strArray.length > 1) {
            for (var i = 0; i < strArray.length; i = i + 3) {
                if (strArray[i] == 'True') {//фиксированная сумма
                    ret += strArray[i + 2] + "р. до " + strArray[i + 1] + " руб.<BR>";
                }
                else {//процент от суммы
                    ret += strArray[i + 2] + "% до " + strArray[i + 1] + " руб.<BR>";
                }
            }
        }//if
    }
    catch (e) {
        Alert(e);
    }
    return ret;
}

function RetGroup(GroupID) {
    var ret = "";
    var strForSplit = window.external.RetGatesForGroup(GroupID);
    var strForSplit2 = window.external.RetSubGroups(GroupID);//подгруппы
//alert(strForSplit);
    strArray = new Array();
    strArray2 = new Array();
    try {
        strArray = strForSplit.split(";");
        strArray2 = strForSplit2.split(";");
        ret = "<table cellpadding=0 border=0>";
        var column = 0;//по сколько в ряд в таблице
        //подгруппы
        if (strArray2.length > 1) {
            for (var i = 0; i < strArray2.length; i = i + 3) {
                if (column == 0)ret += '<TR>';
                if (strArray2[i + 1] == 'nologo') {//нет рисунка оператора
                    ret += '<td align=center valign=middle WIDTH="319" HEIGHT="159" background="img/btn_r.png"><table border=0 width=260 height=110><tr><td onclick=\"RetGroup(' + strArray2[i] + ');\" CLASS="KB" ID=op' + strArray2[i] + ' onclick="kbclick(\'btn_r.png\',\'btn_press.png\',\'op' + strArray2[i] + '\');"><a onclick=\"RetGroup(' + strArray2[i] + ');\"><font class="grfontnologo">' + strArray2[i + 2] + '</font></a></td></tr></table></td>';
                }
                else {//есть рисунок оператора
                    ret += '<td align=center valign=middle WIDTH="319" HEIGHT="159" background="img/btn_r.png"><table border=0 width=260 height=110><tr><td onclick=\"RetGroup(' + strArray2[i] + ');\" CLASS="KB" ID=op' + strArray2[i] + ' onclick="kbclick(\'btn_r.png\',\'btn_press.png\',\'op' + strArray2[i] + '\');"><a onclick=\"RetGroup(' + strArray2[i] + ');\"><img name=logo' + strArray2[i] + ' src="../logo/' + strArray2[i + 1] + '" onclick="logoresize(\'' + strArray2[i + 1] + '\',\'logo' + strArray2[i] + '\');"></a><font class="grfont">' + strArray2[i + 2] + '</font></td></tr></table></td>';
                }
                column = column + 1;
                if (column == 4) {
                    column = 0;
                    ret += '</TR>';
                }
            }
        }
        //шлюзы
        if (strArray.length > 1) {
            for (var i = 0; i < strArray.length; i = i + 3) {
                if (column == 0)ret += '<TR>';
                if (strArray[i + 1] == 'nologo') {//нет рисунка оператора
                    ret += '<td align=center valign=middle WIDTH="319" HEIGHT="159" background="img/btn_r.png"><table border=0 width=260 height=110><tr><td CLASS="KB" ID=op' + strArray[i] + ' onclick="ClickOperator(' + strArray[i] + ');"><font class="grfontnologo">' + strArray[i + 2] + '</font></td></tr></table></td>';
                }
                else {//есть рисунок оператора
                    ret += '<td align=center valign=middle WIDTH="319" HEIGHT="159" background="img/btn_r.png"><table border=0 width=260 height=110><tr><td CLASS="KB" ID=op' + strArray[i] + ' onclick="ClickOperator(' + strArray[i] + ');"><img name=logo' + strArray[i] + ' src="../logo/' + strArray[i + 1] + '" onclick="logoresize(\'' + strArray[i + 1] + '\',\'logo' + strArray[i] + '\');"><span class="grfont">' + strArray[i + 2] + '</span></td></tr></table></td>';
                }
                column = column + 1;
                if (column == 4) {
                    column = 0;
                    ret += '</TR>';
                }
            }

        }
        ret += "</table>";
        document.getElementById('logolist').innerHTML = ret;//вывод результата
    }
    catch (e) {
        Alert(e);
    }
    return ret;
}

//
//
//
//функция перехода к вводу пароля на страницу с блоком или полной ошибкой
var countclick = '';
function countpress(sum) {
    StartTimeOutResetCl();
    countclick += sum;
//alert(countclick);
    countclicknum = 'click' + sum;

//document.getElementById(countclicknum).style.background='#ffffff';
//document.getElementById(countclicknum).style.opacity='0.1';
//document.getElementById(countclicknum).style.filter='alpha(Opacity=10)';

    document.getElementById('termdoesntwork').style.fontWeight = 'lighter';


//setTimeout("document.getElementById('"+countclicknum+"').style.opacity=0",200);
//setTimeout("document.getElementById('"+countclicknum+"').style.filter='alpha(Opacity=0)'",200);

//setTimeout("document.getElementById('"+countclicknum+"').style.background='#000000'",2000);

    setTimeout("document.getElementById('termdoesntwork').style.fontWeight='bold'", 100);

    if (countclick == window.external.RetSecretClick()) {
        document.getElementById("adminpass").style.visibility = "visible";
        document.getElementById("terminalerror").style.visibility = "hidden";
    }
    if (countclick.length > 5) {
        countclick = '';
    }
}


var timeoutcl = null;
function StartTimeOutResetCl() //val
{
    if (timeoutcl) {
        clearTimeout(timeoutcl);
        timeoutcl = null;
    }
    timeoutcl = setTimeout('resetcl()', 3000);
}
function resetcl() {
    countclick = '';
}

//
//BUTTONS CLICK
//
function buttonclick(pic, overpic, idpic) {
    changeImage(idpic, overpic);
    sounder.src = "../sound/click.wav";

    setTimeout("changeImage('" + idpic + "','" + pic + "')", 50);
    setTimeout("buttonmove('" + idpic + "')", 75);
    //id = replace(idpic,'b','');

//if(id>0)dFilter(id,numbox,GetMaskToInterface());//'(###)###-##-##'); 
//if(id>0)dFilter(id,numbox,'(###)###-##-##'); 

}

//нажатие кнопок в окне ввода кода для доступа к системному меню
function passenterclick(pic, overpic, idpic) {
    changeImage(idpic, overpic);
    sounder.src = "../sound/click.wav";
    setTimeout("changeImage('" + idpic + "','" + pic + "')", 100);
    setTimeout("buttonmove('" + idpic + "')", 150);

    var cnt = '';
    cnt = document.getElementById('numbox').value;

    id = replace(idpic, 'b', '');
    if (id <= 10) {
        document.getElementById('numbox').value += id;
    }
    else if (id == 41) {
        document.getElementById('numbox').value = '';
    }
    else if (id == 40) {
        if (cnt.length > 0) {
            cnt = cnt.substring(0, cnt.length - 1);
            document.getElementById('numbox').value = cnt;
        }
    }
}

function changeImage(idpic, overpic) {
    document.getElementById(idpic).src = "img/" + overpic;
}


function buttonmove(idpic) {

    if (idpic == 'info') {
        window.external.LINK('info.html');
    }
    else if (idpic == 'search') {
        window.external.LINK('search.html');
    }
    else if (idpic == 'aboutterm') {
        window.external.LINK('aboutterm.html');
    }
    else if (idpic == 'aboutoperat') {
        window.external.LINK('aboutoperat.html');
    }
    else if (idpic == 'aboutps') {
        window.external.LINK('aboutps.html');
    }
    else if (idpic == 'help') {
        window.external.LINK('help.html');
    }
    else if (idpic == 'group1') {
        window.external.LINK('group1.html');
    }
    else if (idpic == 'group5') {
        window.external.LINK('group5.html');
    }
    else if (idpic == 'group6') {
        window.external.LINK('group6.html');
    }
    else if (idpic == 'group8') {
        window.external.LINK('group8.html');
    }
    else if (idpic == 'group9') {
        window.external.LINK('group9.html');
    }
    else if (idpic == 'group10') {
        window.external.LINK('group10.html');
    }
    else if (idpic == 'group11') {
        window.external.LINK('group11.html');
    }
    else if (idpic == 'group26') {
        window.external.LINK('group26.html');
    }
    else if (idpic == 'group27') {
        window.external.LINK('group27.html');
    }
    else if (idpic == 'group28') {
        window.external.LINK('group28.html');
    }
    else if (idpic == 'group29') {
        window.external.LINK('group29.html');
    }
    else if (idpic == 'group13') {
        window.external.LINK('group13.html');
    }
    else if (idpic == 'back') {
        window.external.STOP_BILL();
        javascript:history.back();
    }
    else if (idpic == 'next') {
    }
    else if (idpic == 'masterback') {
        var titl = window.document.title;
        if (titl == 'NUMBER') {
            try {
                window.external.NextStepVal(document.getElementById('numbox').value);//забрать значение
            } catch (e) {
            }
        }
        if (titl == 'NUMBER8') {
            try {
                window.external.NextStepVal(document.getElementById('numbox').value);//забрать значение
            } catch (e) {
            }
        }
        if (titl == 'KEYBOARD') {
            try {
                window.external.NextStepVal(document.getElementById('search').value);//забрать значение
            } catch (e) {
            }
        }
        if (titl == 'CALENDARMONTH') {
            try {
                window.external.NextStepVal(document.getElementById('calendarmonth').value);//забрать значение
            } catch (e) {
            }
        }

        if (titl == 'CALENDAR') {
            try {
                window.external.NextStepVal(document.getElementById('calendar').value);//забрать значение
            } catch (e) {
            }
        }

        window.external.PrevStep();
    }
    else if (idpic == 'masternext') {
        var titl = window.document.title;
        if (titl == 'NUMBER') {
            try {
                var numstars = replace(document.getElementById('numbox').value, "*", "");
                window.external.NextStepVal(numstars);//забрать значение
            } catch (e) {
            }
        }
        if (titl == 'NUMBER8') {
            try {
                var numstars = replace(document.getElementById('numbox').value, "*", "");
                window.external.NextStepVal(numstars);//забрать значение
            } catch (e) {
            }
        }
        if (titl == 'KEYBOARD') {
            try {
                var numstars = replace(document.getElementById('search').value, "*", "");
                window.external.NextStepVal(numstars);//забрать значение
            } catch (e) {
            }
        }

        if (titl == 'CALENDAR') {
            try {
                window.external.NextStepVal(document.getElementById('calendar').value);//забрать значение
            } catch (e) {
            }
        }

        if (titl == 'CALENDARMONTH') {
            try {
                window.external.NextStepVal(document.getElementById('calendarmonth').value);//забрать значение
            } catch (e) {
            }
        }


        window.external.NextStep();
    }
    else if (idpic == 'paywithoutchek') {
        //alert('dd');
        //start pay
        window.external.MasterStep();
    }
    else if (idpic == 'mainmenu') {
        window.external.LINK('main.html');
    }
    else if (idpic == 'confirm') {
        window.external.LINK('confirm.html');
    }
    else if (idpic == 'payment') {
        window.external.LINK('payment.html');
    }
    else if (idpic == 'oferta') {
        window.external.LINK('oferta.html');
    }
    else if (idpic == 'error') {
        window.external.LINK('error.html');
    }
    else if (idpic == 'check') {
        window.external.STOP_BILL();
        //вызов процедуры оплаты
        setTimeout("pay_fnc()", 1500);
    }
    else if (idpic == 'sng')/*провайдеры СНГ*/
    {
        window.external.LINK('group13.html');
    }

    //ввод кода на странице доступа
    else if (idpic == 'intercode') {
        try {
            window.external.InterCode(document.getElementById('numbox').value);
            document.getElementById('numbox').value = "";
        }
        catch (e) {

            window.external.SaveErrorInLog(e.name + e.description);
        }
    }
    else if (idpic.indexOf('operatorsearch') != -1) {
        idpic = idpic.replace("operatorsearch", "");
        window.external.StartPayCycle(0, idpic);
    }
    else {
    }

}
//
//
//


//
//
//

//
//CLOCK
//
function jsClock() {
    var time = new Date()
    var hour = time.getHours()
    var minute = time.getMinutes()
    var second = time.getSeconds()
    var day = time.getDate()
    var month = time.getMonth() + 1
    var year = time.getFullYear()

    if (day < 10) day = "0" + day
    if (month < 10) month = "0" + month
    var date = "" + day + "." + month + "." + year

    var temp = "" + hour
    if (hour == 0) temp = "12"
    if (temp.length == 1) temp = " " + temp
    temp += ((minute < 10) ? ":0" : ":") + minute
    temp += ((second < 10) ? ":0" : ":") + second
//temp += (hour >= 12) ? " PM" : " AM"
//document.clockForm.digits.value = temp
    document.getElementById('clock').innerHTML = temp
    document.getElementById('date').innerHTML = date
    id = setTimeout("jsClock()", 1000)
}


//
//DFILTER для полей ввода
//
var dFilterStep

function dFilterStrip(dFilterTemp, dFilterMask) {
    dFilterMask = replace(dFilterMask, '=', '');
    dFilterMask = replace(dFilterMask, '#', '');

    for (dFilterStep = 0; dFilterStep < dFilterMask.length++; dFilterStep++) {
        dFilterTemp = replace(dFilterTemp, dFilterMask.substring(dFilterStep, dFilterStep + 1), '');
    }
    return dFilterTemp;
}

function dFilterMax(dFilterMask) {
    dFilterTemp = dFilterMask;
    for (dFilterStep = 0; dFilterStep < (dFilterMask.length + 1); dFilterStep++) {
        if (dFilterMask.charAt(dFilterStep) != '#' && dFilterMask.charAt(dFilterStep) != '=') {
            dFilterTemp = replace(dFilterTemp, dFilterMask.charAt(dFilterStep), '');
        }

    }
    return dFilterTemp.length;
}

function dFilterMax2(dFilterMask) {
    var dFilterTemp = 0;
    for (dFilterStep = 0; dFilterStep < (dFilterMask.length + 1); dFilterStep++) {
        if (dFilterMask.charAt(dFilterStep) == '=') {
            dFilterTemp++;
        }

    }
    return dFilterTemp;
}


function dFilter(key, textbox, dFilterMask) {

    textbox.value = replace(textbox.value, '*', '');

    dFilterNum = dFilterStrip(textbox.value, dFilterMask);

    var titl = window.document.title;

    if (titl == 'NUMBER8') {
        if (dFilterNum.length >= 4 && dFilterNum.substring(0, 1) == 8 && SearchMSISDN(dFilterNum.substring(0, 3)) == undefined) {
            dFilterNum = dFilterNum.substring(1, dFilterNum.length);
        }
    }

    if (key == (43 + LangSmeshenie))//пробел
    {
        dFilterNum = dFilterNum + ' ';
    }
    else if (key == (41 + LangSmeshenie))//очистка
    {
        if (dFilterNum.length != 0) {
            dFilterNum = "";
            textbox.value = "";
        }
        document.getElementById('numbox').style.fontSize = '53px';

        try {
            document.getElementById("masternext").style.visibility = "hidden";
        }
        catch (e) {
        }
    }
    else if (key == (40 + LangSmeshenie))//делете
    {
        if (dFilterNum.length != 0) {
            dFilterNum = dFilterNum.substring(0, dFilterNum.length - 1);
            if (dFilterNum.length > 12 && dFilterNum.length < 20) {
                try {
                    var curfont = parseInt(document.getElementById('numbox').currentStyle['fontSize'], 10);
                    var newfont = curfont + 2;
                    document.getElementById('numbox').style.fontSize = newfont + 'px';
                }
                catch (e) {
                }

            }

        }
    }
    else if (dFilterNum.length < dFilterMax(dFilterMask)) {
        if (dFilterNum.length > 12 && dFilterNum.length < 20) {
            try {
                var curfont = parseInt(document.getElementById('numbox').currentStyle['fontSize'], 10);
                var newfont = curfont - 2;
                document.getElementById('numbox').style.fontSize = newfont + 'px';
            }
            catch (e) {
            }
        }
        if (ShiftDown) {
            dFilterNum = dFilterNum + m2[key];
        }
        else {
            dFilterNum = dFilterNum + m1[key];
        }
    }
    else if (key == 999)//очистка
    {
        dFilterNum = dFilterNum + '';
    }

    var i = 0;
    if (dFilterNum.length >= dFilterMax(dFilterMask)) {
        i = 1;//это значит что шаблон полностью заполнен, можно кнопку далее показывать
    }

    var dFilterFinal = '';
    for (dFilterStep = 0; dFilterStep < dFilterMask.length; dFilterStep++) {
        if (dFilterMask.charAt(dFilterStep) == '#' || dFilterMask.charAt(dFilterStep) == '=') {
            if (dFilterNum.length != 0) {
                dFilterFinal = dFilterFinal + dFilterNum.charAt(0);
                dFilterNum = dFilterNum.substring(1, dFilterNum.length);
            }
            else {
                dFilterFinal = dFilterFinal + "*";//если тут поставить пробел то все смотрится красиво, но тогда надо запрещать пробел в символе букв клавы
            }
        }
        else if (dFilterMask.charAt(dFilterStep) != '#' || dFilterMask.charAt(dFilterStep) != 'a') {
            dFilterFinal = dFilterFinal + dFilterMask.charAt(dFilterStep);
        }
    }


    var dFilterFinalTemp = replace(dFilterFinal, '*', '');
    dFilterNum = dFilterStrip(dFilterFinalTemp, dFilterMask);
    var dFilterNumTemp = dFilterNum.length;
    if (dFilterNumTemp >= dFilterMax2(dFilterMask) && dFilterNum.length > 0) {
        try {
            document.getElementById("masternext").style.visibility = "visible";
        } catch (e) {
        }
    }
    else {
        try {
            document.getElementById("masternext").style.visibility = "hidden";
        } catch (e) {
        }

    }


    textbox.value = dFilterFinal;

    var titl = window.document.title;

    if (titl == 'SEARCH') {
        RetSearchPanel(textbox.value);
    }

//////autoop
    if (titl == 'NUMBER8') {
        /*
         if (window.external.RetFastPanelID() > 0) {
         if (dFilterNum.length >= 10) {

         var newgate = SearchMSISDN(dFilterNum.substring(0,3));
         var changegateresult = 0;
         //alert(newgate);
         //alert(window.external.RetGateNameGateID(77).length);

         if (window.external.RetGateNameGateID(newgate).length > 0) { window.external.ChangeGateId(newgate); changegateresult = 1; }
         else {
         if (newgate == 80) { if (window.external.RetGateNameGateID('79').length > 0) { window.external.ChangeGateId(79); changegateresult = 1; newgate=79;} }      //beeline
         else if (newgate == 78) { if (window.external.RetGateNameGateID('77').length > 0) { window.external.ChangeGateId(77); changegateresult = 1;newgate=77; } }  //megafon
         else if (newgate == 73) {  if (window.external.RetGateNameGateID('75').length > 0) {  window.external.ChangeGateId(75); changegateresult = 1; newgate=75;}

         else if (window.external.RetGateNameGateID('206').length > 0) {  window.external.ChangeGateId(206); changegateresult = 1;newgate=206; }
         else if (window.external.RetGateNameGateID('10').length > 0) {  window.external.ChangeGateId(10); changegateresult = 1;newgate=10; } } //mts


         else { }

         }


         if (newgate > 0 && changegateresult == 1) {
         document.getElementById("opinfologo").innerHTML = "<img src=\"../logo/"+window.external.RetLogotipImgGateID(newgate)+"\" border=0>";
         strArray =new Array();
         var strForSplit = window.external.RetComissionGateID(newgate);
         var ret = "Комиссия<br><br>";
         try
         {
         strArray = strForSplit.split(";");
         if(strArray.length>1)
         {
         for (var i=0;i<strArray.length;i=i+3)
         {
         if(strArray[i]=='True')
         {//фиксированная сумма
         ret+=strArray[i+2]+"р. до "+strArray[i+1]+" руб.<BR>";
         }
         else
         {//процент от суммы
         ret+=strArray[i+2]+"% до "+strArray[i+1]+" руб.<BR>";
         }
         }
         }//if
         }
         catch(e)
         {
         Alert(e);
         }
         document.getElementById("opinfocomis").innerHTML = ret;
         document.getElementById("opinfostepinfo").innerHTML = '<P CLASS="TOPTEXT">'+window.external.RetStepInfoGateID(newgate)+'</p><BR>';
         document.getElementById("opinfostepinfofull").innerHTML = window.external.RetStepInfoFullGateID(newgate);

         }

         }
         return false;
         }
         */
    }
} // if titl8


function replace(fullString, text, by) {
    var strLength = fullString.length, txtLength = text.length;
    if ((strLength == 0) || (txtLength == 0)) return fullString;

    var i = fullString.indexOf(text);
    if ((!i) && (text != fullString.substring(0, txtLength))) return fullString;
    if (i == -1) return fullString;

    var newstr = fullString.substring(0, i) + by;

    if (i + txtLength < strLength)
        newstr += replace(fullString.substring(i + txtLength, strLength), text, by);

    return newstr;
}


function changeImageresizebuttonclickresize(pic, idpic) {
	changeImageresize(idpic,pic);
    sounder.src = "../sound/click.wav";
    changeImageresize(idpic, pic);
    buttonmove(idpic);
}

function changeImageresize(idpic, pic) {
    document.getElementById(idpic).src = "img/" + pic;
    var is = getImageSize(idpic);
    document.getElementById(idpic).width = is.width - 5;
    document.getElementById(idpic).height = is.height - 5;

}

function dd(idElement, ImgSrc) {

}

function getImageSize(id) {
    var oHlpr = document.createElement('IMG');
    var oPic = document.getElementById(id);
    oHlpr.style.visibility = 'hidden';
    oHlpr.style.position = 'absolute';
    oHlpr.top = 0;
    oHlpr.left = 0;
    oHlpr.src = oPic.src;
    document.body.appendChild(oHlpr);
    var imSize = {'width': oHlpr.offsetWidth, 'height': oHlpr.offsetHeight}
    document.body.removeChild(oHlpr);
    return imSize;
}

function logoresize(pic, idpic) {

    changeLogoresize(idpic, pic);
    setTimeout("changeLogoresize('" + idpic + "','" + pic + "')", 50);


}
function changeLogoresize(idpic, pic) {
    document.getElementById(idpic).src = "../logo/" + pic;
    var is = getImageSize(idpic);
    document.getElementById(idpic).width = is.width - 3;
    document.getElementById(idpic).height = is.height - 3;

}


// маска для поля вода конкретного шага
function GetMaskToInterface() {
    var ret = "##########";
    ret = window.external.GetMaskToInterface();
    //alert(ret)
    return ret;
}

//получение ранее введенных данных пользователем
function GetValToInterface() {

    var val = "";
    val = window.external.GetValToInterface();
//alert(val+' - из инт');
//alert(val);
    if (val == undefined)//проверка на пустое значение (нажали back ни чего не введя в текущем шаге)
    {

    }
    else {
        var titl = window.document.title;
        if (titl == 'NUMBER') {
            document.getElementById('numbox').value = val;
            if (val >= 20) {
                document.getElementById('numbox').style.fontSize = '41px';
            }
        }
        if (titl == 'NUMBER8') {
            document.getElementById('numbox').value = val;
            if (val >= 20) {
                document.getElementById('numbox').style.fontSize = '41px';
            }
        }
        if (titl == 'KEYBOARD') {
            document.getElementById('search').value = val;
        }
        if (titl == 'LIST') {
            //document.getElementById(val).style.backgroundImage = 'url(img/fo_search_res_btn_p.png)';
        }
        if (titl == 'CALENDAR') {
            document.getElementById('calendar').value = val;
        }
        if (titl == 'CALENDARMONTH') {
            document.getElementById('calendarmonth').value = val;
        }


    }


}

//данные шагов списокм для страницы оплаты
function RetAllStepDataPay() {
    var ret = "";
    var strForSplit = window.external.RetAllStepData();
    var strArray = new Array();
    strArray = strForSplit.split(";");
    if (strArray.length >= 2) {
        if (strArray.length > 5) {
            ret += "<font style=\"font-size: 15px; padding: 5;\">";
        }

        for (var i = 0; i < strArray.length; i = i + 2) {
            ret += "<BR><b>" + strArray[i] + "</b><BR>" + strArray[i + 1];
        }
    }
    return ret;
}

//данные шагов для подтверждения пользователем
function RetAllStepData() {
    var ret = "";
    var strForSplit = window.external.RetAllStepData();
//alert(strForSplit);
    var strArray = new Array();
    strArray = strForSplit.split(";");
    if (strArray.length >= 2) {

        if (strArray.length > 5) {
            ret += "<font style=\"font-size: 25px\">";
        }

        for (var i = 0; i < strArray.length; i = i + 2) {
            ret += "<BR>" + strArray[i] + "<BR>" + strArray[i + 1];
        }
    }
    // alert(ret);
    return ret;
}


//контейнеры online
function OnlineDiv(id, todo, msgtext) {
    if (id == "loadingdivmsg" && todo == 1) {
        try {
            document.getElementById('loadingdiv').style.visibility = 'visible';
            document.getElementById('cancbut').style.visibility = 'hidden';
            document.getElementById('polew').innerHTML = 60;
            stoptimer = 1;
            setTimeout(timer(), 1000);
        } catch (e) {
        }
    }
    if (id == "loadingdivmsg" && todo == 0) {
        try {
            document.getElementById('loadingdiv').style.visibility = 'hidden';
            stoptimer = 0;
        } catch (e) {
        }
    }
    if (id == "loadingdivmsgyesnomenu" && todo == 1) {
        try {
            document.getElementById('loadingdivmsg').style.visibility = 'hidden';
            stoptimer = 0;
            document.getElementById('loadingdiv').style.visibility = 'visible';
            document.getElementById('msgmenutext').innerHTML = msgtext;
        } catch (e) {
        }
    }
    if (id == "loadingdivmsgyesnomenu" && todo == 0) {
        try {
            stoptimer = 0;
            document.getElementById('loadingdiv').style.visibility = 'hidden';
            document.getElementById('msgnobutton').style.visibility = 'hidden';
            document.getElementById('msgyesbutton').style.visibility = 'hidden';
            document.getElementById('msgmenubutton').style.visibility = 'hidden';
        } catch (e) {
        }
    }

    if (id == "loadingdivPAYMENTmsg" && todo == 1) {
        try {
            document.getElementById('loadingdiv').style.visibility = 'visible';
            document.getElementById('loadingdivPAYMENTmsg').style.visibility = 'visible';
            document.getElementById('cancbut').style.visibility = 'hidden';
            document.getElementById('polew2').innerHTML = 60;
            stoptimer2 = 1;
            setTimeout(timer2(), 1000);

        } catch (e) {
        }
    }
    if (id == "loadingdivPAYMENTmsg" && todo == 0) {
        try {
            //document.getElementById('loadingdiv').style.visibility = 'hidden';
            stoptimer2 = 0;
            document.getElementById('loadingdivPAYMENTmsg').style.visibility = 'hidden';
            document.getElementById('polew2').style.visibility = 'hidden';

        } catch (e) {
        }
    }


    /*
     if (id == "loadingdivmsgyesnomenu" && todo == 1) { try {
     document.getElementById('loadingdivmsg').style.visibility = 'hidden';
     stoptimer=0;
     document.getElementById('loadingdiv').style.visibility = 'visible';
     document.getElementById('msgmenutext').innerHTML = msgtext;
     } catch(e){}
     }
     if (id == "loadingdivmsgyesnomenu" && todo == 0) { try {
     stoptimer=0;
     document.getElementById('loadingdiv').style.visibility = 'hidden';
     document.getElementById('msgnobutton').style.visibility = 'hidden';
     document.getElementById('msgyesbutton').style.visibility = 'hidden';
     document.getElementById('msgmenubutton').style.visibility = 'hidden';
     } catch(e){}
     }

     if (id == "loadingdivPAYMENTmsg" && todo == 1) { try {
     document.getElementById('loadingdiv').style.visibility = 'visible';
     document.getElementById('loadingdivPAYMENTmsg').style.visibility = 'visible';
     document.getElementById('polew2').innerHTML = 60;
     stoptimer=1;
     setTimeout(timer2(),1000);
     } catch(e){}
     }
     if (id == "loadingdivPAYMENTmsg" && todo == 0) { try {
     //document.getElementById('loadingdiv').style.visibility = 'hidden';
     document.getElementById('loadingdivPAYMENTmsg').style.visibility = 'hidden';
     document.getElementById('polew2').style.visibility = 'hidden';
     stoptimer=0;
     } catch(e){}

     */
    if (todo == 0) {
        var todotext = "hidden";
    } else {
        var todotext = "visible";
    }
    try {

        document.getElementById(id).style.visibility = todotext;
    } catch (e) {
    }

}

function timer() {

    var obj = document.getElementById('polew');
    obj.innerHTML--;

    if (obj.innerHTML <= 0) {
        obj.innerHTML = "00";

    }
    else {
        if (stoptimer == 1) {
            setTimeout(timer, 1000);
            StartTimeOut();
        }
    }
}

function timer2() {

    var obj = document.getElementById('polew2');
    obj.innerHTML--;

    if (obj.innerHTML <= 0) {
        obj.innerHTML = "00";

    }
    else {
        if (stoptimer2 == 1) {
            setTimeout(timer2, 1000);
            StartTimeOut();
        }
    }
}

function timeouttimer() {

    var obj2 = document.getElementById('screentimeouttimer');
    obj2.innerHTML--;
    if (obj2.innerHTML <= 0) {
        obj2.innerHTML = "0";
        gt();
    }
    else {
        if (stoptimeouttimer == 1) {
            setTimeout(timeouttimer, 1000);
        }
    }
}

function iamhere() {
    var obj2 = document.getElementById('screentimeouttimer');
    obj2.innerHTML = '11';
    document.getElementById("screentimeout").style.visibility = "hidden";
    document.getElementById("screentimeoutbg").style.visibility = "hidden";
    stoptimeouttimer = 0;
    window.external.START_BILL();

}

var comisshide = 0;
function opinfocomisfunc() {
    if (comisshide == 0) {
        var todotext = "visible";
        var todotext2 = "hidden";
        comisshide = 1;
    } else {
        var todotext = "hidden";
        var todotext2 = "visible";
        comisshide = 0;
    }
    try {

        document.getElementById('opinfocomis').style.visibility = todotext;
        document.getElementById('comison').style.visibility = todotext2;
        document.getElementById('enterednumbersmall').style.visibility = todotext2;

    } catch (e) {
    }

}
