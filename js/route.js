/**
 * Created by lodar on 18.02.15.
 */

/**
 * Определяет условия по которым терминал считается свободен и может выполнять удаленнык команды
 * @constructor
 */
function IsMain() {
    var status = false;
    if(Pages.Link.MAIN == GetCurrentPage()) {
        status = true;
    }
    return status;
}
