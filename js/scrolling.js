//function InWinfoOperatorScroll(id) {
//    this.obj = document.getElementById(id);
//    this.speed = 550;
//    this.height = 570;
//}
//InWinfoOperatorScroll.prototype = inherit(abstractScroll);
//InWinfoOperatorScroll.prototype.finalize = function () {
    //if (this.obj.offsetTop < 0) {
    //    document.getElementById("winfo_down").style.visibility = "visible";
    //}
    //else {
    //    document.getElementById("winfo_down").style.visibility = "hidden";
    //}
    //
    //if ((this.obj.offsetHeight + this.obj.offsetTop) < this.height) {
    //    document.getElementById("winfo_up").style.visibility = "hidden";
    //}
    //else {
    //    document.getElementById("winfo_up").style.visibility = "visible";
    //}
//};

function InGroupOperatorScroll(id) {
    this.obj = document.getElementById(id);
//    this.speed = 804;
//    this.height = 800;
}
InGroupOperatorScroll.prototype = inherit(abstractScroll);
/**
 * Показ скрытие кнопок вперед назад
 */

InGroupOperatorScroll.prototype.finalize = function () {
    if (this.obj.offsetTop < 0) {
        document.getElementById("btnback").style.visibility = "visible";
    }
    else {
        document.getElementById("btnback").style.visibility = "hidden";
    }
//alert(this.obj.offsetHeight);
//alert(this.obj.offsetTop);
//alert(this.height);
var check = this.obj.offsetHeight + this.obj.offsetTop - this.height;

if (check < 50) { document.getElementById("btnmoreop").style.visibility = "hidden"; }
    if ((this.obj.offsetHeight + this.obj.offsetTop) < this.height) {
        document.getElementById("btnmoreop").style.visibility = "hidden";
    }
    else {

        if (check > 50) { 
        document.getElementById("btnmoreop").style.visibility = "visible";
                        }
    }

//    getElemByID('page_num').innerHTML = this.page_num + " из " + CalcPageByGroup(countOperatorByGroup);
};
/*
function InSearchOperator(id) {
    this.obj = document.getElementById(id);
    this.speed = 201;
    this.height = 201;
}
InSearchOperator.prototype = inherit(abstractScroll);
InSearchOperator.prototype.finalize = function () {
    if (this.obj.offsetTop < 0) {
        document.getElementById("searchback").style.visibility = "visible";
    }
    else {
        document.getElementById("searchback").style.visibility = "hidden";
    }

    if ((this.obj.offsetHeight + this.obj.offsetTop) < this.height) {
        document.getElementById("searchnext").style.visibility = "hidden";
    }
    else {
        document.getElementById("searchnext").style.visibility = "visible";
    }
};
/*
function InOferta(id) {
    this.obj = document.getElementById(id);
    this.speed = 315;
    this.height = 315;
}
InOferta.prototype = inherit(abstractScroll);
InOferta.prototype.finalize = function () {
    if (this.obj.offsetTop < 0) {
        document.getElementById("ofertaup").style.visibility = "visible";
        document.getElementById("scrollcart").style.visibility = "visible";
    }
    else {
        document.getElementById("ofertaup").style.visibility = "hidden";
        document.getElementById("scrollcart").style.visibility = "hidden";
    }

    if ((this.obj.offsetHeight + this.obj.offsetTop) < this.height) {
        document.getElementById("scrollcartleft").style.visibility = "hidden";
        document.getElementById("ofertadown").style.visibility = "hidden";
    }
    else {
        document.getElementById("scrollcartleft").style.visibility = "visible";
        document.getElementById("ofertadown").style.visibility = "visible";
    }
};
*/

/**
 * Скролинг для listonline
 * Важно для правильного расчета высоты obj необходимо чтобы елемент не был скрыт
 * @param container_id
 * @constructor
 */
function ListOnlineScroll(container_id) {
    try {
        this.obj = document.getElementById(container_id);
    } catch (e) {
        SaveExceptionToLog(e, "scrolling.js", "ListOnlineScroll(" + container_id + ")");
    }

    this.speed = 510;
    this.height = 510;
}
ListOnlineScroll.prototype = inherit(abstractScroll);

ListOnlineScroll.prototype.init = function () {

    this.obj.up = false;
    this.obj.down = false;
    this.obj.fast = false;

    var container = document.createElement("div");
    var parent = this.obj.parentNode;
    container.id = "wscroll";
    parent.insertBefore(container, this.obj);
    parent.removeChild(this.obj);

    container.style.position = "relative";
    container.style.height = this.height + "px";
    container.style.overflow = "hidden";
    this.obj.style.position = "absolute";
    this.obj.style.top = "0";
    this.obj.style.left = "0";
    container.appendChild(this.obj);
    this.finalize();
};
/**
 * Показ скрытие кнопок вперед назад
 */
ListOnlineScroll.prototype.finalize = function () {
    if (this.obj.offsetTop < 0) {
        document.getElementById("button_up").style.visibility = "visible";
    }
    else {
        document.getElementById("button_up").style.visibility = "hidden";
    }

    if ((this.obj.offsetHeight + this.obj.offsetTop) < this.height) {
        document.getElementById("button_down").style.visibility = "hidden";
    }
    else {
        document.getElementById("button_down").style.visibility = "visible";
    }
};
/**
 * Скролит список указанное количество раз
 * @param countScroll
 */
ListOnlineScroll.prototype.scrollListDown = function(countScroll) {
    for(var i = 0; i < countScroll; i++) {
        this.onclick_down();
    }
};


/**
 * Скролинг для list
 * Важно для правильного расчета высоты obj необходимо чтобы елемент не был скрыт
 * @param container_id
 * @constructor
 */
function ListScroll(container_id) {
    try {
        this.obj = document.getElementById(container_id);
    } catch (e) {
        SaveExceptionToLog(e, "scrolling.js", "ListScroll(" + container_id + ")");
    }

    this.speed = 486;
    this.height = 486;
}
ListScroll.prototype = inherit(abstractScroll);

ListScroll.prototype.init = function () {

    this.obj.up = false;
    this.obj.down = false;
    this.obj.fast = false;

    var container = document.createElement("div");
    var parent = this.obj.parentNode;
    container.id = "wscroll";
    parent.insertBefore(container, this.obj);
    parent.removeChild(this.obj);
    container.style.position = "relative";
    container.style.height = this.height + "px";
    container.style.overflow = "hidden";
    this.obj.style.position = "absolute";
    this.obj.style.top = "0";
    this.obj.style.left = "0";
    container.appendChild(this.obj);
    this.finalize();
};
/**
 * Показ скрытие кнопок вперед назад
 */
ListScroll.prototype.finalize = function () {
    if (this.obj.offsetTop < 0) {
        document.getElementById("button_up").style.visibility = "visible";
    }
    else {
        document.getElementById("button_up").style.visibility = "hidden";
    }

    if ((this.obj.offsetHeight + this.obj.offsetTop) < this.height) {
        document.getElementById("button_down").style.visibility = "hidden";
    }
    else {
        document.getElementById("button_down").style.visibility = "visible";
    }
};
/**
 * Скролит список указанное количество раз
 * @param countScroll
 */
ListScroll.prototype.scrollListDown = function(countScroll) {
    for(var i = 0; i < countScroll; i++) {
        this.onclick_down();
    }
};














function PhotoScroll(container_id) {
    try {
        this.obj = document.getElementById(container_id);
    } catch (e) {
        SaveExceptionToLog(e, "scrolling.js", "PhotoScroll(" + container_id + ")");
    }

    this.speed = 500;
    this.width = 500;
}
PhotoScroll.prototype = inherit(abstractScroll);

PhotoScroll.prototype.init = function () {

    this.obj.up = false;
    this.obj.down = false;
    this.obj.fast = false;
    var container = document.createElement("div");
    var parent = this.obj.parentNode;
    container.id = "wscroll";
    parent.insertBefore(container, this.obj);
    parent.removeChild(this.obj);

    container.style.position = "relative";
    container.style.width = this.width + "px";
    container.style.overflow = "hidden";
    container.style.overflow = "";
    this.obj.style.position = "absolute";
    this.obj.style.top = "0";
    this.obj.style.left = "0";
    container.appendChild(this.obj);
    this.finalize();
};
/**
 * Показ скрытие кнопок вперед назад
 */
PhotoScroll.prototype.finalize = function () {

    if (this.obj.offsetLeft < 0) {
alert('a');
        document.getElementById("button_up2").style.visibility = "visible";
    }
    else {
        document.getElementById("button_up2").style.visibility = "hidden";
    }

    if ((this.obj.offsetWidth + this.obj.offsetLeft) < this.width) {
        document.getElementById("button_down2").style.visibility = "hidden";
    }
    else {
alert('b');
        document.getElementById("button_down2").style.visibility = "visible";
    }
document.getElementById("button_up2").style.visibility = "visible";
};
/**
 * Скролит список указанное количество раз
 * @param countScroll
 */
PhotoScroll.prototype.scrollListDown = function(countScroll) {
alert('c');
    for(var i = 0; i < countScroll; i++) {
        this.onclick_down();
    }
};
