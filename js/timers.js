/**
 * Created by lodar on 17.02.15.
 */

/**
 * Таймер в окне я еще здесь
 * в finalize() добавлены действия по окончанию таймера
 * @param container куда писать оставшееся кол-во секунд
 * @constructor
 */
function WtimersIamHere(container) {
    this.time = 0;
    this.wait = this.time;
    this.timeoutid = 0;
    this.timeContainer = container;
}
WtimersIamHere.prototype = inherit(abstractTimer);
WtimersIamHere.prototype.finalize = function () {
    Pages.HideOne("screentimeoutbg");
    Pages.HideOne("screentimeout");
    clearCart();
    if (GetCurrentPage() == Pages.Link.PAYMENT) {
        var sum = RetACCT();
        if (sum > 0) {
            SaveLog("Запущено проведение платежа из таймера 'я еще здесь' на странице оплаты. Вставлено купюр на сумму: " + sum);
            pay_fnc();
        } else {
            Main();
        }
    } else {
        Main();
    }

};
WtimersIamHere.prototype.stop = function () {
    this.wait = 0;
    clearTimeout(this.timeoutid);
    this.timeoutid = 0;
};
//Создаем обьект таймера для я еще здесь
var IamHereTimer = new WtimersIamHere('screentimeouttimer');


