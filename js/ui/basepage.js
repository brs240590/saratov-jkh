var Scrolls = {
    ScrollOffer: null,
    ScrollInGroup: null,
    ScrollInSearchOperator: null
};

var Pages = {
    //коностанты текущего местоположения
    Link: {
        OTHER: 0,
        STEPS: 1,
        CONFIRM: 2,
        CARD: 3,
        PAYMENT: 4,
        MAIN: 5,
        PRINT_CHECK: 6

    },
    //Текущая страница где находится пользователь
    currentPage: 5,
    timeout: 0,

    currentGroupID: null,

    Init: function() {
        //Скролинг перечня операторов в "О поставщиках услуг"
//        PhotoScroll = new PhotoScroll('masterphotos');
//        PhotoScroll.init();
    },

    ShowListOperatorByGroup: function(group_id) {
        //WNewEffects.click.button('group' + group_id);
        //setTimeout(function () {
        RetGroup(group_id);
        Pages.Show("wgroup");
        Pages.Steps.button.menu.show();
        Pages.ShowOne("wgroup_name");
        //}, 50);
    },

    Show: function(page_id) {
        sounder.src = "../sound/click.wav";
        Pages.Hide();
        Pages.ShowDiv(page_id);
        StartTimeOut();
    },

    ShowDiv: function(page_id) {

        Pages.ShowOne(page_id);
        if(page_id != 'wmainpage') 
            SetCurrentPage(Pages.Link.OTHER);

        //Главная страница

        if(page_id == 'wmainpage') {

   	Pages.Steps.button.back.show();

            SetCurrentPage(Pages.Link.MAIN);
            Pages.ShowOne('group_operator');
        document.getElementById('listenskud').style.display = 'block';
	document.body.style.background="url('img/bg0.jpg') no-repeat";

        } else {
	document.body.style.background="url('img/bg2.jpg')";
         //Pages.ShowOne("cart");
            document.getElementById('listenskud').style.display = 'none';
               }


        if(page_id == 'woferta') {
            Pages.ShowOne('wbutton_search');
            ScrollOferta = new InOferta('oferta');
            ScrollOferta.init();
            setHeaderText("Оферта");
        }
        //список операторов
        if(page_id == 'wgroup') {
            Pages.ShowOne("wbuttons_main");
            Pages.ShowOne("wbuttons");
	Pages.Steps.button.back.hide();
	if (cart.col > 0) {
        //Pages.Steps.button.menu.hide();
	                  } else {
//	Pages.Steps.button.menu.show();
	                         }

            Pages.ShowOne('wbutton_list_operator');
            ScrollInGroup = new InGroupOperatorScroll('logolist');
            ScrollInGroup.init();
        }
        /*
        //страница поиска
        if(page_id == 'wsearch_operator') {
            WNewEffects.click.button('wsearch');
            try {
                document.getElementById('searchresult').innerHTML = '';
            } catch(e) {
                SaveExceptionToLog(e, "basepage.js", 76);
            }

            Pages.ShowOne('wbutton_search');
            new UIFactory().makeKeyboard.search();
            ScrollInSearchOperator = new InSearchOperator('searchresult');
            ScrollInSearchOperator.init();
        }
*/
        /*
        //страница информации
        if(page_id == 'winfo_main' || page_id == "winfo_operator" || page_id == "winfo_help" || page_id == "winfo_payment") {

            setHeaderText("Информация");

            try {
                document.getElementById('waboutterm').src = 'img/btn_terminal_info.png';
                GetInfoAboutTerm("winfo_main");
            } catch(e) {
                SaveExceptionToLog(e, "basepage.js", 91);
            }
            try {
                document.getElementById('waboutoperat').src = 'img/btn_about_operators.png';
            } catch(e) {
                SaveExceptionToLog(e, "basepage.js", 96);
            }
            try {
                document.getElementById('waboutps').src = 'img/company_r.png';
            } catch(e) {
                SaveExceptionToLog(e, "basepage.js", 101);
            }
            try {
                document.getElementById('whelp').src = 'img/help_r.png';
            } catch(e) {
                SaveExceptionToLog(e, "basepage.js", 106);
            }


            if(page_id == 'winfo_main') {
                try {
                    document.getElementById('waboutterm').src = 'img/btn_terminal_info_p.png';
                } catch(e) {
                    SaveExceptionToLog(e, "basepage.js", 116);
                }
            } else if(page_id == "winfo_operator") {
                try {
                    document.getElementById('waboutoperat').src = 'img/btn_about_operators_p.png';
                } catch(e) {
                    SaveExceptionToLog(e, "basepage.js", 122);
                }
            } else if(page_id == "winfo_help") {
                try {
                    document.getElementById('whelp').src = 'img/help_r_p.png';
                } catch(e) {
                    SaveExceptionToLog(e, "basepage.js", 128);
                }
            } else if(page_id == "winfo_payment") {
                try {
                    document.getElementById('waboutps').src = 'img/company_r_p.png';
                } catch(e) {
                    SaveExceptionToLog(e, "basepage.js", 134);
                }
            }

            Pages.ShowOne("wbutton_term");
            Pages.ShowOne("winfoterm");

            var term_id_buttons = [];
            term_id_buttons[0] = 'whelp';
            term_id_buttons[1] = 'waboutps';
            term_id_buttons[2] = 'waboutoperat';
            term_id_buttons[3] = 'waboutterm';
            term_id_buttons[4] = 'wterm_mainmenu';
        }
        */
    },

    ShowOne: function(elem_id) {
        try {
            var showPage = document.getElementById(elem_id);  
            showPage.style.display = '';
        } catch(e) {
            SaveExceptionToLog(e, "basepage.js, ShowOne(" + elem_id + ")", 155);
        }
    },

    HideOne: function(elem_id, label) { 
        try {
            var hidePage = document.getElementById(elem_id);
            hidePage.style.display = 'none';
        } catch(e) {
            SaveExceptionToLog(e, "basepage.js, HideOne(" + elem_id + ") " + label, 164);
        }
    },

    Hide: function() {

        //бэкгроунд
        try {
            document.getElementById('screentimeoutbg').style.display = "none";
        } catch(e) {
            SaveExceptionToLog(e, "basepage.js", 175);
        }


        try {
        document.getElementById('wmainpage').style.display = "none";

            document.getElementById('screentimeoutbg').style.display = "none";
        } catch(e) {
            SaveExceptionToLog(e, "basepage.js", 182);
        }

        try {
//            document.getElementById('woferta').style.display = "none";
        } catch(e) {
//            SaveExceptionToLog(e, "basepage.js", 188);
        }

        try {
            document.getElementById('wupbutton').style.display = "none";
        } catch(e) {
            SaveExceptionToLog(e, "basepage.js", 194);
        }

        try {
            document.getElementById('wgroup').style.display = "none";
        } catch(e) {
            SaveExceptionToLog(e, "basepage.js", 200);
        }

        try {
//            document.getElementById('winfoterm').style.display = "none";
        } catch(e) {
//            SaveExceptionToLog(e, "basepage.js", 206);
        }

        try {
            document.getElementById('wbutton_term').style.display = "none";
        } catch(e) {
            SaveExceptionToLog(e, "basepage.js", 212);
        }

        try {
//            document.getElementById('winfo_main').style.display = "none";
        } catch(e) {
//            SaveExceptionToLog(e, "basepage.js", 218);
        }

        try {
//            document.getElementById('winfo_operator').style.display = "none";
        } catch(e) {
//            SaveExceptionToLog(e, "basepage.js", 224);
        }

        try {
//            document.getElementById('winfo_help').style.display = "none";
        } catch(e) {
//            SaveExceptionToLog(e, "basepage.js", 230);
        }
        try {
//            document.getElementById('winfo_payment').style.display = "none";
        } catch(e) {
//            SaveExceptionToLog(e, "basepage.js", 235);
        }

        try {
            document.getElementById('wbutton_list_operator').style.display = "none";
        } catch(e) {
            SaveExceptionToLog(e, "basepage.js", 247);
        }

        try {
            document.getElementById('wbutton_search').style.display = "none";
        } catch(e) {
            SaveExceptionToLog(e, "basepage.js", 253);
        }
        try {
//            document.getElementById('wsearch_operator').style.display = "none";
        } catch(e) {
//            SaveExceptionToLog(e, "basepage.js", 258);
        }

        try {
            document.getElementById('opinfostep_min_max_sum').style.display = "none";
        } catch(e) {
            SaveExceptionToLog(e, "basepage.js", 264);
        }

        try {
            //Поле для типа степа намбер
            document.getElementById('wdatabox').style.display = "none";
        } catch(e) {
            SaveExceptionToLog(e, "basepage.js", 271);
        }

        try {
            //Поле для типа степа кейборд
            document.getElementById('wdatabox_keyboard').style.display = "none";
        } catch(e) {
            SaveExceptionToLog(e, "basepage.js", 278);
        }

        try {
            //Инпут для фиобокс
            document.getElementById('wfiobox_field').style.display = "none";
        } catch(e) {
            SaveExceptionToLog(e, "basepage.js", 285);
        }
        try {
            //фикседсам
            document.getElementById('wfixedsum').style.display = "none";
        } catch(e) {
            SaveExceptionToLog(e, "basepage.js", 291);
        }
        try {
            //calendarbox
            document.getElementById('wcalendarmonthbox').style.display = "none";
        } catch(e) {
            SaveExceptionToLog(e, "basepage.js", 297);
        }
        try {
            //calendar
            document.getElementById('wcalendarbox').style.display = "none";
        } catch(e) {
            SaveExceptionToLog(e, "basepage.js", 303);
        }
        try {
            //calendar
            document.getElementById('opinfologo').style.display = "none";
        } catch(e) {
            SaveExceptionToLog(e, "basepage.js", 309);
        }
        try {
            //calendar
            document.getElementById('enterednumber').style.display = "none";
        } catch(e) {
            SaveExceptionToLog(e, "basepage.js", 333);
        }
        try {
            //страница вставки купюры и оплаты
            document.getElementById('wpayment').style.display = "none";
        } catch(e) {
            SaveExceptionToLog(e, "basepage.js", 340);
        }
        try {
            //контейнер где содержится цифровая клавиатура
            document.getElementById('wnumboard').style.display = "none";
        } catch(e) {
            SaveExceptionToLog(e, "basepage.js", 345);
        }

        try {
            //Клавиатура
            document.getElementById('wkeyboard').style.display = "none";
        } catch(e) {
            SaveExceptionToLog(e, "basepage.js", 353);
        }
        try {
            //Содержится список
            document.getElementById('wlist').style.display = "none";
        } catch(e) {
            SaveExceptionToLog(e, "basepage.js", 359);
        }


        try {
            document.getElementById('wbuttons_main').style.display = "none";
        } catch(e) {
            SaveExceptionToLog(e, "basepage.js", 366);
        }

        try {
            //страница вставки купюры и оплаты
            document.getElementById('printedcheck').style.display = "none";
        } catch(e) {
            SaveExceptionToLog(e, "basepage.js", 373);
        }

        try {
            //страница вставки купюры и оплаты
            document.getElementById('wcheck_print').style.display = "none";
        } catch(e) {
            SaveExceptionToLog(e, "basepage.js", 380);
        }

        try {
            //страница вставки купюры и оплаты
            document.getElementById('wbuttons_main_pay').style.display = "none";
        } catch(e) {
            SaveExceptionToLog(e, "basepage.js", 387);
        }
        //onlinediv

        try {
            //страница вставки купюры и оплаты
            document.getElementById('loadingdivmsg').style.display = "none";
        } catch(e) {
            SaveExceptionToLog(e, "basepage.js", 395);
        }

        try {
            //страница вставки купюры и оплаты
            document.getElementById('loadingdivmsgyesnomenu').style.display = "none";
        } catch(e) {
            SaveExceptionToLog(e, "basepage.js", 402);
        }
        try {
            //замята купюра
            document.getElementById('fiscalError').style.display = "none";
        } catch(e) {
            SaveExceptionToLog(e, "basepage.js", 414);
        }
        try {
            //замята купюра
            document.getElementById('selectcard').style.display = "none";
        } catch(e) {
            SaveExceptionToLog(e, "basepage.js", 419);
        }
        try {
            //страница 
            document.getElementById('cardfail').style.display = "none";
        } catch(e) {
            SaveExceptionToLog(e, "basepage.js", 429);
        }
        try {
            //страница 
            document.getElementById('pinpadtimerdiv').style.display = "none";
        } catch(e) {
            SaveExceptionToLog(e, "basepage.js", 435);
        }

        try {
	document.getElementById('skudpass').style.display = 'none';	
        } catch(e) {
            SaveExceptionToLog(e, "basepage.js", 436);
        }

        try {
	document.getElementById('moneychangetxt').style.display = 'none';	
        } catch(e) {
            SaveExceptionToLog(e, "basepage.js", 437);
        }

        try {
	document.getElementById('moneychange').style.display = 'none';	
        } catch(e) {
            SaveExceptionToLog(e, "basepage.js", 438);
        }


        try {
//	document.getElementById('wowstep').style.display = 'none';	
        } catch(e) {
//            SaveExceptionToLog(e, "basepage.js", 439);
        }


        Pages.HideOne('wlist_online');
        Pages.HideOne("wdown");
        Pages.HideOne("wup");
	Pages.HideOne("wbuttons_appoint");
        Pages.HideOne("wbuttons_main_pay_replay");

        Pages.HideOne('popup_confirm');
        Pages.HideOne('photo_up');
        Pages.HideOne('photo_down');
        Pages.HideOne('calendar-body');
        Pages.HideOne('schedule');

        //страница помощи абонента
//        Pages.HideOne('help_subscriber');
//        Pages.HideOne('wbutton_help_subscribe');
//        Pages.HideOne('search_print_check');
//        Pages.HideOne('pages_search_check');
//        Pages.HideOne('pages_enter_number');
//        Pages.HideOne('pages_see_search_check');
//        Pages.HideOne('button_subscribe_search');
//        Pages.HideOne('pages_type_search');
//        Pages.HideOne('pages_enter_keyboard');

        Pages.HideOne('wgroup_name');
        Pages.HideOne('wcountry');
        Pages.HideOne('wcurrency');

//        Pages.HideOne('welcomestep');
//        Pages.HideOne('welcomewoman');
//        Pages.HideOne('welcomeman');
        Pages.Steps.button.send_prorose.hide();


    },
    Loading: {
        load: function() {
            Pages.Hide();
            Pages.ShowOne('loadingdiv');
        }
    },
    Error: function() {

    },
    /*
    HelpSubscriber: {
        date: {
            show: function() {
                Pages.Hide();
                Pages.ShowOne('search_print_check');
                Pages.ShowOne('pages_search_check');
                Pages.ShowOne('wbutton_help_subscribe');
                Pages.HelpSubscriber.button.menu.show();
                Pages.HelpSubscriber.button.back.hide();
            },
            hide: function() {
                Pages.HideOne('search_print_check');
            }
        },
        number: {
            show: function() {
                Pages.Hide();
                Pages.ShowOne('search_print_check');
                Pages.ShowOne('pages_enter_number');
                Pages.ShowOne('wbutton_help_subscribe');
                Pages.HelpSubscriber.button.menu.hide();
                Pages.HelpSubscriber.button.back.show();
            },
            hide: function() {
                Pages.HideOne('pages_enter_number');
            }
        },
        list: {
            show: function() {
                Pages.Hide();
                Pages.ShowOne('search_print_check');
                Pages.ShowOne('pages_type_search');
                Pages.ShowOne('wbutton_help_subscribe');
                Pages.HelpSubscriber.button.menu.hide();
                Pages.HelpSubscriber.button.back.show();
            },
            hide: function() {
                Pages.HideOne('pages_type_search')
            }
        },
        keyboard: {
            show: function() {
                Pages.Hide();
                Pages.ShowOne('search_print_check');
                Pages.ShowOne('pages_enter_keyboard');
                Pages.ShowOne('wbutton_help_subscribe');
                Pages.HelpSubscriber.button.menu.hide();
                Pages.HelpSubscriber.button.back.show();
            },
            hide: function() {
                Pages.HideOne('pages_enter_keyboard');
            }
        },
        print: {
            show: function() {
                Pages.Hide();
                Pages.ShowOne('search_print_check');
                Pages.ShowOne('pages_see_search_check');
                Pages.ShowOne('wbutton_help_subscribe');
                Pages.HelpSubscriber.button.menu.show();
                Pages.HelpSubscriber.button.back.show();
                Pages.HelpSubscriber.button.next.hide();
            },
            hide: function() {
                Pages.HideOne('pages_see_search_check');
            }
        },
        button: {
            menu: {
                get: function() {
                    return getElemByID('button_subscribe_mainmenu');
                },
                show: function() {
                    var button = getElemByID('button_subscribe_mainmenu');
                    button.style.display = '';
                },
                hide: function() {
                    var button = getElemByID('button_subscribe_mainmenu');
                    button.style.display = 'none';
                    button.onclick = function() {
                    };
                }
            },
            back: {
                get: function() {
                    return getElemByID('button_subscribe_back');
                },
                show: function() {
                    var button = getElemByID('button_subscribe_back');
                    button.style.display = '';
                },
                hide: function() {
                    var button = getElemByID('button_subscribe_back');
                    button.style.display = 'none';
                    button.onclick = function() {
                    };
                }
            },
            next: {
                get: function() {
                    return getElemByID('button_subscribe_next');
                },
                show: function() {
                    var button = getElemByID('button_subscribe_next');
                    button.style.display = '';
                },
                hide: function() {
                    var button = getElemByID('button_subscribe_next');
                    button.style.display = 'none';
                    button.onclick = function() {
                    };
                }
            },
            search: {
                get: function() {
                    return getElemByID('button_subscribe_search');
                },
                show: function() {
                    var button = getElemByID('button_subscribe_search');
                    button.style.display = '';
                },
                hide: function() {
                    var button = getElemByID('button_subscribe_search');
                    button.style.display = 'none';
                    button.onclick = function() {
                    };
                }
            }

        }
    },
    */
    Steps: {
        autoDetectNumber: {
            show: function() {
                var text = getElemByID('confirm_text');
                Pages.ShowOne("screentimeoutbg");
                Pages.ShowOne('popup_confirm');
            },
            hide: function() {
                Pages.HideOne("screentimeoutbg");
                Pages.HideOne('popup_confirm');

                Pages.Steps.autoDetectNumber.button_ok().onclick = function() {
                };
                Pages.Steps.autoDetectNumber.button_cancel().onclick = function() {
                };
                var no_button = getElemByID('popup_confirm_nobutton');
                getElemByID('confirm_text').innerHTML = "";
            },
            button_ok: function() {
                return getElemByID('popup_confirm_yesbutton');
            },
            button_cancel: function() {
                return getElemByID('popup_confirm_nobutton');
            },
            text: function(text) {
                var text_block = getElemByID('confirm_text');
                text_block.innerHTML = text;
            },
            logo: function(img) {
                var logo = getElemByID('popup_confirm_img').innerHTML = img;
            }
        },
        button: {
            show_all: function() {
                Pages.ShowOne("wbuttons_main");
                Pages.ShowOne("wbuttons");
            },
            send_prorose: {
                show: function() {
                    Pages.ShowOne('wbuttons_no_money_send');

                },
                hide: function() {
                    Pages.HideOne('wbuttons_no_money_send');
                },
                get: function() {
                    return getElemByID('wbuttons_no_money_send');
                }
            },
            next: {
                show: function() {
                    try {
                        document.getElementById('wbuttons_main_next').style.display = '';
                    } catch(e) {
                        SaveExceptionToLog(e, "basepage.js", 440);
                    }
                },
                hide: function() {
                    try {
                        document.getElementById('wbuttons_main_next').style.display = 'none';
                    } catch(e) {
                        SaveExceptionToLog(e, "basepage.js", 447);
                    }
                },
                get: function() {
                    try {
                        return document.getElementById('wbuttons_main_next');
                    } catch(e) {
                        SaveExceptionToLog(e, "basepage.js", 454);
                    }
                }
            },
            back: {
                show: function() {
                    try {
                        document.getElementById('wbuttons_main_back').style.display = '';
                    } catch(e) {
                        SaveExceptionToLog(e, "basepage.js", 463);
                    }
                },
                hide: function() {
                    try {
                        document.getElementById('wbuttons_main_back').style.display = 'none';
                    } catch(e) {
                        SaveExceptionToLog(e, "basepage.js", 470);
                    }
                },
                get: function() {
                    try {
                        return document.getElementById('wbuttons_main_back');
                    } catch(e) {
                        SaveExceptionToLog(e, "basepage.js", 477);
                    }
                }
            },
            menu: {
                show: function() {
                    try {
                        document.getElementById('wbuttons_mainmenu').style.display = '';
                    } catch(e) {
                        SaveExceptionToLog(e, "basepage.js", 486);
                    }
                },
                hide: function() {
                    try {
                        document.getElementById('wbuttons_mainmenu').style.display = 'none';
                    } catch(e) {
                        SaveExceptionToLog(e, "basepage.js", 493);
                    }
                },
                get: function() {
                    try {
                        return document.getElementById('wbuttons_mainmenu');
                    } catch(e) {
                        SaveExceptionToLog(e, "basepage.js", 500);
                    }
                }
            },
            pay: {
                show: function() {
                    try {
                        document.getElementById('wbuttons_main_pay').style.display = '';
                    } catch(e) {
                        SaveExceptionToLog(e, "basepage.js", 509);
                    }
                },
                hide: function() {
                    try {
                        document.getElementById('wbuttons_main_pay').style.display = 'none';
                    } catch(e) {
                        SaveExceptionToLog(e, "basepage.js", 516);
                    }
                },
                get: function() {
                    try {
                        return document.getElementById('wbuttons_main_pay');
                    } catch(e) {
                        SaveExceptionToLog(e, "basepage.js", 523);
                    }
                }
            },
            pay_replay: {
                show: function() {
                    Pages.ShowOne('wbuttons_main_pay_replay')
                },
                hide: function() {
                    Pages.HideOne('wbuttons_main_pay_replay');
                },
                get: function() {
                    return document.getElementById('wbuttons_main_pay_replay');
                }
            }

        },
        listboard: {
            list: function() {
                Pages.Hide();
                Pages.Steps.button.show_all();
                Pages.ShowOne('wsteps');
                Pages.ShowOne('wlist');
                Pages.ShowOne('wup');
                Pages.ShowOne('wdown');
//		Pages.ShowOne('cart');		
//		Pages.ShowOne("newclientdiv");
                Pages.Steps.button.menu.hide();
		
            },
            listonline: function() {
                Pages.Hide();
                Pages.Steps.button.show_all();
                Pages.ShowOne('wsteps');
                Pages.ShowOne('wup');
                Pages.ShowOne('wdown');

                //Pages.ShowOne('opinfocomis');
                Pages.ShowOne('opinfologo');
                Pages.ShowOne('wlist_online');
            }
        },
        numboard: {
            number: function() {
                Pages.Hide();
                Pages.Steps.button.show_all();
                document.body.style.background="url('img/bg0.jpg') no-repeat";
                Pages.ShowOne('opinfologo');
                Pages.ShowOne('wdatabox');
                Pages.ShowOne('wnumboard');
                Pages.ShowOne('wsteps');
		        Pages.Steps.button.menu.show();
                Pages.Steps.button.back.show();
	        //if (PaymentOperatorID == 795) { Pages.Steps.button.back.hide(); }
            },
            fixedsum: function() {
                Pages.Hide();
                Pages.Steps.button.show_all();

//                Pages.ShowOne('opinfologo');
//                Pages.ShowOne('opinfocomis');
//                Pages.ShowOne('opinfostep_min_max_sum');
                Pages.ShowOne('wfixedsum');
                Pages.ShowOne('wnumboard');
                Pages.ShowOne('wsteps');
//		Pages.ShowOne('cart');		
//		Pages.ShowOne("newclientdiv");
                Pages.Steps.button.menu.hide();
            }
        },
        wow: function(){
                Pages.Hide();
                Pages.Steps.button.show_all();
                Pages.ShowOne('wowstep');
                Pages.Steps.button.menu.hide();
                Pages.Steps.button.next.hide();

        },
        keyboard: {
            fiobox: function() {
                Pages.Hide();
                Pages.Steps.button.show_all();
                Pages.ShowOne('opinfologo');
                //Pages.ShowOne('opinfocomis');
                Pages.ShowOne('wfiobox_field');
                Pages.ShowOne('wkeyboard');
                Pages.ShowOne('wsteps');
		document.body.style.background="#FFFFFF";
            },
            keyboard: function() {
                Pages.Hide();
                Pages.Steps.button.show_all();
                Pages.ShowOne('opinfologo');
                //Pages.ShowOne('opinfocomis');
                Pages.ShowOne('wdatabox_keyboard');
                Pages.ShowOne('wkeyboard');
                Pages.ShowOne('wsteps');

            }
        },

        calendar: {
            full: function() {
                Pages.Hide();
                Pages.Steps.button.show_all();
                Pages.ShowOne('opinfologo');
                //Pages.ShowOne('opinfocomis');
                Pages.ShowOne('wcalendarbox');
                Pages.ShowOne('wcalendar');
                Pages.ShowOne('wsteps');
            },
            month: function() {
                Pages.Hide();
                Pages.Steps.button.show_all();
                Pages.ShowOne('opinfologo');
                //Pages.ShowOne('opinfocomis');
                Pages.ShowOne('wcalendarmonthbox');
                Pages.ShowOne('wsteps');
            }
        },
        country: {
            countryList: function() {
                Pages.Hide();
                Pages.Steps.button.show_all();
                Pages.ShowOne('opinfologo');
                //Pages.ShowOne('opinfocomis');
                Pages.ShowOne('wcountry');
                Pages.ShowOne('wcurrency');
                Pages.ShowOne('wsteps');
            }
        },
        confirm: function() { 
            Pages.Hide();
            Pages.ShowOne("wsteps");
            Pages.ShowOne('enterednumber'); 
            Pages.ShowOne("wbuttons_main");
            Pages.ShowOne("wbuttons");
            Pages.ShowOne('opinfologo');
            Pages.ShowOne('carttotal');
            Pages.ShowOne("wbuttons_approve");
            document.body.style.background="url('img/bg0.jpg') no-repeat";
            //Pages.ShowOne('selectcard');
	    //if (IF_PINPAD() == false) { Pages.HideOne("wbuttons_select_card"); } else { Pages.ShowOne("wbuttons_select_card"); }

            Pages.Steps.button.back.hide();
            Pages.Steps.button.next.hide();
//            Pages.Steps.button.menu.hide();
        },
        method: function() {
            Pages.Hide();
            Pages.ShowOne("wsteps");
            Pages.ShowOne("wbuttons_main");
            Pages.ShowOne("wbuttons");
            Pages.ShowOne('selectcard');
            document.body.style.background="url('img/bg2.jpg') no-repeat";
	    if (IF_PINPAD() == false) { Pages.HideOne("wbuttons_select_card"); } else { Pages.ShowOne("wbuttons_select_card"); }
            Pages.Steps.button.next.hide();
            Pages.Steps.button.menu.show();
            Pages.Steps.button.back.show();
        },
        card: function() {
            Pages.Hide();
            Pages.ShowOne("wbuttons_main");
            Pages.ShowOne("wbuttons");
            Pages.ShowOne("pinpadtimerdiv");
            Pages.Steps.button.next.hide();
            Pages.Steps.button.menu.hide();

//            Pages.HideOne("newclientdiv");
//	    Pages.HideOne("newcart");
//	    alert('pin');
        },
        nomoneysend: function() {
            Pages.Steps.button.back.show();
            Pages.Steps.button.next.hide();
            Pages.Steps.button.send_prorose.show();
        },
        payment: function() {
            Pages.Hide();
//            Pages.ShowOne('opinfologo');
            Pages.ShowOne("wsteps");
            Pages.ShowOne('wpayment');
            //Pages.ShowOne('opinfocomisdiv');
//            Pages.ShowOne('opinfocomis');
            Pages.ShowOne("wbuttons_main");
            Pages.Steps.button.back.show();
            Pages.Steps.button.next.hide();
            Pages.Steps.button.menu.hide();
//        Pages.HideOne("newclientdiv");
        Pages.HideOne("pinpadtimerdiv");
//	alert('cash');


        },
        check: {
            printError: function() {
                Pages.Hide();
                Pages.ShowOne('printedcheck');
                Pages.ShowOne("wbuttons_main");
                Pages.Steps.button.back.show();
                Pages.Steps.button.next.show();
            },
            print: function() {
                SetCurrentPage(Pages.Link.PRINT_CHECK);
                Pages.Steps.button.back.hide();
                Pages.Hide();
                Pages.ShowOne("wcheck_print");
                //Pages.ShowOne("wbuttons_mainmenu");
                //ShowPrintButton();
                //Pages.ShowOne("wbuttons_main");
            },
            CardFail: function() {
                Pages.Hide();
                Pages.ShowOne("cardfail");
                Pages.ShowOne("wbuttons_main");
		Pages.Steps.button.back.hide();
                Pages.Steps.button.menu.hide();
            }


        }
    }
};