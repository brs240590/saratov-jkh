//Включает таймер возврата на главную страницу или запуска окна я еще здесь
function timersReturnToMain() {

    //stopWtimers();
    IamHereTimer.stop();
    var run = 1;
    //текущая страница не равно main
    if (GetCurrentPage() != Pages.Link.MAIN) {

        if (GetCurrentPage() == Pages.Link.PRINT_CHECK) {
            //Просто переводим пользователя на главную страницу
            run = 0;
            //Main();
            Link('Main.html');
        } else {

            run = 1;
        }
    } else {
        //Это главная страница и не нужно никуда перемещать
        run = 0;
        //обновляем страницу main.html
        MainUpdate();
    }

    //текущее время запуска
    var currentTime = new Date().getTime();
    //разница с прошлым запуском
    var diff = Math.round((currentTime - lastWtimersTurnOff) / 100) / 10;
    var interval = Math.round(RetTimeOut() / 1000);

    //если я еще здесь можно запускать run = 1 и я еще здесь в данный момент не показан wtimers = 0 и последний запуск
    //был
    if (run == 1 && wtimers == 0 && diff >= interval) {

        if (GetCurrentPage() == Pages.Link.PAYMENT) {
            SaveLog('timersReturnToMain() on payment page stop bill');
            STOP_BILL();
        }
        //Запускаем таймер по истичении которого 10 sec
        IamHereTimer._init(60);
        IamHereTimer.start();
        //wtimers.push(new WtimersIamHere('screentimeouttimer', 10).start());
        //addData(wtimers);
        Pages.HideOne('loadingdivPAYMENTmsg');
        Pages.HideOne('loadingdivmsg');
        Pages.HideOne('loadingdivmsgyesnomenu');
        Pages.ShowOne("screentimeoutbg");
        Pages.ShowOne("screentimeout");
        wtimers = 1;
    }
}

/**
 * Оператор по быстрому доступу
 * При изменении в фаст панели размеров требуется поменять и здесь размеры
 * @param PanelID
 * @param width
 * @param height
 * @returns {string}
 * @constructor
 */
function RetFastPanel(PanelID, width, height) {
    var ret = "";
    var path = "../logo/";
    //ширина по умолчанию
    var w = 190;
    //высота по умолчанию
    var h = 105;
    var GateID = RetFastPanelGateID(PanelID);
    if (GateID < 0) {
        var gate = GateID * -1;
        //подкгруппа
        var img = RetLogotipImgGroupID(gate);
        if (img != '') {
            ret = "<a onclick='Pages.ShowListOperatorByGroup(" + gate + ");'><img width='185' height='80' src='" + path + img + "' border='0'></a>";
        }

    } else if (GateID > 0) {//есть шлюз для этой быстрой панели
        var img = window.external.RetFastPanelLogotipImg(PanelID);
        if (img != 'nologo') {
            if (width != undefined)
                w = width;
            if (height != undefined)
                h = height;
            //ret = "<a onclick='ClickOperator(" + GateID + ");'>"+ createImgByfilter(w, h, path) +"</a>";
            ret = "<a onclick='ClickOperator(" + GateID + ");'><img width='185' height='80' src='" + path + img + "' border='0'></a>";
        }
    }
    return ret;
}

/**
 * Формирует списки операторов
 * @param GroupID
 * @returns {string}
 * @constructor
 */

function RetGroup(GroupID) {
//var abstractScroll = new WScrolling("abstract scroll");
//alert(GroupID);

    SetCurrentPage(Pages.Link.OTHER);
    StartTimeOut();
    try {
        document.getElementById('logolist').style.marginTop = "80px";
        document.getElementById('logolist').style.marginLeft = "50px";
        document.getElementById('servicegroupname').innerHTML = '';
        Pages.Steps.button.next.hide();
        //количество операторов
        countOperatorByGroup = 0;
        //номер страницы при скролинге
        currentNumPage = 0;
        var ret = "";


        var browappArr = JSON.parse(window.external.GetKeyForInterface('browup'));

        var column = 0;//по сколько в ряд в таблице

        //подгруппы

        if (GroupID == 237) {

            if (window.external.GetKeyForInterface('selectMasterFirst') == 1) {
                var servicecartData = '{"col":0,"summ":0,"cartvalue":{}}';
                servicecart = JSON.parse(servicecartData);
                var serviceGroupData = '{"group":{}}';
                serviceGroup = JSON.parse(serviceGroupData);
                window.external.SetKeyFromInterface('selectMasterFirst', 0);
            }
            ret = "<br><br><br><br><br><br><br><br><table cellpadding=0 cellspacing=0 border=0 align=center style='border:0px solid black;left:-20px;position:relative;'>";
            Pages.HideOne('group236');
            Pages.HideOne('booking');
            var grp = window.external.BrowUPGetGroup();
            var groupArray = JSON.parse(grp);
            for (var i = 0; i < groupArray.length; i++) {
                countOperatorByGroup++;
                if (column == 0) ret += '<TR>';
                var groupcol = 0;
                var groupid = groupArray[i].Id;
                try {
                    groupcol = serviceGroup.group[groupid].col;
                } catch (e) {
                }
                if (groupcol > 0) {
                    ret += '<td align=center id="groupid' + groupArray[i].Id + '"  style="text-align: center; vertical-align: middle; width: 370; height: 110; background: url(img/prov_bgr_appoint_p.png) no-repeat;padding-right: 30px; padding-bottom: 30px;">';
                } else {
                    ret += '<td align=center id="groupid' + groupArray[i].Id + '"  style="text-align: center; vertical-align: middle; width: 370; height: 110; background: url(img/prov_bgr_appoint.png) no-repeat;padding-right: 30px; padding-bottom: 30px;">';
                }

                ret += '<table border=0 width=370 height=110 style="margin-left:-10px;margin-top:5px;"><tr>';
                ret += '<td CLASS="KB" style="color: #FFFFFF;" ID="groupidname' + groupArray[i].Id + '" onclick="changeServiceGroupName(\'' + groupArray[i].Name + '\', \'' + groupArray[i].Id + '\');RetGroup(' + groupArray[i].Id + ');">' + groupArray[i].Name + '</td>';
                ret += '</tr></table>';
                ret += '</td>';

                column = column + 1;
                if (column == 3) {
                    column = 0;
                    ret += '</TR>';
                }
            }
        }
        else if (GroupID == 238) {
            document.getElementById('logolist').style.marginTop = "0px";
            ret = "<table cellpadding=0 cellspacing=0 border=0 align=center style='border:0px solid black;left:-100px;position:relative;'>";
            Pages.HideOne('group236');
            Pages.HideOne('booking');
            var grp = window.external.BrowUPGetMasters();
//alert(grp);
            var groupArray = JSON.parse(grp);
            for (var i = 0; i < groupArray.length; i++) {
                countOperatorByGroup++;
                if (column == 0) ret += '<TR>';
                var groupcol = 0;
                var groupid = groupArray[i].Id;
                try {
                    groupcol = serviceGroup.group[groupid].col;
                } catch (e) {
                }
                if (groupArray[i].Photo == null) {
                    groupArray[i].Photo = 'img/avatar.png';
                }
                ;
//                 ret += '<td CLASS="KB" style="width: 121px; color: #000000;height:150px;vertical-align:top;padding-top:0px;" onclick="getMastersTime('+groupArray[i].Id+','+servicesArr+',\''+groupArray[i].Name+'\');"><img src="'+groupArray[i].Photo+'" style="'+selectedmaster+'" id="mastersphoto'+groupArray[i].Id+'" height=110><br>'+groupArray[i].Name+'</td>';

                if (groupcol > 0) {
                    ret += '<td align=center id="groupid' + groupArray[i].Id + '"  style="text-align: center; vertical-align: middle; width: 480; height: 110; background: url(img/prov_bgr_appoint_p.png) no-repeat;padding-right: 30px; padding-bottom: 30px;">';
                } else {
                    ret += '<td align=center id="groupid' + groupArray[i].Id + '"  style="text-align: center; vertical-align: middle; width: 480; height: 110; padding-right: 30px; padding-bottom: 0px;padding-top: 32px;">';
                }

                ret += '<table border=0 width=480 height=155 style="margin-left:0px;margin-top:5px;"><tr>';
                selectedmaster = 'border: 15px solid #FFFFFF;border-radius: 65px;';
                ret += '<td width="150"><img onclick="selectMaster(\'' + groupArray[i].Name + '\', \'' + groupArray[i].Id + '\');" src="' + groupArray[i].Photo + '" style="' + selectedmaster + '" id="mastersphoto' + groupArray[i].Id + '" height=110></td>';
                ret += '<td CLASS="KB" style="margin-left: 50px;background: url(img/bgr_mastername.png) no-repeat;width:407px;color: #FFFFFF;" ID="selectmasterid' + groupArray[i].Id + '" onclick="selectMaster(\'' + groupArray[i].Name + '\', \'' + groupArray[i].Id + '\');">' + groupArray[i].Name + '</td>';
                ret += '</tr></table>';
                ret += '</td>';

                column = column + 1;
                if (column == 1) {
                    column = 0;
                    ret += '</TR>';
                }
            }
        }

        else if (GroupID == 260) { //todo поменять GroupID
            //
        }
        else if (GroupID == 998) {
            ret = "<br><br><br><br><br><br><br><br><table cellpadding=0 cellspacing=0 border=0 align=center style='border:0px solid black;left:-20px;position:relative;'>";
            Pages.HideOne('group236');
            Pages.HideOne('booking');
            var masterid = window.external.GetKeyForInterface('browmasterid');

            var grp = window.external.BrowUPGetGroupForMaster(masterid);
            var groupArray = JSON.parse(grp);
            for (var i = 0; i < groupArray.length; i++) {
                countOperatorByGroup++;
                if (column == 0) ret += '<TR>';
                var groupcol = 0;
                var groupid = groupArray[i].Id;
                try {
                    groupcol = serviceGroup.group[groupid].col;
                } catch (e) {
                }
                if (groupcol > 0) {
                    ret += '<td align=center id="groupid' + groupArray[i].Id + '"  style="text-align: center; vertical-align: middle; width: 370; height: 110; background: url(img/prov_bgr_appoint_p.png) no-repeat;padding-right: 30px; padding-bottom: 30px;">';
                } else {
                    ret += '<td align=center id="groupid' + groupArray[i].Id + '"  style="text-align: center; vertical-align: middle; width: 370; height: 110; background: url(img/prov_bgr_appoint.png) no-repeat;padding-right: 30px; padding-bottom: 30px;">';
                }

                ret += '<table border=0 width=370 height=110 style="margin-left:-10px;margin-top:5px;"><tr>';
                ret += '<td CLASS="KB" style="color: #FFFFFF;" ID="groupidname' + groupArray[i].Id + '" onclick="changeServiceGroupName(\'' + groupArray[i].Name + '\', \'' + groupArray[i].Id + '\');RetGroup(' + groupArray[i].Id + ');">' + groupArray[i].Name + '</td>';
                ret += '</tr></table>';
                ret += '</td>';

                column = column + 1;
                if (column == 3) {
                    column = 0;
                    ret += '</TR>';
                }
            }
        }
        else if (GroupID > 1000) {

            document.getElementById('servicegroupname').innerHTML = serviceGroup.name;//вывод результата

            var serviceFontColor = '';
            ret = "<table cellpadding=0 cellspacing=0 border=0 align=center style='border:0px solid black;position:relative;0px;left:-50px;'>";
            Pages.HideOne('group236');
            Pages.HideOne('booking');
            document.body.style.background = "url('img/bg0.jpg') no-repeat";
            if (window.external.GetKeyForInterface('selectMasterFirst') == 1) {
                var masterid = window.external.GetKeyForInterface('browmasterid');
                var grp = window.external.BrowUPGetGatesForMaster(masterid, GroupID);
            } else {
                var grp = window.external.BrowUPGetGates(GroupID);
            }
            var groupArray = JSON.parse(grp);
            var servicename = '';
            var fontsize = '';
            for (var i = 0; i < groupArray.length; i++) {
                countOperatorByGroup++;

                if (column == 0) ret += '<TR>';

                if (servicecart.cartvalue[groupArray[i].Id]) {
                    ret += '<td align=center  id="service' + groupArray[i].Id + '" style="text-align: center; vertical-align: middle; width: 410; height: 233; background: url(img/prov_bgr_menu_p.png) no-repeat;padding-right: 30px; padding-bottom: 30px;">';
                    serviceFontColor = '#000000';
                } else {
                    serviceFontColor = '#FFFFFF';
                    ret += '<td align=center  id="service' + groupArray[i].Id + '" style="text-align: center; vertical-align: middle; width: 410; height: 233; background: url(img/prov_bgr_menu.png) no-repeat;padding-right: 30px; padding-bottom: 30px;">';
                }
                ret += '<table border=0 width=410 height=233 style="margin-left:0px;margin-top:5px;">';
                servicename = groupArray[i].Name;

                if (servicename.length > 80) {
                    fontsize = 32;
                } else {
                    fontsize = 40;
                }
                ret += '<tr><td colspan=2 CLASS="KB" style="font-size: ' + fontsize + 'px;color: ' + serviceFontColor + ';height:120px;vertical-align:top;padding-top:30px;" ID="servicename' + groupArray[i].Id + '" onclick="AddServiceId(' + groupArray[i].Id + ',\'' + groupArray[i].Pricetxt + '\',' + serviceGroup.id + ',\'' + groupArray[i].Name + '\',\'' + groupArray[i].Minutes + '\');">' + groupArray[i].Name + '</td></tr>';
                ret += '<tr><td CLASS="KB" style="width:300px;color: ' + serviceFontColor + ';font-weight:lighter;text-align: left;padding-left:20px;" ID="servicetime' + groupArray[i].Id + '" onclick="AddServiceId(' + groupArray[i].Id + ',\'' + groupArray[i].Pricetxt + '\',' + serviceGroup.id + ',\'' + groupArray[i].Name + '\',\'' + groupArray[i].Minutes + '\');"><small>' + groupArray[i].Minutes + ' мин.</small></td>';
                ret += '<td CLASS="KB" style="color: ' + serviceFontColor + ';text-align: right;padding-right:20px;" ID="serviceprice' + groupArray[i].Id + '" onclick="AddServiceId(' + groupArray[i].Id + ',\'' + groupArray[i].Pricetxt + '\',' + serviceGroup.id + ',\'' + groupArray[i].Name + '\',\'' + groupArray[i].Minutes + '\');"><small>' + groupArray[i].Pricetxt + ' руб.</small></td></tr>';
                ret += '</table>';
                ret += '</td>';

                column = column + 1;
                if (column == 2) {
                    column = 0;
                    ret += '</TR>';
                }
            }
            if (servicecart.col > 0) {
                Pages.Steps.button.next.show();
            }
            document.getElementById('logolist').style.marginTop = "0px";
        }

        else if (GroupID == 999) {

            Pages.HideOne('group236');
            Pages.HideOne('booking');
            document.body.style.background = "url('img/bg0.jpg') no-repeat";

            var servicesArr = '';

            for (var key in servicecart.cartvalue) {
                servicesArr += key + ',';
            }
            servicesArr = servicesArr.substr(0, servicesArr.length - 1);
            var grp = window.external.BrowUPGetMastersForGates(servicesArr);

            window.external.SetKeyFromInterface('MastersForGates', grp);
            var groupArray = JSON.parse(grp);

            if (groupArray.length > 0) {

                if (window.external.GetKeyForInterface('selectMasterFirst') == 1) {

                    ret += '<div id="masterphotos" style="width:1100px;height:235px;display: inline-block;border:0px solid;overflow:hidden;left: -10px;position:relative;">';
                    selectedmaster = 'border: 15px solid #e3bdb8;border-radius: 65px;';
                    for (var i = 0; i < groupArray.length; i++) {
                        if (groupArray[i].Id == window.external.GetKeyForInterface('browmasterid')) {

                            if (groupArray[i].Photo == null) {
                                groupArray[i].Photo = 'img/avatar.png';
                            }
                            ;
                            ret += '<div style="width:200px;display: inline-block;vertical-align:top;">';
                            ret += '<table border=0 width=200 height=150 style="margin-left:0px;margin-top:0px;">';
                            ret += '<tr>';
                            if (groupArray[i].Photo == null) {
                                groupArray[i].Photo = 'img/avatar.png';
                            }
                            ;
                            ret += '<td CLASS="KB" style="width: 121px; color: #000000;height:150px;vertical-align:top;padding-top:0px;" onclick="getMastersTime(' + groupArray[i].Id + ',' + servicesArr + ',\'' + groupArray[i].Name + '\');"><img src="' + groupArray[i].Photo + '" style="' + selectedmaster + '" id="mastersphoto' + groupArray[i].Id + '" height=110><br>' + groupArray[i].Name + '</td>';
                            ret += '</tr>';
                            ret += '</table>';
                            ret += '</div>';
                            break;
                        }

                    }
                    ret += '</div>';
                } else {
                    var selectedmaster = '';
                    ret += '<div id="masterphotos" style="width:1100px;height:235px;display: inline-block;border:0px solid;overflow:hidden;left: -10px;position:relative;">';
                    for (var i = 0; i < groupArray.length; i++) {

                        if (i == 0) {
                            selectedmaster = 'border: 15px solid #e3bdb8;border-radius: 65px;';
                        } else {
                            selectedmaster = 'border: 15px solid #FFFFFF;border-radius: 65px;';
                        }
                        if (groupArray[i].Photo == null) {
                            groupArray[i].Photo = 'img/avatar.png';
                        }
                        ;
                        ret += '<div style="width:200px;display: inline-block;vertical-align:top;">';
                        ret += '<table border=0 width=200 height=150 style="margin-left:0px;margin-top:0px;">';
                        ret += '<tr>';
                        if (groupArray[i].Photo == null) {
                            groupArray[i].Photo = 'img/avatar.png';
                        }
                        ;
                        ret += '<td CLASS="KB" style="width: 121px; color: #000000;height:150px;vertical-align:top;padding-top:0px;" onclick="getMastersTime(' + groupArray[i].Id + ',' + servicesArr + ',\'' + groupArray[i].Name + '\');"><img src="' + groupArray[i].Photo + '" style="' + selectedmaster + '" id="mastersphoto' + groupArray[i].Id + '" height=110><br>' + groupArray[i].Name + '</td>';
                        ret += '</tr>';
                        ret += '</table>';
                        ret += '</div>';

                        if (groupArray.length > 5) {
                            Pages.ShowOne('photo_down');
                            Pages.ShowOne('photo_up');
                        }

                    }
                    ret += '</div>';
                }
                Pages.ShowOne('calendar-body');
                Pages.ShowOne('schedule');
            } else {
                text = '<br><br><br><font style="color:#000000;">Мы не смогли подобрать мастера на все выбранные услуги :-(</b>';
                OnlineDiv('loadingdivmsgyesnomenu', 1, text);
                OnlineDiv('msgnobutton', 1);
                OnlineDiv('msgyesbutton', 0);
                OnlineDiv('msgmenubutton', 1);
            }
            document.getElementById('logolist').style.marginTop = "0px";
            document.getElementById('logolist').style.marginLeft = "0px";
            if (window.external.GetKeyForInterface('browmasterid') > 1) {
                getMastersTime(window.external.GetKeyForInterface('browmasterid'), servicesArr, window.external.GetKeyForInterface('browmastername'));
            } else {
                getMastersTime(groupArray[0].Id, servicesArr, groupArray[0].Name);
            }
            buildCalendar(0);

/////////////////////////////////////////////////////////////////////////////////////calendar


        }
        else {

            document.body.style.background = "url('img/bg.jpg') no-repeat";
            var strForSplit = RetGatesForGroup(GroupID);
            //подгруппы

            var strForSplit2 = RetSubGroups(GroupID);
            GateID = 0;
            strArray = new Array();
            strArray2 = new Array();
            strArray = strForSplit.split(";");
            strArray2 = strForSplit2.split(";");

            Pages.ShowOne('group236');
            Pages.ShowOne('booking');
            ret = "<table cellpadding=0 cellspacing=50 border=0 align=center>";

            //шлюзы
            if (browappArr.length >= 1) {
                for (var i = 0; i < browappArr.length; i++) {
                    var new_line = false;
                    if (column == 0) ret += '<TR>';
                    if (cart.cartvalue[browappArr[i].services]) {
                        ret += '<td align=left WIDTH="579" HEIGHT="225" style="background: url(img/prov_bgr_gr.png) no-repeat;" id="cell' + strArray[i] + '">';
                        ret += '<div class="provcolinmenu" id="provcolinmenu' + browappArr[i].services + '" style="display:none;">' + cart.cartvalue[browappArr[i].services].providercol + '</div>';
                        buttonfontcolor = "#FFFFFF";
                        rubfontcolor = "#FFFFFF";
                    } else {
                        ret += '<td align=left WIDTH="599" HEIGHT="225" style="border:0px solid black;background: url(img/prov_bgr.png) no-repeat;" id="cell' + browappArr[i].services + '">';
                        ret += '<div class="provcolinmenu" id="provcolinmenu' + browappArr[i].services + '" style="display:none;"></div>';
                        buttonfontcolor = "#FFFFFF";//e3bdb8
                        rubfontcolor = "#e3bdb8";
                    }
                    ret += '<table border=0 width=479 height=175 align=left style="margin-top:-49px;">' +
                        '<tr>';
                    ret += '<td CLASS="KB" ID=op' + browappArr[i].services + ' onclick="AddProvToCart(\'' + browappArr[i].services + '\',\'' + browappArr[i].Name + '\',\'' + browappArr[i].Price + '\',\'' + browappArr[i].MasterID + '\', \'' + browappArr[i].Time + '\', \'' + browappArr[i].FIO + '\', \'' + browappArr[i].Photo + '\');';
                    ret += 'ClickOperator(\'1373\');"><font class="browupprice" style="color: ' + rubfontcolor + '">' + browappArr[i].Price + '<span class="rubprice" style="color: ' + rubfontcolor + '">Р</span></font><br><font style="color: ' + buttonfontcolor + ';"  class="grfontnologo" id="cellname' + browappArr[i].services + '">' + browappArr[i].Name.toUpperCase() + '</font></td>';
                    ret += '</tr>' +
                        '</table>' +
                        '</td>' +
                        '';
                    column = column + 1;
                    if (column == 2) {
                        column = 0;
                        new_line = true;
                        ret += '</TR>';
                    }
                }
            }
            if (strArray2.length > 1) {
                ret = "<br><br><br><br><br><br><br><br><br><br><br><br><table cellpadding=0 cellspacing=50 border=0 align=center>";
                Pages.HideOne('group236');
                Pages.HideOne('booking');
                for (var i = 0; i < strArray2.length; i = i + 3) {

                    countOperatorByGroup++;

                    if (column == 0) ret += '<TR>';


                    if (strArray2[i + 1] == 'nologo') {//нет рисунка оператора
                        ret += '<td align=left WIDTH="579" HEIGHT="225" style="background: url(img/prov_bgr.png) no-repeat;" id="cell' + strArray[i] + '">';
                        ret += '<table border=0 width=479 height=175 style="margin-top:-49px;"><tr>';
                        ret += '<td CLASS="KB" ID=op' + strArray2[i] + ' onclick="RetGroup(' + strArray2[i] + ');"><font style="color:#FFFFFF;font-size:40px;">' + strArray2[i + 2].toUpperCase() + '</font></td>';
                        ret += '</tr></table>';
                        ret += '</td>';
                    }
                    column = column + 1;
                    if (column == 4) {
                        column = 0;
                        ret += '</TR>';
                    }
                }
            }

        }
        ret += createLinkSearchFromClient(new_line);
        ret += "</table>";
        document.getElementById('logolist').innerHTML = ret;//вывод результата

//            ScrollInGroup = new InGroupOperatorScroll('logolist');
//            ScrollInGroup.init();
        Pages.Steps.button.back.show();
        var back = document.getElementById('wbuttons_main_back');
        WEvents.remove(back, 'click');
        back.onclick = function () {

            if (GroupID > 1000) {
                if (window.external.GetKeyForInterface('selectMasterFirst') == 1) {
                    RetGroup(998);
                } else {
                    Pages.ShowListOperatorByGroup(237);
                }
            } else {
                Pages.ShowListOperatorByGroup(236);
            }
        };


    } catch (e) {
        SaveExceptionToLog(e, "Shared/utils.js", "RetGroup(" + GroupID + ") strForSplit: " + strForSplit + " strForSplit2: " + strForSplit2);
    }
    ScrollInGroup = new InGroupOperatorScroll('logolist');
    if (GroupID == 238) {
        ScrollInGroup.speed = 768;
        ScrollInGroup.height = 770;
    } else {
        ScrollInGroup.speed = 804;
        ScrollInGroup.height = 800;
    }
    ScrollInGroup.init();

    return ret;

}


//////////////////////////online start
function getMastersTime(masterid, services, name) {

//alert(masterid+" "+services+" "+name);

    window.external.SetKeyFromInterface('browmasterid', masterid);
    window.external.SetKeyFromInterface('browmastername', name);
    window.external.SetKeyFromInterface('browservices', services);

    document.getElementById('schedule').innerHTML = '';

    var datesel = window.external.GetKeyForInterface('datesel');

    if (datesel.length < 2) {
        Data = new Date();
        Year = Data.getFullYear();
        Month = Data.getMonth() + 1;
        Day = Data.getDate();
        if (Day < 10) {
            Day = '0' + Day;
        }
        if (Month < 10) {
            Month = '0' + Month;
        }
        datesel = Year + "-" + Month + "-" + Day;
        window.external.SetKeyFromInterface('datesel', datesel);
    }
    SaveLog("------->" + masterid + " " + datesel + " " + services);

    window.external.BrowUPGetTimeForMaster(masterid, datesel, services);
    var onlineTimer = new IndependentTimer(60, "Загружаю расписание");
    onlineTimer.timer.directionTimeToBig();
    onlineTimer.timer.finalize = function () {
        onlineTimer.stop();
        var status = getResponseOnlinePayment();
        if (status == 1) {

            var masters = JSON.parse(window.external.GetKeyForInterface('MastersForGates'));

            var selectedmaster = '';
            for (var i = 0; i < masters.length; i++) {
                if (masters[i].Id == masterid) {
                    selectedmaster = '15px solid #e3bdb8';
                } else {
                    selectedmaster = '15px solid #FFFFFF';
                }
                try {
                    document.getElementById('mastersphoto' + masters[i].Id).style.border = selectedmaster;
                } catch (e) {
                }
            }

            var schedule = JSON.parse(window.external.GetKeyForInterface('BrowUPTime'));

            if (schedule.Times.length > 0) {
                var scheduleDiv = '';
                for (var i = 0; i < schedule.Times.length; i++) {
                    scheduleDiv += '<div id="time' + schedule.Times[i] + '" onclick="selectSchedule(' + schedule.Times[i] + ');" style="float:left;margin-bottom: 5px;background: url(img/schedule.png) no-repeat;width:142px;height:53px;display: inline-block;color:#ffffff;font-size: 50px;font-style:FONT-FAMILY: AA Bebas Neue;margin-right:20px;">' + schedule.Times[i].substr(0, 2) + ':' + schedule.Times[i].substr(2, 2) + '</div>';
                }
                document.getElementById('schedule').style.paddingLeft = "85px";
                document.getElementById('schedule').innerHTML = scheduleDiv;
            } else {
                //alert(window.external.GetKeyForInterface('MastersForGates'));
                //alert(masterid);
                for (var i = 0; i < masters.length; i++) {
                    if (masters[i].Id == masterid) {
                        //alert(masters[i].working_days[0]);
                        var masterworkday = masters[i].working_days[0].substr(8, 2) + "-" + masters[i].working_days[0].substr(5, 2) + "-" + masters[i].working_days[0].substr(0, 4);
                        break;
                    }
                }
                document.getElementById('schedule').style.paddingLeft = "0px";
                document.getElementById('schedule').innerHTML = '<br><br>Ближайший день записи к этому мастеру <u>' + masterworkday + '</u><br>Выберите другой день или мастера';
            }

        } else {
            ext.action();
        }
    };
    onlineTimer.timer.execute = function () {
        var status = getResponseOnlinePayment();
        if (status == 1 || status == 0) {
            onlineTimer.stop();
        }
    };
    onlineTimer.start();
}

//////////////////////////online end


function buildCalendar(mon) {
    'use strict';
    var calendarBody = document.querySelector('#calendar-body');
    var dayInMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
    var days = ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'];


    function genCalendar(date) {
        var curDay_today = new Date();
        curDay_today = curDay_today.getDate();
        var curDay = date.getDate();

        var curMonth = date.getMonth() + 1;
        var curMonth_today = new Date();
        curMonth_today = curMonth_today.getMonth() + 1;


//alert(curMonth);
//alert(curMonth_today);

        if (curMonth > curMonth_today) {
            curDay = 1;
        }
        date.setDate(1);
        var startDay = date.getDay();

        var daysTotal = !(date.getFullYear() % 4) && date.getMonth() === 1 ? 29 : dayInMonth[date.getMonth()];

        var content = '';
        content = '<div style="text-align:center;font-style:FONT-FAMILY: AA Bebas Neue;margin-right:20px;font-size: 40px;color:#000000;">' + (getMonthText(date.getMonth())) + '<br>';
        content += '<span onclick="buildCalendar(0);" style="display: inline-block;"> << </span>&nbsp;';
        var month = date.getMonth() + 1;

        if (month < 10) {
            month = "0" + month;
        }
        for (var i = curDay; i <= daysTotal; i++) {
            if (i === curDay && curDay_today == curDay) {
                content += '<div id="day' + month + i + '" style="padding-bottom:2px;border-radius: 25px;margin-right:3px;width:40px; height:40px;border:8px solid #e3bdb8;color:#ffffff;background:#e3bdb8;display: inline-block;font-style:FONT-FAMILY: AA Bebas Neue;font-size: 40px;">' + i + '</div>';
            } else {
                content += '<div id="day' + month + i + '" style="display: inline-block;font-style:FONT-FAMILY: AA Bebas Neue;margin-right:13px;width:25px; height:40px;font-size: 40px;color:#000000;"><span onclick="changeDate(' + month + ',' + i + ');">' + i + '</span></div>';
            }
        }
        content += '&nbsp;<span onclick="buildCalendar(1);" id="plusmonth" style="display: inline-block;"> >> </span>';
        content += '</div>';
        calendarBody.innerHTML = content;
    }

    var getMonthText = function (currentMonth) {
        if (currentMonth === 0) {
            return "Январь";
        }
        else if (currentMonth === 1) {
            return "Февраль";
        }
        else if (currentMonth === 2) {
            return "Март";
        }
        else if (currentMonth === 3) {
            return "Апрель";
        }
        else if (currentMonth === 4) {
            return "Май";
        }
        else if (currentMonth === 5) {
            return "Июнь";
        }
        else if (currentMonth === 6) {
            return "Июль";
        }
        else if (currentMonth === 7) {
            return "Август";
        }
        else if (currentMonth === 8) {
            return "Сентябрь";
        }
        else if (currentMonth === 9) {
            return "Октябрь";
        }
        else if (currentMonth === 10) {
            return "Ноябрь";
        }
        else if (currentMonth === 11) {
            return "Декабрь";
        }
    };
    if (mon == 0) {
        var calendarDate = new Date();
        var day = calendarDate.getDate();
        var month = calendarDate.getMonth() + 1;
        if (month < 10) {
            month = "0" + month;
        }
        var datesel = "2018" + "-" + month + "-" + day;
        window.external.SetKeyFromInterface('datesel', datesel);
    }
    else if (mon == 1) {
        var calendarDate = new Date();
        var m2 = calendarDate.getTime() + (24 * 60 * 60 * 1000 * (dayInMonth[calendarDate.getMonth() + 1]));  //+ dayInMonth[calendarDate.getMonth()+2] - calendarDate.getDate()
        calendarDate.setTime(m2);
    }
    else {
        var calendarDate = new Date();
    }
    var m1 = calendarDate.getTime() + (24 * 60 * 60 * 1000 * (dayInMonth[calendarDate.getMonth()] - calendarDate.getDate() + 1));
    genCalendar(calendarDate);

}


function selectMaster(name, masterid) {
    var servicecartData = '{"col":0,"summ":0,"cartvalue":{}}';
    servicecart = JSON.parse(servicecartData);
    var serviceGroupData = '{"group":{}}';
    serviceGroup = JSON.parse(serviceGroupData);

    window.external.SetKeyFromInterface('selectMasterFirst', '1');
    window.external.SetKeyFromInterface('browmasterid', masterid);
    window.external.SetKeyFromInterface('browmastername', name);
    RetGroup(998);
}