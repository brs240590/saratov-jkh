'use strict';

// SC10204, SC10219, SC204
// CAST(SP9865 AS NVARCHAR(4000)) = ''

const iconv = require("iconv-lite");
const elkassir = require('./lib/elkassir');
const fs = require("fs");
const YADBF = require('yadbf');

const ek = new elkassir({
    server: {
        port: 32500
    },
    ws: {
        port: 65000
    },
    queryClientTerminal: {
        dbPath: ".\\..\\..\\..\\..\\..\\QueryClientTerminal.settings"
    },
    cache: {
        age: 3600
    },
    sql: {
        config: {
            user: 'kassir.lk',
            password: 'zEcmMnIFEM0oLEMJbSdo',
            server: '192.168.69.243',
            localAddress: '192.168.69.243',
            port: 1433,
            database: 'SSK_2013',
            options: {
                tdsVersion: '7_1'
            }
        }
    },
    dbf: {
        paymentResult: "C:\\interfaceData\\saratov_jkh\\paymentResult", // платежи
        waterMeter: "C:\\interfaceData\\saratov_jkh\\waterMeter", // счётчики на воду
        persAcc: "C:\\interfaceData\\saratov_jkh\\persAcc", // персональные данные пользователей
        terrorism: "C:\\interfaceData\\saratov_jkh\\terrorism" // список террористов
    }
});

ek.barcodeHandler(false, "FM430L-U-9E02337");

ek.getCached('/getContractorList', ek.cache(ek.cacheAge), (req, res) => {
    console.log(`[${ek.getCurrentTime()}]: [/getContractorList] Список контрагентов`);
    ek.sql(res, "SELECT * FROM SC204 WHERE SP8868 = 0 AND SP9070 = 0;", '[/getContractorList]');
});

ek.getCached('/getSpreadContracts', ek.cache(ek.cacheAge), (req, res) => {
    console.log(`[${ek.getCurrentTime()}]: [/getSpreadContracts] Справочник распределения договоров`);
    ek.sql(res, "SELECT * FROM SC10204;", '[/getSpreadContracts]');
});

ek.getCached('/getRewardsList', ek.cache(ek.cacheAge), (req, res) => {
    console.log(`[${ek.getCurrentTime()}]: [/getRewardsList] Справочник вознаграждения`);
    ek.sql(res, "SELECT * FROM SC10219;", '[/getRewardsList]');
});

ek.getCached('/getSGUATable', ek.cache(ek.cacheAge), (req, res) => {
    console.log(`[${ek.getCurrentTime()}]: [/getSGUATable] Выборка из таблицы студентов`);
    ek.sql(res, "SELECT * FROM SC172;", '[/getSGUATable]');
});

ek.getCached('/getSGUA', ek.cache(ek.cacheAge), (req, res) => {
    console.log(`[${ek.getCurrentTime()}]: [/getSGUA] Выборка СГЮА`);
    ek.sql(res, "SELECT * FROM SC204 WHERE SP8868 = 0 AND SP9070 = 0 AND SP10107 = 1;", '[/getSGUA]');
});

ek.get('/getPersonalAccount', (req, res) => {
    let personalAcc;
    console.log(`[${ek.getCurrentTime()}]: [/getPersonalAccount] Данные контрагента`);
    // let personalAcc = iconv.decode(Buffer.from(req.query.personalAcc, 'binary'), 'win1251');
    personalAcc = req.query.personalAcc;
    ek.sql(res, `SELECT * FROM SC204 WHERE SP8868 = 0 AND SP9070 = 0 AND SP10164 = '${personalAcc}';`, '[/getPersonalAccount]');
});

ek.get('/drawQRCodePage', (req, res) => {
    console.log(`[${ek.getCurrentTime()}]: [/drawQRCodePage] Страница сканирования QR-кода на документе`);
    res.render('drawQRCodePage');
});

ek.get('/drawPickAuthBtn', (req, res) => {
    console.log(`[${ek.getCurrentTime()}]: [/drawPickAuthBtn] Страница выбора ввода лицевого счёта`);
    res.render('drawPickAuthBtn');
});

ek.get('/drawBarcodeAuthPage', (req, res) => {
    console.log(`[${ek.getCurrentTime()}]: [/drawBarcodeAuthPage] Страница штрих-кода`);
    res.render('drawBarcodeAuthPage');
});

ek.get('/checkPersonalAcc', (req, res) => {
    console.log(`[${ek.getCurrentTime()}]: [/checkPersonalAcc] Проверка лицевого счёта в DBF-файле`);
    let personalAccNumber, dbfFileName, data, from;

    personalAccNumber = iconv.encode(req.query.accNum, "win1251");
    dbfFileName = req.query.dbfFileName;
    from = req.query.from;

    res.setHeader('Content-Type', 'text/html; charset=UTF-8');
    res.setHeader('Cache-Control', 'no-cache');

    if (fs.existsSync(dbfFileName)) {
        fs.createReadStream(dbfFileName).pipe(new YADBF({
            encoding: "ibm-866"
        })).on('data', record => {
            if (record.ID.toString() === personalAccNumber.toString().trim()) {
                delete record["@meta"];
                delete record["ID"];

                data = record;
            }
        }).on('end', () => {
            ek.logs("[/checkPersonalAcc] [Debug]:", JSON.stringify({
                "ID": req.query.accNum,
                "DBfilePath": req.query.dbfFileName
            }), JSON.stringify(data));
            res.render('drawPersonalPage', {
                rows: data,
                from: from,
                accNum: personalAccNumber.toString().trim()
            });
        }).on('error', err => {
            ek.logs("[/checkPersonalAcc] [Error]:", JSON.stringify({
                "ID": req.query.accNum,
                "DBfilePath": req.query.dbfFileName
            }), JSON.stringify(err));
            res.render('drawError', {
                error: "Ошибка запроса"
            });
        });
    } else {
        ek.logs("[/checkPersonalAcc] [Error]:", JSON.stringify({
            "ID": req.query.accNum,
            "DBfilePath": req.query.dbfFileName
        }), "DBF-файл не обнаружен");
        res.render('drawError', {
            error: "DBF-файл не обнаружен"
        });
    }
});

ek.get('/checkIndicators', (req, res) => {
    console.log(`[${ek.getCurrentTime()}]: [/checkIndicators] Проверка лицевого счёта в DBF-файле`);
    let personalAccNumber, dbfFileName, data, from;

    personalAccNumber = iconv.encode(req.query.accNum, "win1251");
    dbfFileName = req.query.dbfFileName;
    from = req.query.from;

    res.setHeader('Content-Type', 'text/html; charset=UTF-8');
    res.setHeader('Cache-Control', 'no-cache');

    if (fs.existsSync(dbfFileName)) {
        fs.createReadStream(dbfFileName).pipe(new YADBF({
            encoding: "ibm-866"
        })).on('data', record => {
            if (record.ID.toString() === personalAccNumber.toString().trim()) {
                delete record["@sequenceNumber"];
                delete record["@deleted"];

                data = record;
            }
        }).on('end', () => {
            ek.logs("[/checkIndicators] [Debug]:", JSON.stringify({
                "ID": req.query.accNum,
                "DBfilePath": req.query.dbfFileName
            }), JSON.stringify(data));
            res.render('drawPersonalPage', {
                rows: data,
                from: from,
                accNum: personalAccNumber.toString().trim()
            });
        }).on('error', err => {
            ek.logs("[/checkIndicators] [Error]:", JSON.stringify({
                "ID": req.query.accNum,
                "DBfilePath": req.query.dbfFileName
            }), JSON.stringify(err));
            res.render('drawError', {
                error: "Ошибка запроса"
            });
        });
    } else {
        ek.logs("[/checkIndicators] [Error]:", JSON.stringify({
            "ID": req.query.accNum,
            "DBfilePath": req.query.dbfFileName
        }), "DBF-файл не обнаружен");
        res.render('drawError', {
            error: "DBF-файл не обнаружен"
        });
    }
});

ek.get('/drawInputPersonalData', (req, res) => {
    console.log(`[${ek.getCurrentTime()}]: [/drawInputPersonalData] Вывод доступных признаков`);

    res.setHeader('Content-Type', 'text/html; charset=UTF-8');
    res.setHeader('Cache-Control', 'no-cache');

    let items = {
        "SP8860": "Телефон",
        "SP8861": "Счёт",
        "SP8862": "Пени",
        "SP8863": "Дата выставления счёта",
        "SP8864": "Номера выставления",
        "SP8865": "Контрольного ЧИ",
        "SP8866": "Код плательщика",
        "SP8867": "Штрих",
        "SP8869": "Ф.И.О",
        "SP8870": "Адрес",
        "SP8871": "Период",
        "SP8872": "Расчётный счёт",
        "SP8873": "ИНН",
        "SP8874": "Наименование банка",
        "SP8875": "БИК",
        "SP8876": "КС",
        "SP8877": "Код сборщика",
        "SP8878": "Код платежа",
        "SP8880": "Корпус",
        "SP9255": "Счетчик",
        "SP10032": "Кредит",
        "SP10038": "Прочие",
        "SGUA1000": "Ф.И.О вностителя средств",
        "SGUA1001": "Факультет",
        "SGUA1002": "Отделение",
        "SGUA1003": "Специальность",
        "SGUA1004": "Курс",
        "SGUA1005": "Группа"
    };

    res.setHeader("x-json-base64", Buffer.from(JSON.stringify(items)).toString("base64"));

    res.render("drawInputPersonalData", {
        items: items
    });
});

ek.get('/drawWaterMeter', (req, res) => {
    console.log(`[${ek.getCurrentTime()}]: [/drawWaterMeter] Страница вывода счётчиков`);
    let dbfFileName, data;

    dbfFileName = req.query.dbfFileName;

    data = [];

    res.setHeader('Content-Type', 'text/html; charset=UTF-8');
    res.setHeader('Cache-Control', 'no-cache');

    if (fs.existsSync(dbfFileName)) {
        fs.createReadStream(dbfFileName).pipe(new YADBF({
            encoding: "ibm-866"
        })).on('data', record => {
            delete record["@sequenceNumber"];
            delete record["@deleted"];
            delete record["@meta"];
            data.push(record);
        }).on('end', () => {
            ek.logs("[/drawWaterMeter] [Debug]:", JSON.stringify({
                "DBfilePath": req.query.dbfFileName
            }), JSON.stringify(data));
            res.render('drawWaterMeter', {
                rows: data,
            });
        }).on('error', err => {
            ek.logs("[/drawWaterMeter] [Error]:", JSON.stringify({
                "DBfilePath": req.query.dbfFileName
            }), JSON.stringify(err));
            res.render('drawError', {
                error: "Ошибка запроса"
            });
        });
    } else {
        ek.logs("[/drawWaterMeter] [Error]:", JSON.stringify({
            "DBfilePath": req.query.dbfFileName
        }), "DBF-файл не обнаружен");
        res.render('drawError', {
            error: "DBF-файл не обнаружен"
        });
    }
});

ek.getCached('/getTerrorTable', ek.cache(ek.cacheAge), (req, res) => {
    console.log(`[${ek.getCurrentTime()}]: [/getTerrorTable] Проверка в БД на терроризм`);
    let dbfFileName, data;

    dbfFileName = ek.settings.dbf.terrorism;

    data = [];

    res.setHeader('Cache-Control', 'no-cache');
    res.setHeader('Content-Type', 'application/json');

    if (fs.existsSync(dbfFileName)) {
        fs.createReadStream(dbfFileName).pipe(new YADBF({
            encoding: "ibm-866"
        })).on('data', record => {
            delete record["@sequenceNumber"];
            delete record["@deleted"];
            delete record["@meta"];
            data.push(record);
        }).on('end', () => {
            ek.logs("[/getTerrorTable] [Debug]:", JSON.stringify({
                "DBfilePath": req.query.dbfFileName
            }), JSON.stringify(data));
            res.send(data);
        }).on('error', err => {
            ek.logs("[/getTerrorTable] [Error]:", JSON.stringify({
                "DBfilePath": req.query.dbfFileName
            }), JSON.stringify(err));
            res.render('drawError', {
                error: "Ошибка запроса"
            });
        });
    } else {
        ek.logs("[/getTerrorTable] [Error]:", JSON.stringify({
            "DBfilePath": req.query.dbfFileName
        }), "DBF-файл не обнаружен");
        res.render('drawError', {
            error: "DBF-файл не обнаружен"
        });
    }
});

ek.get('/drawSetPayment', (req, res) => {
    let summa;
    console.log(`[${ek.getCurrentTime()}]: [/drawSetPayment] Страница ввода платежа`);

    summa = req.query.clientSumma;

    res.setHeader('Content-Type', 'text/html; charset=UTF-8');
    res.setHeader('Cache-Control', 'no-cache');
    res.render('drawSetPayment', {
        value: parseFloat(summa),
        currency: "руб."
    });
});

ek.post('/createFile', (req, res) => {
    let json, text, filepath, filename;
    console.log(`[${ek.getCurrentTime()}]: [/createFile] Создание файла плательщика`);
    res.setHeader('Content-Type', 'application/json');

    filepath = ek.settings.dbf.paymentResult;
    json = req.body;
    text = "";

    Object.entries(json).forEach(([key, value]) => {
        if (key === "sessionNumber") {
            filename = value.value;
        }

        text += `${value.text}=${value.value}|`;
    });

    text = text.slice(0, -1);

    const data = new Uint8Array(Buffer.from(text));
    fs.writeFile(`${filepath}${filename}.txt`, data, (err) => {
        if (err) {
            throw err;
        };

        ek.logs("[/createFile]:", `Входные данные: ${text}`, `Файл сохранён в: ${filepath}${filename}.txt`);
        res.send(JSON.stringify({
            result: `The file has been saved at path ${filepath}${filename}.txt`
        }));
    });
});

ek.server();

/*
номер ;(id операции,10),если возможно должно начинаться на "r19_"
Дата;(Дата операции,"ДД.ММ.ГГГГ")
Тип платежа(5);
контрагент(id контрагента);
сумма(Сумма,полученная с клиента);
Сумма платежа;		
сумма вознаграждения;		
Сумма скидки; 
пени (Сумма пени не входит в сумма );
счет/номер телефона;
улица;
дом;
квартира;
за_период_мес;
за_период_год;				
Фамилия имя Отчество;		
банк наименование;		
бик;		
р_сч;		
комментарий;		
счетчик имя;		
показательсчетчик;		
Дата показаний;("ДД.ММ.ГГГГ")		
Безналичная/наличная оплата;(0-наличная,1-безнал) 
номер Транзакции безнал; 
Номер карты; 
RRN; 
AuthCode; 
ИНН; 
Получатель; 
Сессия(для кибера); 
Факультет; 
Специальность; 
Отделение; 
Курс; 
Группа; 
Вноситель; 
Фраза; 
*/