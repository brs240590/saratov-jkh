'use strict';

const express = require('express');
const fs = require('fs');
const bodyParser = require("body-parser");
const path = require("path");
const sql = require("mssql");
const iconv = require("iconv-lite");
const uuidv1 = require("uuid/v1");
const pug = require("pug");
const sqlite3 = require("sqlite3").verbose();
const pretty = require('prettysize');
const HID = require('node-hid');
const mcache = require('memory-cache');
const webSocket = require("ws");

class Elkassir {
    constructor(settings) {
        this.app = express();

        this.settings = settings;

        this.app.use(bodyParser.json());
        this.app.set('views', path.join(__dirname, '/../views'));
        this.app.set('view engine', 'pug');

        this.wss = this.webSocketServer(settings.ws.port);
        this.mssql = sql;
        this.dbPath = settings.queryClientTerminal.dbPath;
        this.cacheAge = settings.cache.age;
        this.sqlConfig = settings.sql.config;
        this.port = settings.server.port;

        this.barcode = "";
    }

    webSocketServer(port) {
        return new webSocket.Server({
            port: port
        });
    }

    async getTerminalSettings() {
        let db = new sqlite3.Database(this.dbPath, sqlite3.OPEN_READONLY);
        let query = "SELECT * FROM paymentsystem WHERE key IN ('IntegrationValA', 'IntegrationValB', 'IntegrationValC', 'IntegrationValD', 'IntegrationValE', 'IntegrationValF');";

        return await new Promise((resolse, reject) => {
            db.all(query, (err, rows) => {
                let arr, obj;

                arr = rows.map(e => {
                    let buf = Buffer.from(e.value, 'base64').toString();
                    return [e.key, Buffer.from(buf, 'base64').toString('utf16le')];
                });

                obj = arr.reduce((prev, curr) => {
                    prev[curr[0]] = curr[1];
                    return prev;
                }, {});

                resolse(obj);
            });
        });
    }

    barcodeHandler(isShowDevices, serialNumber) {
        let devices, device, devicePath, self;

        self = this;
        devices = HID.devices();

        if (isShowDevices) {
            console.log(devices);
        }

        devices.map(function (el) {
            if (el.serialNumber === serialNumber) {
                devicePath = el.path;
            }
        });

        if (devicePath) {
            device = new HID.HID(devicePath);
            let arr = [];
            device.on('data', (blob) => {
                let buff, str;

                arr.push(Buffer.from(blob).slice(2).slice(0, -6));
                buff = Buffer.concat(arr);
                str = buff.toString().trim().replace(/\0/g, '');

                for (var i = 0; i < str.length; i++) {
                    if (str.charAt(i).charCodeAt() === 13) {
                        self.setBarcode = str;
                        console.log(str);
                        arr = [];
                        self.wss.clients.forEach(function each(ws) {
                            ws.send(str);
                            self.logs("[barcodeHandler]:", "HID-устройство", str);
                        });
                    }
                }
            });
        }
    }

    set setBarcode(str) {
        this.barcode = str;
    }

    get getBarcode() {
        return this.barcode;
    }

    sql(res, query, fnName) {
        let self = this, request, size, len;

        return this.mssql.connect(this.sqlConfig, (err) => {
            if (err) {
                res.send({
                    'error': err.originalError
                });

                self.logs(`${fnName} [Error]: `, JSON.stringify(self.sqlConfig), err);
                return false;
            }

            request = new self.mssql.Request();

            request.query(query, (err, recordset) => {
                try {
                    if (err) {
                        res.send({
                            'error': err
                        });

                        self.logs(`${fnName} [Error]: `, query, err);
                        return false;
                    }

                    // res.set('Cache-Control', `public, max-age=${self.cacheMaxAge}`);
                    res.set('Content-Type', 'application/json');
                    res.send(recordset.recordset);

                    size = pretty(Buffer.byteLength(JSON.stringify(recordset.recordset), 'utf8'), {
                        one: true,
                        places: 3
                    });
                    len = pretty(Buffer.byteLength(JSON.stringify(recordset.recordset), 'utf8'), {
                        numOnly: true
                    });

                    if (len > 1) {
                        self.logs(`${fnName} [Debug]: `, query, `В объекте ${recordset.recordset.length} элементов, размер объекта составляет ${size}`);
                    } else {
                        self.logs(`${fnName} [Debug]: `, query, JSON.stringify(recordset.recordset, null, 4));
                    }
                } catch (errno) {
                    self.logs(`${fnName} [Exception]: `, query, errno);
                    res.send(JSON.parse({
                        'error': errno
                    }));
                }
            });
        })
    }

    server() {
        let self = this;
        this.app.listen(self.port, () => {
            console.log(`Listen on port ${self.port}...`);
        });
    }

    post(page, cb) {
        this.app.post(page, cb);
    }

    get(page, cb) {
        this.app.get(page, cb);
    }

    getCached(page, cache, cb) {
        this.app.get(page, cache, cb);
    }

    cache(duration) {
        return (req, res, next) => {
            let key = '__express__' + req.originalUrl || req.url
            let cachedBody = mcache.get(key)
            if (cachedBody) {
                res.send(cachedBody)
                return
            } else {
                res.sendResponse = res.send
                res.send = (body) => {
                    mcache.put(key, body, duration * 1000);
                    res.sendResponse(body)
                }
                next()
            }
        }
    }

    logs(fnName, req, res) {
        let d, date, time, dir, logFile;

        d = new Date();
        date = d.getFullYear() + ("0" + (d.getMonth() + 1)).slice(-2) + ("0" + d.getDate()).slice(-2);
        time = ("0" + d.getHours()).slice(-2) + ":" + ("0" + d.getMinutes()).slice(-2) + ":" + ("0" + d.getSeconds()).slice(-2);

        dir = "logs";

        if (!fs.existsSync(dir)) {
            fs.mkdirSync(dir, '0755');
        }

        logFile = `${dir}/${date}.log`;

        if (!fs.existsSync(logFile)) {
            fs.writeFileSync(logFile, "");
        }

        fs.appendFileSync(logFile, `[${time}] - ${fnName} - Запрос: ${req} | Ответ: ${res}\r\n`);
    }

    getCurrentTime() {
        let d = new Date();
        return ("0" + d.getHours()).slice(-2) + ":" + ("0" + d.getMinutes()).slice(-2) + ":" + ("0" + d.getSeconds()).slice(-2);
    }
}

module.exports = Elkassir;