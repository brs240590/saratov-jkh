function GroupStructure(name, group_id, button_img, group_id_toplevel) {
    this.Name = name;
    this.GroupID = group_id;
    this.ButtonImg = button_img;
    this.GroupIDTopLevel = group_id_toplevel;
}

function ListGroupStructure() {
    this.list = [];
}
ListGroupStructure.prototype.add = function(groupStructure) {
    this.list.push(groupStructure);
};
/**
 * Создаем список груп
 * @param group_id
 */
ListGroupStructure.prototype.createListGroup = function() {

    this.add(new GroupStructure("Заявка на подключение", -1, "default.png", 0));
    //формируем список груп и субгруп
    var groups = JSON.parse(RetAllGroupsJSON());
    for(var i = 0; i < groups.length; i++) {
        this.add(new GroupStructure(groups[i].Name, groups[i].GroupID, groups[i].ButtonImg, groups[i].GroupIDTopLevel));
    }
};
ListGroupStructure.prototype.get = function(groupID) {
    var elem = null;
    var statusSearch = false;
    for(var i = 0; i < this.list.length; i++) {
        if(this.list[i].GroupID == groupID) {
            elem = this.list[i];
            statusSearch = true;
            break;
        }
    }
    return elem;
};

var listGroup = new ListGroupStructure();
listGroup.createListGroup();

//Возвращаем значение группы
function GetGroupStructure(group_id) {
    return listGroup.get(group_id);
}