/*
 Эффект нажатия на кнопку, меняет текущий бэкгроунд на другой и назад
 Второе фото должно иметь префикс _p. Например, fo_btn.png -> fo_btn_p.png -> fo_btn.png
 */
function EffectClick(elem_id, prefix) {

}

function EffectPress(elem_id, prefix) {
    var _prefix = prefix || "_p";
    //sounder.src = "../sound/click.wav";
    var elem = document.getElementById(elem_id);
    var curBackGroundImage = elem.style.backgroundImage;
    var ind = curBackGroundImage.lastIndexOf('/');

    var name = curBackGroundImage.substr(ind + 1);
    name = name.substring(0, name.length-1);

    var image = name.split('.');
    document.getElementById(elem_id).style.backgroundImage =  curBackGroundImage.substr(0, ind+1) + image[0] + _prefix + "." + image[1] + ')';
}

function EffectUnPress(elem_id) {
    //sounder.src = "../sound/click.wav";
    var elem = document.getElementById(elem_id);
    var curBackGroundImage = elem.style.backgroundImage;
    var ind = curBackGroundImage.lastIndexOf('/');

    var name = curBackGroundImage.substr(ind + 1);
    name = name.substring(0, name.length-1);

    var image = name.split('.');
    document.getElementById(elem_id).style.backgroundImage =  curBackGroundImage.substr(0, ind+1) + image[0].substring(0, image[0].length-2) + "." + image[1] + ')';
}

var WSoundEffects = {
    click: function() {
        sounder.src = "../sound/click.wav";
    },
    sounds: function(path) {
        sounder.src = path;
    }
};

var WNewEffects = {
    click: {
        button: function(img_id) {
            changeImageresize(img_id);
        }
    }
};

