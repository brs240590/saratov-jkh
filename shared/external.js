

/**
 * Остановить купюроприемник
 */
function STOP_BILL() {
//
    window.external.STOP_BILL();
//alert('stop from external');
}

/**
 * Запустить купюроприемник
 */
function START_BILL() {
//
    window.external.START_BILL();
//alert('start from external');
}

/**
* А есть ли у нас пинпад
*/
function IF_PINPAD() {
	var status = window.external.isPinPad();
	return status;
}

/**
* Включаем пинпад
*/
function START_PINPAD() {
	window.external.StartPinPad();
}

/**
* Запрашиваем таймаут пинпада
*/
function PINPAD_TIMEOUT() {
        return window.external.GetPinPadTimeOut();
}

//проверка включен фискальный или нет
function IsFiscalOptionOn() {
    return window.external.IsFiscalOptionOn();
}

//Ответ от фискального сервера
function getResponseFiscal() {
    return window.external.GetResponseFiscal();
}

//команду проверить фискальный сервер
function getStatusFiscalServer() {
    window.external.GetStatusFiscalServer();
}

/**
 * стартуем проверка онлайн сервиса
 * @param isReqForInfo 0 - проверка возможности совершения платежа, 1 - запрос информации
 * @returns {*}
 */
function getOnlinePaymentStatus(isReqForInfo) {
    window.external.GetOnlinePaymentStatus(isReqForInfo);
    return isReqForInfo;
}

function getResponseOnlinePayment() {
    return window.external.GetResponseOnlinePayment();
}

//Функция проверяет системный это номер или нет
function IsSystemNumber(ValFromInterface, GateID) {
    window.external.IsSystemNumber(ValFromInterface, GateID);
}

/**
 * Проверяет значение на регулярное выражение
 * @param StepID int orderId шага
 * @param ValFromInterface значение шага
 * @returns boolean true проверка прошла успешно
 */
function CheckRegexValue(StepID, ValFromInterface) {
    var reg = window.external.CheckRegexValue(StepID, ValFromInterface);
    //addData('step: ' + StepID, " val: " + ValFromInterface, "regex: " + reg);
    return reg;
}

function IsNumberInBlackList(ValFromInterface) {
    var accept = window.external.IsNumberInBlackList(ValFromInterface);
    return accept;
}

//сумма к зачислению
function GetEnrollmentPay() {
    var enroll = window.external.GetEnrollmentPay();
    return enroll;
}

function GetOnlineModeText2Screen() {
    return window.external.GetOnlineModeText2Screen();
}

/**
 * Возвращает если есть логотим или пустосту
 * Используется в степах после startPayCycle
 */
function RetLogotipImg() {
    var ret = window.external.RetLogotipImg();

    if (ret != 'nologo') {
        ret = '../logo/' + ret;
    } else {
        ret = "";
    }
    //можно задействовать и RetButtonImg()
    return ret;
}

function RetLogotipImgGateId(gate_id) {
    var img = window.external.RetLogotipImgGateId(gate_id);
    var path = "";
    if (img.length > 0) {
        path = '../logo/' + img;
    }
    return path;
}

function SaveLog(text) {
    window.external.SaveErrorInLog(text);
}

function Link(url) {
    window.external.LINK(url);
}

function StartPayCycleOld(gateID) {
    window.external.StartPayCycle(0, gateID);
}

/**
 * Запуск цикла оплаті для нового интерфейса
 * @param gateID
 * @constructor
 */
function StartPayCycleJavaInt(gateID) {
    SaveLog("StartPayCycleJavaInt(" + gateID + ")");
    return window.external.StartPayCycleJavaInt(0, gateID);
}

/**
 * Меняет используемый скин
 * @constructor
 */
function ChangeInterface(skin) {
    window.external.ChangeInterface(skin);
}

function MasterStep() {
    window.external.MasterStep();
}

/**
 * Количество милисекунд через сколько показывать я еще здесь
 * @constructor
 */
function RetTimeOut() {
    var interval = window.external.RetTimeOut();
    interval = Number.toInt(interval) + 10000;
    return interval;
}

/**
 * Дополнительные параметры по оплате
 * @constructor
 */
function RetAdditionalPaymentParametersForCheck() {
    return window.external.RetAdditionalPaymentParametersForCheck();
}

/**
 * Устанавливаем фиксированную сумму для степа
 * @param fixSumValue
 * @constructor
 */
function SetFixedSumTxt(fixSumValue) {
    window.external.SetFixedSumTxt(fixSumValue);
}

/**
 * Передает на серверную сторону значение степа
 * @param OrderID
 * @param value
 * @constructor
 */
function SetStepVal(OrderID, value) {
    window.external.SetStepVal(OrderID, value);
}

/***
 * Возвращает минимальный платеж по данному оператору
 * @returns {*}
 * @constructor
 */
function RetMinPaymentSize() {
    return window.external.RetMinPaymentSize();
}
/**
 * Функция используетсся для получения значений для
 * степа типа LISTONLINE
 * @returns {Array}
 */
function GetParamsForListOnline() {
    var param = JSON.parse(window.external.GetParamsForListOnline());
    //var param = [];
    //param.push({Name: "999fhjd379fjk578473kfjf7777777", Value: 1, FixedSum: 15.5, Desc: "Существуют две основных трактовки понятия «текст»: «имманентная» (расширенная, философски нагруженная) и «репрезентативная» (более частная). Имманентный подход подразумевает отношение к тексту как к автономной реальности, нацеленность на выявление его внутренней структуры. Репрезентативный — рассмотрение текста как особой формы представления знаний о внешней тексту действительности."});
    //param.push({Name: "first2", Value: 2, FixedSum: 15.5, Desc: "Существуют две основных трактовки понятия «текст»: «имманентная» (расширенная, философски нагруженная) и «репрезентативная» (более частная). Имманентный подход подразумевает отношение к тексту как к автономной реальности, нацеленность на выявление его внутренней структуры. Репрезентативный — рассмотрение текста как особой формы представления знаний о внешней тексту действительности."});
    //param.push({Name: "first3", Value: 3, FixedSum: 15.5, Desc: "Существуют две основных трактовки понятия «текст»: «имманентная» (расширенная, философски нагруженная) и «репрезентативная» (более частная). Имманентный подход подразумевает отношение к тексту как к автономной реальности, нацеленность на выявление его внутренней структуры. Репрезентативный — рассмотрение текста как особой формы представления знаний о внешней тексту действительности."});
    //param.push({Name: "secondList", Value: 4, FixedSum: 15000.0001, Desc: "Единство предмета речи — это тема высказывания. Тема — это смысловое ядро текста, конденсированное и обобщённое содержание текста.Понятие «содержание высказывания» связано с категорией информативности речи и присуще только тексту. Оно сообщает читателю индивидуально-авторское понимание отношений между явлениями, их значимости во всех сферах придают ему смысловую цельность."});
    //param.push({Name: "secondList", Value: 5, FixedSum: 10, Desc: "Единство предмета речи — это тема высказывания. Тема — это смысловое ядро текста, конденсированное и обобщённое содержание текста.Понятие «содержание высказывания» связано с категорией информативности речи и присуще только тексту. Оно сообщает читателю индивидуально-авторское понимание отношений между явлениями, их значимости во всех сферах придают ему смысловую цельность."});
    return param;
}

/**
 * Получает список опреаторов по группам
 * @param group_id
 * @constructor
 */
function RetGatesForGroup(group_id) {
    return window.external.RetGatesForGroup(group_id);
}

/**
 * Возвращает bool есть в группе гейты или нет
 * @param group_id
 * @constructor
 */
function RetGatesForGroupBool(group_id) {
    return window.external.RetGatesForGroupBool(group_id);
}

/**
 * Получаем список подргрупп для операторов
 */
function RetSubGroups(group_id) {
    return window.external.RetSubGroups(group_id);//подгруппы
}

/**
 * Используется для передачи на сервеную сторону где находится сейчас интерфейс
 * @param status boolean true - на мейне, false - где-то еще
 * @constructor
 */
function CurrentPageMain(status) {
    window.external.CurrentPageMain(status);
}

/**
 * Возвращает строку в json провайдера по гатеайди
 * @param gate_id
 * @returns {*}
 * @constructor
 */
function RetGateJSON(gate_id) {
    return window.external.RetGateJSON(gate_id);
}

/**
 * Возвращает обьект провайдера по гейтайди
 * @param gate_id
 * @returns {*}
 * @constructor
 */
function RetGate(gate_id) {
    return JSON.parse(RetGateJSON(gate_id));
}

/**
 * Получаем путь к логотипу по айди быстрой панели
 * @param panel_id
 * @returns {*}
 * @constructor
 */
function RetFastPanelLogotipImg(panel_id) {
    return window.external.RetFastPanelLogotipImg(panel_id);
}

/**
 * Возвращает сумму вставленных купюр
 * @returns {*}
 * @constructor
 */
function RetACCT() {
    return window.external.RetACCT();
}

/**
 * Запуск проведения платежа
 * @constructor
 */
function PayConfirmNew() {
    //
}

/**
 * Возвращает включена ли подмена
 * @constructor
 */
function AutoDetectGateGyID() {
    var st = window.external.AutoDetectGateID();
    return st;
}

/**
 * Номер системный терминала общий в системе
 */
function RetTerminalNumberSystemMini() {
    return window.external.RetTerminalNumberSystemMini();
}

/**
 * Номер терминала у диллера
 * @returns {*}
 * @constructor
 */
function RetTerminalNumber() {
    return window.external.RetTerminalNumber();
}

/**
 * Номер договора с кредитной организацией
 * @returns {*}
 * @constructor
 */
function RetNumberOfContract() {
    return window.external.RetNumberOfContract();
}

/**
 * ИНН кредитной организации
 * @returns {*}
 * @constructor
 */
function RetPaymentSystemData2() {
    return window.external.RetPaymentSystemData2();
}

/**
 * БИК кредитной организации
 * @returns {*}
 * @constructor
 */
function RetPaymentSystemData1() {
    return window.external.RetPaymentSystemData1();
}

/**
 * Адрес кредитной организации
 * @returns {*}
 * @constructor
 */
function RetPaymentSystemAddress() {
    return window.external.RetPaymentSystemAddress();
}

/**
 * Кредитная организация
 * @returns {*}
 * @constructor
 */
function RetPaymentSystem() {
    return window.external.RetPaymentSystem();
}

/**
 * Телефон службы поддержки
 * @returns {*}
 * @constructor
 */
function RetPhone() {
    return window.external.RetPhone();
}

/**
 * Юридический адрес дилера
 * @returns {*}
 * @constructor
 */
function RetDealerAddress() {
    return window.external.RetDealerAddress()
}

/**
 * ОГРН
 * @returns {*}
 * @constructor
 */
function CompanyInfoData2() {
    return window.external.CompanyInfoData2();
}

/**
 * ИНН
 * @returns {*}
 * @constructor
 */
function CompanyInfoData1() {
    return window.external.CompanyInfoData1();
}

/**
 * Дилер
 * @returns {*}
 * @constructor
 */
function RetDealer() {
    return window.external.RetDealer();
}

/**
 * Адрес терминала
 * @returns {*}
 * @constructor
 */
function RetAddress() {
    return window.external.RetAddress();
}

/**
 * Номер терминала в ПС
 * @returns {*}
 * @constructor
 */
function RetTerminalNumberSystem() {
    return window.external.RetTerminalNumberSystem();
}

function GetSearchResult() {
    var res = window.external.GetSearchResult();
    return JSON.parse(res);
}

/**
 * Возвращает массив строк найденных платежей разделенных ; в формате
 * gate_id;phone;date;sum;session;status
 * @returns {*}
 * @constructor
 */
function GetSearchResultString() {
    var res = window.external.GetSearchResultString();
    return res;
}

function GetResponseSearch() {
    return window.external.GetResponseSearch();
}

/**
 * Запуск поиска платежа
 * @param date дата выбранная пользователем
 * @param number номер сессиии
 * @constructor
 */
function GetOnlineSearch(date, number) {
    window.external.GetOnlineSearch(date, number);
}
/**
 * Печатает чек
 * @param session
 * @constructor
 */
function PrintSearchCheck(session) {
    window.external.PrintSearchCheck(session + "");
    SaveLog(session + "");
    PrintCheckDialog();
}

/**
 * ПингИнтерфейса
 * @constructor
 */
function PingInterface() {
    window.external.PingInterface();
}

/**
 * Автоопределение гейта по введенному номеру
 * Изменился ли гейт для данного оператора
 * @param value
 * @returns {boolean}
 * @constructor
 */
function IsGateChange(value) {
    return window.external.IsGateChange(value);
}

/**
 * Автоопределение гейта по введенному номеру
 * Возвращает статус гейт под проверку номерных емкостей
 * @param gate_id
 * @returns {*}
 * @constructor
 */
function IsNeedMsiGatesFilter(gate_id) {
    return window.external.IsNeedMsiGatesFilter(gate_id);
}

/**
 * Автоопределение гейта по введенному номеру
 * Содержит айди гейта, если нужно сменить провайдера
 * @returns {*}
 * @constructor
 */
function GetNewMSiGate() {
    return window.external.GetNewMSiGate();
}

/**
 * Получение дополнительных параметров из бэкенда
 * @param key
 * @returns {*}
 * @constructor
 */
function RetAdditionalPaymentParameters(key) {
    return window.external.RetAdditionalPaymentParameters(key);
}

/**
 * Получение фиксированной суммы
 * Например, при онлайн проверке
 */
function GetFixedSum() {
    //return RetAdditionalPaymentParameters("FixedSum");
    return window.external.RetFixedSum();
}

/**
 * Юнистрим сумма перевода
 * @returns {*}
 * @constructor
 */
function ExpectedAmount() {
    return window.external.RetAdditionalPaymentParameters('ExpectedAmount')
}

/**
 * Юнистрим ожидаемая комиссия
 * @returns {*}
 * @constructor
 */
function EstimateComission() {
    return window.external.RetAdditionalPaymentParameters('EstimateComission');
}

/**
 * Юнистрим сумма к получению
 */
function EstimatePaidOut() {
    return window.external.RetAdditionalPaymentParameters('EstimatePaidOut');
}

function AddAdditionalPaymentParameters(key, value) {
    window.external.AddAdditionalPaymentParameters(key, value);
}

/**
 * Возвращает все наименования груп в json
 */
function RetAllGroupsJSON() {
    return window.external.RetAllGroupsJSON();
}

/**
 * Возврщает gateId or groupID в фастпанели
 * Если группа гейт отрицательный
 * 0 - нечего нет
 * @param PanelID
 * @returns {*}
 */
function RetFastPanelGateID(PanelID) {
    return window.external.RetFastPanelGateID(PanelID);
}

/**
 * Возвращает логотип для группы или пустоту если логотипа нет
 * @param gate_id
 * @returns {*}
 * @constructor
 */
function RetLogotipImgGroupID(gate_id) {
    return window.external.RetLogotipImgGroupID(gate_id);
}


 //запустить сессию нового Заказа (Orders)
function NewBasketSession() {
    window.external.Basket_Start();
}
//сессии для платежей внутри одной сессии корзины
function GenerateBasketSession() {
    return window.external.Basket_NewSession();
}

  //Закрыть сессию заказа (Orders) 
function BasketClose() {
    window.external.Basket_Close();
}


//Завести нового клиента
function NewBasketClient() {
    window.external.Basket_CreateNewClient();
}

//Какие инструменты оплаты
function GetPaymentMethod() {
var result = window.external.GetTypeOfMoneyIn();
//-1 = no devices
//1 = cup
//2 = card only
//3 = coins
//4 = all
return result;
}

function AddCheckParameter(Key, Value) {
window.external.AddCheckParameter(Key, Value);
}



function RetTopLevelGroupID() {
return window.external.RetTopLevelGroupID();
}

function BarCode(barcode) {
window.external.SetKeyFromInterface("barcode", barcode);
//DisableBarCode();
}


function EnableBarCode(){
window.external.EnableBarCode();
}

function DisableBarCode() {
window.external.DisableBarCode();
}

function ClearBarCode() {
window.external.SetKeyFromInterface("barcode", '0');
}

function CheckAdminBarcode(barcode) {
return window.external.InterCodeEmployeeID(barcode);
}



function UserAction(action, description) {
window.external.UserActionAdd(action, description);
}