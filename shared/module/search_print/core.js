function SearchCheck() {
    var ext = this;

    this.calendar = new NCalender();
    this.calendar.setContainer('pages_search_check');
    this.calendar.isCalender();
    this.calendar.setStepInfo('Укажите дату совершения платежа');
    this.calendar.setPrefix('scheck');
    this.calendar.dFilter = function() {
        StartTimeOut();
        if(this.ManageMask.checkValid()) {
            Pages.HelpSubscriber.button.next.show();
            ext.next.onclick = function () {
//                ext.number8Page();
//                ext.typeSearch();
//ext.typeValue('2');
ext.keyboardPage();
            };
        } else {
            Pages.HelpSubscriber.button.next.hide();
        }
    };

    this.numBoard = null;
    this.dateValue = "";
    this.numberValue = "";
    this.keyboardValue = "";
    this.number8Value = "";
    //номер который используется для поиска
    this.valueToSendSearch = 0;
    this.typeValue = "";
    this.result = null;

    this.back = Pages.HelpSubscriber.button.back.get();
    this.next = Pages.HelpSubscriber.button.next.get();

}
SearchCheck.prototype.start = function () {
    this.initDateView();
    this.initDate();
};
SearchCheck.prototype.initView = function () {
    setHeaderText("Проверка платежа");
};
SearchCheck.prototype.initDateView = function () {
    Pages.HelpSubscriber.date.show();
};
SearchCheck.prototype.initDate = function () {
    var ext = this;
    this.next.onclick = function () {
        //ext.number8Page();
        ext.typeSearch();

    };
    this.back.onclick = function () {
        Pages.Show('help_subscriber');
        Pages.HelpSubscriber.button.back.hide();
    };
    if (this.dateValue.length > 0) {
        this.calendar.setValue(this.dateValue);
        Pages.HelpSubscriber.button.next.show();
    }
    this.initView();
};
/**
 * Тип поиска
 * По номеру телефона
 * По номеру счета/договора
 * Другие реквизиты
 */
SearchCheck.prototype.typeSearch = function () {
    var ext = this;
    this.dateValue = this.calendar.ManageMask.getData();
    var values = [];
    values[0] = {Value: "0", Key: "По номеру телефона"};
    values[1] = {Value: "1", Key: "По номеру сессии"};
    values[2] = {Value: "2", Key: "Другие параметры"};

    this.list = new ListBoard();
    this.list.setPrefix("search");
    this.list.addList(values);
    this.list.init('wlist_element_type', 'info_wlist_type', 'infofull_wlist_type');
    this.list.setStepInfo("Выберите по каким данным необходимо произвести поиск");
    this.list.doValidDenied = function () {
        Pages.HelpSubscriber.button.next.hide();
    };
    this.list.doValidSuccess = function () {
        Pages.HelpSubscriber.button.next.show();
    };
    this.list.checkValid();
    Pages.HelpSubscriber.list.show();

    this.back.onclick = function () {
        ext.start();
    };

    this.list.setValue(this.typeValue);

    this.next.onclick = function () {
        ext.typeValue = ext.list.ManageMask.getData();
        ext.loadPageByType();
    };
    this.initView();
};
SearchCheck.prototype.loadPageByType = function () {
    //если выбрали по номеру телефона
    if (this.typeValue == 0) {
        this.number8Page();
    } else if (this.typeValue == 1) {
        this.numberPage();
    } else {
        this.keyboardPage();
    }
};
/**
 * Показываем страницу ввода номера телефона
 */
SearchCheck.prototype.number8Page = function () {
    var ext = this;

    //Скрываем шаг даты
    Pages.HelpSubscriber.button.next.hide();
    this.back.onclick = function () {
        ext.typeSearch();
    };

    this.numBoard8 = new NumBoard8('print_databox');
    this.numBoard8.setPrefix("num8");
    this.numBoard8.InitNumBoard('numboard_pages_check', 'info_pages_check', 'infofull_pages_check');
    this.numBoard8.setStepInfo("Введите номер телефона");
    this.numBoard8.setMask("(===)===-==-==");
    //что делаем если введен номер полностью
    this.numBoard8.doValidSuccess = function () {
        ext.number8Value = ext.numBoard8.ManageMask.getData();
        ext.valueToSendSearch = ext.number8Value;
        Pages.HelpSubscriber.button.search.show();
        Pages.HelpSubscriber.button.search.get().onclick = function () {
            ext.getDataFromServer();
        };
    };
    //часть номера не введено
    this.numBoard8.doValidDenied = function () {
        Pages.HelpSubscriber.button.search.hide();
    };

    Pages.HelpSubscriber.number.show();
    if (this.number8Value.length > 0) {
        this.numBoard8.setValue(this.number8Value);
        this.numBoard8.checkValid();
    }
    this.initView();

};
/**
 * Шаг поиска по другим реквизитами
 */
SearchCheck.prototype.keyboardPage = function () {
    var ext = this;

    //Скрываем шаг даты
    Pages.HelpSubscriber.button.next.hide();
    this.back.onclick = function () {
//        ext.typeSearch();
ext.start();
    };

    this.keyboard = new KeyBoardEng('print_databox_keyboard');
    this.keyboard.LangDown = "rus";
    this.keyboard.setPrefix("keyb");
    this.keyboard.InitKeyboard('numboard_pages_check_keyboard', 'info_pages_check_keyboard', 'infofull_pages_check_keyboard');
    this.keyboard.setMask("===################");
    this.keyboard.setStepInfo("Введите номер автомобиля");
    this.keyboard.doValidDenied = function () {
        Pages.HelpSubscriber.button.search.hide();
    };
    this.keyboard.doValidSuccess = function () {
        ext.keyboardValue = ext.keyboard.ManageMask.getData();
        ext.valueToSendSearch = ext.keyboardValue;
        Pages.HelpSubscriber.button.search.show();
        Pages.HelpSubscriber.button.search.get().onclick = function () {
            ext.getDataFromServer();
        };
    };

    Pages.HelpSubscriber.keyboard.show();
    if (this.keyboardValue.length > 0) {
        this.keyboard.setValue(this.keyboardValue);
        this.keyboard.checkValid();
    }
    this.initView();
};
/**
 * Шаг поиск по номеру сесиииK
 */
SearchCheck.prototype.numberPage = function () {
    var ext = this;

    //Скрываем шаг даты
    Pages.HelpSubscriber.button.next.hide();
    this.back.onclick = function () {
        ext.typeSearch();
    };

    this.numBoard = new NumBoardNoRule('print_databox');
    this.numBoard.setMask("=====================");
    this.numBoard.setPrefix("num");

    this.numBoard.InitNumBoard('numboard_pages_check', 'info_pages_check', 'infofull_pages_check');
    this.numBoard.setStepInfo("Введите номер сессии. Номер сессии состоит из 21 цифры.");
    //что делаем если введен номер полностью
    this.numBoard.doValidSuccess = function () {
        ext.numberValue = ext.numBoard.ManageMask.getData();
        ext.valueToSendSearch = ext.numberValue;
        Pages.HelpSubscriber.button.search.show();
        Pages.HelpSubscriber.button.search.get().onclick = function () {
            ext.getDataFromServer();
        };
    };
    //часть номера не введено
    this.numBoard.doValidDenied = function () {
        Pages.HelpSubscriber.button.search.hide();
    };

    Pages.HelpSubscriber.number.show();
    if (this.numberValue.length > 0) {
        this.numBoard.setValue(this.numberValue);
        this.numBoard.checkValid();
    }
    this.initView();
};

SearchCheck.prototype.printPage = function () {
    var ext = this;
    StartTimeOut();
    Pages.HelpSubscriber.print.show();
    Pages.HelpSubscriber.button.back.get().onclick = function () {
//        ext.loadPageByType();
ext.keyboardPage();
    };
    Pages.HelpSubscriber.print.show();

    var table = "<table class='list_print_check'><thead>" +
        "<tr><th>Дата платежа</th><th>Сумма</th><th>Сессия</th><th>Операции</th></tr></thead>";

    var res = GetSearchResult();
    var data = ext.result.split("\r\n");

    for (var i = 0; i < data.length; i++) {

        if (data[i].length > 0) {
            var elem = data[i].split(";");
            var gateId = elem[0];
            var number = elem[1];
            var date = elem[2];
            var dateEnter = elem[3]
            var sum = elem[4];
            var session = String(elem[5]);
            var st = elem[6];

            for (var j = 0; j < res.LDGQ.length; j++) {
                if (gateId == res.LDGQ[j].GateID) {
                    var nameGate = res.LDGQ[j].Name;
                break;
            }
        }

        var status = 'В обработке';
            if (st == 1) {
            status = 'Проведен'
            } else if (st == 3) {
            status = 'Ошибка'
            } else if (st == 5) {
                status = 'Отменен'
            } else {
                dateEnter = "";
            }

            if (st == 5 || st == 3) {
                dateEnter = "";
            }

            var check_id = "check_" + i;
            table += "<tr><td align='center'>" + date + "</td><td align='center'>" + sum + "</td><td align='center'>" +
            session + "</td><td align='center'><img id='ch" + i + "' src='img/prtcheck_btn_new.png' onclick='PrintSearchCheck(" + session.substr(0, 11) + ");HidePrintButtonInSearch("+i+")'></td></tr></tdead>";
        }
    }

    table += "</table>";

    getElemByID('pages_see_search_check').innerHTML = table;

    this.initView();
};
SearchCheck.prototype.getDataFromServer = function () {
    var ext = this;
    var text = "";
    //запросить запуск проверка на стороне сервера
    this.dateValue = this.calendar.ManageMask.getData();
    if (this.dateValue.length > 0) {
        this.calendar.setValue(this.dateValue);
}

    GetOnlineSearch(this.dateValue, this.valueToSendSearch);
    var onlineTimer = new IndependentTimer(60, "Идет поиск по введенным реквизитам");
    onlineTimer.timer.directionTimeToBig();
    //указываем что делать по истечению таймера
    onlineTimer.timer.finalize = function () {
        //убираем всплывающее окно
        onlineTimer.stop();
        //запрашиваем статус проверки
        var status = GetResponseSearch();
        //1 - разрешено, 0 - отказ, -1 еще не проверено
        //все таки разрешено
        if (status == 1) {
            //убираем окно, запускаем таймер переходим в степ
            ext.result = GetSearchResultString();
            if (ext.result.length > 0) {
                ext.printPage();
            } else {
                text = "Платеж по указанным реквизитам не найден";
                OnlineDiv('loadingdivmsgyesnomenu', 1, text);
                OnlineDiv('msgnobutton', 1);
                OnlineDiv('msgyesbutton', 0);
                OnlineDiv('msgmenubutton', 1);
            }
        } else {
            text = "Нет связи с сервером поставщика услуг. ";
            OnlineDiv('loadingdivmsgyesnomenu', 1, text);
            OnlineDiv('msgnobutton', 1);
            OnlineDiv('msgyesbutton', 0);
            OnlineDiv('msgmenubutton', 1);
        }
    };

    //выполняется каждую секунду
    onlineTimer.timer.execute = function () {
        var status = GetResponseSearch();
        //1 - разрешено, 0 - отказ, -1 еще не проверено
        //Если сервер ответил положительно или отказ
        if (status == 1 || status == 0) {
            //Метод finalize всегда выполняется при остановке таймера
            onlineTimer.stop();
        }
    };
    //определяем действия для проверки статуса возможности проплаты платежа
    onlineTimer.start();
};

function SearchCheckEnable() {
    var process = new SearchCheck();
    SetCurrentPage(Pages.Link.OTHER);
    process.start();
}

