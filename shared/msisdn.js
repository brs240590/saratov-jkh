/**
 * Файл содержит функции для автоматического определения номерных емкостей
 * Номерные емкости
 * Используется для автоматического определения оператора
 * Между билайн, мтс, мегафон
 * 831 - номерная емкость,
 * 78 - гатеайди без коммисии
 * 77 - гатеайди комисионный
 * @type {*[]}
 */
/**
 * Определение номерной емкости
 * @param listGate
 * @constructor
 */
function MsiSdnManager() {
    this.needGateID = 0;
}
/**
 *
 * @param value
 * @returns {boolean}
 * @constructor
 */
MsiSdnManager.prototype.IsGateChange = function (value) {
    if(IsSystemNumber(value, this.needGateID)) {
        return false;
    } else {
        return IsGateChange(value);
    }
};
MsiSdnManager.prototype.getNewGate = function () {
    return GetNewMSiGate();
};
/**
 * Определяет попадает ли гатеайди под фильтр номерных емкостей
 * Если подходит определяем комиссионный шлюз или безкомиссионный
 * Нулевой шлюз не пройдет проверку
 * @param gate_id
 * @returns {boolean}
 */
MsiSdnManager.prototype.isOneOfListGateID = function (gate_id) {
    this.needGateID = gate_id;
};
/**
 * Геттер возможно ли фильтрация или нет
 * @returns {boolean}
 * @constructor
 */
MsiSdnManager.prototype.IsNeedFilter = function () {
    return IsNeedMsiGatesFilter(this.needGateID);
};