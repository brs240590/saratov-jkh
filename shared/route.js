/**
 * Created by lodar on 18.02.15.
 */
/**
 * Сеттер на какой странице сейчас находится интерфейс
 * @param page_id
 */
function SetCurrentPage(page_id) {
    Pages.currentPage = page_id;
    var isMain = IsMain();
    SaveLog("CurrentPageMain, isMain: " + isMain);
    CurrentPageMain(isMain);
}

/**
 * Геттер на какой странице находится интерфейс
 * @returns {Pages.currentPage|*}
 */
function GetCurrentPage() {
    return Pages.currentPage;
}