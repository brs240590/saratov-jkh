/**
 * Текущая страница, используется для сохранения на какой странице находится клиент
 * в момент выбора оператора
 * @type {number}
 */
var currentNumPage = 0;

function WScrolling(id) {
    this.speed = 550;
    this.height = 570;
    this.page_num = 1;
    this.obj = document.getElementById(id);
}

WScrolling.prototype.init = function () {

    this.obj.up = false;
    this.obj.down = false;
    this.obj.fast = false;
    var container = document.createElement("div");
    var parent = this.obj.parentNode;
    if (parent.id == 'wgroup') {
    container.id = "wscroll";
    container.style.border = 1;
    parent.insertBefore(container, this.obj);
    parent.removeChild(this.obj);

    container.style.position = "relative";
    container.style.height = this.height + "px";
    container.style.overflow = "hidden";
    this.obj.style.position = "absolute";
    this.obj.style.top = "0";
    this.obj.style.left = "0";
    container.appendChild(this.obj);
                            } else {
				    this.obj.style.top = "0";
  				   }
    this.finalize();
};
/**
 * Показ скрытие кнопок вперед назад
 */
WScrolling.prototype.finalize = function () {
    if (this.obj.offsetTop < 0) {
      //  document.getElementById("btnback").style.visibility = "visible";
    }
    else {
        //document.getElementById("btnback").style.visibility = "hidden";
    }

    if ((this.obj.offsetHeight + this.obj.offsetTop) < this.height) {
       // document.getElementById("btnmoreop").style.visibility = "hidden";
    }
    else {
        //document.getElementById("btnmoreop").style.visibility = "visible";
    }
};

WScrolling.prototype.start = function () {

    var newTop;
    var objHeight = this.obj.offsetHeight;
    var top = this.obj.offsetTop;
    var fast = (this.obj.fast) ? 2 : 1;
//alert("top:"+top);
/*
alert("height:"+objHeight);
alert("top:"+top);
alert("thisheight:"+this.height);
alert("speed:"+this.speed);
alert("fast:"+fast);
*/

    if (this.obj.down) {
        newTop = ((objHeight + top) > this.height) ? top - (this.speed * fast) : top;
//alert("newtop_down:"+newTop);
        this.obj.style.top = newTop + "px";
        this.obj.down = false;
    }
    if (this.obj.up) {
        newTop = (top < 0) ? top + (this.speed * fast) : top;
//alert("newtop_up:"+newTop);
        this.obj.style.top = newTop + "px";
        this.obj.up = false;
    }
    this.finalize();
};

WScrolling.prototype.onclick_up = function () {
    this.page_num--;
    currentNumPage--;
    StartTimeOut();
    this.obj.up = true;
    this.start();
};

WScrolling.prototype.onclick_down = function () {
    this.page_num++;
    currentNumPage++;
    StartTimeOut();
    this.obj.down = true;
    this.start();
};
/**
 * Скролит список указанное количество раз
 * @param countScroll
 */
WScrolling.prototype.scrollListDown = function(countScroll) {
    for(var i = 0; i < countScroll; i++) {
        this.onclick_down();
    }
};
var abstractScroll = new WScrolling("abstract scroll");
