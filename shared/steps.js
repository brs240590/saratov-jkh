function StepOne() {
    this.StepID = null;
    this.GateID = null;
    this.ExecutorID = null;
    this.Name = null;
    this.OrderID = null;
    this.InputType = null;
    this.InputMask = null;
    this.Info = null;
    this.InfoFull = null;
    this.Prefix = null;
    this.isonlinequery = null;
    this.isonlinequerynecessity = null;
    this.StepStructure = null;
    this.MaxSum = 0;
    this.MinSum = 0;
    this.value = '';
    this.maskValue = '';
    this.OnlineMode = -1;
    this.isOnline = false;
}

var MsiManager = new MsiSdnManager();

/**
 * Дата-провайдер списка шагов, содержит введенные значения
 * Используется для повторного проведения платежа
 * @constructor
 */
function DataGateStepsList() {
    this.steps = [];
    this.GateID = 0;
    this.ExecutorID = 0;
}
/**
 * Добавление значения степа
 * @param orderID
 * @param value
 */
DataGateStepsList.prototype.addStep = function(orderID, value) {
    this.steps.push({orderID: orderID, value: value});
};
/**
 * Добавление всех степов: ипользуется только в дебагмоде=1
 * @param steps
 */
DataGateStepsList.prototype.addStepAll = function(steps) {
    this.clear();
    for(key in steps) {
        if(steps.hasOwnProperty(key)) {
            //SaveLog("addStepALll orderid " + steps[key].OrderID  + " value " +steps[key].value + " key" + key)
            this.addStep(steps[key].OrderID, steps[key].value);
        }

    }
};
/**
 * Получить значение степа по его ордерид
 * @param orderID
 * @returns {*}
 */
DataGateStepsList.prototype.getValueByID = function(orderID) {
    for(var i = 0; i < this.steps.length; i++) {
        if(this.steps[i].orderID == orderID) {
            return this.steps[i].value;
        }
    }
};
/**
 * Установить значение степа по его ордерид
 * @param orderID
 * @param value
 */
DataGateStepsList.prototype.setValue = function(orderID, value) { 
    for(var i = 0; i < this.steps.length; i++) {
        if(this.steps[i].orderID == orderID) {
            this.steps[i].value = value;
            break;
        }
    }
    //SaveLog("Steplist setValue id " + orderID + " val :" + value);
};
DataGateStepsList.prototype.getSteps = function() {
    return this.steps;
};
DataGateStepsList.prototype.clear = function() {
    this.steps = [];
    this.GateID = 0;
    this.ExecutorID = 0;
};

var StepList = new DataGateStepsList();
var PaymentOperatorID = 0;

function PaymentStart(id) {
    this.page_num = currentNumPage;

    this.steps = [];
    //очищаем дополнительное хранилище степов
    StepList.clear();
    //устанавливаем значение гатеид текущего оператора
    PaymentOperatorID = id;

    this.PAGES = {
        steps: 1,
        confirm: 2
    };
    //текущий степ
    this.currentStep = 0;
    this.factory = new UIFactory();
    this.currentPage = 0;
    //флаг говорит что из конфирма уходим сразу на оплату
    this.NoMoneySend = false;

    this.dataGateQuery = this._getDataGateQuery(id);

    Pages.currentGroupID = this.dataGateQuery.GroupID;
    //Содержит значение фиксированной суммы
    this.fixSumValue = 0;

    //Включаем кнопку повторить платеж
    EnableRepayButton();

    //менеджер автоопределения номера согласно списку операторов
    MsiManager = new MsiSdnManager();
    MsiManager.isOneOfListGateID(id);

}

//Получение информации о датагейте и степах
PaymentStart.prototype._getDataGateQuery = function(id) {
    //обнуляем значение фикседсам
    this.fixSumValue = 0;
    var dataGateQuery = null;
    if(DEBUGMODE) { 
        dataGateQuery = TestStepFactory();
        this.steps = dataGateQuery.Steps;
        StepList.addStepAll(dataGateQuery.Steps);
        StepList.GateID = dataGateQuery.GateID;
    }
    else {
        dataGateQuery = JSON.parse(RetGateJSON(id));
        this.NoMoneySend = Boolean(dataGateQuery.noMoney);

        Pages.currentGroupID = dataGateQuery.GroupID;

//        changeGroupName(Pages.currentGroupID);

        //Хедер для наименование группы
        var group = GetGroupStructure(Pages.currentGroupID);
        //setHeaderText(group.Name);

        this.maxComission = dataGateQuery.MaxComission;
        //Считываем степы и формируем массивы данных
        for(var i = 0; i < dataGateQuery.Steps.length; i++) { 
            var oneStep = new StepOne();
            oneStep.StepID = dataGateQuery.Steps[i].StepID;
            oneStep.GateID = dataGateQuery.GateID;
            oneStep.ExecutorID = dataGateQuery.ExecutorId;
            oneStep.Name = dataGateQuery.Steps[i].Name;
            oneStep.InputType = dataGateQuery.Steps[i].InputType;
            oneStep.InputMask = dataGateQuery.Steps[i].InputMask;
            oneStep.OrderID = dataGateQuery.Steps[i].OrderID;
            oneStep.Info = dataGateQuery.Steps[i].Info;
            oneStep.InfoFull = dataGateQuery.Steps[i].InfoFull;
            oneStep.isonlinequery = dataGateQuery.Steps[i].isonlinequery;
            oneStep.isonlinequerynecessity = dataGateQuery.Steps[i].isonlinequerynecessity;
            //для списка проверить лист
            oneStep.StepStructure = dataGateQuery.Steps[i].StepStructure;
            oneStep.MinSum = dataGateQuery.MinSum;
            oneStep.MaxSum = dataGateQuery.MaxSum;
            //Содержит значение без маски в чистом ввиде, то что передается на серверную сторону
            oneStep.value = '';
            //Содержит текстовое значение для списка выбранного елемента
            oneStep.maskValue = '';
            oneStep.OnlineMode = dataGateQuery.OnlineMode;
            oneStep.isOnline = dataGateQuery.isOnline;

            this.steps[oneStep.OrderID] = oneStep;
            StepList.addStep(oneStep.OrderID, oneStep.value);
        }
        StepList.GateID = dataGateQuery.GateID;
	StepList.ExecutorID = dataGateQuery.ExecutorId;
    }
    return dataGateQuery;
};

PaymentStart.prototype._init = function() {
    try {
        this.showStep();
    } catch(e) {
        SaveExceptionToLog(e, "steps.js", "PaymentStart.prototype._init.showStep()");
    }

    try {
        this.getOnlineData();
    } catch(e) {
        SaveExceptionToLog(e, "steps.js", "PaymentStart.prototype._init._initStep()");
    }
};

PaymentStart.prototype.showStep = function() {

    var step = this.steps[this.currentStep];
    SetCurrentPage(Pages.Link.STEPS);

    var typeStep = step.InputType.toUpperCase();
    var sumMinMaxInfo = '';
    if(typeStep == "NUMBER") {
        Pages.Steps.numboard.number();
    } else if(typeStep == "NUMBER8") {
        Pages.Steps.numboard.number();
    } else if(typeStep == "FIOBOX") {
        Pages.Steps.keyboard.fiobox();
    }
    else if(typeStep == "KEYBOARD") { 
        Pages.Steps.keyboard.keyboard();  
    } else if(typeStep == "KEYBOARDENG") {
        Pages.Steps.keyboard.keyboard();
    } else if(typeStep == "CALENDAR") {
        Pages.Steps.calendar.full();
    } else if(typeStep == "CALENDARMONTH") {
        Pages.Steps.calendar.month();
    } else if(typeStep == "FIXEDSUMM") {
        var determinateMinSum = DeterminateMinSum(step.MinSum, RetMinPaymentSize());
        sumMinMaxInfo = "<span style='color:red;'>Внимание!</span> Минимальный сумма составляет <span style='color:red;'>" + determinateMinSum + " руб.</span>" +
            " Максимальная сумма составляет <span style='color:red;'>" + step.MaxSum + " руб.</span>";
        Pages.Steps.numboard.fixedsum();
    } else if(typeStep == "LIST") {
        Pages.Steps.listboard.list();
    } else if(typeStep == "LISTGOODS") {
        Pages.Steps.listboard.list();
    }else if(typeStep == "LISTONLINE") {
        //!!!Важно если елемент скрыт, скролинг не возможен. Потому как высота елемента все время 0
        //поєтому сначала необходимо показать список
        Pages.Steps.listboard.listonline();
        DisableRepayButton();
    } else if(typeStep == 'TOCOUNTRY') {
        Pages.Steps.country.countryList();
        DisableRepayButton();
    } else if(typeStep == 'STATICKOM') {
        Pages.Steps.numboard.number();
    } else if(typeStep == 'PRICE') {
//        Pages.Steps.numboard.number();
    } else if(typeStep == 'WOWSTEP') {
//        Pages.Steps.wow();
/////////////////////////////////////////////////
        var enteredDataDiv = document.getElementById('enteredData');
        var enteredData = '';
        Data = new Date();
        Day = Data.getDate();
        var i=0;
	  for (var key in cart.cartvalue) {
        Hour = parseFloat(cart.cartvalue[key].time.substring(0,2));
        Minutes = parseFloat(cart.cartvalue[key].time.substring(2,4));

        if (Hour < 10) { Hour = "0"+Hour; }
        if (Minutes < 10) { Minutes = "0"+Minutes; }

            enteredData += "<div id='masterphoto'><img style='border: 15px solid #FFFFFF;border-radius: 111px;margin-top:-16px;' src='"+cart.cartvalue[key].photo+"' width=183 height=182</div>";
            enteredData += "<div id='masterinfo' style='margin-left: 15px;'>ВАШ МАСТЕР<br><div id='mastername'>"+cart.cartvalue[key].FIO+"</div></div>";

            enteredData += "<div id='timeinfo'>";
            enteredData += "<div id='timeinfoday'>"+Day+"</div>";
            enteredData += "<div id='timeinfoweekday'>"+getWeekDay(Data)+"</div>";
            enteredData += "<div id='timeinfohour'>"+Hour+":"+Minutes+"</div>";
            enteredData += "<div id='timeinfotext'>БЛИЖАЙШЕЕ ВРЕМЯ</div>";
            enteredData += "</div>";
            enteredData += "<table width='900' cellspacing=20 cellpadding=20 align=center border=0 id='confirmcart'>";


	  var providerprice = cart.cartvalue[key].stepvalue.replace(' ','');
	  enteredData += "<tr><td height='60' align='left'><span class='rocherinfocart'>" + cart.cartvalue[key].stepname.toUpperCase() + "</span></td>\n";
          enteredData += '<td width="200"><span class="rocherprice">' + cart.cartvalue[key].stepvalue + ' <span class="rubconfirm">Р</span></span></td>\n';
//	  enteredData += '<td width="50"><div class="provmenudelete" onclick="DelProvFromCart(' + cart.cartvalue[key].providerid + ');" style="float: left;"><img src="img/prov_delete.png"></div></td></tr>\n';
	  enteredData += '<td width="50"></td></tr>\n';
	  if (i < cart.col){
	  enteredData += '<tr><td colspan=3 style="height:1px; background-color: #b5b5b5;"></td></tr>';
	                            }
//countcart++;
        	}

        enteredData += '<tr style="height:76px;"><td colspan="3" align="center"><br><img id="wbuttons_backcart" src="img/btn_back_cart.png" style="visibility:hidden;"/></td></tr>';
        enteredData += '<tr><td colspan=3 style="height:1px; background-color: #000000;"></td></tr>';

            enteredData += "</table>";

        enteredDataDiv.innerHTML = enteredData;

        cart.summ = parseFloat(cart.summ);
        carttotal.innerHTML = cart.summ+" <span class=\"rubconfirm\">Р</span>";
        carttotal.style.fontSize = "80px";
        carttotal.style.fontWeight = "bold";
        carttotal.style.position = "absolute";
        carttotal.style.top = "450px";
        carttotal.style.left = "200px";

        var button_approve = document.getElementById('wbuttons_approve');
        button_approve.style.position = "absolute";
        button_approve.style.top = "450px";
        button_approve.style.left = "730px";

        Pages.Steps.confirm();
/////////////////////////////////////////////////

    }


    var infoMinMax = document.getElementById('opinfostep_min_max_sum');
//    infoMinMax.innerHTML = sumMinMaxInfo;
    infoMinMax.innerHTML = '';
    Pages.ShowOne('wgroup_name');

    //Если данные валидные
    //if (step.value.length > 0 && this.uiElement.ManageMask.checkValid())
    //    Pages.Steps.button.next.show();
};
/**
 * Используется для запроса данных из сетир для построения степа
 */
PaymentStart.prototype.getOnlineData = function() {
    var step = this.steps[this.currentStep];
    var typeStep = step.InputType.toUpperCase();
    var ext = this;
    if(typeStep == "LISTONLINE") {
        try {
            //запрос информации для получения данных степа
            //getOnlinePaymentStatus(1);
            SaveLog("online_lst: " + getOnlinePaymentStatus(1));
            var getDataTimer = new IndependentTimer(60, "Получение данных от поставщика услуг");
            getDataTimer.timer.directionTimeToBig();
            //указываем что делать по истечению таймера
            getDataTimer.timer.finalize = function() {
                //убираем всплывающее окно
                getDataTimer.stop();
                //запрашиваем статус получения данных
                var status = getResponseOnlinePayment();
                //1 - получено, 0 - отказ, -1 еще не вернули
                //данные получены
                if(status == 1) {
                    ext._initStep();
                } else {
                    var text = '';
                    if(status == 0) {
                        var text = GetOnlineModeText2Screen();
                    } else {
                        text += "Нет связи с сервером поставщика услуг. <br>";
                    }
                    text += "Нажмите 'Далее' для повторного полученния данных от поставщика услуг";

                    document.getElementById('msgyesbutton').onclick = function() {
                        SaveLog("Переход на шаг назад для повторного запроса. Пользователь согласился и нажал 'Далее'")
                        OnlineDiv('loadingdivmsgyesnomenu', 0);
                    };
                    ext.currentStep--;
                    ext._init();

                    OnlineDiv('loadingdivmsgyesnomenu', 1, text);
                    OnlineDiv('msgnobutton', 0);
                    OnlineDiv('msgyesbutton', 1);
                    OnlineDiv('msgmenubutton', 1);
                }
            };

            //выполняется каждую секунду
            getDataTimer.timer.execute = function() {
                var status = getResponseOnlinePayment();
                //1 - получено, 0 - отказ, -1 еще не вернули
                //Если сервер ответил положительно или отказ
                if(status == 1 || status == 0) {
                    //Метод finalize всегда выполняется при остановке таймера
                    getDataTimer.stop();
                }
            };
            //определяем действия для проверки статуса возможности проплаты платежа
            getDataTimer.start();


        } catch(e) {
            SaveExceptionToLog(e, "steps.js", "GetParamsForListOnline(), currentStep: " + ext.currentStep);
        }
    } else {
        this._initStep();
    }
    return status;
};
PaymentStart.prototype._initStep = function() {
    var ext = this;

    var step = this.steps[this.currentStep];
    var typeStep = step.InputType.toUpperCase();


    if(typeStep == "NUMBER") {
        this.uiElement = this.factory.makeNumBoard.number(step.InputMask);
    } else if(typeStep == "NUMBER8") {
        this.uiElement = this.factory.makeNumBoard.number8(step.InputMask);
    } else if(typeStep == "FIOBOX") {
        this.uiElement = this.factory.makeKeyboard.fiobox(step.InputMask);
    }
    else if(typeStep == "KEYBOARD") {
	this.uiElement = this.factory.makeKeyboard.keyboard(step.InputMask);
    } else if(typeStep == "KEYBOARDENG") {
        this.uiElement = this.factory.makeKeyboard.keyboardeng(step.InputMask);
    } else if(typeStep == "CALENDAR") {
        this.uiElement = this.factory.makeCalendar.calender();
    } else if(typeStep == "CALENDARMONTH") {
        this.uiElement = this.factory.makeCalendar.month();
    } else if(typeStep == "FIXEDSUMM") {
        var determinateMinSum = DeterminateMinSum(step.MinSum, RetMinPaymentSize());
        this.uiElement = this.factory.makeNumBoard.fixedSum(determinateMinSum, step.MaxSum);
    } else if(typeStep == "LIST" || typeStep == "LISTGOODS") {
//        var dataList = JSON.parse(window.external.RetStepStructureListJSON(step.StepID));
        var dataList = JSON.parse(window.external.RetStepStructureListJSON(step.StepID));
        var listleng = dataList.length;
        dataList.push({Key:'gateid', Value:step.GateID});
        dataList.push({Key:'executorid', Value:step.ExecutorID});
        this.uiElement = this.factory.makeListBoard.list(dataList);
	step.value=this.dataGateQuery.Name;
	SetStepVal('0', this.dataGateQuery.Name);
    } else if(typeStep == "LISTONLINE") {
        //!!!Важно если елемент скрыт, скролинг не возможен. Потому как высота елемента все время 0
        //поєтому сначала необходимо показать список
        //убираем окно, запускаем таймер переходим в степ
        var param = GetParamsForListOnline();
        ext.uiElement = ext.factory.makeListBoard.listonline(param);
    } else if(typeStep == 'TOCOUNTRY') {
        this.uiElement = this.factory.makeCountry.listcountry();
    } else if(typeStep == 'STATICKOM') {
        this.uiElement = this.factory.makeNumBoard.statickom(step.InputMask);
    } else if(typeStep == 'PRICE') {
    this.action();
    //ClickOperator(1314);
    } else if(typeStep == 'WOWSTEP') {
        //Устанавливаем обработчик на клавишу вперед
        var next = document.getElementById('wbuttons_approve');
        WEvents.remove(next, 'click');
        next.onclick = function() {
                ext.currentStep=0;
                ext.action();
        };

//    this.currentStep=2;
//    this.action();
    }

    this.uiElement.setStepInfo(step.Info);
    this.uiElement.setStepInfoFull(step.InfoFull);
    //Устанавливаем обработчики

    this.setEvents();
    this.uiElement.setValue(step.value);
    this.setLogo();
    //Если данные валидные
        Pages.Steps.button.next.hide(); 
    if(step.value.length >= 0 && this.uiElement.ManageMask.checkValid()) 
//        Pages.Steps.button.next.show(); 
    StartTimeOut();


};

/**
 * Вешает обработчики на вперед и назад
 */
PaymentStart.prototype.setEvents = function() {
    var ext = this;

    var next = document.getElementById('wbuttons_main_next');
    WEvents.remove(next, 'click');
    next.onclick = function(e) {
        //EffectClick('wbuttons_main_next');
	//if (this.currentStep = 2) {
        ext.next();
        //                          } else {
         //                                ext.next();
          //                               }
    };
    Pages.Steps.button.next.hide();

    var back = document.getElementById('wbuttons_main_back');
    WEvents.remove(back, 'click');
    back.onclick = function() {
        // EffectClick('wbuttons_main_back')

        if (PaymentOperatorID == 1314 && ext.currentStep == 0) {
            Pages.ShowListOperatorByGroup(214); 
        }
        if (PaymentOperatorID == 1314 && ext.currentStep == 1) {
            ext.back();
        } 
        if (PaymentOperatorID == 1314 && ext.currentStep == 2) {
            ext.back();
        } 
//        if (PaymentOperatorID == 1373 && ext.currentStep == 1) {
//        this.currentStep = 0;
//        Pages.
//        } 

        if (PaymentOperatorID == 1374) {
                                       ext.back();
					} else { Main(); }
	//if (ext.currentStep == 0) { Main(); } else { ext.back(); }
    };

    var cartreact = document.getElementById('cart');
    WEvents.remove(cartreact, 'click');
    cartreact.onclick = function() {
	if (ext.currentStep == 0 && cart.col > 0) { opencart(ext); }
    };

    Pages.Steps.button.back.show();


    var next = document.getElementById('wowbutton');
    WEvents.remove(next, 'click');
    next.onclick = function(e) {
        ext.next();
    };
    Pages.Steps.button.next.hide();

};

/**
 * Некст в странице степов
 */
PaymentStart.prototype.next = function() {

    //просмотрететь чтобы дублей не было
    var ext = this;
    StopTimeOutMain();

    //получаем значение текущего степа
    this._setValue();

//    ext.AddToCart();

    var vlp = this._getValue(this, this.currentStep);
    IsSystemNumber(vlp, this.steps[this.currentStep].GateID);

    var step = ext.steps[ext.currentStep];
    var typeStep = step.InputType.toUpperCase();
    if (typeStep == "FIXEDSUMM") { 
				AddProvToCart(step.GateID, ext.dataGateQuery.Name, vlp,step.ExecutorID,'FIXEDSUMM');  
				  }


    //Проверяем заблокирован шаг или нет
    if(IsNumberInBlackList(vlp)) {
        SaveLog(vlp + " в черном списке.");
        OnlineDiv("loadingdivmsgyesnomenu", 1, "Вам запрещено пользоваться данным терминалом.");
        OnlineDiv('msgnobutton', 0);
        OnlineDiv('msgyesbutton', 0);
        OnlineDiv('msgmenubutton', 1);
        return;
    }
/*
    var regexCheck = CheckRegexValue(this.steps[this.currentStep].StepID, vlp);
    //проверяем по регулярному выражению
    if(!regexCheck) {
        document.getElementById('msgyesbutton').onclick = function() {
            SaveLog(vlp + ' не прошел проверку на регулярное выражение');
            nextCheck();
        };
        SaveLog(vlp + ' не прошел проверку на регулярное выражение');
        OnlineDiv("loadingdivmsgyesnomenu", "1", "Введенные вами данные не соответсвуют параметрам поставщика услуг. Вы уверены что хотите совершить данный платеж?");//окно
        OnlineDiv('msgnobutton', 1);
        OnlineDiv('msgyesbutton', 1);
        OnlineDiv('msgmenubutton', 1);
        return;
    } else {
        //Все ок пропускаем дальше выполнение
        nextCheck();
    }
*/
    nextCheck();
    function nextCheck() {
        var online = get_online();
        //Если онлайн режим
        if(online) {
            SaveLog('Включен онлайн режим');
            ext.online();
        } else {
            ext.action();
        }
    }

    //определяем нужно ли выключать онлайн режим или нет
    function get_online() {
        var status = false;
        var isOnline = ext.steps[ext.currentStep].isOnline;//всегда использовать онлайн режим работы
        var OnlineMode = ext.steps[ext.currentStep].OnlineMode;//онлайн в профиле (поддерживает это шлюз или нет)
        var isonlinequery = ext.steps[ext.currentStep].isonlinequery;
        var isonlinequerynecessity = ext.steps[ext.currentStep].isonlinequerynecessity;

        if(isOnline == true && isonlinequery == true) {
            status = true;
        } else if(isOnline == false) {
            if(OnlineMode == -1) {
                if(isonlinequerynecessity == true)
                    status = true;
            } else if(OnlineMode == 1) {
                if(isonlinequery == true)
                    status = true;
            }
        }
        return status;
    }


};
/**
 * Проверка платежа онлайн
 */
PaymentStart.prototype.online = function() {
    var ext = this;
    //запросить запуск проверка на стороне сервера
if (PaymentOperatorID == 1314 && this.currentStep == 0) {
window.external.AuthClient(ext.steps[0].value);
                                                        }
else if (PaymentOperatorID == 1314 && this.currentStep == 1) {

var clientFio = ext.steps[1].value;
var phone = ext.steps[0].value;
SaveLog(clientFio+" "+phone+" "+phone+" "+" "+0+" ");
window.external.RegClient(clientFio, phone, phone, 0, 0, 0);
                                                        }
else if (PaymentOperatorID == 1404) {
var selmaster = window.external.GetKeyForInterface('browmasterid');
var selmastername = window.external.GetKeyForInterface('browmastername');
var selday = window.external.GetKeyForInterface('datesel');
var servicesstr = window.external.GetKeyForInterface('browservices');
var seltime = window.external.GetKeyForInterface('timesel');
var forcabinet='';
AddAdditionalPaymentParameters("br2", window.external.GetKeyForInterface('br2'));
AddAdditionalPaymentParameters("services", servicesstr);
AddAdditionalPaymentParameters("Day", selday);
AddAdditionalPaymentParameters("Time", seltime);
AddAdditionalPaymentParameters("MasterID", selmaster);
AddCheckParameter("Дата записи", selday);
AddCheckParameter("Время записи", seltime.substr(0,2)+":"+seltime.substr(2,2));
AddCheckParameter("Мастер", selmastername);
var countservices=1;
forcabinet+= "Мастер: "+selmastername+", Дата: "+selday+", Время: "+seltime.substr(0,2)+":"+seltime.substr(2,2)+", ";
for (var key in servicecart.cartvalue) {
AddCheckParameter("Услуга "+countservices, servicecart.cartvalue[key].name+", "+servicecart.cartvalue[key].minutes+" мин., "+servicecart.cartvalue[key].price+"руб.");
forcabinet  += "Услуга "+countservices+": "+servicecart.cartvalue[key].name+", "+servicecart.cartvalue[key].minutes+" мин., "+servicecart.cartvalue[key].price+"руб.";
countservices++;
                                       }

window.external.BrowUPGetTimeForMasterChek(selmaster, selday, servicesstr,seltime);
                                                        }
else {}

//    SaveLog("online: " + getOnlinePaymentStatus(0));
    var onlineTimer = new IndependentTimer(60, "Идет проверка введенных данных");
    onlineTimer.timer.directionTimeToBig();
    //указываем что делать по истечению таймера
    onlineTimer.timer.finalize = function() {
        //убираем всплывающее окно
        onlineTimer.stop();
        //запрашиваем статус проверки
        var status = getResponseOnlinePayment();
        //1 - разрешено, 0 - отказ, -1 еще не проверено
        //все таки разрешено
        if(status == 1) {

            var fixSumValue = cart.summ;
            SetFixedSumTxt(fixSumValue);
            AddAdditionalPaymentParameters("Acct", fixSumValue);
            AddAdditionalPaymentParameters("FixedSum", fixSumValue);

            if (PaymentOperatorID == 1314 && ext.currentStep == 2) {
		Pages.Hide();
		Pages.Show.method();
                                                                   } 
            else if (PaymentOperatorID == 1404) {
		Pages.Hide();
                document.getElementById('printchecktxt').innerHTML = "Мы вас записали<br>До скорых встреч!";
                AddAdditionalPaymentParameters("HideSumm_Gate", "true");
//////////////////////////
	var basket='<?xml version="1.0" encoding="utf-16"?><ArrayOfInterfaceBasketStructure>';
	basket += '<InterfaceBasketStructure><GateID>1404</GateID><ExecutorID>621</ExecutorID><Summ>0</Summ><Steps>'+window.external.GetStepVal('0')+';'+window.external.GetStepVal('1')+';'+forcabinet+'</Steps></InterfaceBasketStructure>';
	basket += '</ArrayOfInterfaceBasketStructure>';
	AddAdditionalPaymentParameters('InterfaceBasket',basket);
//////////////////////////
                window.external.PayConfirmNew();
                                                }else {
                                                       Pages.Hide();									    
                                                       Pages.Steps.method();
                                                      }
        } else {
            ext.action();
        }
    };

    //выполняется каждую секунду
    onlineTimer.timer.execute = function() {
        var status = getResponseOnlinePayment();
        //1 - разрешено, 0 - отказ, -1 еще не проверено
        //Если сервер ответил положительно или отказ
        if(status == 1 || status == 0) {
            //Метод finalize всегда выполняется при остановке таймера
            onlineTimer.stop();
        }
    };
    //определяем действия для проверки статуса возможности проплаты платежа
    onlineTimer.start();

};
PaymentStart.prototype.back = function() {
    //обновляем таймер я еще здесь
    //EffectClick('wbuttons_main_back');
    //Если степ 0 бэк возвращает на страницу списка операторов

    if(this.currentStep == 0) { 
        if (PaymentOperatorID == 830) {
        Main();
	}
        else if(PaymentOperatorID != 555) {
            Pages.ShowListOperatorByGroup(Pages.currentGroupID);

            //определяем есть ли это подгруппа, нужна кнопка назад на уровень вверх
            if(groupUp) {
                document.getElementById('wupbutton').style.display = '';
            }
            currentNumPage = this.page_num;
            ScrollInGroup.scrollListDown(currentNumPage);
        } else {
            //добавить кнопку при нажатии назад возвращает назад
            Pages.Show('wsearch_operator');
        }
        return;
    } else {
        this._setValue();
        this.currentStep--;
        this._init();
    }
};
PaymentStart.prototype.action = function() { 
    var ext = this; 
    //Если мы в значении степа

    if(GetCurrentPage() == Pages.Link.STEPS || GetCurrentPage() == 5 || GetCurrentPage() == 0) {
        //Увеличиваем значение степа на 1
        this.currentStep++;

        //Если шагов больше нет 
        if(this.steps.length == 1 || this.currentStep == (this.steps.length)) { 
            //проверка связи
            SetCurrentPage(Pages.Link.CONFIRM);
            //Проверяем если фискальный сервер включен
            var fiscalServer = IsFiscalOptionOn();          
            if(fiscalServer && this.NoMoneySend == false) { 
                SaveLog('Требуется проверка фискального сервера');
                var statusFiscal = getResponseFiscal();
                var fiscalTimer = new IndependentTimer(60, 'Подождите, идет проверка возможности совершения платежа!');
                fiscalTimer.timer.directionTimeToBig();
                //фискальный сервер доступен
                if(statusFiscal) {
                    this.confirm();
                } else {
                    //Запрашиваем статус фискального сервера
                    fiscalTimer.timer.finalize = function() {
                        //убираем всплывающее окно и останавливаем таймер
                        fiscalTimer.stop();
                        //запрашиваем статус фискального сервера
                        var statusFiscalServer = getResponseFiscal();
                        //все таки разрешено
                        if(statusFiscalServer) {
                            ext.confirm();
                        } else {
                            ext.currentStep--;
                            SetCurrentPage(Pages.Link.STEPS);
                            var text = "Извините, в данный момент терминал не может совершить ваш платеж!";
                            OnlineDiv('loadingdivmsgyesnomenu', 1, text);
                            OnlineDiv('msgnobutton', 0);
                            OnlineDiv('msgyesbutton', 0);
                            OnlineDiv('msgmenubutton', 1);
                        }
                    };
                    //выполняется каждую секунду
                    fiscalTimer.timer.execute = function() {
                        //Если сервер ответил положительно
                        if(getResponseFiscal()) {
                            fiscalTimer.stop();
                        }
                    };
                    //определяем действия для проверки статуса возможности проплаты платежа
                    fiscalTimer.start();
                }
            } else { 
                this.confirm(); 
            }

        } else {
            this._init();
        }
    }
};

PaymentStart.prototype.confirm = function() {

    if(GetCurrentPage() == Pages.Link.CONFIRM) {

        SaveLog("Пользователь перешел на страницу confirm");
        StartTimeOut();
        //sounder.src = "../sound/confirm_number.wav";
        //Страница подтверждения
        var enteredDataDiv = document.getElementById('enteredData');
        var minimumSumDiv = document.getElementById('minimum_sum');
        var carttotal = document.getElementById('carttotal');
	    carttotal.innerHTML = "";
        //Минимальная сумма
        var RetMinPayment = RetMinPaymentSize();
        var minSum = DeterminateMinSum(this.dataGateQuery.MinSum, RetMinPayment);
	var countcart=0;
        var ext = this;
        var fixSumValue = cart.summ;
        var getmethods = GetPaymentMethod();
//fixSumValue = 1;
	SetFixedSumTxt(fixSumValue);
        AddAdditionalPaymentParameters("Acct", fixSumValue);
        AddAdditionalPaymentParameters("FixedSum", fixSumValue);

/*
        var enteredData = '';
        Data = new Date();
        Day = Data.getDate();
        var i=0;
	  for (var key in cart.cartvalue) {
        Hour = parseFloat(cart.cartvalue[key].time.substring(0,2));
        Minutes = parseFloat(cart.cartvalue[key].time.substring(2,4));

        if (Hour < 10) { Hour = "0"+Hour; }
        if (Minutes < 10) { Minutes = "0"+Minutes; }

            enteredData += "<div id='masterphoto'><img src='"+cart.cartvalue[key].photo+"' width=183 height=182</div>";
            enteredData += "<div id='masterinfo'>ВАШ МАСТЕР<br><div id='mastername'>"+cart.cartvalue[key].FIO+"</div></div>";

            enteredData += "<div id='timeinfo'>";
            enteredData += "<div id='timeinfoday'>"+Day+"</div>";
            enteredData += "<div id='timeinfoweekday'>"+getWeekDay(Data)+"</div>";
            enteredData += "<div id='timeinfohour'>"+Hour+":"+Minutes+"</div>";
            enteredData += "<div id='timeinfotext'>БЛИЖАЙШЕЕ ВРЕМЯ</div>";
            enteredData += "</div>";
            enteredData += "<table width='900' cellspacing=20 cellpadding=20 align=center border=0 id='confirmcart'>";


	  var providerprice = cart.cartvalue[key].stepvalue.replace(' ','');
	  enteredData += "<tr><td height='60' align='left'><span class='rocherinfocart'>" + cart.cartvalue[key].stepname.toUpperCase() + "</span></td>\n";
          enteredData += '<td width="200"><span class="rocherprice">' + cart.cartvalue[key].stepvalue + ' <span class="rubconfirm">Р</span></span></td>\n';
	  enteredData += '<td width="50"><div class="provmenudelete" onclick="DelProvFromCart(' + cart.cartvalue[key].providerid + ');" style="float: left;"><img src="img/prov_delete.png"></div></td></tr>\n';
	  if (i < cart.col){
	  enteredData += '<tr><td colspan=3 style="height:1px; background-color: #b5b5b5;"></td></tr>';
	                            }
countcart++;
        	}

        enteredData += '<tr style="height:76px;"><td colspan="3" align="center"><br><img id="wbuttons_backcart" src="img/btn_back_cart.png"/></td></tr>';
        enteredData += '<tr><td colspan=3 style="height:1px; background-color: #000000;"></td></tr>';

            enteredData += "</table>";

        enteredDataDiv.innerHTML = enteredData;
        cart.summ = parseFloat(cart.summ);
        carttotal.innerHTML = cart.summ+" <span class=\"rubconfirm\">Р</span>";
        carttotal.style.fontSize = "80px";
        carttotal.style.fontWeight = "bold";
        carttotal.style.position = "absolute";
        carttotal.style.top = "450px";
        carttotal.style.left = "200px";

        var button_approve = document.getElementById('wbuttons_approve');
        button_approve.style.position = "absolute";
        button_approve.style.top = "450px";
        button_approve.style.left = "730px";

        //Устанавливаем обработчик на клавишу вперед
        var next = document.getElementById('wbuttons_approve');
        WEvents.remove(next, 'click');
        next.onclick = function() {
                ext.currentStep=1;
                ext._init();
        };
        Pages.Steps.confirm();
*/

		Pages.Hide();
		Pages.Steps.method();
        if (PaymentOperatorID != 1374 && PaymentOperatorID != 1376) {
        //Устанавливаем обработчик на клавишу назад
        var back = document.getElementById('wbuttons_backcart');
        WEvents.remove(back, 'click');
        back.onclick = function() {
            //EffectClick('wbuttons_main_back');
            //Обновляем таймер я еще здесь
            StartTimeOut();
	    Pages.ShowListOperatorByGroup(214);
        };

SaveLog("Корзина из confirm: "+JSON.stringify(cart));

	  for (var key in cart.cartvalue) {
AddAdditionalPaymentParameters("MasterID", cart.cartvalue[key].masterid);
AddAdditionalPaymentParameters("services", cart.cartvalue[key].providerid);
AddAdditionalPaymentParameters("Time", cart.cartvalue[key].time);
                                          }
             }
//-1 = no devices
//2 = card only
//1 = cup
//3 = coins
//4 = all
///////////////////
            ////если выбрана оплата наличными
            var nextcash = document.getElementById('wbuttons_select_cash');
            WEvents.remove(nextcash, 'click');
            nextcash.onclick = function () {
SaveLog("Текущий гейт:"+PaymentOperatorID);
if (PaymentOperatorID == 1373) {
				  for (var key in cart.cartvalue) {
                                                                SaveLog("Меняем гейт в корзине с "+cart.cartvalue[key].gateid+" на 1375");
								cart.cartvalue[key].gateid = 1375;                                                                
                                                                //cart.cartvalue[key].providerid = 1375;
			                                          }
                                window.external.ChangeGateId(1375);
                                SaveLog("Меняем гейт на: 1375");
                     		} //else {
			//		  for (var key in cart.cartvalue) {
		//							cart.cartvalue[key].gateid = PaymentOperatorID;
		//		                                          }
                //               	}

else if (PaymentOperatorID == 1374) {
				  for (var key in cart.cartvalue) {
                                                                SaveLog("Меняем гейт в корзине с "+cart.cartvalue[key].gateid+" на 1376");
								cart.cartvalue[key].gateid = 1376;
                                                                cart.cartvalue[key].providerid = 1376;
			                                          }
                                window.external.ChangeGateId(1376);
                                SaveLog("Меняем гейт на: 1376");
                     		} else {
					  for (var key in cart.cartvalue) {
									cart.cartvalue[key].gateid = PaymentOperatorID;
				                                          }
                               	}


                SetCurrentPage(Pages.Link.PAYMENT);
                //Показываем блок
                Pages.Steps.payment();
                SaveLog("Пользователь перешел на страницу payment");
                toogleCommision = new WCommission();

                //Обновляем таймер я еще здесь
                StartTimeOut();

                //Отображение блокированных купюр
                if (window.external.RetBlockedBills.length > 1) {
                    var blockedNominale = document.getElementById('nominalWarningPayment');
                    var NominalArray = window.external.RetBlockedBills.split(";");
                    var warning_text = '<div class="blocked_bills">Мы не принимаем купюры НОМИНАЛОМ <br><span class="">';
                    for (var i = 0; i < NominalArray.length; i++) {
                        if (NominalArray[i]) {
                            warning_text += NominalArray[i] + ', ';
                        }
                    }
                    warning_text = warning_text.substr(0, warning_text.length - 2);
                    warning_text += " руб.</span></div>";
                    blockedNominale.innerHTML = warning_text;
                }
                //шаг назад на последний степ
                var back = document.getElementById('wbuttons_main_back');
                WEvents.remove(back, 'click');
                back.onclick = function () {
                    STOP_BILL();
                    SaveLog("stop bill on back button from payment pages");
                    var delayTimerBack = new IndependentTimer(5, "Возврат к платежу...");
                    delayTimerBack.timer.finalize = function () {
                        delayTimerBack.stop();
                        SaveLog("Повторный стоп купюроприемника 1");
                        STOP_BILL();  //второй раз стоп чтобы нв200 успевал выключиться
                        if (!ext.CheckReacctOnPayment(minSum)) {
                            Pages.ShowListOperatorByGroup(214);
                        }
                    };
                    delayTimerBack.timer.execute = function () {
                        StartTimeOut();
                    };
                    delayTimerBack.start();
                };

                var pay = document.getElementById('wbuttons_main_pay');
                WEvents.remove(pay, 'click');
                pay.onclick = function () {
                    //EffectClick('wbuttons_main_pay');
                    ext.CheckReacctOnPayment(minSum);
                };
                //Запускаем купюроприемник
                SaveLog("start bill on payment page 1");
                START_BILL();
//alert('start bill1');
                //Инициализируем блок показа коммисии
                //toogleCommision.clear();
            }

            ///если выбрана оплата картой
            var nextcard = document.getElementById('wbuttons_select_card');
            WEvents.remove(nextcard, 'click');
            nextcard.onclick = function () {

if (PaymentOperatorID == 1373) {
				  for (var key in cart.cartvalue) {
                                                                SaveLog("Меняем гейт в корзине с "+cart.cartvalue[key].gateid+" на 1373");
								cart.cartvalue[key].gateid = 1373;
			                                          }
                     		} else {
					  for (var key in cart.cartvalue) {
									cart.cartvalue[key].gateid = PaymentOperatorID;
				                                          }
                               	}
if (PaymentOperatorID == 1374) {
				  for (var key in cart.cartvalue) {
                                                                SaveLog("Меняем гейт в корзине с "+cart.cartvalue[key].gateid+" на 1374");
								cart.cartvalue[key].gateid = 1374;
			                                          }
                     		} else {
					  for (var key in cart.cartvalue) {
									cart.cartvalue[key].gateid = PaymentOperatorID;
				                                          }
                               	}
                START_PINPAD();
                BasketClose();
                GenerateDPQ(this);
                var cardsumm = cart.summ;
                //clearCart();
//////////////////////////////////////////////////////////////////////////////////////////////////////////
                SetCurrentPage(Pages.Link.CARD);
                //Показываем блок
                Pages.Steps.card();
                SaveLog("Пользователь перешел на страницу paycard");
                toogleCommision = new WCommission();
                StopTimeOutMain();
                num = 1;
                for (var key in cart.cartvalue) {
                    var text = cart.cartvalue[key].stepname + ", стоимость: " + cart.cartvalue[key].stepvalue + " руб.";
                    AddCheckParameter(num, text);
                    num++;
                }

                var pinpadtime = PINPAD_TIMEOUT() / 1000;

                getPinpadTimer.start(pinpadtime);
                //указываем что делать по истечению таймера
                getPinpadTimer.pinpadtimer.finalize = function () {
                    //убираем всплывающее окно
                    getPinpadTimer.stop();
                    CardFail();
                }

                Pages.HideOne('wpayment');
                //Формируем блок информации о возможных суммах
                var infoBill = document.getElementById('wpayment_info_bill');
                var money_needed = document.getElementById('wpayment_money_needed');

                money_needed.style.display = '';
                infoBill.style.display = '';
                var moneyacctpin = document.getElementById('moneyacctpin');

                moneyacctpin.innerHTML = cardsumm + " руб.";
                Pages.ShowOne('moneyacctpin');

                //Pages.HideOne('opinfocomisdiv');

                var pmin = document.getElementById('trminPayment');
                pmin.innerHTML = '';
                Pages.Steps.button.back.hide();
//////////////////////////////////////////////////////////////////////////////////////////////////////////
            }

///////////////////

    } else {
        this._init();
    }



};
/***
 * На странице пеймента проверяет вставлена ли купюра
 * Защита от перехода из страницы пеймента до того как
 * купюроприемник уложил купюру
 * @constructor
 */
PaymentStart.prototype.CheckReacctOnPayment = function(minSum) {
    var money_inside = false;
    addchecktextPay();
    var mainMenu = getElemByID('wbuttons_mainmenu');
    mainMenu.onclick = function() {
        Main();
    };
    if(RetACCT() > 0) {
        checkInSumm(minSum, GetEnrollmentPay(), GetFixedSum());
        money_inside = true;
    }
    return money_inside;
};

PaymentStart.prototype.AddToCart = function() {

    var step = this.steps[this.currentStep];
    var typeStep = step.InputType.toUpperCase();


        if (typeStep == "KEYBOARDENG") {
	var fixsumm = step.Name;
	step.value=fixsumm;
	                                                } else {
                                                                //var fixsumm = step.value; 
                                                               	//step.value=fixsumm;
	                                                       }

//        if (cart.col == 0) {
			   NewBasketSession();
                           NewBasketClient();
//                           }

	if (cart.cartvalue[step.GateID]) { 
        cart.cartvalue[step.GateID].providercol=cart.cartvalue[step.GateID].providercol+1;
	cart.cartvalue[step.GateID].providersumm = parseFloat(cart.cartvalue[step.GateID].providersumm) + parseFloat(fixsumm);
	                                 } else {

	var cartDatatmp = '{"providerid":'+step.GateID+',"stepname":"'+step.Info+'", "stepvalue":"'+step.value+'", "providercol":1, "providersumm":"'+step.value+'"}';
//alert('cartDatatmp'+cartDatatmp);
	var carttmp = JSON.parse(cartDatatmp);
	cart.cartvalue[step.GateID]=carttmp;
	                                        }
//alert('cart1:'+cart.summ);
	cart.summ = parseFloat(cart.summ) + parseFloat(step.value);
//alert('cart2:'+cart.summ);
//alert('stepvalue:'+step.stepvalue);

	if (step.GateID != 830) {
	SetFixedSumTxt(cart.summ);
	                        }
        //cart.summ = priceSet(cart.summ);
	cart.summ = cart.summ.toFixed(2);
	cart.col = parseFloat(cart.col) + 1;
	
	var cartcol = document.getElementById('cartcol');
	var cartsumm = document.getElementById('cartsumm');
	cartcol.innerHTML = cart.col;
	cartsumm.innerHTML = cart.summ;

//	var provcolinmenu = document.getElementById('provcolinmenu'+step.GateID);
//	provcolinmenu.innerHTML = cart.cartvalue[step.GateID].providercol;
//        provcolinmenu.style.display = '';

};
/**
 * Устанавливает значение в степе
 * @param payment ссылка на обьект payment
 * @private
 */
PaymentStart.prototype._setValue = function() {
    //тип степа
    var currentStepID = this.currentStep;
    var type = this.steps[currentStepID].InputType.toUpperCase();
    if(type == "LISTONLINE") {
        this.fixSumValue = this.uiElement.getFixedSum();
        SetFixedSumTxt(this.fixSumValue);
    } else if(type == "FIXEDSUMM") {
        this.fixSumValue = this.uiElement.ManageMask.getData();
        SetFixedSumTxt(this.fixSumValue);
    }
    this.steps[currentStepID].value = this.uiElement.ManageMask.getData();
    this.steps[currentStepID].maskValue = this.uiElement.ManageMask.getValueParseMask();
    //отправляем
    SetStepVal(this.steps[currentStepID].OrderID, this.steps[currentStepID].value);
    StepList.setValue(this.steps[currentStepID].OrderID, this.steps[currentStepID].value);

};

/**
 * Получаем значение шага по орderid
 * @param payment
 * @param orderID
 * @returns {*}
 * @private
 */
PaymentStart.prototype._getValue = function(payment, orderID) {
    return payment.steps[orderID].value;
};
/**
 * Получаем лого по оператору
 */
PaymentStart.prototype.setLogo = function() {
    var logoDiv = document.getElementById("opinfologo");
    var path = RetLogotipImg();
    //расчитываем размеры лого по оператору
    var image = createImgByfilter(220, 110, path);
    logoDiv.innerHTML = image;
};

function ClickOperator(id) {
    SaveLog("ClickOperator: " + id);
    //try {

    // WNewEffects.click.button("op" + id);
    try {
        this.operator = new PaymentStart(id);
    } catch(e) {
        SaveExceptionToLog(e, "steps.js", "ClickOperator_PaymentStart");
    }

    //Устанавливаем первые шаги степами
    SetCurrentPage(Pages.Link.STEPS);

    //Если в профиле включено использования фискального сервера, запросить его доступность
    setTimeout(function() {
        if(IsFiscalOptionOn()) {
            getStatusFiscalServer();
        }
    }, 3000);

    //Возвращает 0 - если печать доступна и -1 если печать чека не возможно
    try {
        var statusPrintCheck = StartPayCycleJavaInt(id);
    } catch(e) {
        SaveExceptionToLog(e, "steps.js", "StartPayCycleJavaInt");
    }
    statusPrintCheck = 1;
    //Если печать чека возможна
    if(statusPrintCheck != -1) {
        try {
            this.operator._init();
        } catch(e) {
            SaveExceptionToLog(e, "steps.js", "statusPrintCheck != -1");
        }
    } else {
        var ext = this;
        //Если печать чека не возможна, но все равно хочется продолжить
        Pages.Steps.button.next.get().onclick = function() {
            try {
                ext.operator._init();
            } catch(e) {
                SaveExceptionToLog(e, "steps.js", "Pages.Steps.button.next.get().onclick");
            }
            SaveLog("Печать чека не возможна, пользователь согласился. GateID " + id);
        };
        Pages.Steps.button.back.get().onclick = function() {
            try {
                Pages.ShowListOperatorByGroup(Pages.currentGroupID);
            } catch(e) {
                SaveExceptionToLog(e, "steps.js", "Pages.Steps.button.back.get().onclick");
            }
        };
        try {
            Pages.Steps.check.printError()
        } catch(e) {
            SaveExceptionToLog(e, "steps.js", "Pages.Steps.check.printError");
        }
    }
}


/**
 * Определение минимальной суммы
 */
function DeterminateMinSum(sum1, sum2) {
    sum1 = Number.toFloat(sum1);
    sum2 = Number.toFloat(sum2);
    if(sum1 < sum2)
        sum1 = sum2;
    return sum1;
}

/**
 *
 * @param minSum минимальная сумма платежа
 * @param enrollSum сумма к зачислению
 * @param fixedSum значение фикседсам
 */
function checkInSumm(minSum, enrollSum, fixedSum) {
    try {
        minSum = Number.toFloat(minSum);
    } catch(e) {
        SaveExceptionToLog(e, "steps.js, minsum: " + minSum, 647);
        pay_fnc();
    }

    try {
        enrollSum = Number.toFloat(enrollSum);
    } catch(e) {
        SaveExceptionToLog(e, "steps.js, enrollSum: " + enrollSum, 653);
        pay_fnc();
    }

    try {
        fixedSum = Number.toFloat(fixedSum);
    } catch(e) {
        SaveExceptionToLog(e, "steps.js, fixedSum: " + fixedSum, 659);
        pay_fnc();
    }


    //Минимальный платеж устанавливается в профиле
    try {

        document.getElementById('msgyesbutton').onclick = function() {
            SaveLog("Введенной суммы недостаточно для оплаты. Пользователь нажал 'Далее' и согласился отправить платеж");
            OnlineDiv('loadingdivmsgyesnomenu', 0);
            pay_fnc();
        };

        //юнистрим суммав ставленных купюр не соотвествует фикседсум
        if(EstimateComission().length > 0) {
            if(fixedSum != enrollSum) {
                var text2 = "Введенная не равна выбранной к переводу.<br>Комиссия и сумма к получению будут пересчитаны согласно тарифам Юнистрим";
                SaveLog('checkInSumm() stop bill');
                STOP_BILL(); 
                OnlineDiv('loadingdivmsgyesnomenu', 1, text2);
                OnlineDiv('msgnobutton', 1);
                OnlineDiv('msgyesbutton', 1);
                OnlineDiv('msgmenubutton', 0);
            } else {
                pay_fnc();
            }
        } else if(minSum > enrollSum || fixedSum > enrollSum) {
            var text = "Введенной суммы недостаточно для оплаты.<br>" +
                "Нажмите 'Далее' для завершения или 'Назад' для добавления купюр!";
            SaveLog('checkInSumm() stop bill');
            STOP_BILL();
            OnlineDiv('loadingdivmsgyesnomenu', 1, text);
            OnlineDiv('msgnobutton', 1);
            OnlineDiv('msgyesbutton', 1);
            OnlineDiv('msgmenubutton', 0);
        } else {
            pay_fnc();
        }
    } catch(e) {
        SaveExceptionToLog(e, "steps.js, minsum: " + minSum + ", enrollsum: " + enrollSum + ", fixedSum: " + fixedSum, 669);
        pay_fnc();
    }
}



var cartData = '{"col":0,"summ":0,"cartvalue":{}}';
// var cart = JSON.parse(cartData);

var servicecartData = '{"col":0,"summ":0,"cartvalue":{}}';
var servicecart = JSON.parse(servicecartData);

var serviceGroupData = '{"group":{}}';
var serviceGroup = JSON.parse(serviceGroupData);
/**
* Переход в корзину
**/
function opencart(ext) {
ext.action(); 
}

function clearCart() {
//	cart.cartvalue={};
	  for (var key in cart.cartvalue) {
try {
var provcolinmenu = document.getElementById('provcolinmenu'+cart.cartvalue[key].providerid);
provcolinmenu.innerHTML = '0';
    } catch(e) {
              }
delete cart.cartvalue[key]; 
        }
cart.cartvalue={};

var servicecartData = '{"col":0,"summ":0,"cartvalue":{}}';
servicecart = JSON.parse(servicecartData);
var serviceGroupData = '{"group":{}}';
serviceGroup = JSON.parse(serviceGroupData);


	cart.summ = 0.00;
	cart.col = 0;
        var enteredDataDiv = document.getElementById('enteredData');
        enteredDataDiv.innerHTML = '';
	NewBasketSession();
	NewBasketClient();

}

function AddProvToCart(num, name, price,masterid, time, fio,photo) { 

	name = name.replace(/ \(ф\)/g, "");
	if (cart.cartvalue[num]) {
        DelProvFromCart(num);
	                                 } else {

	var cartDatatmp = '{"providerid":"'+num+'", "masterid":"'+masterid+'", "stepname":"'+name+'", "stepvalue":"'+price+'", "providercol":1, "providersumm":"'+price+'", "time":"'+time+'", "FIO":"'+fio+'", "photo":"'+photo+'"}';
	var carttmp = JSON.parse(cartDatatmp);
	cart.cartvalue[num]=carttmp;
	cart.col = parseFloat(cart.col) + 1;
	cart.summ = parseFloat(cart.summ) + parseFloat(price);
	cart.summ = cart.summ.toFixed(2);

	                                        }
	SetFixedSumTxt(cart.summ);

if (cart.col > 0) {
    var cartpaybutton = document.getElementById('wbuttons_approve');
    cartpaybutton.style.visibility = 'visible';
    cartpaybutton.style.display = 'block';
                  }

}


function RemProvFromCart(num) {

        if (cart.cartvalue[num]) { 
	cart.summ = parseFloat(cart.summ) - parseFloat(cart.cartvalue[num].stepvalue);
	if (cart.summ > 99999) { document.getElementById('cartrubles').innerHTML = ''; } else { document.getElementById('cartrubles').innerHTML = 'руб.'; } 
	SetFixedSumTxt(cart.summ);
	cart.summ = cart.summ.toFixed(2);
	var provcolinmenu = document.getElementById('provcolinmenu'+num);
        if (cart.cartvalue[num].providercol > 1) {
	cart.cartvalue[num].providercol=cart.cartvalue[num].providercol-1; 
	cart.cartvalue[num].providersumm = parseFloat(cart.cartvalue[num].providersumm) - parseFloat(cart.cartvalue[num].stepvalue);
	provcolinmenu.innerHTML = cart.cartvalue[num].providercol;
	                                                } else {
                                                               delete cart.cartvalue[num]; 
							       provcolinmenu.innerHTML = '0';
                                                               provcolinmenu.style.display = 'none';
	                                                       }
	cart.col = parseFloat(cart.col) - 1;
	var cartcol = document.getElementById('cartcol');
	var cartsumm = document.getElementById('cartsumm');
        var carttotal = document.getElementById('carttotal');
	cartcol.innerHTML = cart.col;
	cartsumm.innerHTML = cart.summ;

                                           }
//if (cart.col <= 0) { Pages.Steps.button.next.hide(); }
}


function GenerateDPQ(ext){
//alert(GenerateBasketSession());
};

function DelProvFromCart(providerid) {

        if (cart.cartvalue[providerid]) { 

	//if (cart.summ > 99999) { document.getElementById('cartrubles').innerHTML = ''; } else { document.getElementById('cartrubles').innerHTML = 'руб.'; }
	cart.summ = parseFloat(cart.summ) - cart.cartvalue[providerid].providersumm;
	cart.col = parseFloat(cart.col) - cart.cartvalue[providerid].providercol;
	delete cart.cartvalue[providerid]; 
	SetFixedSumTxt(cart.summ);
	AddAdditionalPaymentParameters("Acct", cart.summ);
	cart.summ = cart.summ.toFixed(2);
try {
	var cell = document.getElementById('cell'+providerid);
        cell.style.background = "url('img/prov_bgr.png') no-repeat";
	var cellname = document.getElementById('cellname'+providerid);
        cellname.style.color = "#000000";
    } catch(e){}

            Data = new Date();
            Hour = Data.getHours();

            Minutes = Data.getMinutes();
            Day = Data.getDate();
            if (Hour < 10) { Hour = "0"+Hour; }
            if (Minutes < 10) { Minutes = "0"+Minutes; }
            var enteredData = '';
            enteredData += "<div id='masterphoto'><img src='img/master1.png'</div>";
            enteredData += "<div id='masterinfo'>ВАШ МАСТЕР<br><div id='mastername'>АНАСТАСИЯ</div></div>";
            enteredData += "<div id='timeinfo'>";
            enteredData += "<div id='timeinfoday'>"+Day+"</div>";
            enteredData += "<div id='timeinfoweekday'>"+getWeekDay(Data)+"</div>";
            enteredData += "<div id='timeinfohour'>"+Hour+":"+Minutes+"</div>";
            enteredData += "<div id='timeinfotext'>БЛИЖАЙШЕЕ ВРЕМЯ</div>";
            enteredData += "</div>";
            enteredData += "<table width='950' cellspacing=20 cellpadding=20 align=center border=0 id='confirmcart'>";
        var i=0;
	  for (var key in cart.cartvalue) {
	  i=i+1;
	  var providerprice = cart.cartvalue[key].stepvalue.replace(' ','');
	  enteredData += "<tr><td align='left'><span class='rocherinfocart'>" + cart.cartvalue[key].stepname.toUpperCase() + "</span></td>\n";
          enteredData += '<td width="200"><span class="rocherprice">' + cart.cartvalue[key].stepvalue + ' <span class="rubconfirm">Р</span></span></td>\n';
	  enteredData += '<td width="50"><div class="provmenudelete" onclick="DelProvFromCart(' + cart.cartvalue[key].providerid + ');" style="float: left;"><img src="img/prov_delete.png"></div></td></tr>\n';
	  if (i < cart.col){
	  enteredData += '<tr><td colspan=3 style="height:1px; background-color: #b5b5b5;"></td></tr>';
	                            }

        	}
            enteredData += '<tr style="height:76px;"><td colspan="3" align="center"><br><img id="wbuttons_backcart" src="img/btn_back_cart.png"/></td></tr>';
            enteredData += '<tr><td colspan=3 style="height:1px; background-color: #000000;"></td></tr>';

            enteredData += "</table>";
//правая
//            enteredData += "        <div id='scrollcart'>";
//            enteredData +=        "             <img id='ofertadown' onclick = \"WNewEffects.click.button('ofertaup'); ScrollOferta2.onclick_up();\" src=\"img/req_down.png\">";
//            enteredData +=         "</div>";
////левая
//            enteredData += "        <div id='scrollcartleft'>";
//            enteredData +=    "                 <img id='ofertaup' onclick = \"WNewEffects.click.button('ofertadown');ScrollOferta2.onclick_down();\" src=\"img/req_up.png\">";
//            enteredData +=         "</div>";

	var enteredDataDiv = document.getElementById('enteredData');
        enteredDataDiv.innerHTML = enteredData;
//       ScrollOferta2 = new InOferta('confirmcart2');
//       ScrollOferta2.init();

//	var cartcol = document.getElementById('cartcol');
	var carttotal = document.getElementById('carttotal');
	//cartcol.innerHTML = cart.col;
            cart.summ = parseFloat(cart.summ);
            carttotal.innerHTML = cart.summ+" <span class=\"rubconfirm\">Р</span>";


if (cart.col <= 0) {
                  var cartpaybutton = document.getElementById('wbuttons_approve');
                  cartpaybutton.style.visibility = 'hidden';
                  cartpaybutton.style.display = 'none';
		  //Pages.HideOne('selectcard');
                  Pages.Steps.button.menu.show();
                  }

            //Устанавливаем обработчик на клавишу назад
            var back = document.getElementById('wbuttons_backcart');
            WEvents.remove(back, 'click');
            back.onclick = function() {
                //EffectClick('wbuttons_main_back');
//            SetCurrentPage(Pages.Link.STEPS);
                //Обновляем таймер я еще здесь
                StartTimeOut();
                Pages.ShowListOperatorByGroup(214);
                //ext.currentStep--;
                //ext._init();
            };


        }
}
	    var pinpadtimeout = PINPAD_TIMEOUT() / 1000;
            var getPinpadTimer = new PinPadTimer(pinpadtimeout);

function getWeekDay(date) {
    date = date || new Date();
    var days = ['ВОС', 'ПОН', 'ВТ', 'СР', 'ЧТВ', 'ПЯТ', 'СУБ'];
    var day = date.getDay();

    return days[day];
}


function AddServiceId(id, price,groupid, name, minutes) {
        if (servicecart.cartvalue[id]) {
	servicecart.summ = parseFloat(servicecart.summ) - price;
	servicecart.col = parseFloat(servicecart.col) - 1;
	delete servicecart.cartvalue[id]; 
	servicecart.summ = servicecart.summ.toFixed(2);
    var servicebutton = document.getElementById('service'+id);
    servicebutton.style.background="url('img/prov_bgr_menu.png') no-repeat";
    var servicename = document.getElementById('servicename'+id);
    servicename.style.color="#FFFFFF";
    var servicename = document.getElementById('servicetime'+id);
    servicename.style.color="#FFFFFF";
    var servicename = document.getElementById('serviceprice'+id);
    servicename.style.color="#FFFFFF";

    serviceGroup.group[groupid].col=serviceGroup.group[groupid].col-1;

                             } else {
	var cartDatatmp = '{"serviceid":"'+id+'", "price":"'+price+'", "name":"'+name+'", "minutes":"'+minutes+'"}';
	var carttmp = JSON.parse(cartDatatmp);
	servicecart.cartvalue[id]=carttmp;
	servicecart.col = parseFloat(servicecart.col) + 1;
	servicecart.summ = parseFloat(servicecart.summ) + parseFloat(price);
	servicecart.summ = servicecart.summ.toFixed(2);

    var servicebutton = document.getElementById('service'+id);
    servicebutton.style.background="url('img/prov_bgr_menu_p.png') no-repeat";
    var servicename = document.getElementById('servicename'+id);
    servicename.style.color="#000000";
    var servicename = document.getElementById('servicetime'+id);
    servicename.style.color="#000000";
    var servicename = document.getElementById('serviceprice'+id);
    servicename.style.color="#000000";

	var cartDatatmp = '{"col":1}';
	var carttmp = JSON.parse(cartDatatmp);
	serviceGroup.group[groupid]=carttmp;

//alert(JSON.stringify(serviceGroup));
//alert(JSON.stringify(servicecart));
    Pages.Steps.button.next.show();
    var back = document.getElementById('wbuttons_main_next');
    WEvents.remove(back, 'click');
    back.onclick = function() {
            RetGroup(999); 
			      };

					}
}
function changeServiceGroupName(name,id) {
//if (serviceGroup.group[id]) {
//                            }
serviceGroup.name = name;
serviceGroup.id = id;
}