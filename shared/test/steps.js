/**
 * Если стоит 1 - формируется полный тестовый список степов
 * @type {number}
 */
var DEBUGMODE = 0;
var DEBUG_CONSOLE = 0;

function addData(text) {
    if (DEBUG_CONSOLE  == 1) {
        var error_panel = document.getElementById("error_js");
        var value = error_panel.innerHTML;
        error_panel.innerHTML = value + text + "<br>";
    }
}

function TestStepFactory() {
    var dgq = new TestDataGateQuery();

    dgq.Steps.push(new StepTypeNumber());
    dgq.Steps.push(new StepTypeNumber8());
    dgq.Steps.push(new StepTypeFioBox());
    dgq.Steps.push(new StepTypeKeyboard());
    dgq.Steps.push(new StepTypeKeyboardEng());
    dgq.Steps.push(new StepTypeFixedSum());
    dgq.Steps.push(new StepTypeList);
    dgq.Steps.push(new StepTypeCalendar());
    dgq.Steps.push(new StepTypeCalendarMonth());
    //dgq.Steps.push(new StepTypeListOnline());

    for (var i = 0; i < dgq.Steps.length; i++) {
        dgq.Steps[i].OrderID = i;
    }

    return dgq;
}

function TestDataGateQuery() {
    var dgq = [];
    dgq.Name = "Билайн без комиссии";
    dgq.GateID = 178;
    dgq.GroupID = 5;
    dgq.SortNumber = 0;
    dgq.FastPanelId = 0;
    dgq.ExecutorId = 212;
    dgq.Information = 'ОАО "Вымпелком", (495) 755-00-55';
    dgq.MaxSum = 15000.0000;
    dgq.MinSum = 10.0000;
    dgq.ButtonImg = "mob_beeline.gif";
    dgq.LogotipImg = "mob_beeline.gif";
    dgq.MaxComission = 0.0000;
    dgq.MinComission = 10.0000;
    dgq.Steps = [];

    return dgq;
}


function StepTypeNumber() {
    var oneStep = new StepOne();
    oneStep.StepID = 179;
    oneStep.GateID = 178;
    oneStep.Name = "Тип степа number (Номер счета)";
    oneStep.InputType = "NUMBER";
    oneStep.InputMask = "####-####-####-####-#############-#############################";
    oneStep.OrderID = 0;
    oneStep.Info = "Введите номер лицевого счёта 4game";
    oneStep.InfoFull = "Не более 16 цифр";
    oneStep.Prefix = 188;
    oneStep.isonlinequery = true;
    oneStep.isonlinequerynecessity = false;
    oneStep.StepStructure = null;
    oneStep.GateID = 138;
    oneStep.value = '';

    return oneStep;
}


function StepTypeNumber8() {
    var oneStep = new StepOne();
    oneStep.StepID = 402;
    oneStep.GateID = 178;
    oneStep.Name = "Тип степа number8 (Номер телефона)";
    oneStep.InputType = "NUMBER8";
    oneStep.InputMask = "(===)===-==-==";
    oneStep.OrderID = 1;
    oneStep.Info = "Введите номер телефона";
    oneStep.InfoFull = "Номер телефона вводится без '8'";
    oneStep.Prefix = '';
    oneStep.isonlinequery = false;
    oneStep.isonlinequerynecessity = false;
    oneStep.StepStructure = null;
    oneStep.GateID = 285;
    oneStep.value = '';

    return oneStep;
}

function StepTypeFioBox() {
    var oneStep = new StepOne();
    oneStep.StepID = 143;
    oneStep.GateID = 178;
    oneStep.Name = "Тип степа fiobox (фио)";
    oneStep.InputType = "fiobox";
    oneStep.InputMask = "########################################";
    oneStep.OrderID = 2;
    oneStep.Info = "Введите фамилию, имя, отчество";
    oneStep.InfoFull = "фамилия имя и отчество разделяются пробелами";
    oneStep.Prefix = '';
    oneStep.isonlinequery = false;
    oneStep.isonlinequerynecessity = false;
    oneStep.StepStructure = null;
    oneStep.GateID = 285;
    oneStep.value = '';

    return oneStep;
}

function StepTypeKeyboard() {
    var oneStep = new StepOne();
    oneStep.StepID = 145;
    oneStep.Name = "Тип степа keyboard (text rus)";
    oneStep.InputType = "KEYBOARD";
    oneStep.InputMask = "########################################";
    oneStep.OrderID = 3;
    oneStep.Info = "Введите адрес плательщика";
    oneStep.InfoFull = "Введите адрес плательщика infofull";
    oneStep.Prefix = '';
    oneStep.isonlinequery = false;
    oneStep.isonlinequerynecessity = false;
    oneStep.StepStructure = null;
    oneStep.GateID = 285;
    oneStep.value = '';

    return oneStep;
}

function StepTypeKeyboardEng() {
    var oneStep = new StepOne();
    oneStep.StepID = 145;
    oneStep.GateID = 178;
    oneStep.Name = "Тип степа keyboardeng (text eng)";
    oneStep.InputType = "KEYBOARDENG";
    oneStep.InputMask = "########################################";
    oneStep.OrderID = 0;
    oneStep.Info = "Вв4дите text on english";
    oneStep.InfoFull = "Вв4дите text on english infofull";
    oneStep.Prefix = '';
    oneStep.isonlinequery = false;
    oneStep.isonlinequerynecessity = false;
    oneStep.StepStructure = null;
    oneStep.GateID = 285;
    oneStep.value = '';

    return oneStep;
}

function StepTypeFixedSum() {
    var oneStep = new StepOne();
    oneStep.StepID = 479;
    oneStep.GateID = 178;
    oneStep.Name = "Тип степа fixedsum (Сумма оплаты)";
    oneStep.InputType = "FIXEDSUMM";
    oneStep.InputMask = "########";
    oneStep.OrderID = 5;
    oneStep.Info = "Введите сумму к оплате";
    oneStep.InfoFull = "Сдача будет зачислена на Ваш мобильный телефон";
    oneStep.Prefix = '';
    oneStep.isonlinequery = false;
    oneStep.isonlinequerynecessity = false;
    oneStep.StepStructure = null;
    oneStep.GateID = 285;
    oneStep.MinSum = 10.00;
    oneStep.MaxSum = 15000.00;
    oneStep.value = '';

    return oneStep;
}

function StepTypeList() {
    var oneStep = new StepOne();
    oneStep.StepID = 480;
    oneStep.GateID = 178;
    oneStep.Name = "Тип степа list (Оператор для сдачи)";
    oneStep.InputType = "LIST";
    oneStep.InputMask = "########";
    oneStep.OrderID = 6;
    oneStep.Info = "Оператор для сдачи";
    oneStep.InfoFull = "Выберите оператора Вашей мобильной связи";
    oneStep.Prefix = '';
    oneStep.isonlinequery = false;
    oneStep.isonlinequerynecessity = false;
    oneStep.StepStructure = null;
    oneStep.GateID = 285;
    oneStep.value = '';
    oneStep.value2 = '';

    return oneStep;
}

function StepTypeListOnline() {
    var oneStep = new StepOne();
    oneStep.StepID = 480;
    oneStep.GateID = 178;
    oneStep.Name = "Тип степа onlinelist (Оператор для сдачи)";
    oneStep.InputType = "LISTONLINE";
    oneStep.InputMask = "########";
    oneStep.OrderID = 7;
    oneStep.Info = "Здесь должно быть инфо";
    oneStep.InfoFull = "Здесь должно быть инфофул. Тестовое значение";
    oneStep.Prefix = '';
    oneStep.isonlinequery = false;
    oneStep.isonlinequerynecessity = false;
    oneStep.StepStructure = null;
    oneStep.GateID = 285;
    oneStep.value = '';
    oneStep.value2 = '';

    return oneStep;
}

function StepTypeCalendar() {
    var oneStep = new StepOne();
    oneStep.StepID = 466;
    oneStep.GateID = 178;
    oneStep.Name = "Тип степа calendar (дата рождения)";
    oneStep.InputType = "CALENDAR";
    oneStep.InputMask = "########";
    oneStep.OrderID = 8;
    oneStep.Info = "Введите дату рождения";
    oneStep.InfoFull = "Дотроньтесь до строки 'год' в календаре, для удобства выбора";
    oneStep.Prefix = '';
    oneStep.isonlinequery = false;
    oneStep.isonlinequerynecessity = false;
    oneStep.StepStructure = null;
    oneStep.GateID = 285;
    oneStep.value = '';

    return oneStep;
}

function StepTypeCalendarMonth() {
    var oneStep = new StepOne();
    oneStep.StepID = 86;
    oneStep.GateID = 178;
    oneStep.Name = "Тип степа calendar (месяц оплаты)";
    oneStep.InputType = "CALENDARMONTH";
    oneStep.InputMask = "##.##";
    oneStep.OrderID = 9;
    oneStep.Info = "Месяц оплаты";
    oneStep.InfoFull = "Выберите месяц оплаты";
    oneStep.Prefix = '';
    oneStep.isonlinequery = false;
    oneStep.isonlinequerynecessity = false;
    oneStep.StepStructure = null;
    oneStep.GateID = 285;
    oneStep.value = '';

    return oneStep;
}

