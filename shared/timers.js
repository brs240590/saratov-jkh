/**
 * Класс таймера
 * @param container
 * @constructor
 */
function Wtimers(container) {
    this.time = 0;
    this.wait = this.time;
    this.timeoutid = 0;
    //время отсчета 60 сек
    this.timeContainer = container;
    //в какую сторону делать отсчет
    this.direct = true;
};
/**
 * Устанавливает отсчет от меньшего к большему
 */
Wtimers.prototype.directionTimeToBig = function () {
    this.direct = false;
};
/**
 * Устанавливает отсчет от большего к меньшему (по умолчанию)
 */
Wtimers.prototype.directionTimeToSmall = function () {
    this.direct = true;
};
Wtimers.prototype._init = function (seconds) {
    this.time = seconds;
    this.wait = this.time;
    this.timeoutid = 0;
};
Wtimers.prototype.setTime = function (seconds) {
    this.wait = seconds;
};
Wtimers.prototype.start = function () {

    var seconds = this.wait;
    if (this.direct == false) {
        seconds = this.time - this.wait;
    }

    //отрисовываем время
    document.getElementById(this.timeContainer).innerHTML = seconds;
    //какое-то последовательное действие, запускается каждые 2 сек
    this.execute();
//уменьшаем общее время на одну секунду
//смотрим время не закончилось
    if (this.wait > 0) {
        this.wait--;
        //если нет то повторяем процедуру заново
        var ext = this;
        this.timeoutid = setTimeout(function () {
            ext.start();
        }, 1000);
        //если закончилось, то выводим сообщение на экран, и делаем кнопку запуска активной
    } else {
        var latelyTime = 0;
        if (this.direct == false) {
            latelyTime = this.time;
        }
        document.getElementById(this.timeContainer).innerHTML = latelyTime;
        this.stop();
        this.finalize();
    }
    return this;
};
Wtimers.prototype.stop = function () {
    this.wait = 0;
    clearTimeout(this.timeoutid);
    this.timeoutid = 0;
};
/**
 * Метод finalize всегда выполняется при остановке таймера
 * Даже после вызова вручную stop(). Поэтому нет надобности вызывать его вручную
 */
Wtimers.prototype.finalize = function () {

};
Wtimers.prototype.execute = function () {

};

var abstractTimer = new Wtimers('abstract container value');


function WtimersOnlineCheck(container) {
    this.time = 0;
    this.wait = this.time;
    this.timeoutid = 0;
    this.timeContainer = container;
}
WtimersOnlineCheck.prototype = inherit(abstractTimer);


var onlinedivtimer = new Wtimers('polew');
var onlinedivtimer_pay = new WtimersOnlineCheck('polew2');

/**
 * Показывает скрывает всплывающие окна
 * @param id
 * @param todo флаг показывать или скрывать елемент
 * @param msgtext текст сообщения
 * @constructor
 */
function OnlineDiv(id, todo, msgtext, timer) {
    msgtext = msgtext || '';
    timer = timer || 0;

    if (id == "loadingdivmsg" && todo == 1) {

        StopTimeOutMain();
        Pages.ShowOne("screentimeoutbg");
        Pages.ShowOne('loadingdivmsg');
        document.getElementById('cancbut').style.visibility = 'hidden';
        document.getElementById('wtext_load').innerHTML = msgtext;
        if (timer == 0)
            timer = 130;
        onlinedivtimer = new Wtimers('polew');
        onlinedivtimer.directionTimeToBig();
        onlinedivtimer._init(timer);
        onlinedivtimer.start();
        onlinedivtimer.finalize = function () {
            ShowPrintCheckPage();
        }

    }
    if (id == "loadingdivmsg" && todo == 0) {
        try {
            Pages.HideOne('loadingdivmsg', " OnlineDiv(loadingdivmsg, 0)");
            Pages.HideOne("screentimeoutbg", " OnlineDiv(loadingdivmsg, 0)");
            onlinedivtimer.stop();
        } catch (e) {
        }
    }
    if (id == "loadingdivmsgyesnomenu" && todo == 1) {
        StartTimeOut();
        Pages.ShowOne("screentimeoutbg");
        Pages.ShowOne("loadingdivmsgyesnomenu");
        document.getElementById('msgnobutton').style.visibility = 'hidden';
        document.getElementById('msgyesbutton').style.visibility = 'hidden';
        document.getElementById('msgmenubutton').style.visibility = 'hidden';
        document.getElementById('msgmenutext').innerHTML = msgtext;
    }
    if (id == "loadingdivmsgyesnomenu" && todo == 0) {
        try {
            Pages.HideOne("screentimeoutbg", " OnlineDiv(screentimeoutbg, 0)");
            document.getElementById('msgnobutton').style.visibility = 'hidden';
            document.getElementById('msgyesbutton').style.visibility = 'hidden';
            document.getElementById('msgmenubutton').style.visibility = 'hidden';
            StartTimeOut();
        } catch (e) {
        }
    }
    //совершение платежа hotkey -v
    if (id == "loadingdivPAYMENTmsg" && todo == 1) {

        StopTimeOutMain();
        Pages.ShowOne("screentimeoutbg");
        Pages.ShowOne("loadingdivPAYMENTmsg");
        onlinedivtimer_pay._init(60);
        onlinedivtimer_pay.directionTimeToBig();
        onlinedivtimer_pay.start();
    }

    //hotkey -b
    if (id == "loadingdivPAYMENTmsg" && todo == 0) {
        onlinedivtimer_pay.stop();
        Pages.HideOne('screentimeoutbg', " OnlineDiv(loadingdivPAYMENTmsg, 0), screentimeoutbg");
        Pages.HideOne('loadingdivPAYMENTmsg', " OnlineDiv(loadingdivPAYMENTmsg, 0), loadingdivPAYMENTmsg");
        StartTimeOut();
    }

    if (todo == 0) {
        var todotext = "hidden";
    } else {
        var todotext = "visible";
    }
    try {

        document.getElementById(id).style.visibility = todotext;
    } catch (e) {
    }

}

/**
 * Независимый таймер, метод которого можно переопределять
 * timer.finilize, timer.execute
 * @param seconds
 * @param msgtext
 * @constructor
 */
function IndependentTimer(seconds, msgtext) {
    this.seconds = seconds;
    this.timer = new Wtimers('polew');
    this.timer._init(this.seconds);
    this.text = msgtext;
}
IndependentTimer.prototype.start = function () {
    StopTimeOutMain();
    Pages.ShowOne("screentimeoutbg");
    Pages.ShowOne('loadingdivmsg');
    document.getElementById('loadingdivmsg').style.visibility = 'visible';
    document.getElementById('cancbut').style.visibility = 'hidden';
    document.getElementById('wtext_load').innerHTML = this.text;
    this.timer.start();

};
IndependentTimer.prototype.stop = function () {
    document.getElementById('loadingdivmsg').style.visibility = 'hidden';
    Pages.HideOne('loadingdivmsg', " IndependentTimer.prototype.stop, loadingdivmsg");
    Pages.HideOne("screentimeoutbg", " IndependentTimer.prototype.stop, screentimeoutbg");
    this.timer.stop();
};


function PinPadTimer(seconds) {  
    this.seconds = seconds;
    this.pinpadtimer = new Wtimers('polew3');
    this.pinpadtimer._init(this.seconds);
}
PinPadTimer.prototype.start = function (seconds) { 
    StopTimeOutMain();
    this.pinpadtimer._init(seconds);
    Pages.ShowOne('polew3');
    this.pinpadtimer.start();
};

PinPadTimer.prototype.stop = function () {  
//    document.getElementById('pinpadtimerdiv').style.display = 'none';
    this.pinpadtimer.stop(); 
};



function BarcodeTimer(seconds) {  
    this.seconds = seconds;
    this.BarcodeTimer = new Wtimers('polew4');
    this.BarcodeTimer._init(this.seconds);
}
BarcodeTimer.prototype.start = function (seconds) { 
    StopTimeOutMain();
    this.BarcodeTimer._init(seconds);
    this.BarcodeTimer.start();
};

BarcodeTimer.prototype.stop = function () {  
    this.BarcodeTimer.stop(); 
};

