function WCalendar(container) {
    this.name = "";
    this.calbox = 'calbox';
    this.value_default = "";

    this.cal_value = "";

    this.input = document.getElementById(container);
    this.stepinfo_id = '';
    this.stepinfofull_id = '';
}

WCalendar.prototype.init = function (stepinfoID, stepinfofullID) {
    this.create();
    this.stepinfo_id = stepinfoID;
    this.stepinfofull_id = stepinfofullID;
};
/**
 * Устанавливает степинфо для шага
 * @param text
 */
WCalendar.prototype.setStepInfo = function(text) {
    document.getElementById(this.stepinfo_id).innerHTML = text;
};
/**
 * Устанавливает степинфофул для шага
 * @param text
 */
WCalendar.prototype.setStepInfoFull = function(text) {
    document.getElementById(this.stepinfofull_id).innerHTML = text;
};
WCalendar.prototype.create = function () {
   alert('abstract create');
};
WCalendar.prototype.dFilter = function (value) {
    StartTimeOut();
    this.setValue(value);
    if (this.ManageMask.checkValid()) {
        Pages.Steps.button.next.show();
    } else {
        Pages.Steps.button.next.hide();
    }
};
WCalendar.prototype.setValue = function (value) {
    if (value.length > 0) {
        this.cal_value = value;
        this.input.innerHTML = value;
    }
};
var abstractCalendar = new WCalendar('abstractCalendar');

/**
 * Calendar
 * @param container
 * @constructor
 */
function WCalendarFull(container) {
    this.value_default = "ДД.ММ.ГГГГ";
    this.cal_value = "";
    this.input = document.getElementById(container);
    this.input.innerHTML = this.value_default;
    var ext = this;

    this.ManageMask = {
        getData: function () {
            return ext.cal_value;
        },
        checkValid: function () {
            if (ext.ManageMask.getData().length > 0)
                return true;
            else
                return false;
        },
        getValueParseMask: function() {
            return ext.cal_value;
        }
    };

}
WCalendarFull.prototype = inherit(abstractCalendar);

WCalendarFull.prototype.create = function () {
    var calendar = document.getElementById('wcalendar');
    calendar.innerHTML = '';
    var ext = this;
    var calendarXIII = new CalendarEightysix('wcalendar', {
        'injectInsideTarget': true,
        'alwaysShow': true,
        'pickable': true,
        'startMonday': true,
        'format': '%d-%m-%Y'
    });
    calendarXIII.render(); //Render again because while initializing and doing the first render it did not have the event set yet
    calendarXIII.addEvent('change', function (date) {
        var time = date.clone();
        var day = time.getDate();
        var month = time.getMonth()+1;
        var year = time.getFullYear();
        if (day < 10) day = "0" + day;
        if (month < 10) month = "0" + month;
        var mydate = day + "." + month + "." + year;
        ext.dFilter(mydate);
    });
};

/**
 * CalendarMonth
 * @constructor
 */
function WCalendarMonth(container) {
    this.value_default = "ММ.ГГГГ";
    this.cal_value = "";
    this.input = document.getElementById(container);
    this.input.innerHTML = this.value_default;

    var ext = this;
    this.ManageMask = {
        getData: function () {
            return ext.cal_value;
        },
        checkValid: function () {
            if (ext.ManageMask.getData().length > 0)
                return true;
            else
                return false;
        },
        getValueParseMask: function() {
            return ext.cal_value;
        }
    };
}
WCalendarMonth.prototype = inherit(abstractCalendar);
WCalendarMonth.prototype.create = function () {
    var calendar = document.getElementById('wcalendarmonth');
    calendar.innerHTML = '';
    var ext = this;
    var month = new CalendarEightysix('wcalendarmonth', {
        defaultView: 'year',
        'injectInsideTarget': true,
        'alwaysShow': true,
        'pickable': true,
        'startMonday': true,
        'format': '%d-%m-%Y',
         months: ['Январь', 'Февраль', 'Maart', 'April', 'Mei', 'Juni', 'Juli', 'Augustus', 'September', 'October', 'November', 'December']
    });
    month.render(); //Render again because while initializing and doing the first render it did not have the event set yet
    month.addEvent('change', function (date) {
        var time = date.clone();
        var month = time.getMonth()+1;
        var year = time.getFullYear();
        if (month < 10) month = "0" + month;
        var monthyear = month + "." + year;
        ext.dFilter(monthyear);
    });
};



/**
 * Календарь для печати чека
 * @param container
 * @constructor
 */
function WCalendarFullPrint(container) {
    this.value_default = "ДД.ММ.ГГГГ";
    this.cal_value = "";
    this.input = document.getElementById(container);
    this.input.innerHTML = this.value_default;
    var ext = this;

    this.ManageMask = {
        getData: function () {
            return ext.cal_value;
        },
        checkValid: function () {
            if (ext.ManageMask.getData().length > 0)
                return true;
            else
                return false;
        },
        getValueParseMask: function() {
            return ext.cal_value;
        }
    };

}
WCalendarFullPrint.prototype = inherit(abstractCalendar);

WCalendarFullPrint.prototype.create = function () {
    var calendar = document.getElementById('calendar_search_check');
    calendar.innerHTML = '';
    var ext = this;
    var calendarXIII = new CalendarEightysix('calendar_search_check', {
        'injectInsideTarget': true,
        'alwaysShow': true,
        'pickable': true,
        'startMonday': true,
        'format': '%d-%m-%Y'
    });
    calendarXIII.render(); //Render again because while initializing and doing the first render it did not have the event set yet
    calendarXIII.addEvent('change', function (date) {
        var time = date.clone();
        var day = time.getDate();
        var month = time.getMonth()+1;
        var year = time.getFullYear();
        if (day < 10) day = "0" + day;
        if (month < 10) month = "0" + month;
        var mydate = day + "." + month + "." + year;
        ext.dFilter(mydate);
    });
};
WCalendarFullPrint.prototype.dFilter = function (value) {
    StartTimeOut();
    this.setValue(value);
    if (this.ManageMask.checkValid()) {
        Pages.HelpSubscriber.button.next.show();
    } else {
        Pages.HelpSubscriber.button.next.hide();
    }
};