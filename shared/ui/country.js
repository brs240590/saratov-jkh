function WCountry() {
    this.prefix = "wcountry";
    this.selectedCounryName = '';
    this.selectedCurrencyName = '';

    this.selectedCountry = 0;
    this.selectedCurrency = 0;


    this.countryButtonName = "";
    this.currencyButtonName = "";

    //страны
    this.country = [];
    this.country[1] = 301622;
    this.country[2] = 95;
    this.country[3] = 2521;
    this.country[4] = 30;
    this.country[5] = 8241;
    this.country[6] = 27;
    this.country[7] = 289;
    this.country[8] = 572;
    this.country[9] = 1507;
    this.country[10] = 811;
    this.country[11] = 481;
    this.country[12] = 1313;
    this.country[13] = 840;
    this.country[14] = 854;
    this.country[15] = 133;
    this.country[16] = 5110;
    this.country[17] = 227;
    this.country[18] = 1469;

    //валюта
    this.currency = [];
    this.currency[1] = 'РУБ';
    this.currency[2] = 'USD';
    this.currency[3] = 'EUR';
    this.currency[4] = 'GBR';

    var ext = this;
    this.ManageMask = {
        getData: function () {
            var ret = "";
            if (ext.selectedCountry != 0 && ext.selectedCurrency != 0) {
                ret = ext.selectedCountry + "_" + ext.selectedCurrency;
            }
            return ret;
        },
        getValueParseMask: function () {
            return ext.selectedCounryName + ", " + ext.selectedCurrencyName;
        },
        checkValid: function () {
            var valid = false;
            if (ext.selectedCountry != 0 && ext.selectedCurrency != 0)
                valid = true;
            return valid;
        }
    };
}
WCountry.prototype.init = function (containerCountry, containerCurrency) {
    var elem = getElemByID(containerCountry);
    elem.innerHTML = this.createCountry();
    this.containerCur = containerCurrency;
    this._start();
};
WCountry.prototype.createCurency = function (id) {
    var elem = getElemByID(this.prefix + 'wcurrency_panel');
    if (elem != null)
        elem.parentNode.removeChild(elem);
    var currency = "" +
        "<div class='currency_text'>Выберите валюту переводa</div>" +
        "<table>" +
        "<tr>";

    if (id == 1 || id == 2 || id == 3 || id == 4 || id == 6 || id == 7 || id == 8 || id == 9 || id == 11 || id == 14 || id == 17 || id == 15)
        currency += "<td class='currency_one' name='rub' id='" + this.currencyButtonName + "1' style='background: url(img/unistream_curr.png) no-repeat;'>РУБ</td>";
    if (id != 6 && id != 13) {
        currency += "<td class='currency_one' name='usd' id='" + this.currencyButtonName + "2' style='background: url(img/unistream_curr.png) no-repeat;'>USD</td>";
        currency += "<td class='currency_one' name='eur' id='" + this.currencyButtonName + "3' style='background: url(img/unistream_curr.png) no-repeat;'>EUR</td>";
    }
    if (id == 13) {
        currency += "<td class='currency_one' name='gbr' id='" + this.currencyButtonName + "4' style='background: url(img/unistream_curr.png) no-repeat;'>GBR</td>";
    }

    currency += "</tr>" +
        "</table>" +
        "";
    return currency;
};
WCountry.prototype.createCountry = function () {
    var elem = getElemByID(this.prefix + 'wcountry_panel');
    if (elem != null)
        elem.parentNode.removeChild(elem);
    var country = "" +
        "<div class='country_text'>Выберите страну получателя перевода</div>" +
        "<table>" +
        "<tr>" +
        "<td class='country_one' name='ABHZ' id='" + this.countryButtonName + "1' style='background: url(img/unistream_button.png) no-repeat;'>Абхазия</td>" +
        "<td class='country_one' name='GRUZ' id='" + this.countryButtonName + "2' style='background: url(img/unistream_button.png) no-repeat;'>Грузия</td>" +
        "<td class='country_one' name='MONGOL' id='" + this.countryButtonName + "3' style='background: url(img/unistream_button.png) no-repeat;'>Монголия</td>" +
        "</tr>" +
        "<tr>" +
        "<td class='country_one' name='ARM' id='" + this.countryButtonName + "4' style='background: url(img/unistream_button.png) no-repeat;'>Армения</td>" +
        "<td class='country_one' name='IZR' id='" + this.countryButtonName + "5' style='background: url(img/unistream_button.png) no-repeat;'>Израиль</td>" +
        "<td  class='country_one' name='RUS' id='" + this.countryButtonName + "6' style='background: url(img/unistream_button.png) no-repeat;'>Россия</td>" +
        "</tr>" +
        "<tr>" +
        "<td class='country_one' name='BEL' id='" + this.countryButtonName + "7' style='background: url(img/unistream_button.png) no-repeat;'>Беларусь</td>" +
        "<td class='country_one' name='KAZ' id='" + this.countryButtonName + "8' style='background: url(img/unistream_button.png) no-repeat;'>Казахстан</td>" +
        "<td class='country_one' name='TAJ' id='" + this.countryButtonName + "9' style='background: url(img/unistream_button.png) no-repeat;'>Таджикистан</td>" +
        "</tr>" +
        "<tr>" +
        "<td class='country_one' name='BOL' id='" + this.countryButtonName + "10' style='background: url(img/unistream_button.png) no-repeat;'>Болгария</td>" +
        "<td class='country_one' name='KIR' id='" + this.countryButtonName + "11' style='background: url(img/unistream_button.png) no-repeat;'>Кыргызстан</td>" +
        "<td class='country_one' name='UZB' id='" + this.countryButtonName + "12' style='background: url(img/unistream_button.png) no-repeat;'>Узбекистан</td>" +
        "</tr>" +
        "<tr>" +
        "<td class='country_one' name='UK' id='" + this.countryButtonName + "13' style='background: url(img/unistream_button.png) no-repeat;'>Великобритания</td>" +
        "<td class='country_one' name='LTV' id='" + this.countryButtonName + "14' style='background: url(img/unistream_button.png) no-repeat;'>Латвия</td>" +
        "<td class='country_one' name='UKR' id='" + this.countryButtonName + "15' style='background: url(img/unistream_button.png) no-repeat;'>Украина</td>" +
        "</tr>" +
        "<tr>" +
        "<td class='country_one' name='VIET' id='" + this.countryButtonName + "16' style='background: url(img/unistream_button.png) no-repeat;'>Вьетнам</td>" +
        "<td class='country_one' name='MOLD' id='" + this.countryButtonName + "17' style='background: url(img/unistream_button.png) no-repeat;'>Молдова</td>" +
        "<td class='country_one' name='CHKH' id='" + this.countryButtonName + "18' style='background: url(img/unistream_button.png) no-repeat;'>Чехия</td>" +
        "</tr>" +
        "</table>" +
        "";
    return country;
};
WCountry.prototype._start = function () {
    var ext = this;
    var elem = getElemByID(this.containerCur);
    if(elem != null)
        elem.innerHTML = '';
    for (var key in this.country) {
        var name = this.countryButtonName + key;
        var el = getElemByID(name);
        if (!this.country.hasOwnProperty(key))
            continue;
        if (el != null) {
            el.onclick = function () {
                ext._keyClickCountry(this.id);
            };
        }
    }
};
WCountry.prototype._keyClickCountry = function (id) {
    //Перезапуск таймера я еще здесь
    //if(!this.ManageMask.checkCountEnter()) {
    StartTimeOut();
    //}
    this.selectedCurrency = 0;
    this.selectedCurrencyName = "";
    this.dFilterCountry(id);
};
WCountry.prototype._startCurrency = function (id) {
    StartTimeOut();
    this.dFilterCurrency(id);
};
WCountry.prototype._keyClickCurrency = function (id) {
    this.selectedCurrency = id.substr(this.currencyButtonName.length);

    for (var key in this.currency) {
        if (!this.currency.hasOwnProperty(key))
            continue;
        var el = getElemByID(this.currencyButtonName + key);
        if (el != null) {
            el.style.backgroundImage = "url('img/unistream_curr.png')";
        }
    }
    var cur = getElemByID(id);
    if (cur != null) {
        cur.style.backgroundImage = "url('img/unistream_curr_p.png')";
        this.setCurrency(this.selectedCurrency, cur.innerHTML);
    }
    this.checkValid();
};
WCountry.prototype.setStepInfo = function () {

};
WCountry.prototype.setStepInfoFull = function () {

};
WCountry.prototype.setValue = function (value) {
    if (value.length > 0) {
        var val = value.split("_");
        var country_id = val[0];
        var currency_id = val[1];
        //делаем активным выбранною страну
        for(var i = 1; i < this.country.length; i++) {
            if(this.country[i] == country_id) {
                this.dFilterCountry(this.countryButtonName + i);
                break;
            }
        }
        //делаем активной выбранную валюту
        for(var j = 1; j < this.currency.length; j++) {
            if(j == currency_id) {
                this._keyClickCurrency(this.currencyButtonName + j);
                break;
            }
        }
    }
};
WCountry.prototype.setPrefix = function (value) {
    this.prefix = value;
    this.countryButtonName = this.prefix + "country";
    this.currencyButtonName = this.prefix + "currency";
};
WCountry.prototype.dFilterCountry = function (id) {
    //устанавливаем бекграунд по умолчанию
    for (var key in this.country) {
        var name = this.countryButtonName + key;
        if (!this.country.hasOwnProperty(key))
            continue;
        getElemByID(name).style.backgroundImage = "url('img/unistream_button.png')";
    }
    var num = id.substr(this.countryButtonName.length);

    this._startCurrency(num);

    //столбец страны
    var el = getElemByID(this.countryButtonName + num);
    el.style.backgroundImage = "url('img/unistream_button_p.png')";

    this.setCountry(this.country[num], el.innerHTML);
    this.checkValid();
};
WCountry.prototype.dFilterCurrency = function (id) {
    var ext = this;
    var elem = getElemByID(this.containerCur);
    elem.innerHTML = this.createCurency(id);
    for (var key in this.currency) {
        var name = this.currencyButtonName + key;
        var el = getElemByID(name);
        if (!this.country.hasOwnProperty(key))
            continue;
        if (el != null) {
            el.onclick = function () {
                ext._keyClickCurrency(this.id);
            };
        }
    }
};
WCountry.prototype.setCountry = function (country_id, country_name) {
    this.selectedCounryName = country_name;
    this.selectedCountry = country_id;
    AddAdditionalPaymentParameters("Recipient_Country", country_id);
    AddAdditionalPaymentParameters("CountryWord", country_name);
};
WCountry.prototype.setCurrency = function (currency_id, currency_name) {
    this.selectedCurrencyName = currency_name;
    this.selectedCurrency = currency_id;
    AddAdditionalPaymentParameters('CurrencyWord', currency_name);
    AddAdditionalPaymentParameters('Currency', currency_id);
};
WCountry.prototype.checkValid = function () {
    if (this.ManageMask.checkValid()) {
        this.doValidSuccess();
    } else {
        this.doValidDenied();
    }
};
WCountry.prototype.doValidSuccess = function () {
    Pages.Steps.button.next.show();
};
WCountry.prototype.doValidDenied = function () {
    Pages.Steps.button.next.hide();
};