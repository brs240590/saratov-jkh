/**
 Абстрактный класс клавиатуры
 */
function Keyboard() {
    //Контайнер, который содержит клавиатуру
    this.KeyboardContainerName = "";
    //Нажимали шифт или нет
    this.ShiftDown = true;
    //Текущий язык клавиатуры
    this.LangDown = 'rus';
    this.LangSmeshenie = 0;
    this.mask = null;
    this.ManageMask = null;
//    this.soundPath = "../sound/enter_number.wav";

    this.stepinfo_id = '';
    this.stepinfofull_id = '';
    this.prefix = "";
}
/**
 * Добавление префикса
 * @param _prefix
 */
Keyboard.prototype.setPrefix = function(_prefix) {
    this.prefix = _prefix;
};
/*
 Метод используется для формирования клавиатуры
 Возвращает хтмл таблицу клавиатуры. После добавления на клавиатуры
 Необходимо повесить обработчики на каждую кнопку
 */
Keyboard.prototype.CreateKeyboard = function() {
    var elem = document.getElementById('wkeyboard_panel');
    if(elem != null)
        elem.parentNode.removeChild(elem);
    var keyboard = "\
        <table id='" + this.prefix + "wkeyboard_panel' class='wkeyboard_panel' valign='top' cellspacing=0 cellpadding=0 border=0>\
            <tr>\
                <td align='center' valign='top'>\
                   <table border=0 cellspacing=3 cellpadding=3>\
                       <tr>\
                           <td class='KB_KEY' id='" + this.prefix + "kb1' style='background: url(img/fo_btn.png) no-repeat;'></td>\
                           <td class='KB_KEY' id='" + this.prefix + "kb2' style='background: url(img/fo_btn.png) no-repeat;'></td>\
                           <td class='KB_KEY' id='" + this.prefix + "kb3' style='background: url(img/fo_btn.png) no-repeat;'></td>\
                           <td class='KB_KEY' id='" + this.prefix + "kb4' style='background: url(img/fo_btn.png) no-repeat;'></td>\
                           <td class='KB_KEY' id='" + this.prefix + "kb5' style='background: url(img/fo_btn.png) no-repeat;'></td>\
                           <td class='KB_KEY' id='" + this.prefix + "kb6' style='background: url(img/fo_btn.png) no-repeat;'></td>\
                           <td class='KB_KEY' id='" + this.prefix + "kb7' style='background: url(img/fo_btn.png) no-repeat;'></td>\
                           <td class='KB_KEY' id='" + this.prefix + "kb8' style='background: url(img/fo_btn.png) no-repeat;'></td>\
                           <td class='KB_KEY' id='" + this.prefix + "kb9' style='background: url(img/fo_btn.png) no-repeat;'></td>\
                           <td class='KB_KEY' id='" + this.prefix + "kb10' style='background: url(img/fo_btn.png) no-repeat;'></td>\
                       </tr>\
                   </table>\
                   <table cellspacing=3 cellpadding=3>\
                       <tr>\
                           <td class='KB_KEY' id='" + this.prefix + "kb11' style='background: url(img/fo_btn.png) no-repeat;'></td>\
                           <td class='KB_KEY' id='" + this.prefix + "kb12' style='background: url(img/fo_btn.png) no-repeat;'></td>\
                           <td class='KB_KEY' id='" + this.prefix + "kb13' style='background: url(img/fo_btn.png) no-repeat;'></td>\
                           <td class='KB_KEY' id='" + this.prefix + "kb14' style='background: url(img/fo_btn.png) no-repeat;'></td>\
                           <td class='KB_KEY' id='" + this.prefix + "kb15' style='background: url(img/fo_btn.png) no-repeat;'></td>\
                           <td class='KB_KEY' id='" + this.prefix + "kb16' style='background: url(img/fo_btn.png) no-repeat;'></td>\
                           <td class='KB_KEY' id='" + this.prefix + "kb17' style='background: url(img/fo_btn.png) no-repeat;'></td>\
                           <td class='KB_KEY' id='" + this.prefix + "kb18' style='background: url(img/fo_btn.png) no-repeat;'></td>\
                           <td class='KB_KEY' id='" + this.prefix + "kb19' style='background: url(img/fo_btn.png) no-repeat;'></td>\
                           <td class='KB_KEY' id='" + this.prefix + "kb20' style='background: url(img/fo_btn.png) no-repeat;'></td>\
                           <td class='KB_KEY' id='" + this.prefix + "kb45' style='background: url(img/fo_btn.png) no-repeat;'></td>\
                           <td class='KB_KEY' id='" + this.prefix + "kb46' style='background: url(img/fo_btn.png) no-repeat;'></td>\
                       </tr>\
                   </table>\
                   <table cellspacing=3 cellpadding=3>\
                       <tr>\
                           <td class='KB_KEY' id='" + this.prefix + "kb21' style='background: url(img/fo_btn.png) no-repeat;'></td>\
                           <td class='KB_KEY' id='" + this.prefix + "kb22' style='background: url(img/fo_btn.png) no-repeat;'></td>\
                           <td class='KB_KEY' id='" + this.prefix + "kb23' style='background: url(img/fo_btn.png) no-repeat;'></td>\
                           <td class='KB_KEY' id='" + this.prefix + "kb24' style='background: url(img/fo_btn.png) no-repeat;'></td>\
                           <td class='KB_KEY' id='" + this.prefix + "kb25' style='background: url(img/fo_btn.png) no-repeat;'></td>\
                           <td class='KB_KEY' id='" + this.prefix + "kb26' style='background: url(img/fo_btn.png) no-repeat;'></td>\
                           <td class='KB_KEY' id='" + this.prefix + "kb27' style='background: url(img/fo_btn.png) no-repeat;'></td>\
                           <td class='KB_KEY' id='" + this.prefix + "kb28' style='background: url(img/fo_btn.png) no-repeat;'></td>\
                           <td class='KB_KEY' id='" + this.prefix + "kb29' style='background: url(img/fo_btn.png) no-repeat;'></td>\
                           <td class='KB_KEY' id='" + this.prefix + "kb47' style='background: url(img/fo_btn.png) no-repeat;'></td>\
                           <td class='KB_KEY' id='" + this.prefix + "kb48' style='background: url(img/fo_btn.png) no-repeat;'></td>\
                       </tr>\
                   </table>\
                   <table cellspacing=3 cellpadding=3>\
                       <tr>\
                           <td class='KB_KEY' id='" + this.prefix + "kb31' style='background: url(img/fo_btn.png) no-repeat;'></td>\
                           <td class='KB_KEY' id='" + this.prefix + "kb32' style='background: url(img/fo_btn.png) no-repeat;'></td>\
                           <td class='KB_KEY' id='" + this.prefix + "kb33' style='background: url(img/fo_btn.png) no-repeat;'></td>\
                           <td class='KB_KEY' id='" + this.prefix + "kb34' style='background: url(img/fo_btn.png) no-repeat;'></td>\
                           <td class='KB_KEY' id='" + this.prefix + "kb35' style='background: url(img/fo_btn.png) no-repeat;'></td>\
                           <td class='KB_KEY' id='" + this.prefix + "kb36' style='background: url(img/fo_btn.png) no-repeat;'></td>\
                           <td class='KB_KEY' id='" + this.prefix + "kb37' style='background: url(img/fo_btn.png) no-repeat;'></td>\
                           <td class='KB_KEY' id='" + this.prefix + "kb38' style='background: url(img/fo_btn.png) no-repeat;'></td>\
                           <td class='KB_KEY' id='" + this.prefix + "kb39' style='background: url(img/fo_btn.png) no-repeat;'></td>\
                           <td class='KB_KEY' id='" + this.prefix + "kb103' style='background: url(img/fo_btn.png) no-repeat;'></td>\
                       </tr>\
                   </table>\
               </td>\
           </tr>\
       </table>\
    ";

    return keyboard;
};
/*
 Инициализация клавиатуры
 String containerID содержит айди контейнера, в который момещается клавиатура
 */
Keyboard.prototype.InitKeyboard = function(containerID, stepinfoID, stepinfofullID) {
    var container = document.getElementById(containerID);
    this.stepinfo_id = stepinfoID;
    this.stepinfofull_id = stepinfofullID;
    container.innerHTML = this.CreateKeyboard();
    this._startKB();
};

/**
 * Устанавливает степинфо для шага
 * @param text
 */
Keyboard.prototype.setStepInfo = function(text) {
    document.getElementById(this.stepinfo_id).innerHTML = text+":";
};
/**
 * Устанавливает степинфофул для шага
 * @param text
 */
Keyboard.prototype.setStepInfoFull = function(text) {
    document.getElementById(this.stepinfofull_id).innerHTML = text;
};
/**
 * Эффект нажатия на клавиатуру
 * @param id
 */
Keyboard.prototype.effectClick = function(id) {
    WSoundEffects.click();
    var urlButton = "url('img/fo_btn.png')";
    var urlPressButton = "url('img/fo_btn_p.png')";
    if(id == this.prefix + 'kb42' || id == this.prefix + 'kb43' || id == this.prefix + 'kb41' || id == this.prefix + 'kb102' || id == this.prefix + 'kb103') {
        var urlButton = "url('img/fo_btn.png')";
        var urlPressButton = "url('img/fo_btn_p.png')";
    }

    var element = document.getElementById(id);
    element.style.backgroundImage = urlPressButton;
    var ext = this;
    setTimeout(function() {
        element.style.backgroundImage = urlButton;
    }, 10);
}
;
/*
 Обработчик нажатия на клавиатуру
 Содержит обработку нажатия на шифт и смене клавиш
 */
Keyboard.prototype._keyClick = function(idpic) {
    //Перезапуск таймера я еще здесь
    StartTimeOut();

    if(idpic == this.prefix + 'kb42')//CHANGE SHIFT KEY
    {
        this.ShiftDown = !this.ShiftDown;
        this._startKB();
    } else if(idpic == this.prefix + 'kb102') { //Change language
        this.changeLanguage();
    }
    else {
        var id = replace(idpic, this.prefix + 'kb', '');
        id = Number(id) + this.LangSmeshenie;
        this.dFilter(id);
    }
};
//Обработчик нажатия на клавиши клавиатуры
Keyboard.prototype.dFilter = function(id) {
    alert("Keyboard.dFilter");
};
Keyboard.prototype.changeLanguage = function() {
    //Сейчас стоит русский
    if(this.LangDown == 'rus') {
        this.LangDown = 'eng';
    } else if(this.LangDown == 'eng') {
        this.LangDown = 'rus';
    }
    this.ShiftDown = false;
    this._setTextInKey();

};

Keyboard.prototype._pressHandler = function(key, textBoxValue) {
    if(key == (43 + this.LangSmeshenie))//пробел
    {
        this.ManageMask.filter(' ');
    }
    else if(key == (41 + this.LangSmeshenie))//очистка
    {
        if(textBoxValue.length != 0) {
            textBoxValue = "";
            this.ManageMask.clear();
        }
        //this.finalize();
    }
    else if(key == (40 + this.LangSmeshenie))//делете
    {
        if(textBoxValue.length != 0) {
            textBoxValue = textBoxValue.substring(0, textBoxValue.length - 1);
            if(textBoxValue.length == 0)
                this.finalize();
        }
    }
    else if(key == 154)//очистка
    {
        textBoxValue = textBoxValue + '';
    } else if(key == (103 + this.LangSmeshenie))//нажали бекспейс
    {
        textBoxValue = this.ManageMask.backspace();
    }
    else {
        var letter = '';
        if(this.ShiftDown) {
            letter = m2[key];
        }
        else {
            letter = m1[key];
        }
        //Пропускаем через маску
        this.ManageMask.filter(letter);

    }
    this.checkValid();
    textBoxValue = '';

    var dt = this.ManageMask.getDataMask();

    for(var i = 0; i < dt.length; i++) {
        textBoxValue += dt.charAt(i).replace("\&", "\&amp;");
    }
    return textBoxValue;
};
/**
 * Проверка валидности
 */
Keyboard.prototype.checkValid = function() {
    if(this.ManageMask.checkValid()) {
        this.doValidSuccess();
    } else {
        this.doValidDenied();
    }
};
Keyboard.prototype.doValidDenied = function() {
    Pages.Steps.button.next.hide();
};
Keyboard.prototype.doValidSuccess = function() {
    Pages.Steps.button.next.show();
};

Keyboard.prototype._findoutSmechenie = function() {
    if(this.LangDown == 'rus') {
        this.LangSmeshenie = 50;
    }
    else if(this.LangDown == 'eng') {
        this.LangSmeshenie = 0;
    }
};
Keyboard.prototype._setTextInKey = function() {
    var i = 0;
    this._findoutSmechenie();
    i = i + this.LangSmeshenie;
    for(i; i < m1.length; i++) {
        var name = this.prefix + 'kb' + (i - this.LangSmeshenie);
        var key = document.getElementById(name);
        if(key != null) {
            if(this.ShiftDown) {
                try {
                    key.innerHTML = 1[i];
                } catch(e) {
                }
            }
            else {
                try {
                    key.innerHTML = m1[i];
                } catch(e) {
                }
            }
        }
    }
    getElemByID(this.prefix + 'kb105').innerHTML = "\&amp;";
};
//Заполняет клавиатуру значениями и вешает событие клик на кнопки
Keyboard.prototype._startKB = function() {
    var i = 0;
    var ext = this;
    this._findoutSmechenie();
    i = i + this.LangSmeshenie;
    for(i; i < m1.length; i++) {
        var name = this.prefix + 'kb' + (i - this.LangSmeshenie);
        var key = document.getElementById(name);
        if(key != null) {
            if(this.ShiftDown) {
                try {
                    key.innerHTML = m2[i];
                } catch(e) {
                }
            }
            else {
                try {
                    key.innerHTML = m1[i];
                } catch(e) {
                }
            }

            key.onclick = function(e) {
                var event = fixEvent(e);
                ext.effectClick(event.target.id);
                ext._keyClick(event.target.id);
            };
        }
    }
    //заплатка, если не ставить проверку. помощник абонента по другим платежам перестает работать
    var ampersand = getElemByID(this.prefix + 'kb105');
    if(ampersand != null)
        ampersand.innerHTML = "\&amp;";
    this.SetAdditionalEvent(this);
};
/**
 * Устанавливает маску для ввода
 * @param mask string
 */
Keyboard.prototype.setMask = function(mask) {
    this.mask = mask;
    this.ManageMask = new MaskFactory().makeMask.keyboard(mask);
};
/*
 В этом методе возможно установить дополнительно обработчики на свои елементы
 ext - замыкание на this Keyboard
 */
Keyboard.prototype.SetAdditionalEvent = function(ext) {

};
var abstractKeyboard = new Keyboard();

/*
 Клавиатура в поиске операторов
 */
function KeyboardSearch() {
    this.ShiftDown = true;
    this.LangDown = 'rus';
    this.LangSmeshenie = 0;
    //айди инпута куда вводим данные
    this.textBoxId = "wsearchbox";
    this.databox = document.getElementById(this.textBoxId);
    this.databox.innerHTML = '';
    //Значение поля которое заполняется по нажатию на клавиатуру
    //this._startKB();
    //this.finalize();
}
KeyboardSearch.prototype = inherit(abstractKeyboard);
//Добавляем событие на бэкспейс

KeyboardSearch.prototype.dFilter = function(key) {
    var searchTextBox = document.getElementById(this.textBoxId);
    //возможно придется обьявить в
    this.textBoxValue = searchTextBox.innerHTML;
    if(key == (43 + this.LangSmeshenie))//пробел
    {
        this.textBoxValue = this.textBoxValue + ' ';
    }
    else if(key == (41 + this.LangSmeshenie))//очистка
    {
        if(this.textBoxValue.length != 0) {
            this.textBoxValue = "";
            searchTextBox.innerHTML = "";
        }
        //this.finalize();
    }
    else if(key == (40 + this.LangSmeshenie))//делете
    {
        if(this.textBoxValue.length != 0) {
            this.textBoxValue = this.textBoxValue.substring(0, this.textBoxValue.length - 1);
            if(this.textBoxValue.length == 0)
                this.finalize();
        }
    } else if(key == (103 + this.LangSmeshenie))//нажали бекспейс
    {
        if(this.textBoxValue.length != 0) {
            this.textBoxValue = this.textBoxValue.substring(0, this.textBoxValue.length - 1);
            if(this.textBoxValue.length == 0)
                this.finalize();
        }
    }
    else if(key == 154)//очистка
    {
        this.textBoxValue = this.textBoxValue + '';
    }
    else {
        if(this.ShiftDown) {
            this.textBoxValue = this.textBoxValue + m2[key];
        }
        else {
            this.textBoxValue = this.textBoxValue + m1[key];
        }
    }
    searchTextBox.innerHTML = this.textBoxValue;

    this.updateSearchPanel(this.textBoxValue);
};
KeyboardSearch.prototype.finalize = function() {

};
KeyboardSearch.prototype.updateSearchPanel = function(val) {
    var ext = this;
    var newdiv = document.createElement("div");
    //Перезапускаем поиск
    var ret = '';
    if(val.length > 2) {
        var ret = RetSearchPanel(val);
        newdiv.innerHTML = ret;
    }

    var search = getElemByID('searchresult');
    while(search.firstChild) {
        search.removeChild(search.firstChild);
    }
    search.appendChild(newdiv);

    //Обнуляем скроллинг
    //ScrollInSearchOperator = new InSearchOperator('searchresult');
    ScrollInSearchOperator.init();

    //вешаем обработчик на кнопку "Добавить кнопку?"
    var button_to_send = getElemByID('send_order_to_add');
    if(button_to_send != null)
        button_to_send.onclick = function() {
            CreateGateForSend(ext.databox.innerHTML);
        };
};
//Keyboard.prototype.textBox = function () {
//    return document.getElementById(this.textBoxId);
//};


function KeyboardFioBox() {
    this.ShiftDown = true;
    this.LangDown = 'rus';
    this.LangSmeshenie = 0;

    //айди инпута куда вводим данные
    this.currentInput = this.lastName = document.getElementById('lastname');
    this.secondName = document.getElementById('secondname');
    this.firstName = document.getElementById('firstname');
    var ext = this;

    getElemByID('lastname_tr').onclick = function() {
        ext.currentInput = ext.lastName;
        StartTimeOut();
        document.getElementById('fiobox_last_center').className = "winput_center_fiobox_red";
        document.getElementById('fiobox_last_right').className = "winput_right_red";
        document.getElementById('fiobox_last_left').className = "winput_left_red";

        document.getElementById('fiobox_first_center').className = "winput_center_fiobox";
        document.getElementById('fiobox_first_right').className = "winput_right";
        document.getElementById('fiobox_first_left').className = "winput_left";

//        document.getElementById('fiobox_second_center').className = "winput_center_fiobox";
//        document.getElementById('fiobox_second_right').className = "winput_right";
//        document.getElementById('fiobox_second_left').className = "winput_left";
    };
    this.lastName.innerHTML = '';

/*
    getElemByID('secondname_tr').onclick = function() {
        ext.currentInput = ext.secondName;
        StartTimeOut();
        document.getElementById('fiobox_last_center').className = "winput_center_fiobox";
        document.getElementById('fiobox_last_right').className = "winput_right";
        document.getElementById('fiobox_last_left').className = "winput_left";

        document.getElementById('fiobox_first_center').className = "winput_center_fiobox";
        document.getElementById('fiobox_first_right').className = "winput_right";
        document.getElementById('fiobox_first_left').className = "winput_left";

        document.getElementById('fiobox_second_center').className = "winput_center_fiobox_red";
        document.getElementById('fiobox_second_right').className = "winput_right_red";
        document.getElementById('fiobox_second_left').className = "winput_left_red";
    };
    this.secondName.innerHTML = '';
*/

    getElemByID('firstname_tr').onclick = function() {
        ext.currentInput = ext.firstName;
        StartTimeOut();
        document.getElementById('fiobox_last_center').className = "winput_center_fiobox";
        document.getElementById('fiobox_last_right').className = "winput_right";
        document.getElementById('fiobox_last_left').className = "winput_left";

        document.getElementById('fiobox_first_center').className = "winput_center_fiobox_red";
        document.getElementById('fiobox_first_right').className = "winput_right_red";
        document.getElementById('fiobox_first_left').className = "winput_left_red";

//        document.getElementById('fiobox_second_center').className = "winput_center_fiobox";
//        document.getElementById('fiobox_second_right').className = "winput_right";
//        document.getElementById('fiobox_second_left').className = "winput_left";
    };
    this.firstName.innerHTML = '';

    this.ManageMask = null;
    //Значение поля которое заполняется по нажатию на клавиатуру
    this.lastName.click();
}
KeyboardFioBox.prototype = inherit(abstractKeyboard);

KeyboardFioBox.prototype.setMask = function(mask) {
    this.mask = mask;
    this.ManageMask = new MaskFactory().makeMask.fiobox(mask);
};
KeyboardFioBox.prototype.dFilter = function(key) {
    var type_input = type(this);
    if(key == (43 + this.LangSmeshenie))//пробел
    {
        this.ManageMask.filter(' ', type_input);
    }
    else if(key == (41 + this.LangSmeshenie))//очистка
    {
        this.firstName.innerHTML = '';
        this.secondName.innerHTML = '';
        this.lastName.innerHTML = '';
        this.ManageMask.clear();
    }
    else if(key == (40 + this.LangSmeshenie))//делете
    {
        this.currentInput.innerHTML = this.ManageMask.backspace(type_input)[type_input];
        alert(4);
    }
    else if(key == (103 + this.LangSmeshenie))//нажали бекспейс
    {
        this.currentInput.innerHTML = this.ManageMask.backspace(type_input)[type_input];
    }
    else {
        var letter = '';
        if(this.ShiftDown) {
            letter = m2[key];
        }
        else {
            letter = m1[key];
        }

        //Пропускаем через маску
        this.ManageMask.filter(letter, type_input);
        this.currentInput.innerHTML = this.ManageMask.getDataMask()[type_input];

    }
    if(this.ManageMask.checkValid()) {
        Pages.Steps.button.next.show();
        //console.log('valid');
    } else {
        Pages.Steps.button.next.hide();
        //console.log('novalid');
    }


    function type(val) {
        if(val.currentInput == val.firstName) {
            return 2;
        } else if(val.currentInput == val.secondName) {
            return 3;
        } else if(val.currentInput == val.lastName) {
            return 1;
        }
    }
};
KeyboardFioBox.prototype.setValue = function(value) {
    this.ManageMask.setValue(value);
    var data = this.ManageMask.getDataMask();
    this.lastName.innerHTML = data[1];
    this.firstName.innerHTML = data[2];
    this.secondName.innerHTML = data[3];

};
//Дополнительно позволяет вешать обработчики на любой елемент вне клавиатуры
KeyboardFioBox.prototype.SetAdditionalEvent = function(ext) {

};

/**
 *
 * @constructor
 */
function KeyBoardEng(input_id) {
    this.ShiftDown = true;
    this.LangDown = 'eng';
    this.LangSmeshenie = 0;
    //айди инпута куда вводим данные
    this.textBoxId = input_id;
    //Очистка формы
    this.databox = document.getElementById(this.textBoxId);
    this.databox.innerHTML = '';
    //Значение поля которое заполняется по нажатию на клавиатуру
    WSoundEffects.sounds(this.soundPath);
}
KeyBoardEng.prototype = inherit(abstractKeyboard);

KeyBoardEng.prototype.dFilter = function(key) {
    //Получаем значение
    this.databox.innerHTML = this._pressHandler(key, this.databox.innerHTML);
    this.databox.style.fontSize = this.ManageMask.getCurrentFontSize();
};
KeyBoardEng.prototype.setValue = function(value) {
    this.ManageMask.setValue(value);
    this.databox.innerHTML = this.ManageMask.getDataMask();
    this.databox.style.fontSize = this.ManageMask.getCurrentFontSize();

    if(this.ManageMask.checkValid()) {
        Pages.Steps.button.next.show();
    }
};

KeyBoardEng.prototype.SetAdditionalEvent = function(ext) {

};

var m1 = [];
m1[1] = '1';
m1[2] = '2';
m1[3] = '3';
m1[4] = '4';
m1[5] = '5';
m1[6] = '6';
m1[7] = '7';
m1[8] = '8';
m1[9] = '9';
m1[10] = '0';
m1[11] = 'q';
m1[12] = 'w';
m1[13] = 'e';
m1[14] = 'r';
m1[15] = 't';
m1[16] = 'y';
m1[17] = 'u';
m1[18] = 'i';
m1[19] = 'o';
m1[20] = 'p';
m1[21] = 'a';
m1[22] = 's';
m1[23] = 'd';
m1[24] = 'f';
m1[25] = 'g';
m1[26] = 'h';
m1[27] = 'j';
m1[28] = 'k';
m1[29] = 'l';
m1[30] = '@';
m1[31] = 'z';
m1[32] = 'x';
m1[33] = 'c';
m1[34] = 'v';
m1[35] = 'b';
m1[36] = 'n';
m1[37] = 'm';
m1[38] = '_';
m1[39] = '-';
m1[40] = '';
m1[41] = 'Очистить';
m1[42] = 'ШИФТ';
m1[43] = 'Пробел';
m1[44] = '.';
m1[45] = '[';
m1[46] = ']';
m1[47] = ';';
m1[48] = '\'';
m1[49] = '#';
m1[50] = '*';
m1[101] = '\\';
m1[102] = "RUS/ENG";
m1[103] = '\&#8592';
m1[154] = '';
m1[105] = '\&';
var m2 = [];
m2[51] = '1';
m2[52] = '2';
m2[53] = '3';
m2[54] = '4';
m2[55] = '5';
m2[56] = '6';
m2[57] = '7';
m2[58] = '8';
m2[59] = '9';
m2[60] = '0';
m2[61] = 'й';
m2[62] = 'ц';
m2[63] = 'у';
m2[64] = 'к';
m2[65] = 'е';
m2[66] = 'н';
m2[67] = 'г';
m2[68] = 'ш';
m2[69] = 'щ';
m2[70] = 'з';
m2[71] = 'ф';
m2[72] = 'ы';
m2[73] = 'в';
m2[74] = 'а';
m2[75] = 'п';
m2[76] = 'р';
m2[77] = 'о';
m2[78] = 'л';
m2[79] = 'д';
m2[80] = '@';
m2[81] = 'я';
m2[82] = 'ч';
m2[83] = 'с';
m2[84] = 'м';
m2[85] = 'и';
m2[86] = 'т';
m2[87] = 'ь';
m2[88] = 'ю';
m2[89] = 'б';
m2[90] = '';
m2[91] = 'Очистить';
m2[92] = 'ШИФТ';
m2[93] = 'Пробел';
m2[94] = '-';
m2[95] = 'х';
m2[96] = 'ъ';
m2[97] = 'ж';
m2[98] = 'э';
m2[99] = '.';
m2[100] = '*';
m2[151] = '\\';
m2[152] = "RUS/ENG";
m2[153] = '\&#8592';
m2[155] = '\&';


m2[1] = '1';
m2[2] = '2';
m2[3] = '3';
m2[4] = '4';
m2[5] = '5';
m2[6] = '6';
m2[7] = '7';
m2[8] = '8';
m2[9] = '9';
m2[10] = '0';
m2[11] = 'Q';
m2[12] = 'W';
m2[13] = 'E';
m2[14] = 'R';
m2[15] = 'T';
m2[16] = 'Y';
m2[17] = 'U';
m2[18] = 'I';
m2[19] = 'O';
m2[20] = 'P';
m2[21] = 'A';
m2[22] = 'S';
m2[23] = 'D';
m2[24] = 'F';
m2[25] = 'G';
m2[26] = 'H';
m2[27] = 'J';
m2[28] = 'K';
m2[29] = 'L';
m2[30] = '@';
m2[31] = 'Z';
m2[32] = 'X';
m2[33] = 'C';
m2[34] = 'V';
m2[35] = 'B';
m2[36] = 'N';
m2[37] = 'M';
m2[38] = '_';
m2[39] = '-';
m2[40] = '';
m2[41] = 'Очистить';
m2[42] = 'ШИФТ';
m2[43] = 'Пробел';
m2[44] = '.';
m2[45] = '[';
m2[46] = ']';
m2[47] = ';';
m2[48] = '\'';
m2[49] = '#';
m2[50] = '*';
m2[101] = '\/';
m2[102] = "RUS/ENG";
m2[103] = '\&#8592';
m2[105] = '\&';

m1[51] = '1';
m1[52] = '2';
m1[53] = '3';
m1[54] = '4';
m1[55] = '5';
m1[56] = '6';
m1[57] = '7';
m1[58] = '8';
m1[59] = '9';
m1[60] = '0';
m1[61] = 'Й';
m1[62] = 'Ц';
m1[63] = 'У';
m1[64] = 'К';
m1[65] = 'Е';
m1[66] = 'Н';
m1[67] = 'Г';
m1[68] = 'Ш';
m1[69] = 'Щ';
m1[70] = 'З';
m1[71] = 'Ф';
m1[72] = 'Ы';
m1[73] = 'В';
m1[74] = 'А';
m1[75] = 'П';
m1[76] = 'Р';
m1[77] = 'О';
m1[78] = 'Л';
m1[79] = 'Д';
m1[80] = '@';
m1[81] = 'Я';
m1[82] = 'Ч';
m1[83] = 'С';
m1[84] = 'М';
m1[85] = 'И';
m1[86] = 'Т';
m1[87] = 'Ь';
m1[88] = 'Ю';
m1[89] = 'Б';
m1[90] = '';
m1[91] = 'Очистить';
m1[92] = 'ШИФТ';
m1[93] = 'Пробел';
m1[94] = '-';
m1[95] = 'Х';
m1[96] = 'Ъ';
m1[97] = 'Ж';
m1[98] = 'Э';
m1[99] = '.';
m1[100] = '*';
m1[151] = '\/';
m1[152] = "RUS/ENG";
m1[154] = '';
m1[153] = '\&#8592';
m1[155] = '\&';
