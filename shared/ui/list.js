/**
 * Класс формирует список
 * елементы списка предварительно добавляется чере
 * addElement(), после чего выполняется init();
 * @constructor
 */
function ListBoard() {
    var ext = this;
    this.listData = [];
    this.data = [];
    this.gateid = [];
    this.ManageMask = {
        getData: function () {
            if (ext.data.length > 0)
                return ext.data[0];
            else
                return '';
        },
        getValueParseMask: function () {
            for (var i = 0; i < ext.listData.length; i++) {
                if (ext.data[0] == ext.listData[i].Value) {
                    return ext.listData[i].Key;
                }
            }
        },
        checkValid: function () {
            if (ext.data.length > 0)
                return true;
            else
                return false;
        }
    };
    this.stepinfo_id = '';
    this.stepinfofull_id = '';
    this.prefix = "";
}
ListBoard.prototype.setPrefix = function (_prefix) {
    this.prefix = _prefix;
};
ListBoard.prototype.init = function (container, stepinfoID, stepinfofullID) {
    document.getElementById(container).innerHTML = this.create();
    this.stepinfo_id = stepinfoID;
    this.stepinfofull_id = stepinfofullID;
    this._start();
};
/**
 * Устанавливает степинфо для шага
 * @param text
 */
ListBoard.prototype.setStepInfo = function(text) {
    document.getElementById(this.stepinfo_id).innerHTML = text;
};
/**
 * Устанавливает степинфофул для шага
 * @param text
 */
ListBoard.prototype.setStepInfoFull = function(text) {
    document.getElementById(this.stepinfofull_id).innerHTML = text;
};
ListBoard.prototype.create = function () {

    var elem = document.getElementById(this.prefix + 'wlist_panel');
    if (elem != null)
        elem.parentNode.removeChild(elem);
    var list = "<table id='" + this.prefix + "wlist_panel' cellspacing='0' border=0>";

    var checktr = 0;

    var newlength = this.listData.length -2;
    var gatelength = this.listData.length -1;
    var GateID = this.listData[newlength].Value;
    var ExecutorID = this.listData[gatelength].Value;


    for (var i = 0; i < newlength; i++) {
        if (checktr == 3 || checktr == 0) { list += "<tr>"; }

var position = GateID + "" + i;
		 list += '<td align=center><div id="provlist" class="provlist"  id="' + this.prefix + 'lst' + i + '" >';
if (this.listData[i].Key.length > 40) { size=17;}  else { size=22; }
//size=17;

if (cart.cartvalue[position]){
				 list += '<div class="RemProvFromCart" id="RemProvFromCart" onclick="RemProvFromCart(\'' + GateID + i +'\');"><img src="img/prov_remove.png"></div><div class="provcolinmenu" id="provcolinmenu'+GateID+i+'" style="float: left;">'+cart.cartvalue[position].providercol+'</div>';
                               } else {
                                      list += '<div class="RemProvFromCart" id="RemProvFromCart" onclick="RemProvFromCart(\'' + GateID + i +'\');"><img src="img/prov_remove.png"></div><div class="provcolinmenu provmenu" id="provcolinmenu'+GateID+i+'" style="float: left;">0</div>';
                                      }
		 list += '<DIV class="grfontnologolist" style="font-size: ' + size + 'px;">' + this.listData[i].Key.toUpperCase() + '</div>';
		 list += '<div class="AddProvToCart" id="AddProvToCart" onclick="AddProvToCart(\'' + GateID  + i + '\',\'' + this.listData[i].Key + '\',\'' + this.listData[i].FixedPrice + '\',\'' + ExecutorID + '\');"><img src="img/prov_add.png"></div>';
		 list += '</div></td>';

        checktr = checktr + 1;
        if (checktr == 3) { list += "</tr>"; checktr=0;}
    }
    list += "</table>";

    return list;
};
ListBoard.prototype._start = function () {
    var ext = this;
    var newlength = this.listData.length -2;
    for (var i = 0; i < newlength; i++) {
//        var elem = document.getElementById(this.prefix + "lst" + i);
//        WEvents.remove(elem, 'click');
//        WEvents.add(elem, 'click', function (e) {
//            var event = fixEvent(e);
//            ext._keyClick(event.target.id);
//        });
    }

};
ListBoard.prototype._keyClick = function (id) {
    //EffectClick(id, "p");
    this.dFilter(id);
};
ListBoard.prototype.dFilter = function (id) {
    //Перезапуск таймера я еще здесь
    StartTimeOut();
    for (var i = 0; i < this.listData.length; i++) {
    if (this.listData[0].Value < 100) {
//        document.getElementById(this.prefix + 'lst' + i).style.backgroundImage = "url('img/clock.png')";
                                      } else {
//					        document.getElementById(this.prefix + 'lst' + i).style.backgroundImage = "url('img/calendar.png')";
                                             }
    }

    if (this.listData[0].Value < 100) {
                                      } else {
                                             }

};
ListBoard.prototype.checkValid = function() {
    if(this.ManageMask.checkValid()) {
        this.doValidSuccess();
    } else {
        this.doValidDenied();
    }
};
ListBoard.prototype.doValidDenied = function() {
    Pages.Steps.button.next.hide();
};
ListBoard.prototype.doValidSuccess = function() {
    Pages.Steps.button.next.show();
};
/**
 * Формирование массива данных this.data
 * @param id string айди выбранного списка
 * @private
 */
ListBoard.prototype._setData = function (id) {
    this.data = [];
    this.data.push(this.listData[id.substr(3 + this.prefix.length)].Value);
};
/**
 * array[i].Value значение
 * array[i].Key тестовое описание
 * @param arrays
 */
ListBoard.prototype.addList = function (arrays) {

    for (var i = 0; i < arrays.length; i++) {
        this.listData.push(arrays[i]);
    }
};
/**
 * Устанавливает выбранное значение
 * @param val значение структуры списка Value
 */
ListBoard.prototype.setValue = function (val) {
    if (val.length > 0) {
        for (var i = 0; i < this.listData.length; i++) {
            if (this.listData[i].Value == val) {
                this.dFilter(this.prefix + 'lst' + i);
                break;
            }
        }
    }
};
var listBoard = new ListBoard();




function ListOnline() {
    var ext = this;
    //Количество видимых елементов в списке
    this.elem_for_scroll = 4;
    this.listData = [];
    this.data = [];
    this.scroll = new ListOnlineScroll("wlist_online_select");
    this.ManageMask = {
        getData: function () {
            if (ext.data.length > 0)
                return ext.data[0];
            else
                return '';
        },
        getValueParseMask: function () {
            for (var i = 0; i < ext.listData.length; i++) {
                if (ext.data[0] == ext.listData[i].Value) {
                    return ext.listData[i].Name;
                }
            }
        },
        checkValid: function () {
            if (ext.data.length > 0)
                return true;
            else
                return false;
        }
    };
    this.stepinfo_id = '';
    this.stepinfofull_id = '';

}
ListOnline.prototype = inherit(listBoard);

ListOnline.prototype.init = function (container, stepinfoID, stepinfofullID) {
    var ext = this;
    this.stepinfo_id = stepinfoID;
    this.stepinfofull_id = stepinfofullID;
    //добавляем список на страницу
    document.getElementById(container).innerHTML = this.create();
    //Добавляем скролинг

    this.scroll.height = 510;
    this.scroll.speed = 510;
    this.scroll.init();

    try {
        document.getElementById('button_up').onclick = function () {
            WNewEffects.click.button('button_up');
            ext.scroll.onclick_up();
        }
    } catch (e) {
        SaveExceptionToLog(e, "list.js", "ListOnline.prototype.init, button_up");
    }

    try {
        document.getElementById('button_down').onclick = function () {
            WNewEffects.click.button('button_down');
            ext.scroll.onclick_down();
        }
    } catch (e) {
        SaveExceptionToLog(e, "list.js", "ListOnline.prototype.init, button_down");
    }

    //вешаем обработчики
    this._start();
};

ListOnline.prototype.create = function () {
    var elem = document.getElementById('wonline_list_panel');
    if (elem != null)
        elem.parentNode.removeChild(elem);
    var list = '';
    try {
        if (this.listData.length > 0) {
            list = "<table id='wonline_list_panel' cellpadding='0' cellspacing='5'>";
            for (var i = 0; i < this.listData.length; i++) {
                list += "<tr id='onl" + i + "' class='KB_LIST_TR'>";
                list += "<td id='onl" + i + "_active' class='KB_LIST' width='250' height='110' style='font-size: 15px; solid red;text-align: center; background: url(img/listonline.png)'><div style='margin: 10px;'>" + i + " :" + this.listData[i].Name.substr(0, 200) + "<div></td>";
                list += "<td  class='KB_LIST' width='400' height='110' style='font-size: 15px; text-align: center; background: url(img/listonline_desc.png)'><div style='margin: 10px;'>" + this.listData[i].Desc.substr(0, 200) + "<div></td>";
                list += "<td  class='KB_LIST' width='160' height='110' style='font-size: 15px; text-align: center; background: url(img/listonline_sum.png)'><div style='margin: 10px;'>" + this.listData[i].FixedSum + " руб.<div></td>";
                list += "</tr>";
                list += "<tr><td height='10'> </td></tr>";
            }
            list += "</table>";
        }
    } catch (e) {
        SaveExceptionToLog(e, "list.js", "lab_listonline_create_1");
    }

    return list;
};
ListOnline.prototype._start = function () {
    var ext = this;
    for (var i = 0; i < this.listData.length; i++) {
        var id = "onl" + i;
        document.getElementById(id).onclick = function () {
            ext._keyClick(this.id);
        };
    }
};
ListOnline.prototype.dFilter = function (id) {
    //Перезапуск таймера я еще здесь
    StartTimeOut();
    for (var i = 0; i < this.listData.length; i++) {
        document.getElementById('onl' + i + "_active").style.backgroundImage = "url('img/listonline.png')";
    }

    if(this._setData(id)) {
        document.getElementById(id + "_active").style.backgroundImage = "url('img/listonline_select.png')";
    } else {
        var text = "Максимальная сумма платежа не может превышать 15000 тыс. руб";
        OnlineDiv('loadingdivmsgyesnomenu', 1, text);
        OnlineDiv('msgnobutton', 1);
        OnlineDiv('msgyesbutton', 0);
        OnlineDiv('msgmenubutton', 1);
    }

    if (this.ManageMask.checkValid()) {
        Pages.Steps.button.next.show();
    } else {
        Pages.Steps.button.next.hide();
    }
};
/**
 * Формирование массива данных this.data
 * @param id string айди выбранного списка
 * @private
 */
ListOnline.prototype._setData = function (id) {
    var value = this.listData[id.substr(3)].Value;
    var list = this.getSelectListOnline(value);
    var access = false;
    this.data = [];
    //Проверка, если сумма больше 15000, список невозможно выбрать
    if(list.FixedSum <= 15000) {
        this.data.push(value);
        access = true;
    }
    return access;
};
/**
 * Устанавливает выбранное значение
 * @param value значение структуры списка Value
 */
ListOnline.prototype.setValue = function (value) {
//по каким-то приминам value.length undefined - не ясно почему
    //if (value.length > 0) {

    for (var i = 0; i < this.listData.length; i++) {
        if (this.listData[i].Value == value) {

            //считаем на какой странице скрола находится выбранный список
            var count_scroll = (i + 1 ) / this.elem_for_scroll;
            if (count_scroll > 1) {
                this.scroll.scrollListDown(Math.floor(count_scroll));
            }
            this.dFilter('onl' + i);
            break;
        }
        //}
    }
};
/**
 * Возвращает выбранный список
 * @returns {FixedSum|*}
 */
ListOnline.prototype.getSelectListOnline = function (value) {
    for (var i = 0; i < this.listData.length; i++) {
        if (this.listData[i].Value == value) {
            return this.listData[i];
        }
    }
};
/**
 * Возвращаем сумму по выбранному елементу
 * @returns {FixedSum|*}
 */
ListOnline.prototype.getFixedSum = function () {
    var value = this.ManageMask.getData();
    for (var i = 0; i < this.listData.length; i++) {
        if (this.listData[i].Value == value) {
            return this.listData[i].FixedSum;
        }
    }
};

/**
 * Шаг типа лист
 * @constructor
 */
function List() {
    var ext = this;
    //Количество видимых елементов в списке
    this.elem_for_scroll = 9;
    this.listData = [];
    this.data = [];
    this.scroll = new ListScroll("wlist_element");
    this.ManageMask = {
        getData: function () {
            if (ext.data.length > 0)
                return ext.data[0];
            else
                return '';
        },
        getValueParseMask: function () {
            for (var i = 0; i < ext.listData.length; i++) {
                if (ext.data[0] == ext.listData[i].Value) {
                    return ext.listData[i].Key;
                }
            }
        },
        checkValid: function () {
            if (ext.data.length > 0)
                return true;
            else
                return false;
        }
    };
    this.stepinfo_id = '';
    this.stepinfofull_id = '';
}
List.prototype = inherit(listBoard);

List.prototype.init = function (container, stepinfoID, stepinfofullID) {
    var ext = this;
    document.getElementById(container).innerHTML = this.create();
    this.stepinfo_id = stepinfoID;
    this.stepinfofull_id = stepinfofullID;

    this.scroll.height = 600;
    this.scroll.speed = 600;
    this.scroll.init();

    try {
        document.getElementById('button_up').onclick = function () {
            WNewEffects.click.button('button_up');
            ext.scroll.onclick_up();
        }
    } catch (e) {
        SaveExceptionToLog(e, "list.js", "ListOnline.prototype.init, button_up");
    }

    try {
        document.getElementById('button_down').onclick = function () {
            WNewEffects.click.button('button_down');
            ext.scroll.onclick_down();
        }
    } catch (e) {
        SaveExceptionToLog(e, "list.js", "ListOnline.prototype.init, button_down");
    }

    this._start();
};
/**
 * Устанавливает выбранное значение
 * @param value значение структуры списка Value
 */
List.prototype.setValue = function (value) {
//по каким-то приминам value.length undefined - не ясно почему
    for (var i = 0; i < this.listData.length; i++) {
        if (this.listData[i].Value == value) {

            //считаем на какой странице скрола находится выбранный список
            var count_scroll = (i + 1 ) / this.elem_for_scroll;
            if (count_scroll > 1) {
                this.scroll.scrollListDown(Math.floor(count_scroll));
            }
            this.dFilter('lst' + i);
            break;
        }
    }
};




/**
 * Шаг типа лист
 * @constructor
 */
function ListGoods() {
    var ext = this;
    //Количество видимых елементов в списке
    this.elem_for_scroll = 9;
    this.listData = [];
    this.data = [];
    this.scroll = new ListScroll("wlist_element");
    this.ManageMask = {
        getData: function () {
            if (ext.data.length > 0)
                return ext.data[0];
            else
                return '';
        },
        getValueParseMask: function () {
            for (var i = 0; i < ext.listData.length; i++) {
                if (ext.data[0] == ext.listData[i].Value) {
                    return ext.listData[i].Key;
                }
            }
        },
        checkValid: function () {
            if (ext.data.length > 0)
                return true;
            else
                return false;
        }
    };
    this.stepinfo_id = '';
    this.stepinfofull_id = '';
}
ListGoods.prototype = inherit(listBoard);

ListGoods.prototype.init = function (container, stepinfoID, stepinfofullID) {
    var ext = this;
    document.getElementById(container).innerHTML = this.create();
    this.stepinfo_id = stepinfoID;
    this.stepinfofull_id = stepinfofullID;

    this.scroll.height = 600;
    this.scroll.speed = 600;
    this.scroll.init();

    try {
        document.getElementById('button_up').onclick = function () {
            WNewEffects.click.button('button_up');
            ext.scroll.onclick_up();
        }
    } catch (e) {
        SaveExceptionToLog(e, "list.js", "ListOnline.prototype.init, button_up");
    }

    try {
        document.getElementById('button_down').onclick = function () {
            WNewEffects.click.button('button_down');
            ext.scroll.onclick_down();
        }
    } catch (e) {
        SaveExceptionToLog(e, "list.js", "ListOnline.prototype.init, button_down");
    }

    this._start();
};
/**
 * Устанавливает выбранное значение
 * @param value значение структуры списка Value
 */
ListGoods.prototype.setValue = function (value) {
//по каким-то приминам value.length undefined - не ясно почему
    for (var i = 0; i < this.listData.length; i++) {
        if (this.listData[i].Value == value) {

            //считаем на какой странице скрола находится выбранный список
            var count_scroll = (i + 1 ) / this.elem_for_scroll;
            if (count_scroll > 1) {
                this.scroll.scrollListDown(Math.floor(count_scroll));
            }
            this.dFilter('lst' + i);
            break;
        }
    }
};
