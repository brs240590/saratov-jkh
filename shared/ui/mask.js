/*
 Проверка введенны все симоволы или нет
 сравнивавается this.data.length >= this.minLength
 */
function WMask() {
    this.mask = '';
    //Обязательная длина строки согласно маски
    this.minLength = 0;
    this.maxLength = 0;
    //Маска в массиве
    this.maskArray = [];
    //введенные данные массивом согласно шаблона
    this.dataMask = [];
    //введенные данные в виде массива без шаблона
    this.data = [];
    //размер шрифта в инпуте по умолчанию
    this.defaultFontSize = 110;
    //текущий размер в инпуте
    this.currentFontSize = 110;
    //количество символов которые помещаются в инпуте
    this.countVisibleSybmol = 25;

}
/**
 * Считает размер шрифта в зависимости от количаства символов
 */
WMask.prototype.calcCurrentFontSize = function () {
    var val_count = this.getDataMask().length;
    //если количество символов больше - считаем новый размер
    if (val_count > this.countVisibleSybmol) {
        var diff = val_count - this.countVisibleSybmol;
        this.currentFontSize = this.defaultFontSize - diff;
    } else {
        this.currentFontSize = this.defaultFontSize;
    }
};
/**
 * Возвращает размер шрифта
 * @returns {number}
 */
WMask.prototype.getCurrentFontSize = function () {
    return this.currentFontSize + "px";
};
WMask.prototype.checkCountEnter = function() {
    if(this.getData().length == this.maxLength)
        return true;
    return false;
};
WMask.prototype._init = function (mask) {
    var data = [];
    parseMask(this);
    //Парсим маску
    function parseMask(parent) {
        for (var i = 0; i < mask.length; i++) {
            //равно обязательный символ
            var char = mask.charAt(i);
            if (char == '=') {
                parent.minLength++;
                parent.maxLength++;
            } else if (char == '*' || char == '#') {
                parent.maxLength++;
            }
            if (char == '#' || char == '=')
                data.push('*');
            else
                data.push(char);
        }
        //Если нет обязательных символов, устанавливаем вручную минимальные введенные 1
        //if (parent.minLength == 0)
          //  parent.minLength = 1;
    }

    return data;

};
/**
 * Парсим согласно маске посимвольно
 * @param symbol
 * @private
 */
WMask.prototype._parse = function (symbol) {
    //Если длина введенных данных меньше максимальной длины строки
    if (this.data.length < this.maxLength) {
        for (var i = 0; i < this.maskArray.length; i++) {
            if (this.dataMask.length > i)
                continue;
            if (this.maskArray[i] == "*") {
                this.dataMask[i] = symbol;
                this.data.push(symbol);
                break;
            }
            this.dataMask.push(this.maskArray[i]);
        }
    }
    this.calcCurrentFontSize();
};
//Удаляет один символ
WMask.prototype.backspace = function () {
    if (this.data.length > 0) {
        var newData = this.data.slice(0, this.data.length - 1);
        //удаляем один елемент
        this.clear();
        for (var i = 0; i < newData.length; i++) {
            this._parse(newData[i]);
        }
    }
    return this.getDataMask();
};
/**
 * парсим согласно маске всю строку
 * @param value
 * @returns {string}
 */
WMask.prototype.setValue = function (value) {
    this.clear();
    SaveLog("uiElement.setValue: " + value);
    for (var i = 0; i < value.length; i++) {
        this._parse(value.charAt(i));
    }
    return this.getDataMask();
};

/**
 * Сравнивает минимальную длину строки данных и с длинной введенных
 * @return bool
 */
WMask.prototype.checkValid = function () {
    if (this.data.length >= this.minLength)
        return true;
    else
        false;
};
WMask.prototype.clear = function () {
    this.data = [];
    this.dataMask = [];
};
/**
 * Возвращает строку форматированную под маску
 * @returns {string}
 */
WMask.prototype.getDataMask = function () {
    return this.dataMask.join('');
};
/**
 * Возврщает строку форматированную под маску. От предыдущего метода отличается,
 * что в потомках именно строка будет возвращается в переопределенных методах
 * @returns {string}
 */
WMask.prototype.getValueParseMask = function() {
    return this.dataMask.join('');
};
/**
 * возвращает сырую строку введенную пользователем
 */
WMask.prototype.getData = function () {
    return this.data.join('');
};
WMask.prototype.echo = function () {
    console.log(this.maskArray.join('') + " min: " + this.minLength + " max: " + this.maxLength);
    console.log(this.dataMask.join('') + " min: " + this.minLength + " max: " + this.maxLength);
};
WMask.prototype.filter = function (symbol) {

};
var abstractMask = new WMask();

//Маска для намбера
function NumberMask(mask) {
    this.maskArray = this._init(mask);
    //содержит данные введенные пользователем согласно маске
    this.dataMask = [];
    //введенные данные
    this.data = [];
    //размер шрифта в инпуте по умолчанию
    this.defaultFontSize = 110;
    //текущий размер в инпуте
    this.currentFontSize = 110;
    //количество символов которые помещаются в инпуте
    this.countVisibleSybmol = 25;
}
NumberMask.prototype = inherit(abstractMask);

NumberMask.prototype.filter = function (symbol) {
    //что-то делаем с введенными данными
    this._parse(symbol);
};

//Маска для намбер8
function Number8Mask(mask) {
    this.maskArray = this._init(mask);
    //содержит данные введенные пользователем согласно маске
    this.dataMask = [];
    //введенные данные
    this.data = [];
    //размер шрифта в инпуте по умолчанию
    this.defaultFontSize = 110;
    //текущий размер в инпуте
    this.currentFontSize = 110;
    //количество символов которые помещаются в инпуте
    this.countVisibleSybmol = 25;
}
Number8Mask.prototype = inherit(abstractMask);

Number8Mask.prototype.filter = function (symbol) {
    //если первая восьмерка нечего не делаем
    if (this.data.length == 0 && symbol == 8) {
        return;
    }
    this._parse(symbol);
};
/**
 * Добавляет к маске 8
 * @returns {string}
 */
Number8Mask.prototype.setMaskWithNumber8 = function () {
    var mask = [];
    for (var i = 0; i < this.maskArray.length; i++) {
        if (this.maskArray[i] == "*") {
            var symb = this.dataMask[i];
            if (symb != undefined) {
                mask[i] = this.dataMask[i];
                continue;
            }
        }
        mask.push(this.maskArray[i]);
    }
    return "8" + mask.join('');
};
/**
 * Возвращает строку форматированную под маску
 * @returns {string}
 */
Number8Mask.prototype.getDataMask = function () {
    return this.setMaskWithNumber8();
};
Number8Mask.prototype.getValueParseMask = function() {
    return this.setMaskWithNumber8();
};


/**
 * Маска для фикседсам
 * @param mask
 * @constructor
 */
function NumberFixedSumMask(minSum, maxSum) {
    this.MinSum = minSum;
    this.MaxSum = maxSum;
    var mask = '########';
    this.maskArray = cutMask(this._init(mask));
    //содержит данные введенные пользователем согласно маске
    this.separator = this.maskArray.join('').indexOf(".");

    this.dataMask = [];
    //целая часть
    this.dataMask[1] = [];
    //Дробная часть
    this.dataMask[2] = [];
    //введенные данные
    this.data = [];

    function cutMask(mask) {
        var newmask = [];
        for (var i = 0; i < mask.length; i++) {

            if (i == (mask.length - 3)) {
                newmask.push(".");
            } else {
                newmask.push(mask[i]);
            }
        }
        return newmask;
    }

    function toArray(value) {
        var data = [];
        for (var i = 0; i < value.length; i++) {
            data.push(value[i]);
        }
        return data;
    }
}
NumberFixedSumMask.prototype = inherit(abstractMask);

/**
 * Удаляет один символ
 * @param type что удалять дробную или целу часть 1 - целая 2 дробная
 * @returns {*}
 */
NumberFixedSumMask.prototype.backspace = function (type) {
    if (this.data.length > 0) {
        var newData = this.dataMask[type].slice(0, this.dataMask[type].length - 1);
        this.dataMask[type] = [];
        //удалили последний символ
        if (newData.length > 0) {
            for (var i = 0; i < newData.length; i++) {
                this._parse(newData[i], type);
            }
        } else {
            this.formData();
        }
    }
    return this.getDataMask();
};

NumberFixedSumMask.prototype.clear = function () {
    this.dataMask[1] = [];
    this.dataMask[2] = [];
    this.data = [];
};

/**
 * Проверяет значение поля согласно фильтру
 * @param symbol введенное число
 * @param type 1 - рубль, 2 - копейки
 */
NumberFixedSumMask.prototype.filter = function (symbol, type) {
    //Если первое число 0 в рублях - можем вводить только копейки
    if (type == 1 && this.dataMask[type][0] == 0) {
        return;
    }
    //Если длина введенных данных меньше максимальной длины строки
    if (this.data.length < this.maxLength) {
        this._parse(symbol, type);
    }
};
NumberFixedSumMask.prototype._parse = function (symbol, type) {
    //Маска для целой части
    var wholeMask = this.maskArray.slice(0, this.maskArray.length - 3);
    //Mаска для дробной части
    var ceilMask = this.maskArray.slice(-2);

    var ext = this;
    if (type == 1) {
        for (var i = 0; i < wholeMask.length; i++) {
            if (this.dataMask[type].length > i)
                continue;
            if (wholeMask[i] == "*") {
                this.dataMask[type].push(symbol);
                break;
            }
        }
    } else if (type == 2) {
        for (var i = 0; i < ceilMask.length; i++) {
            if (this.dataMask[type].length > i)
                continue;
            if (ceilMask[i] == "*") {
                this.dataMask[type].push(symbol);
                break;
            }
        }
    }
    this.formData();
};
/**
 * Формирует массив сырых данных
 */
NumberFixedSumMask.prototype.formData = function () {
    var dataStr = '';
    this.data = [];
    if (this.dataMask[2].length > 0) {
        dataStr = this.dataMask[1].join('') + "." + this.dataMask[2].join('');
    } else {
        dataStr = this.dataMask[1].join('');
    }
    for (var i = 0; i < dataStr.length; i++) {
        this.data.push(dataStr.charAt(i));
    }
};
/**
 * Отдаем итоговое значение
 * @returns {string}
 */
NumberFixedSumMask.prototype.getData = function () {
    var dataStr = '';
    //если есть дробная часть
    if (this.dataMask[2].length > 0) {
        //если только 1 число
        if (this.dataMask[2].length == 1) {
            //если это число равно 0
            if (this.dataMask[2][0] == 0) {
                dataStr = this.dataMask[1].join('') + "." + "00";
            } else {
                dataStr = this.dataMask[1].join('') + "." + this.dataMask[2].join('') + "0";
            }
        } else {
            //дробная часть есть полностью
            dataStr = this.dataMask[1].join('') + "." + this.dataMask[2].join('');
        }
    } else {
        //если есть целая часть
        if (this.dataMask[1].length > 0) {
            dataStr = this.dataMask[1].join('') + "." + "00";
        } else {
            //dataStr = "0.00";
        }
    }
    return dataStr;
};
NumberFixedSumMask.prototype.setValue = function (value) {
    this.clear();
    var tmp = value.split(".");

    for (var i = 0; i < tmp[0].length; i++) {
        this._parse(tmp[0].charAt(i), 1);
    }
    if (tmp.length > 1) {
        for (var j = 0; j < tmp[1].length; j++) {
            this._parse(tmp[1].charAt(j), 2);
        }
    }
    return this.getDataMask();
};
NumberFixedSumMask.prototype.checkValid = function () {
    var data = this.getData();
    var check = false;
    if (data.length > 0) {
        if (Number(data) >= Number(this.MinSum) && Number(data) <= Number(this.MaxSum)) {
            check = true;
        }
    } else {
        check = false;
    }
    return check;
    //Если введены рубли
};
NumberFixedSumMask.prototype.getDataMask = function () {
    var dmask = [];
    dmask[1] = this.dataMask[1].join('');
    dmask[2] = this.dataMask[2].join('');
    return dmask;
};
NumberFixedSumMask.prototype.getValueParseMask = function() {
    return this.getData() + " руб.";
};


//Маска для намбера
function NumberStaticKomMask(mask) {
    this.maskArray = this._init(mask);
    //содержит данные введенные пользователем согласно маске
    this.dataMask = [];
    //введенные данные
    this.data = [];
    //размер шрифта в инпуте по умолчанию
    this.defaultFontSize = 60;
    //текущий размер в инпуте
    this.currentFontSize = 60;
    //количество символов которые помещаются в инпуте
    this.countVisibleSybmol = 25;
}
NumberStaticKomMask.prototype = inherit(abstractMask);

NumberStaticKomMask.prototype.filter = function (symbol) {
    //что-то делаем с введенными данными
    this._parse(symbol);
};
NumberStaticKomMask.prototype.getValueParseMask = function() {
    var dat = this.getData();
    var fio = "";
    for(var i = 0; i < dataKom.length; i++) {
        if(dataKom[i].num == dat) {
            fio = "</br>Фио: <span class='confirm_maskvalue'>"+dataKom[i].fio+"</span>";
            break;
        }
    }

    return this.dataMask.join('') + fio;
};



/**
 * Парсер маски для типа степа кейборд и кейборденг
 * @param mask
 * @constructor
 */
function KeyboardMask(mask) {
    this.maskArray = this._init(mask);
    //содержит данные введенные пользователем согласно маске
    this.dataMask = [];
    //введенные данные
    this.data = [];
    //размер шрифта в инпуте по умолчанию
    this.defaultFontSize = 60;
    //текущий размер в инпуте
    this.currentFontSize = 60;
    //количество символов которые помещаются в инпуте
    this.countVisibleSybmol = 25;
}
KeyboardMask.prototype = inherit(abstractMask);
KeyboardMask.prototype.filter = function (key) {
    this._parse(key);
};

function FioboxMask(mask) {
    mask = "############################";
    this.maskArray = this._init(mask);
    //содержит данные введенные пользователем согласно маске
    this.dataMask = [];
    this.dataMask[1] = [];
    this.dataMask[2] = [];
    this.dataMask[3] = [];
    this.data = [];

    //размер шрифта в инпуте по умолчанию
    this.defaultFontSize = 60;
    //текущий размер в инпуте
    this.currentFontSize = 60;
    //количество символов которые помещаются в инпуте
    this.countVisibleSybmol = 25;

}
FioboxMask.prototype = inherit(abstractMask);
FioboxMask.prototype.clear = function () {
    this.data = [];
    //фамилия
    this.dataMask[1] = [];
    //имя
    this.dataMask[2] = [];
    //отчество
    this.dataMask[3] = [];
};
/**
 *
 * @param symbol
 * @param type int 1 - фамилия, 2 имя, 3 отчество
 * @private
 */
FioboxMask.prototype._parse = function (symbol, type) {    
    	
    if (type == 2) {
    if (symbol >= 0 && symbol <= 9) {
    if (this.dataMask[type].length < 10) {
    if (this.dataMask[type].length == 2 || this.dataMask[type].length == 5) {
        this.dataMask[type].push('.');
        this.dataMask[type].push(symbol);
    } else {
           this.dataMask[type].push(symbol);
           }
                                  }
                                }
                   } 
     else if (type == 1) {
                         if (this.dataMask[type].length < 10) {
                                                              this.dataMask[type].push(symbol);
                                                              }
                         }
			  else {
                          this.dataMask[type].push(symbol);
                          }
this.getData();
};
FioboxMask.prototype.getData = function () {
    var fio = '';
    if (this.dataMask[1].length > 0)
        fio += this.dataMask[1].join('');

    if (this.dataMask[2].length > 0)
        fio += " " + this.dataMask[2].join('');


//    if (this.dataMask[3].length > 0)
//        fio += " " + this.dataMask[3].join('');

    var gender = document.getElementById('m-option').checked;
    if (gender == true) {
                        fio += " 1";
                        } else {
                               fio += " 2";
                               }
    this.data = fio;
    return fio;
};
FioboxMask.prototype.backspace = function (type) {
//    this.dataMask[type].pop();
    if (type == 2) {
    if (this.dataMask[type].length == 4 || this.dataMask[type].length == 7) {
                                                                            this.dataMask[type].pop();
                                                                            this.dataMask[type].pop();
                                                                            } else {this.dataMask[type].pop(); }
                   } else { this.dataMask[type].pop(); }
    return this.getDataMask();
};
FioboxMask.prototype.checkValid = function () {
    var valid = true;
    var len = 0;
    //отчевство не обязательное
    for (var i = 1; i < this.dataMask.length-1; i++) {
        if (this.dataMask[i].length < 2) {
            valid = false;
            break;
        } else {
            len += this.dataMask[i].length;
        }
    }
    //длина фио не может превышать максимальную дозволенную длину
    if (len < this.minLength)
        valid = false;
    return valid;
};
FioboxMask.prototype.filter = function (key, type) {
    this._parse(key, type);
};
FioboxMask.prototype.setValue = function (value) {
    if (value.length > 0) {
        this.clear();
        var data = value.split(" ");
        //если заполненны все поля
        //if (data.length == 3) {
            for (var i = 0; i < data.length; i++) {
                for (var j = 0; j < data[i].length; j++) {
                    this.filter(data[i].charAt(j), i + 1);
                }
            }/*
        } else {
            alert('fioboxmask setvalue error');
        }*/
    }

    return this.getDataMask();
};
FioboxMask.prototype.getDataMask = function () {
    var data = [];
    data[1] = this.dataMask[1].join('');
    data[2] = this.dataMask[2].join('');
    data[3] = this.dataMask[3].join('');
    return data;
};
FioboxMask.prototype.getValueParseMask = function() {
    var data = this.getDataMask();
    return data[1] + ' ' + data[2] + ' ' + data[3];
};