function MaskFactory() {}
MaskFactory.prototype = {
    prototype: MaskFactory,
    makeMask: {
        number: function(mask) {
            return new NumberMask(mask);
        },
        number8: function(mask) {
            return new Number8Mask(mask);
        },
        keyboard: function(mask) {
            return new KeyboardMask(mask);
        },
        fixedsum: function(minSum, maxSum) {
            return new NumberFixedSumMask(minSum, maxSum);
        },
        fiobox: function(mask) {
            return new FioboxMask(mask);
        },
        statickom: function(mask) {
            return new NumberStaticKomMask(mask);
        }

    }
};