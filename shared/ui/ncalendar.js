/**
 * Created by lodar on 25/06/15.
 * Новый календарь для интерфейса
 */

function NCalender() {
    var ext = this;
    //может быть CALENDARMONTH - тогда не отображается дата
    this.type = 'CALENDAR';

    //флаги отвечающие за тип календаря
    this.is_calendar = true;
    this.is_calendarMonth = false;

    this.CONST_DAY = 'ДД';
    this.CONST_MONTH = 'ММ';
    this.CONST_YEAR = 'ГГГГ';

    //хранят выбранное значение в календаре
    this.day = 'ДД';
    //месяц хранится в значении от 0-11. Для удобства работы с обьектом Date
    this.month = 'ММ';
    this.year = 'ГГГГ';

    //управляющие переменные, используются для формирование календаря
    this.day_tmp = 'ДД';
    this.month_tmp = 'ММ';
    this.year_tmp = 'ГГГГ';

    this.ManageMask = {
        getData: function() {
            var _now = new Date();

            if(ext.day == 'ДД') {
                var _day = ext.convertDayShow(_now.getDate());
            } else {
                _day = ext.convertDayShow(ext.day);
            }
            if(ext.month == 'ММ') {
                var _month = ext.convertMonthShow(ext.getMonth());
            } else {
                _month = ext.convertMonthShow(ext.month);
            }

            if(ext.year == 'ГГГГ') {
                var _year = _now.getFullYear();
            } else {
                _year = ext.year;
            }

            if(ext.is_calendar)
                var ret = _day + "." + _month + "." + _year;
            else
                ret = _month + "." + _year;

            return ret;
        },
        checkValid: function() {
            var valid = false;

            //тип календарь
            if(ext.is_calendar) {
                if(ext.day != 'ДД' && ext.month != 'ММ' && ext.year != 'ГГГГ') {
                    valid = true;
                }
            } else if(ext.is_calendarMonth) {
                //календарьмантс
                if(ext.month != 'ММ' && ext.year != 'ГГГГ') {
                    valid = true;
                }
            }
            return valid;
        },
        getValueParseMask: function() {
            return this.getData();
        }
    };

    //блок где хранится
    this.container = '';
    this.prefix = '';
    this.init = false;

    this.SelectDate = new Date();

    this.count_year = 12;
    //устанавливаем стартовый год отсчета
    this.startYear = this.SelectDate.getFullYear() - this.count_year / 2;

    this.print_day_cal = 'print_day_cal';
    this.print_month_cal = 'print_month_cal';
    this.print_year_cal = 'print_year_cal';

    this.print_day_text = 'print_day_text';
    this.print_month_text = 'print_month_text';
    this.print_year_text = 'print_year_text';

    this.div_cal = 'div_cal';
    this.div_cal_child = 'div_cal_child';

    //календарь с списком дней
    this.td_cal_dayin_month = 'td_cal_dayin_';
    this.td_cal_dayin_month_head = 'td_cal_dayin_head';
    this.td_cal_dayin_month_back = 'td_cal_dayin_month_back';
    this.td_cal_dayin_month_next = 'td_cal_dayin_month_next';

    //годовой календарь
    this.td_cal_year = 'td_cal_year_';
    this.td_cal_year_head = 'td_cal_year_head';
    this.td_cal_year_next = 'td_cal_year_next';
    this.td_cal_year_back = 'td_cal_year_back';

    //месячный календарь
    this.td_cal_month = 'td_cal_month_';

    this.info = 'info_cal';
    this.infofull = 'infofull_cal';
    this.info_text = 'Укажите требуемую дату';
    this.info_full_text = '';
}
/**
 * Устанавливаем контейнер, где будет отображаться календарь
 * @param _container
 */
NCalender.prototype.setContainer = function(_container) {
    this.container = _container;
};

/**
 * Тип календаря - calendar
 * дд мм гггг
 */
NCalender.prototype.isCalender = function() {
    this.type = 'CALENDAR';
    this.is_calendar = true;
    this.is_calendarMonth = false;
};

/**
 * Тип календаря - calendarmonth
 * мм гггг
 */
NCalender.prototype.isCalenderMonth = function() {
    this.type = 'CALENDARMONTH';
    this.is_calendar = false;
    this.is_calendarMonth = true;
};
/**
 * Устанавливает префикс для текущего елемента
 * @param _prefix
 */
NCalender.prototype.setPrefix = function(_prefix) {
    this.prefix = _prefix + '_';
    this.print_day_cal = _prefix + 'print_day_cal';
    this.print_month_cal = _prefix + 'print_month_cal';
    this.print_year_cal = _prefix + 'print_year_cal';

    this.print_day_text = _prefix + 'print_day_text';
    this.print_month_text = _prefix + 'print_month_text';
    this.print_year_text = _prefix + 'print_year_text';

    this.div_cal = _prefix + 'div_cal';
    this.div_cal_child = _prefix + 'div_cal_child';

    this.td_cal_dayin_month = _prefix + 'td_cal_';
    this.td_cal_dayin_month_head = _prefix + 'td_cal_head';
    this.td_cal_dayin_month_back = _prefix + 'td_cal_dayin_month_back';
    this.td_cal_dayin_month_next = _prefix + 'td_cal_dayin_month_next';

    this.td_cal_year = _prefix + 'td_cal_year_';
    this.td_cal_year_head = _prefix + 'td_cal_year_head';
    this.td_cal_year_next = _prefix + 'td_cal_year_next';
    this.td_cal_year_back = _prefix + 'td_cal_year_back';

    //айди выбранного елемента года
    this.td_select_id = "";

    this.td_cal_month = _prefix + 'td_cal_month_';

    // блоки информации
    this.info = _prefix + 'info_cal';
    this.infofull = _prefix + 'infofull_cal';

    this.initialize();
};
NCalender.prototype.initialize = function() {

    try {
        var con = getElemByID(this.container);
        //не найден контейнер - инициализация не прошла
        if(con != null)
            this.init = true;
        else
            this.init = false;

        con.innerHTML = this.create();

        this.start();
    } catch(e) {
        this.init = false;
        SaveLog("NCalender.Init: " + e.toString());
    }
};

NCalender.prototype.start = function() {
    var ext = this;
    //получаем див где содержится календарь
    var el = getElemByID(this.div_cal);

    try {
        //инициализация прошла успешно, вешаем обработчики на дату
        if(this.init) {
            if(this.is_calendar) {
                var day = getElemByID(this.print_day_cal);
                day.onclick = function() {
                    ext.initDayCalendar();
                };
            }

            var month = getElemByID(this.print_month_cal);
            month.onclick = function() {
                ext.initMonthCalendar();
            };

            var year = getElemByID(this.print_year_cal);
            year.onclick = function() {
//                ext.initYearCalendar();
            };

        }
    } catch(e) {

    }
};

NCalender.prototype.create = function() {
    var cal_id = this.prefix + "ncal";

    var elem = document.getElementById(cal_id);
    if(elem != null)
        elem.parentNode.removeChild(elem);

    elem =
        //"<div  style='border: 1px solid red;'>" +
        "<table id='" + cal_id + "' align='center'><tr align='center'><td>" +

        "<div id='" + this.info + "' class='stepinfo'>" + this.info_text + "</div>" +
//день

        "<table><tr><td>" +
        "";

    if(this.is_calendar) {
        elem = elem +
            "<div class='div_day'>" +
            "<table id='" + this.print_day_cal + "'>" +
            "<tr>" +
            "<td class='winput_left_cal'></td>" +
            "<td class='winput_center_cal cal_input_size_font'><div id='" + this.print_day_text + "'>" + this.day + "</div></td>" +
            "<td class='winput_right_cal'></td>" +
            "</tr>" +
            "</table>" +
            "</div>";
    }

//месяц
    elem = elem + "<div class='div_month'>" +
        "<table id='" + this.print_month_cal + "'>" +
        "<tr>" +
        "<td class='winput_left_cal'></td>" +
        "<td class='winput_center_cal cal_input_size_font'><div id='" + this.print_month_text + "'>" + this.month + "</div></td>" +
        "<td class='winput_right_cal'></td>" +
        "</tr>" +
        "</table>" +
        "</div>" +
//год
        "<div class='div_year'>" +
        "<table id='" + this.print_year_cal + "'>" +
        "<tr>" +
        "<td class='winput_left_cal'></td>" +
        "<td class='winput_center_cal cal_input_size_font'><div id='" + this.print_year_text + "'>" + this.year + "</div></td>" +
        "<td class='winput_right_cal'></td>" +
        "</tr>" +
        "</table>" +
        "</div>" +
        "<div class='div_clear'></div>" +
        "" +
        "</td></tr>" +
        "" +
        "<tr><td>" +
        "<div id='" + this.infofull + "' class='stepinfo'>" + this.info_full_text + "&nbsp;</div>" +
        "</td></tr>" +
        "</table>" +

        "<div class='div_calender' id='" + this.div_cal + "'>" +
        "" +
        "</div>" + //календарь, меняется от выбора инпута

        "<div class='div_clear'></div>" +


            //"</div></td></tr></table>";
        "</td></tr></table>";

    return elem;
};
/**
 * Обновляем значение выбранной даты в зависимости от указанных частей даты
 */
NCalender.prototype.updateSelectDate = function() {
    this.calcDayCalender();
    this.SelectDate = new Date(this.year_tmp, this.month_tmp);
};
/**
 * Инициализируем годовой каледарь
 * Устанавливаем выбранное значение активным
 */
NCalender.prototype.initYearCalendar = function() {
    var ext = this;

    try {
        //получаем див где содержится календарь
        var el = getElemByID(this.div_cal);
        var cal = document.createElement("div");
        cal.id = ext.div_cal_child;
        cal.innerHTML = ext.createYearCalendar();

        el.appendChild(cal);

        //вешаем обработчики
        this.setOnClickYearCalendar();
        //устанавливаем активный елемент, если выбран месяц
        this.updateYearValue();
    } catch(error) {
        SaveLog("NCalender.prototype.initYearCalendar: " + error.message);
    }
};
/**
 * Обработчик на нажатие назад в списке годов
 */
NCalender.prototype.initNextYearCalendar = function() {
    this.startYear = this.startYear + this.count_year;
    this.initYearCalendar();
};
/**
 * Обработчик на нажатие вперед в списке годов
 */
NCalender.prototype.initBackYearCalendar = function() {
    this.startYear = this.startYear - this.count_year;
    this.initYearCalendar();
};
/**
 * Создает разметку для годового календаря
 * @returns {string}
 */
NCalender.prototype.createYearCalendar = function() {

    var div_el = getElemByID(this.div_cal_child);
    if(div_el != null) {
        div_el.parentNode.removeChild(div_el);
    }

    //формируем список лет
    var list = [];

    var elem = "<div class='div_table_cal_year'>" +
        "<table cellpadding='0' cellspacing='0' class='cal_table_year'>" +

        "<tr class='tr_1_cal'>" +
        "<td colspan='3'>" +
        "<table class='cal_head_link'>" +
        "<tr>" +
        "<td class='td_back' id='" + this.td_cal_year_back + "'></td>" +
        "<td class='td_center' id='" + this.td_cal_year_head + "'></td>" +
        "<td class='td_next' id='" + this.td_cal_year_next + "'></td>" +
        "</tr>" +
        "</table>" +

        "</td>" +
        "</tr>" +

        "<tr class='tr_year'>" +
        "<td class='left year' id='" + this.td_cal_year + "0'></td>" +
        "<td class='left year' id='" + this.td_cal_year + "1'></td>" +
        "<td class='right year' id='" + this.td_cal_year + "2'></td>" +
        "</tr>" +

        "<tr class='tr_year'>" +
        "<td class='left year' id='" + this.td_cal_year + "3'></td>" +
        "<td class='left year' id='" + this.td_cal_year + "4'></td>" +
        "<td class='right year' id='" + this.td_cal_year + "5'></td>" +
        "</tr>" +

        "<tr class='tr_year'>" +
        "<td class='left year' id='" + this.td_cal_year + "6'></td>" +
        "<td class='left year' id='" + this.td_cal_year + "7'></td>" +
        "<td class='right year' id='" + this.td_cal_year + "8'></td>" +
        "</tr>" +


        "<tr class='tr_year'>" +
        "<td class='left year' id='" + this.td_cal_year + "9'></td>" +
        "<td class='left year' id='" + this.td_cal_year + "10'></td>" +
        "<td class='right year' id='" + this.td_cal_year + "11'></td>" +
        "</tr>" +
        "";


    var elem = elem + "</table></div>";

    return elem;
};
NCalender.prototype.setOnClickYearCalendar = function() {
    var ext = this;
    try {
        //обработчик на кнопку вперед
        getElemByID(this.td_cal_year_next).onclick = function() {
            ext.initNextYearCalendar();
        };
        //обработчик на кнопку назад
        getElemByID(this.td_cal_year_back).onclick = function() {
            ext.initBackYearCalendar();
        };

        clear();

        var start_year = this.startYear;
        //формируем хеддер для годового календаря
        var text_head = start_year + " - ";

        for(var i = 0; i < this.count_year; i++) {
            var el = getElemByID(this.td_cal_year + i);
            el.style.backgroundImage = '';
            el.style.color = 'black';
            el.innerHTML = start_year++;
            el.onclick = function() {
                clear();
                ext.year = this.innerHTML;
                ext.year_tmp = this.innerHTML;
                ext.updateYearValue();
            }
        }
        text_head = text_head + --start_year;

        getElemByID(this.td_cal_year_head).innerHTML = text_head;

    } catch(error) {
        SaveLog("NCalender.prototype.setOnClickYearCalendar:" + error.message);
    }

    //очищаем оформление
    function clear() {
        for(var i = 0; i < 12; i++) {
            var el = getElemByID(ext.td_cal_year + i);
            el.style.backgroundImage = '';
            el.style.color = 'black';
        }
    }
};
/**
 * Обновляет инпут в зависимости от выбранного значения
 */
NCalender.prototype.updateYearValue = function() {
    if(this.year != 'ГГГГ') {
        for(var i = 0; i < 12; i++) {
            var el = getElemByID(this.td_cal_year + i);
            if(el.innerHTML == this.year) {
                this.setYearValue(this.year);
                this.setActiveSelectYear(el.id);
                break;
            }
        }
    }
};
/**
 * Устанавливает значение в инпуте
 * Устанавливает год в календаре
 * @param val
 */
NCalender.prototype.setYearValue = function(val) {
    getElemByID(this.print_year_text).innerHTML = val;
    this.year = val;
    this.dFilter();
    this.updateSelectDate();
};
/**
 * Устанавливаем активным выбранный год
 * @param id
 */
NCalender.prototype.setActiveSelectYear = function(id) {
    var el = getElemByID(id);
    //для января подложка другая
    el.style.backgroundImage = "url('img/calendar_year_press.png')";
    el.style.backgroundRepeat = 'no-repeat';
    el.style.color = "black";
};

/**
 * Инициализируем месячный каледарь
 * Устанавливаем выбранное значение активным
 */
NCalender.prototype.initMonthCalendar = function() {

    try {
        //получаем див где содержится календарь
        var el = getElemByID(this.div_cal);

        var cal = document.createElement("div");
        cal.id = this.div_cal_child;
        cal.innerHTML = this.createMonthCalendar();
        //добавляем разметку в дом
        el.appendChild(cal);
        //вешаем обработчики
        this.setOnClickMonthCalendar();
        //устанавливаем активный елемент, если выбран месяц
        this.updateMonthValue();
    } catch(error) {
        SaveLog("NCalender.prototype.initMonthCalendar: " + error.message);
    }
};
/**
 * Создает разметку для месячного календаря
 * @returns {string}
 */
NCalender.prototype.createMonthCalendar = function() {

    var div_el = getElemByID(this.div_cal_child);
    if(div_el != null) {
        div_el.parentNode.removeChild(div_el);
    }

    var elem = "<div class='div_table_cal_all_month'>" +
        "<table cellpadding='0' cellspacing='0' class='cal_table_all_month'>" +
        "<tr class='tr_all_month'>" +
        "<td class='left all_month' id='" + this.td_cal_month + "0'>Январь</td>" +
        "<td class='left all_month' id='" + this.td_cal_month + "1'>Февраль</td>" +
        "<td class='right all_month' id='" + this.td_cal_month + "2'>Март</td>" +
        "</tr>" +

        "<tr class='tr_all_month'>" +
        "<td class='left all_month' id='" + this.td_cal_month + "3'>Апрель</td>" +
        "<td class='left all_month' id='" + this.td_cal_month + "4'>Май</td>" +
        "<td class='right all_month' id='" + this.td_cal_month + "5'>Июнь</td>" +
        "</tr>" +

        "<tr class='tr_all_month'>" +
        "<td class='left all_month' id='" + this.td_cal_month + "6'>Июль</td>" +
        "<td class='left all_month' id='" + this.td_cal_month + "7'>Август</td>" +
        "<td class='right all_month' id='" + this.td_cal_month + "8'>Сентябрь</td>" +
        "</tr>" +

        "<tr class='tr_all_month'>" +
        "<td class='left all_month' id='" + this.td_cal_month + "9'>Октябрь</td>" +
        "<td class='left all_month' id='" + this.td_cal_month + "10'>Ноябрь</td>" +
        "<td class='right all_month' id='" + this.td_cal_month + "11'>Декабрь</td>" +
        "</tr>" +

        "</table>" +
        "</div>";

    return elem;
};
/**
 * Вешает обработчики на месячный календарь
 */
NCalender.prototype.setOnClickMonthCalendar = function() {
    var ext = this;

    try {
        for(var i = 0; i < 12; i++) {
            var el = getElemByID(this.td_cal_month + i);

            el.onclick = function() {
                clear();
                var sp = this.id.split('_');
                ext.month = sp[3];
                ext.month_tmp = sp[3];
                //обновляем значение
                ext.updateMonthValue();
            }
        }
    } catch(error) {
        SaveLog("NCalender.prototype.setOnClickMonthCalendar:" + error.message);
    }
    //очищаем оформление
    function clear() {
        for(var i = 0; i < 12; i++) {
            var el = getElemByID(ext.td_cal_month + i);
            el.style.backgroundImage = '';
            el.style.color = 'black';
        }
    }
};
/**
 * Обновляет инпут в зависимости от выбранного значения
 */
NCalender.prototype.updateMonthValue = function() {
    try {
        if(this.month != 'ММ') {
            this.setActiveSelectMonth(this.td_cal_month + this.month);
            //так как нумерация месяцев с 0-11, формируем для отображения
            this.setMonthValue(this.month);
        }
    } catch(error) {
        SaveLog("NCalender.prototype.updateMonthValue: " + error.message);
    }
};
/**
 * Устанавливает значение в инпуте
 * Устанавливает год в календаре
 * @param val
 */
NCalender.prototype.setMonthValue = function(val) {
    var mon = parseInt(val) + 1;
    getElemByID(this.print_month_text).innerHTML = (mon < 10) ? "0" + mon : mon;
    this.month = parseInt(val);

    this.updateSelectDate();

    if(this.is_calendar) {
        if(this.day != 'ДД') {
            //определяю последний день месяца
            var count_day = getLastDayOfMonth(this.SelectDate.getFullYear(), this.SelectDate.getMonth());
            if(parseInt(this.day) > count_day) {
                this.setDayValue('ДД');
            }
        }
    }
    this.dFilter();
};
/**
 * Устанавливаем активным выбранный месяц
 * @param id
 */
NCalender.prototype.setActiveSelectMonth = function(id) {
    var el = getElemByID(id);
    var sp = id.split('_');

    var num = sp[3];

    if(num == 0) {
        //для января подложка другая
        el.style.backgroundImage = "url('img/calendar_month_press_0.png')";
    } else if(num == 2) {
        //для марта тоже
        el.style.backgroundImage = "url('img/calendar_month_press_2.png')";
    } else {
        el.style.backgroundImage = "url('img/calendar_month_press.png')";
    }
    el.style.backgroundRepeat = 'no-repeat';
    el.style.color = "#00bebc";
};
/**
 * Инициализируем дневной каледарь
 * Устанавливаем выбранное значение активным
 */
NCalender.prototype.initDayCalendar = function(updateStatus) {
    var ext = this;
    try {
        //получаем див где содержится календарь
        var el = getElemByID(this.div_cal);
        var cal = document.createElement("div");
        cal.id = ext.div_cal_child;
        cal.innerHTML = ext.createDayCalendar();
        //
        el.appendChild(cal);

        //вешаем обработчики
        this.setOnClickDayCalendar();

        //устанавливаем активный елемент, если выбран месяц
        if(updateStatus === undefined)
            this.updateDayValue();

        //формируем лейбл в ячейках
        //ext.formDate();
        //формируем хедер месячного календаря
        //ext.setDayCalendarHead();
    } catch(error) {
        SaveLog("NCalender.prototype.initDayCalendar: " + error.message);
    }
};
/**
 * Обработчик на нажатие назад в списке годов
 */
NCalender.prototype.initNextDayCalendar = function() {
    this.calcDayCalender();

    if((parseInt(this.month_tmp) + 1) > 11) {
        this.month_tmp = 0;
        this.year_tmp = parseInt(this.year_tmp) + 1;
    } else {
        this.month_tmp = parseInt(this.month_tmp) + 1;
    }
    this.initDayCalendar(false);
};
/**
 * Расчитывает временные интервалы для календаря
 */
NCalender.prototype.calcDayCalender = function() {
    if(this.month_tmp == 'ММ' && this.month == 'ММ')
        this.month_tmp = new Date().getMonth();
    else if(this.month_tmp == 'ММ' && this.month != 'ММ')
        this.month_tmp = this.month;

    if(this.year_tmp == 'ГГГГ' && this.year == 'ГГГГ')
        this.year_tmp = new Date().getFullYear();
    else if(this.year_tmp == 'ГГГГ' && this.year != 'ГГГГ')
        this.year_tmp = this.year;
};
/**
 * Обработчик на нажатие вперед в списке годов
 */
NCalender.prototype.initBackDayCalendar = function() {
    this.calcDayCalender();
    if((parseInt(this.month_tmp) - 1) < 0) {
        this.month_tmp = 11;
        this.year_tmp = parseInt(this.year_tmp) - 1;
    } else {
        this.month_tmp = parseInt(this.month_tmp) - 1;
    }
    this.initDayCalendar(false);
};
NCalender.prototype.createDayCalendar = function() {
    var date_now = new Date();

    var div_el = getElemByID(this.div_cal_child);
    if(div_el != null) {
        div_el.parentNode.removeChild(div_el);
    }

    var elem = "<div class='div_table_cal_one_month'>" +
        "<table cellpadding='0' cellspacing='0' class='cal_table_month'>" +

        "<tr class='tr_1_cal'>" +
        "<td colspan='7'>" +

        "<table class='cal_head_link'>" +
        "<tr>" +
        "<td class='td_back' id='" + this.td_cal_dayin_month_back + "'></td>" +
        "<td class='td_center' id='" + this.td_cal_dayin_month_head + "'></td>" +
        "<td class='td_next' id='" + this.td_cal_dayin_month_next + "'></td>" +
        "</tr>" +
        "</table>" +

        "</td>" +
        "</tr>" +

        "<tr class='tr_2_cal'>" +
        "<td>пн</td>" +
        "<td>вт</td>" +
        "<td>ср</td>" +
        "<td>чт</td>" +
        "<td>пт</td>" +
        "<td>сб</td>" +
        "<td>вс</td>" +
        "</tr>" +

        "<tr class='tr_3_cal'>" +
        "<td class='left day' id='" + this.td_cal_dayin_month + "0_0'></td>" +
        "<td class='left day' id='" + this.td_cal_dayin_month + "0_1'></td>" +
        "<td class='left day' id='" + this.td_cal_dayin_month + "0_2'></td>" +
        "<td class='left day' id='" + this.td_cal_dayin_month + "0_3'></td>" +
        "<td class='left day' id='" + this.td_cal_dayin_month + "0_4'></td>" +
        "<td class='left day' id='" + this.td_cal_dayin_month + "0_5'></td>" +
        "<td class='right day' id='" + this.td_cal_dayin_month + "0_6'></td>" +
        "</tr>" +

        "<tr class='tr_3_cal'>" +
        "<td class='left day' id='" + this.td_cal_dayin_month + "1_0'></td>" +
        "<td class='left day' id='" + this.td_cal_dayin_month + "1_1'></td>" +
        "<td class='left day' id='" + this.td_cal_dayin_month + "1_2'></td>" +
        "<td class='left day' id='" + this.td_cal_dayin_month + "1_3'></td>" +
        "<td class='left day' id='" + this.td_cal_dayin_month + "1_4'></td>" +
        "<td class='left day' id='" + this.td_cal_dayin_month + "1_5'></td>" +
        "<td class='right day' id='" + this.td_cal_dayin_month + "1_6'></td>" +
        "</tr>" +

        "<tr class='tr_3_cal'>" +
        "<td class='left day' id='" + this.td_cal_dayin_month + "2_0'></td>" +
        "<td class='left day' id='" + this.td_cal_dayin_month + "2_1'></td>" +
        "<td class='left day' id='" + this.td_cal_dayin_month + "2_2'></td>" +
        "<td class='left day' id='" + this.td_cal_dayin_month + "2_3'></td>" +
        "<td class='left day' id='" + this.td_cal_dayin_month + "2_4'></td>" +
        "<td class='left day' id='" + this.td_cal_dayin_month + "2_5'></td>" +
        "<td class='right day' id='" + this.td_cal_dayin_month + "2_6'></td>" +
        "</tr>" +

        "<tr class='tr_3_cal'>" +
        "<td class='left day' id='" + this.td_cal_dayin_month + "3_0'></td>" +
        "<td class='left day' id='" + this.td_cal_dayin_month + "3_1'></td>" +
        "<td class='left day' id='" + this.td_cal_dayin_month + "3_2'></td>" +
        "<td class='left day' id='" + this.td_cal_dayin_month + "3_3'></td>" +
        "<td class='left day' id='" + this.td_cal_dayin_month + "3_4'></td>" +
        "<td class='left day' id='" + this.td_cal_dayin_month + "3_5'></td>" +
        "<td class='right day' id='" + this.td_cal_dayin_month + "3_6'></td>" +
        "</tr>" +

        "<tr class='tr_3_cal'>" +
        "<td class='left day' id='" + this.td_cal_dayin_month + "4_0'></td>" +
        "<td class='left day' id='" + this.td_cal_dayin_month + "4_1'></td>" +
        "<td class='left day' id='" + this.td_cal_dayin_month + "4_2'></td>" +
        "<td class='left day' id='" + this.td_cal_dayin_month + "4_3'></td>" +
        "<td class='left day' id='" + this.td_cal_dayin_month + "4_4'></td>" +
        "<td class='left day' id='" + this.td_cal_dayin_month + "4_5'></td>" +
        "<td class='right day' id='" + this.td_cal_dayin_month + "4_6'></td>" +
        "</tr>" +

        "<tr class='tr_3_cal'>" +
        "<td class='left day' id='" + this.td_cal_dayin_month + "5_0'></td>" +
        "<td class='left day' id='" + this.td_cal_dayin_month + "5_1'></td>" +
        "<td class='left day' id='" + this.td_cal_dayin_month + "5_2'></td>" +
        "<td class='left day' id='" + this.td_cal_dayin_month + "5_3'></td>" +
        "<td class='left day' id='" + this.td_cal_dayin_month + "5_4'></td>" +
        "<td class='left day' id='" + this.td_cal_dayin_month + "5_5'></td>" +
        "<td class='right day' id='" + this.td_cal_dayin_month + "5_6'></td>" +
        "</tr>" +

        "</table></div>";

    return elem;
};

/**
 * Вешает обработчики на дневной календарь
 */
NCalender.prototype.setOnClickDayCalendar = function() {
    var ext = this;

    try {
        //обработчик на кнопку вперед
        getElemByID(this.td_cal_dayin_month_back).onclick = function() {
            ext.initBackDayCalendar();
        };

        getElemByID(this.td_cal_dayin_month_next).onclick = function() {
            ext.initNextDayCalendar();
        };


        //Формируем обьект даты
        var currentDate = new Date();

        if(this.month_tmp == 'ММ')
            var _month = currentDate.getMonth();
        else
            _month = this.month_tmp;

        if(this.year_tmp == 'ГГГГ')
            var _year = currentDate.getFullYear();
        else
            _year = this.year_tmp;

        //определяем дату отсчета
        this.SelectDate = new Date(_year, _month);

        //определяю последний день месяца
        var count_day = getLastDayOfMonth(this.SelectDate.getFullYear(), this.SelectDate.getMonth());
        for(var i = 1, j = 0; i <= count_day; i++) {

            var day = new Date(this.SelectDate.getFullYear(), this.SelectDate.getMonth(), i);
            //преобразуем порядковый день недели в номер в ряду в сет
            var week_day = this.getNumberOfCell(day.getDay());

            var td = getElemByID(this.td_cal_dayin_month + j + "_" + week_day);
            if(td != null) {
                td.innerHTML = i;

                td.onclick = function() {
                    clear();
                    //устанавливаем значения по выбранной дате
                    ext.day = this.innerHTML;
                    ext.month = ext.SelectDate.getMonth();
                    ext.year = ext.SelectDate.getFullYear();

                    ext.updateDayValue();
                }
            }

            if(week_day == 6) {
                j++;
                continue;
            }

        }
        //формируем оглавление для дневного календаря
        this.setDayCalendarHead();

    } catch(error) {
        SaveLog("NCalender.prototype.setOnClickDayCalendar: " + error.message);
    }
    //устанавливаем оформление по-умолчанию
    function clear() {
        for(var i = 0; i < 6; i++) {
            for(var j = 0; j < 7; j++) {
                var td = getElemByID(ext.td_cal_dayin_month + i + "_" + j);
                if(td != null) {
                    td.style.backgroundImage = '';
                    td.style.color = "black";
                }
            }
        }
    }
};
/**
 * Обновляет инпут в зависимости от выбранного значения
 */
NCalender.prototype.updateDayValue = function() {
    if(this.day != 'ДД') {

        //определяю последний день месяца
        var count_day = getLastDayOfMonth(this.SelectDate.getFullYear(), this.SelectDate.getMonth());
        var isIssetDate = false;
        for(var i = 1, j = 0; i <= count_day; i++) {

            var day = new Date(this.SelectDate.getFullYear(), this.SelectDate.getMonth(), i);
            //преобразуем порядковый день недели в номер в ряду в сет
            var week_day = this.getNumberOfCell(day.getDay());

            var td = getElemByID(this.td_cal_dayin_month + j + "_" + week_day);
            if(td.innerHTML == this.day) {

                this.setYearValue(this.year);
                this.setMonthValue(this.month);
                this.setDayValue(this.day);

                this.setActiveSelectDay(td.id);

                isIssetDate = true;
                break;
            }
            if(week_day == 6) {
                j++;
                continue;
            }
        }
        //дата не найдена. требуется
        if(!isIssetDate) {
            this.setDayValue('ММ');
        }
    }
};
/**
 * Устанавливает значение в инпуте
 * Устанавливает день в календаре
 * @param val
 */
NCalender.prototype.setDayValue = function(val) {
    if(val == 'ДД') {
        daynum = val;
    } else {
        //необходимо отображать с ведущим нулем
        var daynum = this.convertDayShow(val);
        val = parseInt(val);
    }

    getElemByID(this.print_day_text).innerHTML = daynum;
    //устанавливаем выбранную дату
    this.day = val;

    this.updateSelectDate();

    this.dFilter();
};
/**
 * Устанавливаем активным выбранный день
 * @param id
 */
NCalender.prototype.setActiveSelectDay = function(id) {
    var cell = getElemByID(id);
    cell.style.backgroundImage = "url('img/calender_dayinmonth_press.png')";
    cell.style.backgroundRepeat = 'no-repeat';
    cell.style.color = "#00bebc";
};

/**
 * Очищаем бэкграунд всей сетки для календаря с днями
 * @param id
 */
NCalender.prototype.clearBackgroundDayInMonth = function() {
    for(var i = 0; i < 5; i++) {
        for(var j = 0; j < 7; j++) {
            var td = getElemByID(this.td_cal_dayin_month + i + "_" + j);
            if(td != null) {
                td.style.backgroundImage = '';
                td.style.color = "black";
            }
        }
    }
};
/**
 * Определяем значение в ряду сетки календаря по номеру дня
 * @param num
 */
NCalender.prototype.getNumberOfCell = function(num) {
    var ret = 0;

    switch(num) {
        case 0:
            //вс
            ret = 6;
            break;
        case 1:
            //пн
            ret = 0;
            break;
        case 2:
            //вт
            ret = 1;
            break;
        case 3:
            //ср
            ret = 2;
            break;
        case 4:
            //чт
            ret = 3;
            break;
        case 5:
            //пт
            ret = 4;
            break;
        case 6:
            //сб
            ret = 5;
            break;
    }

    return ret;
};
/**
 * Устанавливает значение месяца и года в месячном календаре
 */
NCalender.prototype.setDayCalendarHead = function() {
    var ret = "";
    try {

        switch(this.SelectDate.getMonth()) {
            case 0:
                ret = 'Январь';
                break;
            case 1:
                ret = 'Февраль';
                break;
            case 2:
                ret = 'Март';
                break;
            case 3:
                ret = 'Апрель';
                break;
            case 4:
                ret = 'Май';
                break;
            case 5:
                ret = 'Июнь';
                break;
            case 6:
                ret = 'Июль';
                break;
            case 7:
                ret = 'Август';
                break;
            case 8:
                ret = 'Сентябрь';
                break;
            case 9:
                ret = 'Октябрь';
                break;
            case 10:
                ret = 'Ноябрь';
                break;
            case 11:
                ret = 'Декабрь';
                break;
        }

        ret = ret + " " + this.SelectDate.getFullYear();
        getElemByID(this.td_cal_dayin_month_head).innerHTML = ret;
        getElemByID(this.td_cal_dayin_month_head).style.color = "#00bebc";
        getElemByID(this.td_cal_dayin_month_head).style.fontSize = "26";
     } catch(error) {
        SaveLog("NCalender.prototype.setDayCalendarHead: " + error.message);
    }
};
/**
 * Короткое описание
 * @param text
 */
NCalender.prototype.setStepInfo = function(text) {
    var info = getElemByID(this.info)
    if(info != null)
        info.innerHTML = text;
    this.info_text = text;
};
/**
 * Полное описание
 * @param text
 */
NCalender.prototype.setStepInfoFull = function(text) {
    var info_full = getElemByID(this.infofull);
    if(info_full != null)
        info_full.innerHTML = text;
    this.info_full_text = text;
};
/**
 * Устанавливает значение
 */
NCalender.prototype.setValue = function(val) {
    if(val.length > 0) {
        var date_tmp = val.split('.');

        //календарь
        if(date_tmp.length == 3) {
            this.setYearValue(date_tmp[2]);
            this.setMonthValue(this.convertMonthToData(date_tmp[1]));
            this.setDayValue(parseInt(date_tmp[0]));
        } else if(date_tmp.length == 2) {
            //календарьмонтс
            this.setYearValue(date_tmp[1]);
            this.setMonthValue(this.convertMonthToData(date_tmp[0]));
        }
    }
};
/**
 * Тригер на изменение полей
 * В случае необходимости переопределяется
 * @param value
 */
NCalender.prototype.dFilter = function() {
    StartTimeOut();
    if(this.ManageMask.checkValid()) {
        Pages.Steps.button.next.show();
    } else {
        Pages.Steps.button.next.hide();
    }
};
/**
 * Месяц хранится как значение от 0-11
 * Для отображение необходимо конвертировать в диапазон 01-12 (с ведущими нулями)
 * @param month
 */
NCalender.prototype.convertMonthShow = function(month) {
    var month_tmp = parseInt(month) + 1;
    return (month_tmp < 10) ? '0' + month_tmp : month_tmp;
};
/**
 * Месяц хранится как значение от 0-11 в переменной this.month
 * Для хранения необходимо конвертировать диапазон 01-12 (с ведущими нулями)
 * в внутренний формат 0-11
 * @param month
 */
NCalender.prototype.convertMonthToData = function(month) {
    return parseInt(month) - 1;
};
/**
 * День хранится как значение от 1-31
 * Для отображение необходимо конвертировать в диапазон 01-31 (с ведущими нулями)
 * @param day
 */
NCalender.prototype.convertDayShow = function(day) {
    var tmp = parseInt(day);
    return (tmp < 10) ? '0' + tmp : tmp;
};
/**
 * День хранится как значение от 0-31 в переменной this.day
 * Для хранения необходимо конвертировать диапазон 01-31 (с ведущими нулями)
 * в внутренний формат 1-31
 * @param day
 */
NCalender.prototype.convertDayToData = function(day) {
    return parseInt(day);
};


/**
 * определяем последний день месяца
 * @param year год
 * @param month месяц
 * @returns {number}
 */
function getLastDayOfMonth(year, month) {
    var date = new Date(year, month + 1, 0);
    var num = date.getDate();

    return num;
}