/**
 * Шаблон для цифровой клавиатуры
 * Клавитура полностью независимаa
 * @constructor
 */
function NumBoard() {
    this.elem = [];
    this.elem[0] = 0;
    this.elem[1] = 1;
    this.elem[2] = 2;
    this.elem[3] = 3;
    this.elem[4] = 4;
    this.elem[5] = 5;
    this.elem[6] = 6;
    this.elem[7] = 7;
    this.elem[8] = 8;
    this.elem[9] = 9;
    this.elem[40] = "b";
    this.elem[41] = "c";

    this.mask = null;
    this.ManageMask = null;
    this.input = null;
    //путь к звуковому файлу
//    this.soundPath = "../sound/enter_number.wav";
    this.stepinfo_id = '';
    this.stepinfofull_id = '';
    this.prefix = "";
}
NumBoard.prototype.setPrefix = function (_prefix) {
    this.prefix = _prefix;
};
NumBoard.prototype.CreateNumboard = function () {
    var elem = document.getElementById(this.prefix + 'wnumboard_panel');
    if (elem != null)
        elem.parentNode.removeChild(elem);

    var numboard = "\
    <table d=" + this.prefix + "'wnumboard_panel' istyle='BORDER-RIGHT: 0px; BORDER-TOP: 0px; BORDER-LEFT: 0px; WIDTH: 740px; BORDER-BOTTOM: 0px; HEIGHT: 460px;' border=0 cellspacing=0 cellpadding=0>\
    <tr>\
        <td class='cell_num_b1' id='" + this.prefix + "b1' style='background: url(img/pad_1.png) no-repeat;'></td>\
        <td class='cell_num_b2' id='" + this.prefix + "b2' style='background: url(img/pad_2.png) no-repeat;'></td>\
        <td class='cell_num_b3' id='" + this.prefix + "b3' style='background: url(img/pad_3.png) no-repeat;'></td>\
    </tr>\
    <tr>\
        <td class='cell_num_b4' id='" + this.prefix + "b4' style='background: url(img/pad_4.png) no-repeat;'></td>\
        <td class='cell_num_b5' id='" + this.prefix + "b5' style='background: url(img/pad_5.png) no-repeat;'></td>\
        <td class='cell_num_b6' id='" + this.prefix + "b6' style='background: url(img/pad_6.png) no-repeat;'></td>\
    </tr>\
    <tr>\
        <td class='cell_num_b7' id='" + this.prefix + "b7' style='background: url(img/pad_7.png) no-repeat;'></td>\
        <td class='cell_num_b8' id='" + this.prefix + "b8' style='background: url(img/pad_8.png) no-repeat;'></td>\
        <td class='cell_num_b9' id='" + this.prefix + "b9' style='background: url(img/pad_9.png) no-repeat;'></td>\
    </tr>\
    <tr>\
        <td class='cell_num_b41' id='" + this.prefix + "b41' style='background: url(img/pad_c.png) no-repeat;'></td>\
        <td class='cell_num_b0' id='" + this.prefix + "b0' style='background: url(img/pad_0.png) no-repeat;'></td>\
        <td class='cell_num_b40' id='" + this.prefix + "b40' style='background: url(img/pad_b.png) no-repeat;'></td>\
    </tr>\
    </table>\
    ";
    return numboard;
};
NumBoard.prototype.InitNumBoard = function (containerID, stepinfoID, stepinfofullID) {
    var container = document.getElementById(containerID);
    this.stepinfo_id = stepinfoID;
    this.stepinfofull_id = stepinfofullID;
    container.innerHTML = this.CreateNumboard();
    WSoundEffects.sounds(this.soundPath);
    this._start();
};
/**
 * Устанавливает степинфо для шага
 * @param text
 */
NumBoard.prototype.setStepInfo = function(text) {
    document.getElementById(this.stepinfo_id).innerHTML = text;
};
/**
 * Устанавливает степинфофул для шага
 * @param text
 */
NumBoard.prototype.setStepInfoFull = function(text) {
    document.getElementById(this.stepinfofull_id).innerHTML = text;
};
NumBoard.prototype._keyClick = function (id) {
    //Перезапуск таймера я еще здесь
    //if(!this.ManageMask.checkCountEnter()) {
        StartTimeOut();
    //}
    this.dFilter(id);
};
NumBoard.prototype.effectClick = function (id) {
    WSoundEffects.click();
    var button = id.substr(1+this.prefix.length);
    var element = document.getElementById(id);
    element.style.backgroundImage = "url('img/pad_" + this.elem[button] + "p.png')";
    var ext = this;
    setTimeout(function() {
        element.style.backgroundImage = "url('img/pad_" + ext.elem[button] + ".png')";
    }, 40);
};
NumBoard.prototype._start = function () {
    var ext = this;
    for (var key in this.elem) {
        var elem = document.getElementById(this.prefix + "b" + key);
        if (!this.elem.hasOwnProperty(key))
            continue;
        WEvents.remove(elem, 'click');
        WEvents.add(elem, 'click', function (e) {
            var event = fixEvent(e);
            ext.effectClick(event.target.id);
            ext._keyClick(event.target.id);
        });

    }
};
/**
 * Устанавливает маску для ввода
 * @param mask string
 */
NumBoard.prototype.setMask = function (mask) {
    this.mask = mask;
    this.ManageMask = new MaskFactory().makeMask.number(mask);
};
/**
 * Метод необходимо переопределить в потомке
 * Вызывается при нажатии на кнопку
 * @param id
 */
/**
 *
 * @param id string айди нажатого елемента
 */
NumBoard.prototype.dFilter = function (id) {
    var button = id.substr(1 + this.prefix.length);
    this.input.style.fontSize = this.ManageMask.getCurrentFontSize();

    if (button == 41) {//нажали С
        this.ManageMask.clear();
        this.input.innerHTML = this.ManageMask.getDataMask();
    } else if (button == 40) {//нажали бекспейс
        this.input.innerHTML = this.ManageMask.backspace();
    } else {
        //нажали цифры
        this.ManageMask.filter(button);
        this.input.innerHTML = this.ManageMask.getDataMask();
    }
    this.checkValid();
};
NumBoard.prototype.checkValid = function() {
    if (this.ManageMask.checkValid()) {
        this.doValidSuccess();
    } else {
        this.doValidDenied();
    }
};
NumBoard.prototype.doValidSuccess = function() {
    //проверяем нужно ли автоопределение номера
    //определяем нужно ли фильтровать номерные емкости
    var needFilter = MsiManager.IsNeedFilter();
    SaveLog("IsNeedFilter: " + needFilter);

    if (needFilter) {

        var gateChange = -1;
        var val = this.ManageMask.getData();

        var getDataTimer = new IndependentTimer(60, "Ожидайте, проверка номер телефона");
        getDataTimer.timer.directionTimeToBig();
        //указываем что делать по истечению таймера
        getDataTimer.timer.finalize = function() {
            //убираем всплывающее окно
            getDataTimer.stop();

            SaveLog("GateChange: " + gateChange + " val: " + val);
            if(gateChange) {
                ChangeOperatorByMsiIsdn(MsiManager.getNewGate(), val);
            }
        };

        //выполняется каждую секунду
        getDataTimer.timer.execute = function() {
            //был получен ответ
            if(gateChange != -1) {
                //Метод finalize всегда выполняется при остановке таймера
                getDataTimer.stop();
            }
        };
        //определяем действия для проверки статуса возможности проплаты платежа
        getDataTimer.start();

        setTimeout(function () {
            gateChange = MsiManager.IsGateChange(val);
        }, 1000);
    }
    Pages.Steps.button.next.show();
};
NumBoard.prototype.doValidDenied = function() {
    Pages.Steps.button.next.hide();
};
NumBoard.prototype.setValue = function (value) {
    this.input.innerHTML = this.ManageMask.setValue(value);
};
var abstractNumBoard = new NumBoard();

/**
 * Степ цифровой клавиатуры и поля ввода databox
 * @constructor
 */
function NumBoardNoRule(input_id) {
    this.mask = null;
    this.ManageMask = null;
    this.input = document.getElementById(input_id);
    this.input.innerHTML = '';
    this.prefix = "";
}
NumBoardNoRule.prototype = inherit(abstractNumBoard);

function NumBoard8(input_id) {
    this.mask = null;
    this.ManageMask = null;
    this.input = document.getElementById(input_id);
    this.input.innerHTML = '';
    this.prefix = "";
}
NumBoard8.prototype = inherit(abstractNumBoard);

/**
 * Устанавливает маску для ввода
 * @param mask string
 */
NumBoard8.prototype.setMask = function (mask) {
    this.mask = mask;
    this.ManageMask = new MaskFactory().makeMask.number8(mask);
    this.input.innerHTML = this.ManageMask.setMaskWithNumber8();
};

/**
 * Для ввода фиксированной суммы
 * @constructor
 */
function NumBoardFixedSum() {
    this.ManageMask = null;
    //элемент на который установлен фокус
    //рубли
    this.currentInput = this.rubles = document.getElementById('rubles');
    //копейки
    this.coops = document.getElementById('koops');

    var ext = this;
    this.rubles.onclick = function () {
        ext.currentInput = ext.rubles;
        ext.rubles.className = 'winput_center_fixedsum';
        document.getElementById('rubles_right').className = "winput_right";
        document.getElementById('rubles_left').className = "winput_left";

        ext.coops.className = 'winput_center_fixedsum';
        document.getElementById('koops_right').className = "winput_right";
        document.getElementById('koops_left').className = "winput_left";
    };
    this.coops.onclick = function () {
        ext.currentInput = ext.coops;
        ext.rubles.className = 'winput_center_fixedsum';
        document.getElementById('rubles_right').className = "winput_right";
        document.getElementById('rubles_left').className = "winput_left";

       ext.coops.className = 'winput_center_fixedsum_red';
        document.getElementById('koops_right').className = "winput_right_red";
        document.getElementById('koops_left').className = "winput_left_red";

    };
    this.coops.innerHTML = '';
    this.rubles.innerHTML = '';
    this.rubles.click();
    this.prefix = "";
}
NumBoardFixedSum.prototype = inherit(abstractNumBoard);

NumBoardFixedSum.prototype.setMask = function (minSum, maxSum) {
    this.ManageMask = new MaskFactory().makeMask.fixedsum(minSum, maxSum);
};
NumBoardFixedSum.prototype.setValue = function (value) { 
    var data = this.ManageMask.setValue(value);
    this.rubles.innerHTML = data[1];
    this.coops.innerHTML = data[2];
};
NumBoardFixedSum.prototype.dFilter = function (keyID) {
    var button = keyID.substr(1+this.prefix.length);
    if (button == 41) {//нажали С
        this.ManageMask.clear();
        this.rubles.innerHTML = '';
        this.coops.innerHTML = '';
        this.rubles.click();
    } else if (button == 40) {//нажали бекспейс
        if (this.currentInput === this.rubles) {
            this.currentInput.innerHTML = this.ManageMask.backspace(1)[1];
        } else if (this.currentInput === this.coops) {
            this.currentInput.innerHTML = this.ManageMask.backspace(2)[2];
        }
        //this.currentInput.focus();
        //Pages.Steps.button.next.hide();
    } else {
        //нажали цифры
        if (this.currentInput === this.rubles) {
            //вводят рубли
            this.ManageMask.filter(button, 1);
            this.currentInput.innerHTML = this.ManageMask.getDataMask()[1];
        } else if (this.currentInput === this.coops) {
            this.ManageMask.filter(button, 2);
            this.currentInput.innerHTML = this.ManageMask.getDataMask()[2];
        }
    }
    //this.currentInput.focus();
    if (this.ManageMask.checkValid()) {
        Pages.Steps.button.next.show();
    } else {
        Pages.Steps.button.next.hide();
    }
};


function NumBoardStaticKom(input_id) {
    this.mask = null;
    this.ManageMask = null;
    this.input = document.getElementById(input_id);
    this.input.innerHTML = '';
    this.prefix = "";
}
NumBoardStaticKom.prototype = inherit(abstractNumBoard);
/**
 * Устанавливает маску для ввода
 * @param mask string
 */
NumBoardStaticKom.prototype.setMask = function (mask) {
    this.mask = mask;
    this.ManageMask = new MaskFactory().makeMask.statickom(mask);
};