//Фабрика компонентов
function UIFactory() {
}
UIFactory.prototype = {
    constructor: UIFactory,

    makeKeyboard: {
        fiobox: function (mask) {
            var keyboard = new KeyboardFioBox();
            keyboard.InitKeyboard('wkeyboard', 'info_wfiobox', 'infofull_fiobox');
            keyboard.setMask(mask);
            return keyboard;
        },
        keyboard: function (mask) {
            var keyboard = new KeyBoardEng("databox_keyboard");
            keyboard.LangDown = "rus";
            keyboard.InitKeyboard('wkeyboard', 'info_wdatabox_keyboard', 'infofull_wdatabox_keyboard');
            keyboard.setMask(mask);
            return keyboard;
        },
        keyboardeng: function (mask) {
            var keyboard = new KeyBoardEng();
            keyboard.InitKeyboard('wkeyboard', 'info_wdatabox_keyboard', 'infofull_wdatabox_keyboard');
            keyboard.setMask(mask);
            return keyboard;
        },
        search: function () {
            var keyboard = new KeyboardSearch();
            keyboard.InitKeyboard('wsearch_keyboard_container');
            return keyboard;
        }
    },
    makeNumBoard: {
        number: function (mask) {
            var numBoardNoRule = new NumBoardNoRule('databox');
            numBoardNoRule.InitNumBoard('wnumboard', 'info_wdatabox', 'infofull_wdatabox');
            numBoardNoRule.setMask(mask);
            return numBoardNoRule;
        },
        number8: function (mask) {
            var numBoard8 = new NumBoard8('databox');
            numBoard8.InitNumBoard('wnumboard', 'info_wdatabox', 'infofull_wdatabox');
            numBoard8.setMask(mask);
            return numBoard8;
        },
        statickom: function(mask) {
            var numBoardStaticKom = new NumBoardStaticKom('databox');
            numBoardStaticKom.InitNumBoard('wnumboard', 'info_wdatabox', 'infofull_wdatabox');
            numBoardStaticKom.setMask(mask);
            return numBoardStaticKom;
        },
        fixedSum: function (minSum, maxSum) {
            var fixedsum = new NumBoardFixedSum();
            fixedsum.InitNumBoard('wnumboard', 'info_wfixedsum', 'infofull_wfixedsum');
            fixedsum.setMask(minSum, maxSum);
            return fixedsum;
        }
    },
    makeCalendar: {
        calender: function () {
            var calendar = new NCalender();
            calendar.setContainer('wcalendarbox');
            calendar.setPrefix('cal1');
            return calendar;
        },
        month: function() {
            var calendar = new NCalender();
            calendar.isCalenderMonth();
            calendar.setContainer('wcalendarmonthbox');
            calendar.setPrefix('cal2');
            return calendar;
        }
    },
    makeListBoard: {
        list: function(values) {
            var list = new List();
            list.addList(values);
            list.init('wlist_element', 'info_wlist', 'infofull_wlist');
            return list;
        },
        listgoods: function(values) {
            var list = new ListGoods();
            list.addList(values);
            list.init('wlist_element', 'info_wlist', 'infofull_wlist');
            return list;
        },
        listonline: function(values) {
            var listonline = new ListOnline();
            listonline.addList(values);
            listonline.init('wlist_online_select', 'info_wlist_online', 'infofull_wlist_online');
            return listonline;
        }
    },
    makeCountry: {
        listcountry: function() {
            var country = new WCountry();
            country.setPrefix("pre");
            country.init('wcountry', 'wcurrency');
            return country;
        }
    }

};