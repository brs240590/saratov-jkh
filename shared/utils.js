/**
 * Используется для реализации на прототипах
 * @param proto
 * @returns {inherit.F}
 */
function inherit(proto) {
    function F() {
    }

    F.prototype = proto;
    return new F;
}

function setEvent(elem, func) {
    WEvents.remove(elem);
    WEvents.add(elem, 'click', func);
}

function replace(fullString, text, by) {
    var strLength = fullString.length, txtLength = text.length;
    if((strLength == 0) || (txtLength == 0)) return fullString;

    var i = fullString.indexOf(text);
    if((!i) && (text != fullString.substring(0, txtLength))) return fullString;
    if(i == -1) return fullString;

    var newstr = fullString.substring(0, i) + by;

    if(i + txtLength < strLength)
        newstr += replace(fullString.substring(i + txtLength, strLength), text, by);

    return newstr;
}

/**
 * Пишем в лог ексепшн
 * @param e обьект исключения
 * @param fileName имя файла откуда вызывается
 * @param number номер строки в файле
 * @constructor
 */
function SaveExceptionToLog(e, fileName, label) {
    SaveLog('exception m_error in file ' + fileName + " label_:" + label);
    SaveLog("name: " + e.name + ", desc: " + e.description);
}

/***
 * Запуск страницы по таймеру
 */
//айди возвращаемое timeoutID;
var timeOutReturnToMain = [];
//флаг запущен я еще здесь или нет. 1 - на экране сейчас показывается. 0 - не показывается
var wtimers = 0;

//время последнего запуска я еще здесь в милисекундах от 1 января 1971
var lastWtimersTurnOff = 0;
// количество я еще здесь, используется для перезапуска каждых 60 раз мейна
var countStartIamHere = 0;

/**
 * Функция исопльзуется для принудительного обновления главной страницы
 * После 60 запусков StartTimeOut на главной странице интерфейс перезагрузится
 * @constructor
 */
function MainUpdate() {
    countStartIamHere++;
    if(countStartIamHere > 60) {
        countStartIamHere = 0;
        SaveLog("Принудительное обновление главной страницы: main_update: " + countStartIamHere);
        //Обновляем главную страницу
        Link("main.html");
    } else {
        SaveLog("main_not_update: " + countStartIamHere);
        StartTimeOut();
    }
}

function clearTimeOutReturn() {
    timeOutReturnToMain = [];
    wtimers = 0;
}
/**
 * Функция будет стартует таймер
 * @constructor
 */
function StartTimeOut() {
    PingInterface();
    lastWtimersTurnOff = new Date().getTime();
    // var interval = RetTimeOut();
    // alert(RetTimeOut());
    var interval = 3000;
    StopTimeOutMain();
    wtimers = 0;
    //Если находимся не на мейне
    timeOutReturnToMain.push(setTimeout(timersReturnToMain, interval));
}

/**
 * Функция останавливает таймер
 */
function StopTimeOutMain() {

    for(var i = 0; i < timeOutReturnToMain.length; i++) {
        if(timeOutReturnToMain[i] != undefined) {
            clearTimeout(timeOutReturnToMain[i]);
            delete timeOutReturnToMain[i];
        }

    }
    wtimers = 0;
    lastWtimersTurnOff = new Date().getTime();
}

/**
 * Срабатывает при нажатии "я здесь" в "я еще здесь"
 * @constructor
 */
function IamHereNew() {
    Pages.HideOne("screentimeoutbg", "IamHereNew(), screentimeoutbg");
    Pages.HideOne("screentimeout", "IamHereNew(), screentimeout");

    //если страница оплаты, включаем еще раз купюроприемник
    if(GetCurrentPage() == Pages.Link.PAYMENT) {
        SaveLog('Start bill from i am here on payment page');
        START_BILL();
    }
    IamHereTimer.stop();
    //stopWtimers();
    StopTimeOutMain();
    wtimers = 0;
    //Запускаем еще раз
    setTimeout(StartTimeOut, 3000);
}
//
//PEYMENT.HTML
//
//вызывается позже для того чтобы купюра успела встать в стекер если ее вставляют при уже нажатой кнопке оплатить
function pay_fnc() {
    alert('pay_fnc');
    SaveLog('pay_fnc() stop bill');
    STOP_BILL();
    Pages.Steps.button.pay.hide();
var fix = GetFixedSum();
fix = parseFloat(fix);
if (RetACCT() < fix) {
         var delayTimerBack = new IndependentTimer(5, "Возврат средств...");
         delayTimerBack.timer.finalize = function() {
         delayTimerBack.stop();
         window.external.PaymentCancel();
         };
         delayTimerBack.timer.execute = function() {
         StartTimeOut();
         };
         delayTimerBack.start();
} else {
         var delayTimerBack = new IndependentTimer(5, "Завершение оплаты...");
         delayTimerBack.timer.finalize = function() {
         delayTimerBack.stop();
var num=1;
	  for (var key in cart.cartvalue) {
						var text = cart.cartvalue[key].stepname+", стоимость: "+cart.cartvalue[key].stepvalue+" руб.";
						AddCheckParameter(num, text);
						num++;
                                          }

        PayConfirmNew();
         };
         delayTimerBack.timer.execute = function() {
         StartTimeOut();
         };
         delayTimerBack.start();
       }
    Pages.Steps.button.pay.hide();
}
function moneyinchange(str) {
    try {
        document.getElementById('moneyin').innerHTML = str + " руб.";
    } catch(e) {
        SaveExceptionToLog(e, "js/utils.js" + id, "163");
    }
    if(str > 0) {
        Pages.Steps.button.next.hide();
        Pages.Steps.button.back.hide();
        Pages.Steps.button.menu.hide();
        Pages.Steps.button.pay.hide();
        //clearCart();
        ///Показываем кнопку оплатить через 1.5 сек, чтобы купюра успела встать в купюроприемник
        setTimeout(function() {
            Pages.Steps.button.pay.show();
        }, 500);
    }

}
//к зачислению
function moneyacctchange(str) {
    try {
        var elm = getElemByID('moneyacct');
        if(EstimatePaidOut().length > 0)
            str = EstimatePaidOut();
        elm.innerHTML = str + " руб.";
    } catch(e) {
        SaveExceptionToLog(e, "js/utils.js" + id, 182);
    }
}
//
function moneypercentchange(str) {
    try {
        var elm = getElemByID('moneypercent');
        //есть фиксированная сумма комисии
        if(EstimateComission().length > 0)
            str = EstimateComission();
        elm.innerHTML = str + " руб.";
    } catch(e) {
        SaveExceptionToLog(e, "js/utils.js" + id, 190);
    }
}
//
function moneychange(str) {
    try {
        document.getElementById('moneychange').innerHTML = str + " руб.";
    } catch(e) {
        SaveExceptionToLog(e, "js/utils.js" + id, 198);
    }
str = parseFloat(str);
if (str > 0) {
        try {
	document.getElementById('moneychange').style.display = 'inline';
        document.getElementById('moneychangetxt').style.display = 'inline';
            } catch(e){}
             }
}

//осталось внести
function moneyneeded(str) {
fix = GetFixedSum()
    try {
        if(EstimatePaidOut().length == 0) {
            var money_needed = getElemByID('wpayment_money_needed');
            money_needed.innerHTML = "" + fix + " руб.";
        }
    } catch(e) {
        SaveExceptionToLog(e, "js/utils.js" + id, 207);
    }
}

/**
 * Ниже методы для обратного вызова из серверной стороны терминала
 */

/**
 * Переход на главную страницу
 */
function Main() {
    //Если находимся на странице оплаты
    if(GetCurrentPage() == Pages.Link.PAYMENT) {
        SaveLog("STOP_BILL() button main menu on payment page");
        STOP_BILL();
        var mainMenu = getElemByID('wbuttons_mainmenu');
        mainMenu.onclick = function() {
            Main();
        };
    }
    SetCurrentPage(Pages.Link.MAIN);
    Pages.Show('wmainpage');
    StopTimeOutMain();
    clearTimeOutReturn();
    StartTimeOut();
    clearCart();
    // AskBarcode();
    window.external.SetKeyFromInterface('datesel','0');


    //EffectClick('wbuttons_mainmenu');

/*
'use strict';
var calendarBody = document.querySelector('#calendar-body');
var dayInMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

var days = ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'];


function genCalendar(date) {
//  var curDay = date.getDate();

var calendarDate1 = new Date();
var calendarDate2 = new Date();
var m1 = calendarDate1.getTime() + (24 * 60 * 60 * 1000 * (dayInMonth[calendarDate1.getMonth()] - calendarDate1.getDate() + 1));
var m2 = calendarDate2.getTime() + (24 * 60 * 60 * 1000 * (dayInMonth[calendarDate2.getMonth()] + dayInMonth[calendarDate2.getMonth()+1] - calendarDate2.getDate() + 1));

  var curDay_today = new Date();
  curDay_today = curDay_today.getDate();
  var curDay = date.getDate();
if (curDay > curDay_today) { curDay = 1; }
  date.setDate(1);
  var startDay = date.getDay();
  var daysTotal = !(date.getFullYear() % 4) && date.getMonth() === 1 ? 29 : dayInMonth[date.getMonth()];
  var content = '';
  content = '<div style="text-align:center;font-style:FONT-FAMILY: AA Bebas Neue;margin-right:20px;font-size: 40px;">'+(getMonthText(date.getMonth()))+'<br>';
  content += '<span onclick="calendarDate1.setTime(m1);genCalendar(calendarDate1);" style="display: inline-block;"> << </span>';
  for(var i = curDay; i <= daysTotal; i++) {
    if(i === curDay &&   curDay_today ==   curDay) {
      content += '<div style="padding-bottom:2px;border-radius: 25px;margin-right:3px;width:40px; height:40px;border:8px solid #e3bdb8;color:#ffffff;background:#e3bdb8;display: inline-block;font-style:FONT-FAMILY: AA Bebas Neue;font-size: 40px;">' + i + '</div>';
    } else {
      content += '<div style="display: inline-block;font-style:FONT-FAMILY: AA Bebas Neue;margin-right:17px;font-size: 40px;"><span onclick="changeDate('+(date.getMonth()+1)+','+i+');">' + i + '</span></div>';
    }
  }
  content += '<span onclick="genCalendar();" style="display: inline-block;"> >> </span>';
  content += '</div>';
  calendarBody.innerHTML = content;
}
var getMonthText = function(currentMonth) {
    if (currentMonth === 0) { return "Январь"; }
    else if (currentMonth === 1) { return "Февраль"; }
    else if (currentMonth === 2) { return "Март"; }
    else if (currentMonth === 3) { return "Апрель"; }
    else if (currentMonth === 4) { return "Май"; }
    else if (currentMonth === 5) { return "Июнь"; }
    else if (currentMonth === 6) { return "Июль"; }
    else if (currentMonth === 7) { return "Август"; }
    else if (currentMonth === 8) { return "Сентябрь"; }
    else if (currentMonth === 9) { return "Октябрь"; }
    else if (currentMonth === 10) { return "Ноябрь"; }
    else if (currentMonth === 11) { return "Декабрь"; }
};
var calendarDate = new Date();
var m1 = calendarDate.getTime() + (24 * 60 * 60 * 1000 * (dayInMonth[calendarDate.getMonth()] - calendarDate.getDate() + 1));
var m2 = calendarDate.getTime() + (24 * 60 * 60 * 1000 * (dayInMonth[calendarDate.getMonth()] + dayInMonth[calendarDate.getMonth()+1] - calendarDate.getDate() + 1));
//calendarDate.setTime(m2);
genCalendar(calendarDate);
*/
}

function changeDate(month, day){

if (month < 10) { month="0"+month; }
var datesel = "2018"+"-"+month+"-"+day;
var olddate = window.external.GetKeyForInterface('datesel');
var oldmonth = olddate.substr(5,2);
var oldday = olddate.substr(8,2);
try {
document.getElementById('day'+oldmonth+""+oldday).style.border = "0px";
document.getElementById('day'+oldmonth+""+oldday).style.color = "#000000";
document.getElementById('day'+oldmonth+""+oldday).style.background="#FFFFFF";
document.getElementById('day'+oldmonth+""+oldday).style.marginRight="13px";
document.getElementById('day'+oldmonth+""+oldday).style.marginLeft="-3px";
document.getElementById('day'+oldmonth+""+oldday).style.width="25px";
    } catch(e){}

window.external.SetKeyFromInterface('datesel',datesel);
var masterid = window.external.GetKeyForInterface('browmasterid');
var serviceid = window.external.GetKeyForInterface('browservices');
document.getElementById('day'+month+""+day).style.border = "8px solid #e3bdb8";
document.getElementById('day'+month+""+day).style.color = "#FFFFFF";
document.getElementById('day'+month+""+day).style.borderRadius = "25px";
document.getElementById('day'+month+""+day).style.background="#e3bdb8";
document.getElementById('day'+month+""+day).style.width="40px";
document.getElementById('day'+month+""+day).style.height="40px";
document.getElementById('day'+month+""+day).style.marginRight="3px";
document.getElementById('day'+month+""+day).style.marginLeft="-13px";

//"padding-bottom:2px;border-radius: 25px;margin-right:3px;width:40px; height:40px;border:8px solid #e3bdb8;color:#ffffff;background:#e3bdb8;display: inline-block;font-style:FONT-FAMILY: AA Bebas Neue;font-size: 40px;";
var mastername = window.external.GetKeyForInterface('browmastername');
getMastersTime(masterid, serviceid,mastername);
}

function selectSchedule(time) {
//alert(time);
var schedule = JSON.parse(window.external.GetKeyForInterface('BrowUPTime'));
for (var i = 0; i < schedule.Times.length; i ++) {
document.getElementById('time'+schedule.Times[i]).style.background="url(img/schedule.png) no-repeat";
document.getElementById('time'+schedule.Times[i]).style.fontColor="#000000";
                                                 }
document.getElementById('time'+time).style.background="url(img/schedule_p.png) no-repeat";
document.getElementById('time'+time).style.fontColor="#FFFFFF";

window.external.SetKeyFromInterface('timesel',time);
Pages.ShowOne("wbuttons_appoint");

        var next = document.getElementById('wbuttons_appoint');
        WEvents.remove(next, 'click');
        next.onclick = function() {
        ClickOperator(1404);
        };


}
/**
 * Ошибка оборудования
 */
function ShowFiscalErrorPage() {
    Pages.Show('fiscalError');
}
/**
 * Показывает "Терминал временно не работает"
 */
function ShowErrorPage() {
    StopTimeOutMain();

}

/**
 *
 * @type {boolean}
 */
var isEnableReplay = true;

/**
 * Включает кнопку повторить платеж
 * @constructor
 */
function EnableRepayButton() {
    isEnableReplay = true;
}

/**
 * Выключаем кнопку повторить платеж
 * @constructor
 */
function DisableRepayButton() {
    isEnableReplay = false;
}

/**
 * Показывает или скрывает кнопку повторить платеж
 * в зависимости от возможно ее показывать или нет
 * @constructor
 */
function ShowPrintButton() {
    if(isEnableReplay) {
        Pages.ShowOne("wbuttons_main_pay_replay");
    } else {
        getElemByID("wbuttons_main_pay_replay").onclick = function() {
        };
        Pages.HideOne("wbuttons_main_pay_replay");
    }
}
/**
 * Функция вызова check
 */
function ShowPrintCheckPage() {
//    showAdditionalParams('wchecktext');
//    sounder.src = "../sound/take_cheque.wav";
    StartTimeOut();
//    document.getElementById('wbuttons_main_pay_replay').onclick = function() {
//        ReplayPay(PaymentOperatorID);
//    };
    getPinpadTimer.stop();
    Pages.Steps.check.print();
        setTimeout(function() {
            Link('Main.html');
        }, 8000);

}

function showAdditionalParams(container) {
    var checktext = RetAdditionalPaymentParametersForCheck();

    var divel = getElemByID(container);
    if(checktext.length > 1) {
        var textarr = checktext.split(";");
        var textcheck = '';
        for(var i = 0; i < textarr.length; i++) {
            textcheck += textarr[i] + ': <span class=\'confirm_maskvalue1\'>' + textarr[i + 1] + '</span><br>';
            i = i + 1;
        }

    } else {
        textcheck = "";
    }
    divel.innerHTML = textcheck;
}


/**
 * Прогрессбар
 * @constructor
 */
function Loading() {
    Pages.Loading.load();
}


/**
 *
 * /Ниже методы для обратного вызова из серверной стороны терминала
 */

/**
 * Функции из fnc.js
 */
function LoadPage() {
    // jsClock();
}

/**
 * Меняет размер изображения и возвращает его назад
 * @param idpic
 */
function changeImageresize(idpic) {
    var is = getImageSize(idpic);
    var el = document.getElementById(idpic);
    el.width = is.width - 5;
    el.height = is.height - 5;
    setTimeout(function() {
        el.width = is.width;
        el.height = is.height;
    }, 50);
}


function getImageSize(id) {
    var oHlpr = document.createElement('IMG');
    var oPic = document.getElementById(id);
    oHlpr.style.visibility = 'hidden';
    oHlpr.style.position = 'absolute';
    oHlpr.top = 0;
    oHlpr.left = 0;
    oHlpr.src = oPic.src;
    document.body.appendChild(oHlpr);
    var imSize = {'width': oHlpr.offsetWidth, 'height': oHlpr.offsetHeight};
    document.body.removeChild(oHlpr);
    return imSize;
}


////поиск провайдера на странице поиска
function RetSearchPanel(txt) {
    var ret = "";
    txt = replace(txt, '*', '');
    var strForSplit = window.external.RetSearchPanel(txt);
    var strArray = [];
    try {
        strArray = strForSplit.split(";");
        var column = 0;//по сколько в ряд в таблице
        var new_line = true;
        ret += "<table cellpadding=0 border=0 cellspacing='0'>";
        if(strArray.length > 1) {
            for(var i = 0; i < strArray.length; i = i + 2) {
                new_line = false;
                if(column == 0)ret += '<TR>';
                ret += "<td CLASS=\"KB_SEARCH\" ID='operatorsearch" + strArray[i] + "' WIDTH=\"398\" HEIGHT=\"50\" background=\"img/fo_search_res_btn.png\" onclick='ClickOperator(" + strArray[i] + ");'>" + strArray[i + 1] + "</td>"; //strArray[i]+"р. до "+strArray[i+1]+"";
                column = column + 1;
                if(column == 2) {
                    column = 0;
                    new_line = true;
                    ret += '</TR>';
                }
            }
            if(column == 1)ret += '</TR>';//если в цикле не закрылась TR ка
        }//if
//        ret += createAddNewOperatorFromClient(new_line);
        ret += "</table>";
    }
    catch(e) {
        SaveLog("RetSearchPanel exception");
    }
    return ret;
}

function createAddNewOperatorFromClient(new_line) {
    var ret = "";
    if(new_line)
        ret += "<tr>"
    ret += "<td CLASS='KB_SEARCH' ID='send_order_to_add' WIDTH='398' HEIGHT='50' background='img/fo_search_res_btn.png'>ДОБАВИТЬ КНОПКУ?</td>"; //strArray[i]+"р. до "+strArray[i+1]+"";
    if(new_line)
        ret += "</tr>";

    return ret;
}

//
//CLOCK
//
function jsClock() {
    var time = new Date()
    var hour = time.getHours()
    var minute = time.getMinutes()
    var second = time.getSeconds()
    var day = time.getDate()
    var month = time.getMonth() + 1
    var year = time.getFullYear()

    if(day < 10) day = "0" + day
    if(month < 10) month = "0" + month
    var date = "" + day + "." + month + "." + year

    var temp = "" + hour
    if(hour == 0) temp = "12"
    if(temp.length == 1) temp = " " + temp
    temp += ((minute < 10) ? ":0" : ":") + minute
    temp += ((second < 10) ? ":0" : ":") + second
//temp += (hour >= 12) ? " PM" : " AM"
//document.clockForm.digits.value = temp
    document.getElementById('clock').innerHTML = temp
    document.getElementById('date').innerHTML = date
    var id = setTimeout("jsClock()", 1000)
}

/**
 * Генерирует строку имг хтмл с подогнанными размерами
 *
 * @param maxWidth максимальная высота блока
 * @param maxHeight минимальная высота блока
 * @param src путь к файлу с изображением
 * @returns {string}
 */
function createImgByfilter(maxWidth, maxHeight, src) {
    var height = 0;
    var width = 0;
    var koef = 0;
    var image = "";
    if(src != "") {
        //получаем размеры изображения
        var img = getImageSizeFromSrc(src);
        if(img.height > maxHeight) {
            koef = maxHeight / img.height;
            height = koef * img.height;
            width = koef * img.width;
        } else if(img.width > maxWidth) {
            koef = maxWidth / img.width;
            height = koef * img.height;
            width = koef * img.width;
        } else {
            height = img.height;
            width = img.width;
        }

        image = "<img width='" + width + "' height='" + height + "' src='" + src + "' border='0'>";
    }

    return image;
}

/**
 * Получение размера изображения
 * @param src
 * @returns {{width: number, height: number}}
 */
function getImageSizeFromSrc(src) {
    var oHlpr = document.createElement('IMG');
    oHlpr.style.visibility = 'hidden';
    oHlpr.style.position = 'absolute';
    oHlpr.top = 0;
    oHlpr.left = 0;
    oHlpr.src = src;
    document.body.appendChild(oHlpr);
    var imSize = {'width': oHlpr.offsetWidth, 'height': oHlpr.offsetHeight};
    document.body.removeChild(oHlpr);
    return imSize;
}

function RetComission() {
    var ret = "";
    var strForSplit = window.external.RetComission();
    strArray = new Array();
//alert(strForSplit);
    ret = "Комиссия<br><br>";
    try {
        strArray = strForSplit.split(";");
        if(strArray.length > 1) {
            for(var i = 0; i < strArray.length; i = i + 3) {
                if(strArray[i] == 'True') {//фиксированная сумма
//                    ret += strArray[i + 2] + "р. до " + strArray[i + 1] + " руб.<BR>";

                }
                else {//процент от суммы
  //                  ret += strArray[i + 2] + "% до " + strArray[i + 1] + " руб.<BR>";
                }
            }
        } else {
    //        ret += "0р. до 15000 руб. <br>";
        }
    }
    catch(e) {
        SaveLog(e.toString());
    }
    return ret;
}

//флаг заплатка нужно показывать группу вверх или нет
var groupUp = false;

/**
 * Функция ипользуется для формировния наименования группы
 */
function changeGroupName(group_id) {
    groupUp = false;

    var GroupStructure = GetGroupStructure(group_id);
    //картинка группы
    var imageIcon = document.getElementById('wgroup_name_left');
    //Имя группы
    var nameGroup = document.getElementById('wgroup_category_name');

    SaveLog("Начато построение группы: " + GroupStructure.Name + "(" + group_id + ")");

    //imageIcon.style.backgroundImage = url('img/' + GroupStructure.ButtonImg);
    nameGroup.innerHTML = GroupStructure.Name;

    //Показываем кнопку на уровень вверх - если это подгруппа
    if(GroupStructure.GroupIDTopLevel != 0) {
        try {
            setBackInGroup(GroupStructure.GroupIDTopLevel);
        } catch(e) {
            SaveLog(e);
        }
    }
}
/*
 Установки обработчика назад из меню групп
 */
function setBackInGroup(group_id) {
    var back = document.getElementById('wupbutton');
    back.onclick = function() {
        Pages.ShowListOperatorByGroup(group_id);
    };
    groupUp = true;
    back.style.display = '';

}


var countOperatorByGroup = 0;

/**
 * Выводит кол-во страниц по кол-ву операторов
 * @param count
 * @constructor
 */
function CalcPageByGroup(count_op) {
    var cc = count_op / 16;
    //если есть дробная часть
    if(cc - Math.floor(cc) > 0) {
        cc = Math.floor(cc) + 1;
    }
    if(cc == 0)
        cc = 1;
    return cc;
}

/**
 * Формирует списки операторов
 * @param GroupID
 * @returns {string}
 * @constructor
 */
/*
function RetGroup(GroupID) {
    SetCurrentPage(Pages.Link.OTHER);
    StartTimeOut();
    try {
        //количество операторов
        countOperatorByGroup = 0;
        //номер страницы при скролинге
        currentNumPage = 0;

        var ret = "";
        var strForSplit = RetGatesForGroup(GroupID);
        //подгруппы
        var strForSplit2 = RetSubGroups(GroupID);

        changeGroupName(GroupID);

        strArray = new Array();
        strArray2 = new Array();

        strArray = strForSplit.split(";");
        strArray2 = strForSplit2.split(";");
        ret = "<table cellpadding=0 border=0>";
        var column = 0;//по сколько в ряд в таблице
        //подгруппы
        if(strArray2.length > 1) {
            for(var i = 0; i < strArray2.length; i = i + 3) {
                if(IsGroupNeedShow(strArray2[i])) {
                    countOperatorByGroup++;
                    if(column == 0)ret += '<TR>';
                    if(strArray2[i + 1] == 'nologo') {//нет рисунка оператора
                        ret += '<td align=center valign=middle WIDTH="319" HEIGHT="159" background="img/btn_r.png"><table border=0 width=260 height=110><tr><td onclick=\"Pages.ShowListOperatorByGroup(' + strArray2[i] + ');\" CLASS="KB" ID=op' + strArray2[i] + ' onclick="kbclick(\'btn_r.png\',\'btn_press.png\',\'op' + strArray2[i] + '\');"><a onclick=\"RetGroup(' + strArray2[i] + ');\"><font class="grfontnologo">' + strArray2[i + 2] + '</font></a></td></tr></table></td>';
                    }
                    else {//есть рисунок оператора
                        ret += '<td align=center valign=middle WIDTH="319" HEIGHT="159" background="img/btn_r.png"><table border=0 width=260 height=110><tr><td onclick=\"Pages.ShowListOperatorByGroup(' + strArray2[i] + ');\" CLASS="KB" ID=op' + strArray2[i] + ' onclick="kbclick(\'btn_r.png\',\'btn_press.png\',\'op' + strArray2[i] + '\');"><a onclick=\"RetGroup(' + strArray2[i] + ');\"><img name=logo' + strArray2[i] + ' src="../logo/' + strArray2[i + 1] + '"></a><div class="grfont">' + strArray2[i + 2] + '</div></td></tr></table></td>';
                    }
                    column = column + 1;
                    if(column == 4) {
                        column = 0;
                        ret += '</TR>';
                    }
                }
            }
        }
        var new_line = false;

        //шлюзы
        if(strArray.length > 1) {
            for(var i = 0; i < strArray.length; i = i + 3) {
                new_line = false;
                //шлюз добавить поставщика добавим вручную
                if(strArray[i] == 555)
                    continue;

                countOperatorByGroup++;

                if(column == 0)ret += '<TR>';
                if(strArray[i + 1] == 'nologo') {//нет рисунка оператора
                    ret += '<td align=center valign=middle WIDTH="319" HEIGHT="159" background="img/btn_r.png"><table border=0 width=260 height=110><tr><td CLASS="KB" ID=op' + strArray[i] + ' onclick="ClickOperator(' + strArray[i] + ');"><font class="grfontnologo">' + strArray[i + 2] + '</font></td></tr></table></td>';
                }
                else {//есть рисунок оператора
                    ret += '<td align=center valign=middle WIDTH="319" HEIGHT="159" background="img/btn_r.png"><table border=0 width=260 height=110><tr><td CLASS="KB" ID=op' + strArray[i] + ' onclick="ClickOperator(' + strArray[i] + ');"><img name=logo' + strArray[i] + ' src="../logo/' + strArray[i + 1] + '"><div class="grfont">' + strArray[i + 2] + '</div></td></tr></table></td>';
                }
                column = column + 1;
                if(column == 4) {
                    column = 0;
                    new_line = true;
                    ret += '</TR>';
                }
            }
        }
        ret += createLinkSearchFromClient(new_line);
        ret += "</table>";
        document.getElementById('logolist').innerHTML = ret;//вывод результата
    } catch(e) {
        SaveExceptionToLog(e, "Shared/utils.js", "RetGroup(" + GroupID + ") strForSplit: " + strForSplit + " strForSplit2: " + strForSplit2);
    }
    return ret;
}
*/
/**
 * Проверяет имеет ли группа операторов или группы операторов
 * @param group_id
 * @constructor
 */
function IsGroupNeedShow(group_id) {
    var showGroup = false;

    var strForSplit = RetGatesForGroupBool(group_id);
    //в группе есть гейты
    if(strForSplit) {
        showGroup = true;
    }

    return showGroup;

}

/**
 * Добавляет дополнительную кнопку добавить провайдера
 */
function createLinkSearchFromClient(new_line) {
    var ret = "";
//    if(new_line)
//        ret += "<tr>";

//    ret += '<td align=center valign=middle WIDTH="319" HEIGHT="159" background="img/btn_r.png"><table border=0 width=260 height=110><tr><td CLASS="KB" ID="new_provider_1" onclick="Pages.Show(\'wsearch_operator\');"><div class="grfontnologo">ДОБАВИТЬ КНОПКУ</div></td></tr></table></td>';
//    if(new_line)
//        ret += "</tr>";
    return ret;
}

function buttonclick(pic, overpic, idpic) {
    changeImage(idpic, overpic);
    sounder.src = "../sound/click.wav";

    setTimeout("changeImage('" + idpic + "','" + pic + "')", 50);
}

function changeImage(idpic, overpic) {
    document.getElementById(idpic).src = "img/" + overpic;
}

function logoresize(pic, idpic) {

    changeLogoresize(idpic, pic);
    setTimeout("changeLogoresize('" + idpic + "','" + pic + "')", 50);


}
function changeLogoresize(idpic, pic) {
    document.getElementById(idpic).src = "../logo/" + pic;
    var is = getImageSize(idpic);
    document.getElementById(idpic).width = is.width - 3;
    document.getElementById(idpic).height = is.height - 3;

}

/**
 *
 * @returns {string}
 * @constructor
 */
function RetComission() {
    var ret = "";
    var strForSplit = window.external.RetComission();
    strArray = new Array();
//alert(strForSplit);
    ret = "<table>";
    try {
        strArray = strForSplit.split(";");
        if(strArray.length > 1) {
            for(var i = 0; i < strArray.length; i = i + 3) {
                if(strArray[i] == 'True') {//фиксированная сумма
//                    ret += "<tr><td>" + strArray[i + 2] + "р. до " + strArray[i + 1] + " руб.</td></tr>";
                }
                else {//процент от суммы
//                    ret += "<tr><td>" + strArray[i + 2] + "% до " + strArray[i + 1] + " руб.</td></tr>";
                }
            }
        }//if
        ret += "<tr><td></td></tr></table>";
    }
    catch(e) {
        alert(e);
    }
    return ret;
}

function closeNoMenuButton() {
    OnlineDiv('loadingdivmsgyesnomenu', 0);
    if(GetCurrentPage() == Pages.Link.PAYMENT) {
        SaveLog('start bill from popup closeNoMenuButton on payment pages');
        START_BILL();
    }
}

/**
 * Показ и скрытия коммисии на странице оплаты
 * @constructor
 */
function WCommission() {
    this.comisshide = 0;
    this.Commission = this.retCommission();
}
WCommission.prototype.toogle = function() {
    if(this.comisshide == 0) {
        var todotext = "visible";
        var todotext2 = "hidden";
        this.comisshide = 1;
    } else {
        var todotext = "hidden";
        var todotext2 = "visible";
        this.comisshide = 0;
    }
//    document.getElementById('opinfocomis').style.visibility = todotext;
//    document.getElementById('comison').style.visibility = todotext2;
};
WCommission.prototype.clear = function() {
    this.comisshide = 0;
    this.Commission = '';
};
WCommission.prototype.retCommission = function() {
//    document.getElementById('info_commision_payemnt').innerHTML = RetComission();
};
WCommission.prototype.ShowNullCommision = function() {
    ret = "<table>";
//    ret += "<tr><td>0% до 15000 руб.</td></tr>";
    ret += "<tr><td></td></tr></table>";
//    document.getElementById('info_commision_payemnt').innerHTML = ret;
};


/**
 * Поиск елемента по айди елемента
 * @param elem_id
 * @returns {HTMLElement}
 */
function getElemByID(elem_id) {
    var elem = document.getElementById(elem_id);
    return elem;
}

/**
 * Обновляет информация об терминале
 * @param container_id
 * @constructor
 */
function GetInfoAboutTerm(container_id) {
    var text = '' +
        '<table border=0 class="toptext" width="1000" cellpadding=5 cellspacing=5>' +
        '<tr>' +
        '<td class="toptext" align="right">Номер терминала:</td>' +
        '<td width="500" class="toptext" align=LEFT>' +
        RetTerminalNumber() +
        '</td>' +
        '</tr>' +
        '<tr>' +
        '<td class="toptext" align="right">Номер терминала в ПС:</td>' +
        '<td align=LEFT width="500" class="toptext">' +
        RetTerminalNumberSystem() +
        '</td>' +
        '</tr>' +
        '<tr>' +
        '<td class="toptext" align="right">Адрес терминала:</td>' +
        '<td class="toptext" align=LEFT>' +
        RetAddress() +
        '</td>' +
        '</tr>' +
        '<tr>' +
        '<td class="toptext" align="right">Дилер:</td>' +
        '<td class="toptext" align=LEFT>' +
        RetDealer() +
        '</td>' +
        '</tr>' +
        '<tr>' +
        '<td class="toptext" align="right">ИНН:</td>' +
        '<td class="toptext" align=LEFT>' +
        CompanyInfoData1() +
        '</td>' +
        '</tr>' +
        '<tr>' +
        '<td class="toptext" align="right">ОГРН:</td>' +
        '<td class="toptext" align=LEFT>' +
        CompanyInfoData2() +
        '</td>' +
        '</tr>' +
        '<tr>' +
        '<td class="toptext" align="right">Юридический адрес дилера:</td>' +
        '<td class="toptext" align=LEFT>' +
        RetDealerAddress() +
        '</td>' +
        '</tr>' +
        '<tr>' +
        '<td class="toptext" align="right">Телефон службы поддержки:</td>' +
        '<td class="toptext" align=LEFT>' +
        RetPhone() +
        '</td>' +
        '</tr>' +
        '<tr>' +
        '<td class="toptext" align="right">Кредитная организация:</td>' +
        '<td class="toptext" align=LEFT>' +
        RetPaymentSystem() +
        '</td>' +
        '</tr>' +
        '<tr>' +
        '<td class="toptext" align="right">Адрес кредитной организации:</td>' +
        '<td class="toptext" align=LEFT>' +
        RetPaymentSystemAddress() +
        '</td>' +
        '</tr>' +
        '<tr>' +
        '<td class="toptext" align="right">БИК кредитной организации:</td>' +
        '<td class="toptext" align=LEFT>' +
        RetPaymentSystemData1() +
        '</td>' +
        '</tr>' +
        '<tr>' +
        '<td class="toptext" align="right">ИНН кредитной организации:</td>' +
        '<td class="toptext" align=LEFT>' +
        RetPaymentSystemData2() +
        '</td>' +
        '</tr>' +
        '<tr>' +
        '<td class="toptext" align="right">Номер договора с кредитной организацией:</td>' +
        '<td class="toptext" align=LEFT>' +
        RetNumberOfContract() +
        '</td>' +
        '</tr>' +
        '</table>'

    getElemByID(container_id).innerHTML = text;
}


/**
 * Диалоговое окно печати при нажати печать из поиска платежа
 * Используется в помощи абоненту
 */
function PrintCheckDialog() {
    var ext = this;
    var onlineTimer = new IndependentTimer(5, "Идет печать дубликата чека");
    //onlineTimer.timer.directionTimeToBig();
    //указываем что делать по истечению таймера
    onlineTimer.timer.finalize = function() {
        //убираем всплывающее окно
        onlineTimer.stop();
        StartTimeOut();
    };
    //определяем действия для проверки статуса возможности проплаты платежа
    onlineTimer.start();

}
/**
 * Скрывает кнопку печати чека из поиска платежа
 * @param check_id
 * @constructor
 */
function HidePrintButtonInSearch(check_id) {
    getElemByID("ch" + check_id).style.display = 'none';
}

/**
 * Устанавливает значение хеддера
 * @param text
 */
function setHeaderText(text) {
    getElemByID('wgroup_category_name').innerHTML = text;
    Pages.ShowOne('wgroup_name');
}

function addchecktextPay() {
    getElemByID('add_check_text').innerHTML = "";
}

function addchecktextSendPropose() {
    getElemByID('add_check_text').innerHTML = "Ваша заявка успешно принята!";
}



/**
 * Функция вызова ошибки кард ридера
 */
function CardFail() {
    //
}



function priceSet(data){
        /*
         * В переменной price приводим получаемую переменную в нужный вид:
         * 1. принудительно приводим тип в число с плавающей точкой,
         *    учли результат 'NAN' то по умолчанию 0
         * 2. фиксируем, что после точки только в сотых долях
         */
        var price       = Number.prototype.toFixed.call(parseFloat(data) || 0, 2),
            //заменяем точку на запятую
            price_sep   = price.replace(/(\D)/g, ",");
            //добавляем пробел как разделитель в целых
            //price_sep   = price_sep.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1 ");

        return price_sep;
    };
var countclickskud='';
function countpressskud (sum) {
StartTimeOutResetClSkud();
countclickskud += sum;
countclickskudnum = 'skudclick'+sum;
if (countclickskud == window.external.RetSecretClick()) {
    document.getElementById("skudpass").style.display = "flex";
    if (typeof runnHideKeyboards !== 'undefined') setTimeout(function(){runnHideKeyboards(['#loginInput', '#passwordInput']);},200)
}
if (countclickskud.length > 5) { countclick = ''; }
}
var timeoutclskud = null;
function StartTimeOutResetClSkud() //val
{
	if(timeoutclskud)
	{
		clearTimeout(timeoutclskud);
		timeoutclskud = null;
	}
	timeoutclskud = setTimeout('resetclskud()',3000);
}
function resetclskud()
{
    countclickskud = '';
}


function skudenterclick(idpic)
{
	var cnt='';
	cnt = document.getElementById('numbox').value;

	id = replace(idpic,'b','');
	if (id <= 10 ) { document.getElementById('numbox').value += id; }
	else if (id == 41) { document.getElementById('numbox').value = ''; }
	else if (id == 40)
	{
	    if(cnt.length>0)
	    {
	        cnt = cnt.substring(0,cnt.length-1);
	        document.getElementById('numbox').value = cnt;
	    }
	 }
}


function skudpasscheck() {
    try {
        window.external.InterCode(document.getElementById('numbox').value);
        document.getElementById('numbox').value = "";
    } catch (e) {
        // alert('[Исключение] skudpasscheck: ' + e);
        SaveLog('[Исключение] skudpasscheck: ' + e);
        // window.external.SaveErrorInLog(e.name + e.description);
    }
}



function AskBarcode() {
	ClearBarCode();
	EnableBarCode();
    var barTimer = new BarcodeTimer(6000);
    barTimer.BarcodeTimer.start();
	barTimer.BarcodeTimer.execute = function() {
        var barcode = window.external.GetKeyForInterface('barcode').substring(0, 12);
//barcode = '111';
       if(barcode.length > 1) {
        SaveLog('Поднесен штрих код : '+barcode);
	    if (CheckAdminBarcode(barcode).length > 1) {
	    window.external.SetKeyFromInterface('managerfio',CheckAdminBarcode(barcode));
	    SaveLog('Штрих код администратора валиден');
            barTimer.stop();
            DisableBarCode();
            location.href = 'adm.html';
        } else {
            SaveLog('Штрих код администратора не верный');
	    ClearBarCode();
               }
        }
    };

}
